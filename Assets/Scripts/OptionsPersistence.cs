﻿using System.IO;
using UnityEngine;

public class OptionsPersistence
{   
    public OptionsData Options;
    private string path;

    public OptionsPersistence()
    {
        path = Path.Combine(Application.persistentDataPath, "RFSB_Options.json");
    }

    public void SetDefaultOptions()
    {
        Options = new OptionsData();
    }

    public bool Load()
    {
        bool loaded = true;
        try
        {
            Options = JsonUtility.FromJson<OptionsData>(File.ReadAllText(path));
        }
        catch
        {
            loaded = false;
        }
        return loaded;
    }

    public bool Save()
    {
        if (Options == null)
        {
            Debug.LogError("Error: Cannot save a null Options set!");
            return false;
        }

        bool saved = true;
        try
        {
            File.WriteAllText(path, JsonUtility.ToJson(Options, true));
        }
        catch
        {
            saved = false;
        }
        return saved;
    }
}
