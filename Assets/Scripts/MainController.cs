﻿using UnityEngine;

public class MainController : MonoBehaviour
{
    [Header("Options")]
    [SerializeField]
    private MyBool FPSUIActive;
    [SerializeField]
    private MyBool TerrainDetailsActive;
    [SerializeField]
    private MyBool PositionBenchmarksActive;
    [SerializeField]
    private MyFloat LightIntensity;
    [SerializeField]
    private MyInt ShadowGroupsLevel;

    [Header("FPS counter")]
    [SerializeField]
    private GameObject fpsCounterGameObj;

    [Header("Cameras")]
    [SerializeField]
    private CameraManager cameraManager;

    [Header("Light")]
    [SerializeField]
    private Light mainDirLight;

    [Header("Trackers")]
    [SerializeField]
    private GameObject positionBenchmarksGameObj;

    [Header("Tree Groups")]
    [SerializeField]
    private GameObject[] treeGroups;

    [Header("Terrain")]
    [SerializeField]
    private Terrain terrain;
    [SerializeField]
    private GameObject[] terrainDetails;

    private OptionsPersistence optionsPersistence;

    private void Awake()
    {
        optionsPersistence = new OptionsPersistence();

        if (!optionsPersistence.Load())
        {
            if (!PlayerPrefs.HasKey("RFSB_BenchmarkOptions_Initialized"))
            {
                optionsPersistence.SetDefaultOptions();
                optionsPersistence.Save();

                PlayerPrefs.SetString("RFSB_BenchmarkOptions1.0_Initialized", "True");
            }
        }
    }

    void Start ()
    {
	    if (cameraManager == null)
        {
            cameraManager = FindObjectOfType<CameraManager>();
        }        

        if (positionBenchmarksGameObj != null)
        {
            positionBenchmarksGameObj.SetActive(false);
        }

        SetOptions();        
    }	

    #region Options
    public void LoadOptions()
    {
        optionsPersistence.Load();

        SetOptions();
    }

    public void SaveOptions()
    {
        optionsPersistence.Options.CamerasOptions = cameraManager.GetCameraOptions();

        optionsPersistence.Options.ShowFPSUI = FPSUIActive.Value;
        optionsPersistence.Options.TerrainDetailsActive = TerrainDetailsActive.Value;
        optionsPersistence.Options.DrawPositionBenchmarks = PositionBenchmarksActive.Value;
        optionsPersistence.Options.LightIntensity = LightIntensity.Value;
        optionsPersistence.Options.ActiveShadowGroups = ShadowGroupsLevel.Value;

        optionsPersistence.Save();
    }

    public void SetDefaultOptions()
    {
        optionsPersistence.SetDefaultOptions();
        optionsPersistence.Save();

        SetOptions();
    }

    private void SetOptions()
    {
        SetPositionBenchmarksActive(optionsPersistence.Options.DrawPositionBenchmarks);
        SetActiveTerrainDetails(optionsPersistence.Options.TerrainDetailsActive);
        SetLightIntensity(optionsPersistence.Options.LightIntensity);
        SetTreeGroupActive(optionsPersistence.Options.ActiveShadowGroups);
        SetFPSCounterActive(optionsPersistence.Options.ShowFPSUI);

        cameraManager.SetOptions(optionsPersistence.Options.CamerasOptions);
    }

    public void SetTreeGroupActive(float activeGroups)
    {
        for (int i = 0; i < treeGroups.Length; ++i)
        {
            treeGroups[i].SetActive(i < activeGroups);
        }

        ShadowGroupsLevel.Set((int)activeGroups);
    }

    public void SetLightIntensity(float value)
    {
        if (value < 0.001f)
        {
            value = 0.001f;
        }

        mainDirLight.intensity = value;

        LightIntensity.Set(value);
    }

    public void SetActiveTerrainDetails(bool active)
    {
        terrain.drawTreesAndFoliage = active;

        if (terrainDetails != null)
        {
            foreach (GameObject g in terrainDetails)
            {
                g.SetActive(active);
            }
        }

        TerrainDetailsActive.Set(active);
    }

    public void SetPositionBenchmarksActive(bool active)
    {
        positionBenchmarksGameObj.SetActive(active);

        PositionBenchmarksActive.Set(active);
    }

    public void SetFPSCounterActive(bool active)
    {
        fpsCounterGameObj.SetActive(active);

        FPSUIActive.Set(active);
    }
    #endregion
}
