﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MyIntDropdownUI : MonoBehaviour, IMyDataCallbackReceiver
{
    [SerializeField]
    private MyInt value;

    [SerializeField]
    private Dropdown UI;

    private void Start()
    {
        if (value != null)
        {
            value.AddReceiver(this);
            NotifyValueChanged();
        }
    }

    public void NotifyValueChanged()
    {
        UI.value = value.Value;
    }

    public void SetValue(Int32 value)
    {
        this.value.Set(value);
    }
}