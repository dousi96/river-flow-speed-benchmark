﻿using UnityEngine;
using UnityEngine.UI;

public class MyFloatTextUI : MonoBehaviour, IMyDataCallbackReceiver
{
    [SerializeField]
    private MyFloat value;

    [SerializeField]
    private Text textUI;

    private void Start()
    {
        if (value != null)
        {
            value.AddReceiver(this);
            NotifyValueChanged();
        }   
    }

    public void NotifyValueChanged()
    {
        textUI.text = string.Format("{0:0.##}", value.Value);
    }

    public void SetValue(float value)
    {
        this.value.Set(value);
    }
}
