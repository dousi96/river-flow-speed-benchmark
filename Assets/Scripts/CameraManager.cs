﻿using System;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public enum RiverCameraOrientation : int
    {
        Longitudinal,
        Orthogonal,
    }

    public enum StereoRenderingOrientation : int
    {
        Vertical,
        Horizontal
    }

    [Header("Options")]
    [SerializeField]
    private MyBool StereoModeActive;
    [SerializeField]
    private MyInt StereoModeOrientation;

    [Header("Longitudinal Cameras")]
    [SerializeField]
    private Camera longCam;
    [SerializeField]
    private Transform longStereoCamsPivot;
    [SerializeField]
    private Camera longStereoCam_1;
    [SerializeField]
    private Camera longStereoCam_2;

    [Header("Orthogonal Cameras")]
    [SerializeField]
    private Camera orthoCam;
    [SerializeField]
    private Transform orthoStereoCamsPivot;
    [SerializeField]
    private Camera orthoStereoCam_1;
    [SerializeField]
    private Camera orthoStereoCam_2;

    public RiverCameraOrientation ActiveCamera { get; private set; }

	void Start() { }

    public void SetOptions(CamerasOptionsData options)
    {
        if (options == null)
        {
            Debug.LogError("Error: impossible to set up null options!");
            return;
        }

        //SINGLE LONG cam
        longCam.transform.position              = options.LongCamera_NotStereo_Options.LocalPosition;
        longCam.transform.rotation              = options.LongCamera_NotStereo_Options.Rotation;
        longCam.fieldOfView                     = options.LongCamera_NotStereo_Options.FOV;

        //STEREO LONG cams
        //pivot
        longStereoCamsPivot.transform.position  = options.LongStereoCams_Pivot;
        //cam 1
        longStereoCam_1.transform.localPosition = options.LongCamera_Stereo_1_Options.LocalPosition;
        longStereoCam_1.transform.rotation      = options.LongCamera_Stereo_1_Options.Rotation;
        longStereoCam_1.fieldOfView             = options.LongCamera_Stereo_1_Options.FOV;
        //cam 2
        longStereoCam_2.transform.localPosition = options.LongCamera_Stereo_2_Options.LocalPosition;
        longStereoCam_2.transform.rotation      = options.LongCamera_Stereo_2_Options.Rotation;
        longStereoCam_2.fieldOfView             = options.LongCamera_Stereo_2_Options.FOV;

        //SINGLE ORTHO cam
        orthoCam.transform.position             = options.OrthoCamera_NotStereo_Options.LocalPosition;
        orthoCam.transform.rotation             = options.OrthoCamera_NotStereo_Options.Rotation;
        orthoCam.fieldOfView                    = options.OrthoCamera_NotStereo_Options.FOV;

        //STEREO ORTHO cams
        //pivot
        orthoStereoCamsPivot.transform.position  = options.OrthoStereoCams_Pivot;
        //cam 1                                           
        orthoStereoCam_1.transform.localPosition = options.OrthoCamera_Stereo_1_Options.LocalPosition;
        orthoStereoCam_1.transform.rotation      = options.OrthoCamera_Stereo_1_Options.Rotation;
        orthoStereoCam_1.fieldOfView             = options.OrthoCamera_Stereo_1_Options.FOV;
        //cam 2                                          
        orthoStereoCam_2.transform.localPosition = options.OrthoCamera_Stereo_2_Options.LocalPosition;
        orthoStereoCam_2.transform.rotation      = options.OrthoCamera_Stereo_2_Options.Rotation;
        orthoStereoCam_2.fieldOfView             = options.OrthoCamera_Stereo_2_Options.FOV;
        //check if activating stereo cams
        ActivateStereoRendering(options.DrawStereoCameras);
        SetStereoRenderingOrientation(options.StereoCamsOrientation);
    }

    public void ActivateStereoRendering(bool active)
    {
        StereoModeActive.Set(active);
         
        switch (ActiveCamera)
        {
            case RiverCameraOrientation.Orthogonal:
                {
                    orthoStereoCamsPivot.gameObject.SetActive(StereoModeActive.Value);
                    orthoCam.gameObject.SetActive(!StereoModeActive.Value);
                }
                break;
            default:
                {
                    longStereoCamsPivot.gameObject.SetActive(StereoModeActive.Value);
                    longCam.gameObject.SetActive(!StereoModeActive.Value);
                }
                break;
        }
    }

    public void SetStereoRenderingOrientation(Int32 orientation)
    {
        SetStereoRenderingOrientation((StereoRenderingOrientation)orientation);
    }

    public void SetStereoRenderingOrientation(StereoRenderingOrientation orientation)
    {
        StereoModeOrientation.Set((int)orientation);

        Rect cam1opt;
        Rect cam2opt;
        switch (orientation)
        {
            case StereoRenderingOrientation.Vertical:
                {
                    cam1opt = new Rect(0, 0, .5f, 1);
                    cam2opt = new Rect(.5f, 0, 1, 1);
                }
                break;
            default:
                {
                    cam1opt = new Rect(0, 0, 1, .5f);
                    cam2opt = new Rect(0, .5f, 1, .5f);
                }
                break;
        }

        longStereoCam_1.rect = cam1opt;
        longStereoCam_2.rect = cam2opt;
        orthoStereoCam_1.rect = cam1opt;
        orthoStereoCam_2.rect = cam2opt;
    }

    public void ChangeActiveCamera()
    {
        longCam.gameObject.SetActive(false);
        longStereoCamsPivot.gameObject.SetActive(false);

        orthoCam.gameObject.SetActive(false);
        orthoStereoCamsPivot.gameObject.SetActive(false);

        switch (ActiveCamera)
        {
            case RiverCameraOrientation.Orthogonal:
                {
                    ActiveCamera = RiverCameraOrientation.Longitudinal;

                    longStereoCamsPivot.gameObject.SetActive(StereoModeActive.Value);
                    longCam.gameObject.SetActive(!StereoModeActive.Value);
                }
                break;
            default:
                {
                    ActiveCamera = RiverCameraOrientation.Orthogonal;

                    orthoStereoCamsPivot.gameObject.SetActive(StereoModeActive.Value);
                    orthoCam.gameObject.SetActive(!StereoModeActive.Value);
                }
                break;
        }
    }

    public CamerasOptionsData GetCameraOptions()
    {
        CamerasOptionsData options = new CamerasOptionsData();

        //SINGLE LONG cam
        options.LongCamera_NotStereo_Options.LocalPosition = longCam.transform.position;
        options.LongCamera_NotStereo_Options.Rotation = longCam.transform.rotation ;
        options.LongCamera_NotStereo_Options.FOV = longCam.fieldOfView;

        //STEREO LONG cams
        //pivot
        options.LongStereoCams_Pivot = longStereoCamsPivot.transform.position;
        //cam 1
        options.LongCamera_Stereo_1_Options.LocalPosition = longStereoCam_1.transform.localPosition;
        options.LongCamera_Stereo_1_Options.Rotation = longStereoCam_1.transform.rotation;      
        options.LongCamera_Stereo_1_Options.FOV = longStereoCam_1.fieldOfView;
        //cam 2
        options.LongCamera_Stereo_2_Options.LocalPosition = longStereoCam_2.transform.localPosition;
        options.LongCamera_Stereo_2_Options.Rotation = longStereoCam_2.transform.rotation;
        options.LongCamera_Stereo_2_Options.FOV = longStereoCam_2.fieldOfView;

        //SINGLE ORTHO cam
        orthoCam.transform.position = options.OrthoCamera_NotStereo_Options.LocalPosition;
        orthoCam.transform.rotation = options.OrthoCamera_NotStereo_Options.Rotation;
        orthoCam.fieldOfView = options.OrthoCamera_NotStereo_Options.FOV;

        //STEREO ORTHO cams
        //pivot
        options.OrthoStereoCams_Pivot = orthoStereoCamsPivot.transform.position;
        //cam 1                                           
        options.OrthoCamera_Stereo_1_Options.LocalPosition = orthoStereoCam_1.transform.localPosition;
        options.OrthoCamera_Stereo_1_Options.Rotation = orthoStereoCam_1.transform.rotation;
        options.OrthoCamera_Stereo_1_Options.FOV = orthoStereoCam_1.fieldOfView;
        //cam 2                                          
        options.OrthoCamera_Stereo_2_Options.LocalPosition = orthoStereoCam_2.transform.localPosition;
        options.OrthoCamera_Stereo_2_Options.Rotation = orthoStereoCam_2.transform.rotation;
        options.OrthoCamera_Stereo_2_Options.FOV = orthoStereoCam_2.fieldOfView;

        options.DrawStereoCameras = StereoModeActive.Value;
        options.StereoCamsOrientation = (StereoRenderingOrientation)StereoModeOrientation.Value;

        return options;
    }
}
