﻿using UnityEngine;
using UnityEngine.UI;

public class MyFloatSliderUI : MonoBehaviour, IMyDataCallbackReceiver
{
    [SerializeField]
    private MyFloat value;

    [SerializeField]
    private Slider UI;

    private void Start()
    {
        if (value != null)
        {
            value.AddReceiver(this);
            NotifyValueChanged();
        }
    }

    public void NotifyValueChanged()
    {
        UI.value = value.Value;
    }

    public void SetValue(float value)
    {
        this.value.Set(value);
    }
}
