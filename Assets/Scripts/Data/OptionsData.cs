﻿using System;
using UnityEngine;

[Serializable]
public class OptionsData
{    
    [SerializeField]
    public bool ShowFPSUI;
    [SerializeField]
    public bool DrawPositionBenchmarks;
    [SerializeField]
    public bool TerrainDetailsActive;
    [SerializeField]
    public float LightIntensity;
    [SerializeField]
    public int ActiveShadowGroups;

    [SerializeField]
    public CamerasOptionsData CamerasOptions;

    public OptionsData()
    {      
        ShowFPSUI                   = false;
        DrawPositionBenchmarks      = false;
        TerrainDetailsActive        = true;
        LightIntensity              = 1.0f;
        ActiveShadowGroups          = 5;

        CamerasOptions = new CamerasOptionsData();
    }
}

[Serializable]
public class CamerasOptionsData
{
    [SerializeField]
    public bool DrawStereoCameras;
    [SerializeField]
    public CameraManager.StereoRenderingOrientation StereoCamsOrientation;
    //Longitudinal Cams
    [SerializeField]
    public CameraData LongCamera_NotStereo_Options;

    [SerializeField]
    public Vector3 LongStereoCams_Pivot;
    [SerializeField]
    public CameraData LongCamera_Stereo_1_Options;
    [SerializeField]
    public CameraData LongCamera_Stereo_2_Options;

    //Orthogonal Cams
    [SerializeField]
    public CameraData OrthoCamera_NotStereo_Options;

    [SerializeField]
    public Vector3 OrthoStereoCams_Pivot;
    [SerializeField]
    public CameraData OrthoCamera_Stereo_1_Options;
    [SerializeField]
    public CameraData OrthoCamera_Stereo_2_Options;

    public CamerasOptionsData()
    {
        DrawStereoCameras = false;
        StereoCamsOrientation = CameraManager.StereoRenderingOrientation.Vertical;

        //Long cams      
        LongCamera_NotStereo_Options.LocalPosition = new Vector3(0, 5, -75);
        LongCamera_NotStereo_Options.Rotation = Quaternion.Euler(40, 0, 0);
        LongCamera_NotStereo_Options.FOV = 60;

        LongStereoCams_Pivot = new Vector3(0, 5, -75);
        LongCamera_Stereo_1_Options.LocalPosition = new Vector3(-1, 0, 0);
        LongCamera_Stereo_1_Options.Rotation = Quaternion.Euler(40, 0, 0);
        LongCamera_Stereo_1_Options.FOV = 60;

        LongCamera_Stereo_2_Options.LocalPosition = new Vector3(1, 0, 0);
        LongCamera_Stereo_2_Options.Rotation = Quaternion.Euler(40, 0, 0);
        LongCamera_Stereo_2_Options.FOV = 60;

        //Ortho cams
        OrthoCamera_NotStereo_Options.LocalPosition = new Vector3(0, 9, -65);
        OrthoCamera_NotStereo_Options.Rotation = Quaternion.Euler(90, 0, 0);
        OrthoCamera_NotStereo_Options.FOV = 60;

        OrthoStereoCams_Pivot = new Vector3(0, 9, -65);
        OrthoCamera_Stereo_1_Options.LocalPosition = new Vector3(-1, 0, 0);
        OrthoCamera_Stereo_1_Options.Rotation = Quaternion.Euler(90, 0, 0);
        OrthoCamera_Stereo_1_Options.FOV = 60;

        OrthoCamera_Stereo_2_Options.LocalPosition = new Vector3(1, 0, 0);
        OrthoCamera_Stereo_2_Options.Rotation = Quaternion.Euler(90, 0, 0);
        OrthoCamera_Stereo_2_Options.FOV = 60;
    }
}

[Serializable]
public struct CameraData
{
    [SerializeField]
    public Vector3 LocalPosition;
    [SerializeField]
    public Quaternion Rotation;
    [SerializeField]
    public float FOV;
}
