﻿using UnityEngine;

[CreateAssetMenu()]
public class MyString : MyData
{
    public string Value { get; private set; }

    public void Set(string value)
    {
        Value = value;

        NotifyValueChanged();
    }
}