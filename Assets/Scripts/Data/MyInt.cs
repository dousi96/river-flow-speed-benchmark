﻿using UnityEngine;

[CreateAssetMenu()]
public class MyInt : MyData
{
    public int Value { get; private set; }

    public void Set(int value)
    {
        Value = value;

        NotifyValueChanged();
    }
}
