﻿using System.Collections.Generic;
using UnityEngine;

public class MyData : ScriptableObject
{
    private List<IMyDataCallbackReceiver> callbackReveivers;

    private void OnEnable()
    {
        if (callbackReveivers == null)
        {
            callbackReveivers = new List<IMyDataCallbackReceiver>();
        }
    }

    public void AddReceiver(IMyDataCallbackReceiver receiver)
    {
        if (receiver == null) return;

        callbackReveivers.Add(receiver);
    }

    public void ClearReceivers()
    {
        callbackReveivers.Clear();
    }

    protected void NotifyValueChanged()
    {
        if (callbackReveivers.Count < 1) return;

        foreach(IMyDataCallbackReceiver receiver in callbackReveivers)
        {
            receiver.NotifyValueChanged();
        }
    }
}

public interface IMyDataCallbackReceiver
{
    void NotifyValueChanged();
}
