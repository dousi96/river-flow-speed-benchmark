﻿using UnityEngine;

[CreateAssetMenu()]
public class MyBool : MyData
{
    public bool Value { get; private set; }

    public void Set(bool value)
    {
        Value = value;

        NotifyValueChanged();
    }
}
