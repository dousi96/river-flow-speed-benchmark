﻿using UnityEngine;

[CreateAssetMenu()]
public class MyFloat : MyData
{
    public float Value { get; private set; }

    public void Set(float value)
    {
        Value = value;

        NotifyValueChanged();
    }
}
