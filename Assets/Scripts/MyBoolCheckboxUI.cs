﻿using UnityEngine;
using UnityEngine.UI;

public class MyBoolCheckboxUI : MonoBehaviour, IMyDataCallbackReceiver
{
    [SerializeField]
    private MyBool value;

    [SerializeField]
    private Toggle UI;

    private void Start()
    {
        if (value != null)
        {
            value.AddReceiver(this);
            NotifyValueChanged();
        }
    }

    public void NotifyValueChanged()
    {
        UI.isOn = value.Value;
    }

    public void SetValue(bool value)
    {
        this.value.Set(value);
    }
}
