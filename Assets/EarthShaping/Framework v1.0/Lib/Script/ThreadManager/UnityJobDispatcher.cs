﻿/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/
using System;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace com.earthshaping.framework.ThreadManager
{
	[ExecuteInEditMode]
	public class UnityJobDispatcher: MonoBehaviourSingleton<UnityJobDispatcher>
    {
		private struct UnityTask
		{
			public  Delegate Function;
			public  object[] Arguments;

			public UnityTask(Delegate function, object[] arguments)
			{
				Function = function;
				Arguments = arguments;
			}
		}
		// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		// MUST BE EMPTY
		// NOT REMOVE
		// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		protected UnityJobDispatcher()
		{
		}


		// TODO Replace with a logging level
		public bool _enableLogging;

		private static readonly Queue<UnityTask> TaskQueue = new Queue<UnityTask>();	
		private static bool AreTasksAvailable
		{
			get
			{
				lock (TaskQueue)
				{
					return TaskQueue.Count > 0;
				}
			}
		}

		/// <summary>
		/// Dispatches the specified action delegate.
		/// </summary>
		/// <param name='function'>
		/// The function delegate being requested
		/// </param>
		public static void Dispatch(Action function)
		{
			Dispatch(function, null);
		}

        /// <summary>
        /// Blocks until all worker threads have returned to the thread pool.
        /// A good timeout value is 30 seconds.
        /// </summary>
        //protected void WaitForThreads()
        //{
        //    int maxThreads = 0;
        //    int placeHolder = 0;
        //    int availThreads = 0;
        //    int timeOutSeconds = YourTimeoutPropertyHere;

        //    //Now wait until all threads from the Threadpool have returned
        //    while (timeOutSeconds > 0)
        //    {
        //        //figure out what the max worker thread count it
        //        System.Threading.ThreadPool.GetMaxThreads(out
        //            maxThreads, out placeHolder);
        //        System.Threading.ThreadPool.GetAvailableThreads(out availThreads,
        //            out placeHolder);

        //        if (availThreads == maxThreads) break;
        //        // Sleep
        //        System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(1000));
        //        --timeOutSeconds;
        //    }
        //    // You can add logic here to log timeouts
        //}

        /// <summary>
        /// Dispatches the specified function delegate with the desired delegates
        /// </summary>
        /// <param name='function'>
        /// The function delegate being requested
        /// </param>
        /// <param name='arguments'>
        /// The arguments to be passed to the function delegate
        /// </param>
        /// <exception cref='System.NotSupportedException'>
        /// Is thrown when this method is called from the Unity Player
        /// </exception>
        public static void Dispatch(Delegate function, params object[] arguments)
		{

			lock (TaskQueue)
			{
				TaskQueue.Enqueue(new UnityTask(function, arguments));
			}

		}

		/// <summary>
		/// Clears the queued tasks
		/// </summary>
		/// <exception cref='System.NotSupportedException'>
		/// Is thrown when this method is called from the Unity Player
		/// </exception>
		public static void ClearTasks()
		{

			if (AreTasksAvailable)
			{
				lock (TaskQueue)
				{
					TaskQueue.Clear();
				}
			}

		}

		/// <summary>
		/// Dispatches the tasks that has been requested since the last call to this function
		/// </summary>
		/// <exception cref='System.NotSupportedException'>
		/// Is thrown when this method is called from the Unity Player
		/// </exception>
		private static void DispatchTasks()
		{

			if (AreTasksAvailable)
			{
				lock (TaskQueue)
				{
					foreach (UnityTask task in TaskQueue)
					{
						task.Function.DynamicInvoke(task.Arguments);
						
					}
					
					TaskQueue.Clear();
				}
			}

		}


#if UNITY_EDITOR
		void OnEnable()
		{
			if (!Application.isPlaying)
			{
				EditorApplication.update += EditorUpdate;
			}
		}
		void OnDisable()
		{
			if (!Application.isPlaying)
			{
				EditorApplication.update -= EditorUpdate;
			}

		}
#endif


		public override void Update()
		{ }



		public static void EnableLogging(bool enable)
		{

			Instance._enableLogging = enable;
		}

		void EditorUpdate()
		{
			DispatchTasks();
		}

		
	}

}
