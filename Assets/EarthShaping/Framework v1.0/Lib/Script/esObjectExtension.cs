﻿
using UnityEngine;


namespace com.earthshaping.Helper.Extension
{
	public static class esObjectExtension
	{

		// Get a component and add if not exist
		public static T GetOrAddComponent<T>(this Object obj) where  T : Component 
		{
			return ((GameObject) obj).GetComponent<T>() ?? ((GameObject)obj).AddComponent<T>();
		}


		// Destroy Object
		public static void Destroy(this Object obj)
		{
			if (obj != null)
			{
	#if UNITY_EDITOR
				Object.DestroyImmediate(obj, false);
	#else
					Object.Destroy (obj);
	#endif
			}
		}
	
		public static void Destroy(this Object[] obj)
		{
			if (obj != null)
			{
	
				for (int i = 0; i < obj.Length; i++)
				{
	#if UNITY_EDITOR
					Object.DestroyImmediate(obj[i], false);
	#else
					Object.Destroy (obj[i]);
	#endif
				}
			}
		}
		
	}
}



