﻿

using System;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using Object = UnityEngine.Object;


namespace com.earthshaping.framework.Extension
{

	public class ESAssetTool
	{



        #region Get Asset UID

        /// <summary>
        /// Get the GUID for the asset 
        /// </summary>
        /// <param name="asset"></param>
        /// <returns></returns>
        public static string GetAssetGUID(TerrainData asset)
        {
          
#if UNITY_EDITOR
            string lAssetPath = AssetDatabase.GetAssetPath(asset);
#else
			string lAssetPath = "Assets";
#endif

            string lInstanceId = string.Empty;
#if UNITY_EDITOR
            lInstanceId = GetAssetGUID(lAssetPath);

#endif

            return lInstanceId;
        }

        /// <summary>
        /// Path to GUIOD
        /// <param name="assetPath"></param>
        /// <returns></returns>
        public static string GetAssetGUID(string assetPath)
        {
#if UNITY_EDITOR
            return AssetDatabase.AssetPathToGUID(assetPath);
#else
			return "";
#endif

        }
        

        #endregion

        /// <summary>
        /// Get directory where asset is stored
        /// </summary>
        /// <param name="assetPath">Full asset path relative to Unity Project</param>
        /// <returns></returns>
        public static string GetAssetDirectory(string assetPath)
        {
            var lCacheDirectory = assetPath.Replace("\\", "/");
            if (lCacheDirectory.Contains("/"))
                lCacheDirectory = lCacheDirectory.Substring(0, lCacheDirectory.LastIndexOf("/"));
            return lCacheDirectory;
        }
        /// <summary>
        /// Get name of the asset and director where asset is stored
        /// </summary>
        /// <param name="path">Path and asset name</param>
        /// <returns>Asset Name</returns>
        public static string GetAssetNameAndDirectory(ref string path)
		{
			string assetName = path.Substring(path.LastIndexOf("/"));
			assetName = assetName.Substring(0, assetName.IndexOf("."));
			path = path.Substring(0, path.LastIndexOf("/"));
			return assetName;
		}





		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="folderName"></param>
		/// <returns></returns>
		public static List<T> FindAssetsByType<T>(string[] folderName = null) where T : UnityEngine.Object
		{


			List<T> assets = new List<T>();
            
        #if UNITY_EDITOR
			string[] guids = UnityEditor.AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
			for (int i = 0; i < guids.Length; i++)
			{
				string assetPath = UnityEditor.AssetDatabase.GUIDToAssetPath(guids[i]);
				T asset = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(assetPath);
				if (asset != null)
				{
					assets.Add(asset);
				}
			}
        #endif
			return assets;
		}

	



		public static T LoadAsset<T>(T asset) where T : UnityEngine.Object
		{
#if UNITY_EDITOR
			var assetPath = UnityEditor.AssetDatabase.GetAssetPath(asset) ;
			var dbAsset = UnityEditor.AssetDatabase.LoadMainAssetAtPath(assetPath) as T;
			return dbAsset;
#else
			return null;
#endif
		}

		public static T LoadAsset<T>(string fileNameOrPath) where T : UnityEngine.Object
		{

			
#if UNITY_EDITOR
			if (!string.IsNullOrEmpty(fileNameOrPath))
			{
				T obj = UnityEditor.AssetDatabase.LoadAssetAtPath(fileNameOrPath, typeof(T)) as T;
				if (obj != null)
				{
					return obj;
				}
				else
				{
					string path = ESAssetTool.GetAssetPath(System.IO.Path.GetFileName(fileNameOrPath));
					if (!string.IsNullOrEmpty(path))
					{
						return (T)UnityEditor.AssetDatabase.LoadAssetAtPath(path, typeof(T));
					}
				}
			}
#endif
			return null;
		}

	
		public static UnityEngine.Object LoadAsset(string fileNameOrPath, Type assetType)
		{
		#if UNITY_EDITOR
            if (!string.IsNullOrEmpty(fileNameOrPath))
            {
                UnityEngine.Object obj = UnityEditor.AssetDatabase.LoadAssetAtPath(fileNameOrPath, assetType);
                if (obj != null)
                {
                    return obj;
                }
                else
                {
                    string path = ESAssetTool.GetAssetPath(System.IO.Path.GetFileName(fileNameOrPath));
                    if (!string.IsNullOrEmpty(path))
                    {
                        return UnityEditor.AssetDatabase.LoadAssetAtPath(path, assetType);
                    }
                }
            }
            #endif
            return null;
		}
		public static string GetAssetPath(string fileName)
		{
#if UNITY_EDITOR
			string fName = System.IO.Path.GetFileNameWithoutExtension(fileName);
			string[] assets = UnityEditor.AssetDatabase.FindAssets(fName, null);
			for (int idx = 0; idx < assets.Length; idx++)
			{
				string path = UnityEditor.AssetDatabase.GUIDToAssetPath(assets[idx]);
				if (System.IO.Path.GetFileName(path) == fileName)
				{
					return path;
				}
			}
#endif
			return "";
		}

		public static void CreateAsset(UnityEngine.Object profile, string pathname,bool appendTimeStamp=false )
		{
#if UNITY_EDITOR
			string assetFileName =
				appendTimeStamp?
				string.Format("{0}_{1:yyyy_dd_MM_HH_mm_ss}.asset", pathname,
				DateTime.Now):
				string.Format("{0}.asset", pathname);

            UnityEditor.AssetDatabase.CreateAsset(profile,assetFileName);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.ImportAsset(assetFileName);
#endif
        }
		
		

	}

}
