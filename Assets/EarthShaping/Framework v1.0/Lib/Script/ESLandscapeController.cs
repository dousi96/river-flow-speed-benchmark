﻿/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/
using System;
using UnityEngine;
using com.earthshaping.framework.Extension;
using com.earthshaping.Setting;
using com.earthshaping.Terrains;


namespace com.earthshaping.framework.Landscape
{
    /// <summary>
    /// Landscape Manager
    /// </summary>
    public  class ESLandscapeController1
    {
    
        #region Ctor

        static ESLandscapeController1()
        {
           Init(true);
            //if (debug)
            //	Debug.Log(" ES_TerrainManager() constructor");
        }

        public static void Init(bool loadHolders )
        {

            ESLandscapeModel1.Init();
			ESLandscapeModel1.UpdateFromCurrentScene();
            if(!loadHolders) return;

            if (ESLandscapeModel1.PersistentHolder)
            {

                // Check Backup Instances

     //           for (int i = 0; i < ESLandscapeModel1.TerrainBackups.Length; i++)
     //           {
					//ESLandscapeModel1.TerrainBackups[i] = new EsTerrainBackup
     //               {
     //                   TerrainId = i,
     //                   TerraiHolder = ESLandscapeModel1.LoadOrCreateTerrainHolderData(i)
     //               };
     //           }
                //if(debug)
                //	Debug.Log(" ES_TerrainManager TerrainManagerInit End"); 
            }
            else
            {

                //_terrainBackups = new EsTerrainBackup[_activeTerrains.Length];
                //for (int i = 0; i < _terrainBackups.Length; i++)
                //{
                //    _terrainBackups[i] = new EsTerrainBackup()
                //    {
                //        TerrainId = i,
                //        TerraiHolder = MemoryTerrainHolderData(i)
                //    };
                //}
            }
        }

        #endregion


        public static void TerrainManagerReset()
        {
            //_terrainBackups = null;
            //_activeTerrains = null;
            //GC.Collect();
            //if(debug)
            //Debug.Log(" ES_TerrainManager Terrain reset");
        }

     
        /// <summary>
        /// Refresh Terrain Manager Loading all the terrain active in the scene
        /// </summary>
        public static void TerrainManagerRefresh()
        {
			ESLandscapeModel1.UpdateFromCurrentScene();
            //if(debug)
            //	Debug.Log(" ES_TerrainManager TerrainManagerRefres");

        }



        #region Holder Data Manager

//        /// <summary>
//        /// Check if backup file exists. If not create a new one and Init that			
//        /// </summary>
//        /// <param name="idx">Active terrain index</param>
//        /// <returns></returns>
//        public static TerrainHolderData LoadOrCreateTerrainHolderData(int idx)
//        {

//            TerrainHolderData assetTerrainHolderData = default(TerrainHolderData);//= ScriptableObject.CreateInstance<TerrainHolderData>();

//#if UNITY_EDITOR

//            //ESTerrainManagerEditor.FolderCacheName = Config.FolderTerrainCacheName;
//            //assetTerrainHolderData = ESTerrainManagerEditor.GetTerrainHolderData(ESLandscapeModel1.ActiveTerrains[idx].TTerrain,idx);
//#endif
//            return assetTerrainHolderData;
//        }

     
      
     


        #endregion

        public static void RefreshBackup(UnityEngine.Terrain terrain)
		{
			 var guid= ESAssetTool.GetAssetGUID(terrain.terrainData);

			foreach (var backup in ESLandscapeModel1.TerrainBackups)
			{
				if (backup.TerraiHolder.SourceTerrainId == guid)
				{
					var assetTerrainHolderData = backup.TerraiHolder;
					//assetTerrainHolderData = LoadOrCreateTerrainHolderData(backup.TerrainId);
					//ESTerrainManager.BackupTerrain(backup.TerrainId, assetTerrainHolderData);
				}
			}

		}






		private static DetailPrototype SetDefaultPrototype
		{
			get
			{
				DetailPrototype p = new DetailPrototype();
				p.usePrototypeMesh = false;
				p.renderMode = DetailRenderMode.Grass;
				p.bendFactor = 0.1f;
				p.noiseSpread = 0.0f;
				p.minWidth = 1.0f;
				p.maxWidth = 2.0f;
				p.minHeight = 1.0f;
				p.maxHeight = 2.0f;
				p.healthyColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
				p.dryColor = new Color(0.7f, 0.6f, 0.5f, 1.0f);
				return p;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="terrainIdx"></param>
		/// <param name="assetTerrainHolderData"></param>
		/// <returns></returns>
		public static bool BackupTerrain(int terrainIdx, TerrainHolderData assetTerrainHolderData)
		{
            

			//assetTerrainHolderData.SetHeightMap(ESTerrainManager.ActiveTerrains[terrainIdx].GetHeight());
            //assetTerrainHolderData.SetSource(ESTerrainManager.ActiveTerrains[terrainIdx].TData);
            //assetTerrainHolderData.SetSplatmapData(ESTerrainManager.ActiveTerrains[terrainIdx].GetMap());
            //assetTerrainHolderData.SetSplatPrototype(ESTerrainManager.ActiveTerrains[terrainIdx].TData.splatPrototypes);


            return true;
		}

		public static bool RestoreTerrain(int terrainIdx)
		{

			var hm = ESLandscapeModel1.TerrainBackups[terrainIdx].TerraiHolder.GetHeightMap();
			ESLandscapeModel1.ActiveTerrains[terrainIdx].TData.SetHeights(0, 0, hm);

			//var sm = ESTerrainManager.TerrainBackups[terrainIdx].terrainBackup.GetSplatmapData();
			//ESTerrainManager.ActiveTerrains[terrainIdx].TData.SetAlphamaps(0, 0, sm);

			//var splatPrototype = ESTerrainManager.TerrainBackups[terrainIdx].terrainBackup.GetSplatPrototype();
			//ESTerrainManager.ActiveTerrains[terrainIdx].TData.splatPrototypes = splatPrototype;
			//ESTerrainManager.ActiveTerrains[terrainIdx].TData.RefreshPrototypes();

			return true;
		}
    

		//public static void RestoreHeightMap()
		//{
          

  //          for (int i = 0; i < ESLandscapeModel1.ActiveTerrains.Length; i++)
		//	{
		//		var hm = ESLandscapeModel1.TerrainBackups[i].TerraiHolder.GetHeightMap();

		//		ESLandscapeManager.ESTerrains[i].TData.SetHeights(0,0,hm);
  //              //ESLandscapeModel.ActiveTerrains[i].TData.SetHeights(0, 0, hm);
		//	}

		//}
		//public static void RestoreSplatMap()
		//{
		//	for (int i = 0; i < ESLandscapeModel1.ActiveTerrains.Length; i++)
		//	{
		//		//var sm = ESTerrainManager.TerrainBackups[i].TerraiHolder.GetSplatmapData();
		//		//ESTerrainManager.ActiveTerrains[i].TData.SetAlphamaps(0, 0, sm);
		//	}

		//}
		//public static void RestoreProtoMap()
		//{
		//	for (int i = 0; i < ESLandscapeModel1.ActiveTerrains.Length; i++)
		//	{

		//		//var splatPrototype = ESTerrainManager.TerrainBackups[i].TerraiHolder.GetSplatPrototype();
		//		//ESTerrainManager.ActiveTerrains[i].TData.splatPrototypes = splatPrototype;
		//		//ESTerrainManager.ActiveTerrains[i].TData.RefreshPrototypes();

		//	}

		//}

		//public static void RestoreLandscape(bool hm = true,bool sm = true,bool o = true)
		//{
  //          if(hm)
		//		RestoreHeightMap();
  //          if (sm)
  //              RestoreSplatMap();
  //          if (o)
  //              RestoreProtoMap();

  //      }


      
    }
}

