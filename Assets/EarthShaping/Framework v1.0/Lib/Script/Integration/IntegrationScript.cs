﻿/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/

using UnityEngine;
using System.Collections.Generic;
using com.earthshaping.Helper.Extension;
#if VEGETATION_STUDIO
using AwesomeTechnologies;
using AwesomeTechnologies.VegetationStudio;
#endif
namespace com.earthshaping.Integration
{
	public class IntegrationScript 
	{


#region Awesome Vegetation Studio

		public static class VegetationStudio 
		{

			public enum VSMaskMode
			{
				Single,
				Multi
			}

			public static bool IsInstalled()
			{
				try
				{
					var vegetationSystemBaseType = System.Type.GetType("AwesomeTechnologies.VegetationSystem, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null", true, true);
					return vegetationSystemBaseType!=null;
				}
				catch
				{
				
					return false;
				}
			}

			public static void RefreshTerrainHeightMap(Bounds bounds)
			{
#if VEGETATION_STUDIO
				if (!IsInstalled()) return;
				VegetationStudioManager.RefreshTerrainHeightMap(bounds);
#endif
			}

			/// <summary>
			///  Refresh Vegetation Mask 
			/// </summary>
			/// <param name="vegationMaskObject">Vegation Mask Object</param>
			/// <param name="Vertices">Array of Vertices</param>
			/// <param name="parent">If not null transform to to world space of the object</param>
			public static  void  UpdateVegationStudioMask(GameObject vegationMaskObject, Vector3[] Vertices,bool IsActive,  bool showHandle = true, GameObject parent =null)
			{

#if VEGETATION_STUDIO
				if(!IsInstalled()) return;
				if (!IsActive)
				{
					vegationMaskObject.GetComponent<VegetationMaskArea>().ClearNodes();
					return;
				}
					

				VegetationMaskArea vegationMaskArea = 
					vegationMaskObject.GetComponent<VegetationMaskArea>() ??
					vegationMaskObject.AddComponent<VegetationMaskArea>();

				vegationMaskArea.ClearNodes();

				for (int i = 0; i < Vertices.Length-1; i++)
				{
					var worldCoordinate = parent == null? Vertices[i]: parent.transform.TransformVector(Vertices[i]);
					vegationMaskArea.AddNodeToEnd(worldCoordinate);
				}
				vegationMaskArea.UpdateVegetationMask();
				vegationMaskArea.ShowHandles = showHandle;
				
#endif
			}

#if VEGETATION_STUDIO
			/// <summary>
			///  Refresh Vegetation Mask 
			/// </summary>
			/// <param name="vegationMaskArea"></param>
			/// <param name="vertices">Array of Vertices</param>
			/// <param name="showHandle"></param>
			public static void UpdateVegationStudioMask(VegetationMaskArea vegationMaskArea, Vector3[] vertices, bool showHandle = true)
			{


				if (!IsInstalled()) return;
				
				vegationMaskArea.ClearNodes();

				for (int i = 0; i < vertices.Length ; i++)
				{
					vegationMaskArea.AddNodeToEnd(vertices[i]);
				}
				vegationMaskArea.PositionNodes();
				vegationMaskArea.UpdateVegetationMask();
				vegationMaskArea.ShowHandles = showHandle;

		}

			public  static void UpdateVegationStudioLineMask(VegetationMaskLine vegetationMaskLine, Vector3[] nodes, string maskName, float Witdh, List<VegetationTypeSettings> vegetationTypeList )
			{

				vegetationMaskLine.MaskName = "Mask - " + maskName;
				vegetationMaskLine.ClearNodes();
				vegetationMaskLine.LineWidth = Witdh;
				var numChildren = nodes.Length-1;
				for (int i = 0; i < numChildren; i++)
					vegetationMaskLine.AddNodeToEnd(nodes[i]);

				if (vegetationTypeList != null)
				{
					vegetationMaskLine.IncludeVegetationType = true;
					vegetationMaskLine.VegetationTypeList = vegetationTypeList;
				}
				vegetationMaskLine.RemoveGrass = false;
				vegetationMaskLine.RemoveLargeObjects = false;
				vegetationMaskLine.RemoveObjects = false;
				vegetationMaskLine.RemovePlants = false;
				vegetationMaskLine.RemoveTrees = false;

				vegetationMaskLine.PositionNodes();
				vegetationMaskLine.UpdateVegetationMask();
				


			}

#endif


			public static void ClearNodeVegetationStudioMask(GameObject vegationMaskObject)
			{
				if (!IsInstalled()) return;
#if VEGETATION_STUDIO
				var mask = vegationMaskObject.GetComponents<VegetationMask>();
				if (mask != null)

				{
					for (int i = 0; i < mask.Length; i++)
					{
						mask[i].ClearNodes();
					}
				}
					
#endif
			}

			public static void DestroyVegetationStudioMask(GameObject vegationMaskObject)
			{
				if (!IsInstalled()) return;
#if VEGETATION_STUDIO
				var mask = vegationMaskObject.GetComponents<VegetationMask>() ;
				if (mask != null) mask.Destroy();
#endif
			}

			public static void SetCamera(Camera camera)
			{
#if VEGETATION_STUDIO
				VegetationStudioManager.SetCamera(camera);
#endif
			}

			public static void RefreshTerrainHeightMap()
			{

#if VEGETATION_STUDIO
				VegetationStudioManager.RefreshTerrainHeightMap();
#endif
			}

#if VEGETATION_STUDIO

			//internal static VegetationMaskArea AddAreaMask(GameObject gameObject, string Id, Vector3[] nodes, string maskName, float Witdh)
			//{
			//	var vegetationMaskArea = gameObject.GetComponents<VegetationMaskArea>();


			//	vegetationMaskArea.UpdateVegetationMask();

			//	return vegetationMaskLines;
			//}

	
			internal static VegetationMaskLine AddLineMask(GameObject gameObject, string Id ,Vector3[] nodes, string maskName,float Witdh)
			{

				var vegetationMaskLines= gameObject.GetComponents<VegetationMaskLine>();
				VegetationMaskLine vegetationMaskLine = null;

				for (int i = 0; i < vegetationMaskLines.Length; i++)
				{
					if (vegetationMaskLines[i].Id == Id)
					{
						vegetationMaskLine = vegetationMaskLines[i];
					}
				}

				if (vegetationMaskLine == null )
				{
					vegetationMaskLine = gameObject.AddComponent<AwesomeTechnologies.VegetationMaskLine>();
					vegetationMaskLine.MaskName = "Mask - " + maskName;
				}
				vegetationMaskLine.ClearNodes();
				vegetationMaskLine.LineWidth = Witdh;


				var numChildren = nodes.Length;
				for (int i = 0; i < numChildren; i ++)
					vegetationMaskLine.AddNodeToEnd(nodes[i]);

				vegetationMaskLine.UpdateVegetationMask();
				return vegetationMaskLine;

				//public VegetationMaskLine VegetationStudioMaskLineNodesUpdateMeshCreation(int pathId)
				//{
				//	var path = Paths[pathId];
				//	var maskIdx = 0;
				//	if (VegetationMaskLine == null || VegetationMaskLine.Length < 1)
				//	{
				//		VegetationMaskLine = new VegetationMaskLine[2];

				//		VegetationMaskLine[maskIdx] = path.gameObject.AddComponent<AwesomeTechnologies.VegetationMaskLine>();
				//		VegetationMaskLine[maskIdx].MaskName = "Mask - " + FriendlyName;
				//	}


				//	VegetationMaskLine[maskIdx].ClearNodes();
				//	VegetationMaskLine[maskIdx].LineWidth = PathProfile.LaneDefaultSetting.GlobalWitdh;


				//	//int numChildren = path.Spline.Markers.Length;

				//	//for (int i = 0; i < numChildren; ++i)
				//	//	VegetationMaskLine.AddNodeToEnd(path.Spline.Markers[i].SegmentHolderObject.transform.position);

				//	////if (track.ClosedTrack)
				//	//	vegetationMaskLine.AddNodeToEnd(track.Markers[0].SegmentHolderObject.transform.position);
				//	var _numChildren = path.SplineCenterPointsCache.Length;
				//	for (int i = 0; i < _numChildren; i += 5)
				//	{
				//		var segmentIdN = path.SplineCenterPointsCache[i].SegmentId;
				//		var splineCenter = path.Spline.Markers[segmentIdN].SegmentHolderObject.transform.TransformPoint(path.SplineCenterPointsCache[i].Position);

				//		VegetationMaskLine[maskIdx].AddNodeToEnd(splineCenter);
				//	}


				//	VegetationStudioMaskLineUpdateSetting(path);
				//}

				//public void VegetationStudioMaskLineUpdateLandscaper(int pathId)
				//{
				//	var path = Paths[pathId];
				//	var maskIdx = 0;

				//	if (VegetationMaskLine == null)
				//		VegetationMaskLine[maskIdx] = path.gameObject.GetComponent<AwesomeTechnologies.VegetationMaskLine>();

				//	if (VegetationMaskLine == null)
				//	{
				//		VegetationMaskLine[maskIdx] = path.gameObject.AddComponent<AwesomeTechnologies.VegetationMaskLine>();
				//		VegetationMaskLine[maskIdx].MaskName = "Mask - " + FriendlyName;
				//		VegetationMaskLine[maskIdx].ShowHandles = false;
				//		VegetationMaskLine[maskIdx].ShowArea = true;
				//	}
				//	if (VegetationMaskLine != null)
				//	{
				//		VegetationStudioMaskLineUpdateSetting(path);
				//	}
				//}

				//private void VegetationStudioMaskLineUpdateSetting(Path path)
				//{
				//	int maskIdx = 0;

				//	if (path.LandscapeModifier.VSMaskEnable)
				//	{
				//		VegetationMaskLine[maskIdx].RemoveGrass = path.LandscapeModifier.VSRemoveGrass;
				//		VegetationMaskLine[maskIdx].RemovePlants = path.LandscapeModifier.VSRemovePlants;
				//		VegetationMaskLine[maskIdx].RemoveTrees = path.LandscapeModifier.VSRemoveTrees;
				//		VegetationMaskLine[maskIdx].RemoveObjects = path.LandscapeModifier.VSRemoveObjects;
				//		VegetationMaskLine[maskIdx].RemoveLargeObjects = path.LandscapeModifier.VSRemoveLargeObjects;
				//	}
				//	else
				//	{
				//		VegetationMaskLine[maskIdx].RemoveGrass = false;
				//		VegetationMaskLine[maskIdx].RemovePlants = false;
				//		VegetationMaskLine[maskIdx].RemoveTrees = false;
				//		VegetationMaskLine[maskIdx].RemoveObjects = false;
				//		VegetationMaskLine[maskIdx].RemoveLargeObjects = false;
				//	}

				//	VegetationMaskLine[maskIdx].AdditionalGrassPerimiter = path.LandscapeModifier.VSRemoveGrassPadding;
				//	VegetationMaskLine[maskIdx].AdditionalPlantPerimiter = path.LandscapeModifier.VSRemovePlantsPadding;
				//	VegetationMaskLine[maskIdx].AdditionalTreePerimiter = path.LandscapeModifier.VSRemoveTreesPadding;
				//	VegetationMaskLine[maskIdx].AdditionalObjectPerimiter = path.LandscapeModifier.VSRemoveObjectsPadding;
				//	VegetationMaskLine[maskIdx].AdditionalLargeObjectPerimiter = path.LandscapeModifier.VSRemoveLargeObjectsPadding;


				//	VegetationMaskLine[maskIdx].UpdateVegetationMask();
				//}
			}
#endif
			
			
			
			#endregion


#region MicroSplat
			public static class MicroSplat
			{

				public static bool IsInstalled()
				{
					try
					{
						var microSplatSystemBaseType = System.Type.GetType("MicroSplatTerrain, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null", true, true);
						return microSplatSystemBaseType!=null;
					}
					catch
					{
						//Debug.LogWarning("Awesome Vegetation Studio version 1.x is not installed.");
						return false;
					}
				}

			}
#endregion


		}



		#region MicroSplat
		public static class MicroSplat
		{
			public static void RemoveBlendableobject(GameObject gameObject)
			{

#if __MICROSPLAT_TERRAINBLEND__

				MicroSplatBlendableObject c = gameObject.GetComponent<MicroSplatBlendableObject>();
				if (c != null)
					c.Destroy();
#endif
			}

			public static void SyncAll()
			{
#if __MICROSPLAT__

				MicroSplatTerrain.SyncAll();
#endif
			}
		}
		#endregion
		#region MegaSplat
		public static class MegaSplat
		{
			/// <summary>
			/// Check to see if Vegetation Studio is installed
			/// </summary>
			/// <returns></returns>
			public static bool IsMegaSplatInstalled()
			{

				try
				{
					var megasplatSystemBaseType = System.Type.GetType("JBooth.MegaSplat.TextureCluster, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null", true, true);
					return megasplatSystemBaseType!=null;
				}
				catch
				{
					
					return false;
				}
			}
		}
		#endregion
	


	}
}


