﻿/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/
/* Class for Vegetation Studio Integration */


namespace com.earthshaping.Integration
{

	public struct LineMaskSetting
	{

	public bool removeGrass ;
	public bool removePlants ;
	public bool removeTrees ;
	public bool removeObjects ;
	public bool removeLargeObjects ;
	public float additionalGrassWidth ;
	public float additionalPlantWidth ;
	public float additionalTreeWidth ;
	public float additionalObjectWidth ;
	public float additionalLargeObjectWidth ;

	public float additionalGrassWidthMax ;
	public float additionalPlantWidthMax ;
	public float additionalTreeWidthMax ;
	public float additionalObjectWidthMax;
	public float additionalLargeObjectWidthMax;

	public void SetDefault()
	{
		removeGrass = true;
		removePlants = true;
		removeTrees = true;
		removeObjects = true;
		removeLargeObjects = true;
		additionalGrassWidth = 0;
		additionalPlantWidth = 0;
		additionalTreeWidth = 0;
		additionalObjectWidth = 0;
		additionalLargeObjectWidth = 0;

		additionalGrassWidthMax = 0;
		additionalPlantWidthMax = 0;
		additionalTreeWidthMax = 0;
		additionalObjectWidthMax = 0;
		additionalLargeObjectWidthMax = 0;

}

}
}
