﻿/* Copyright (C) Antonio Ripa - All Rights Reserved
 * This file is part of EarthShaping Project 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
 */
using UnityEngine;
using UnityEditor;
using com.earthshaping.framework.EditorHelper;
using com.earthshaping.framework.Landscape;


[CustomEditor(typeof(EsTerrainPlaceholder))]
public class EsTerrainEditor : EarthShapingBaseEditor 
{
	private bool _heightMap;
	private bool _splatMap;
	 private bool _detailMap;
	private bool _TreeMap;

	EsTerrainPlaceholder Target { get { return base.target as EsTerrainPlaceholder ; } }
	// Initialization
	void OnEnable() {
	
	}
	// Drawing the Custom Inspector
    public override void OnInspectorGUI() {
    
		ShowOpenWindowButton = false;
		InspectorDescription = "EarthShaping Terrain" ;
		showLogo = true;
		LogoName = "Sentieri_UI_titlebar_Terrain";

		base.OnInspectorGUI();

		var terrainGui = Target.terrainHolderData !=null ? Target.terrainHolderData.SourceTerrainId : "NA";
		//var ss = Target.terrainHolderData != null ? Target.terrainHolderData.AssetFileName : "NA";
		esInspectorHelper.LineSingle();
		esInspectorHelper.ShowWarning("This script should be not removed during edit stage");
		GUILayout.Space(8);
		EditorGUILayout.LabelField("Terrain GUID", terrainGui);
		GUILayout.Space(8);

		EditorGUILayout.ObjectField("Cache File", Target.terrainHolderData, typeof(TerrainHolderData),false);

		using (new GUILayout.VerticalScope(GUI.skin.box))
		{
			_heightMap = EditorGUILayout.Toggle("Heigth Map", _heightMap);
			_splatMap = EditorGUILayout.Toggle("Splat Map", _splatMap);
			_detailMap = EditorGUILayout.Toggle("Detail Map", _detailMap);
			_TreeMap = EditorGUILayout.Toggle("Trees Map", _TreeMap);
		}
		using (new GUILayout.HorizontalScope(GUI.skin.box))
		{

			if (Target.terrainHolderData != null && GUILayout.Button("Restore Landscape"))
			{
				esLandscapeManager.RestoreLandscape();
			
			}
			//if (GUILayout.Button("Update Terrain Backup"))
			//{

			//	ESLandscapeManager.RefreshBackup(Target.GetComponent<Terrain>() );

			//}
			GUILayout.FlexibleSpace();

		}
		
	

	

	}
	
}



