﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


namespace com.earthshaping.framework
{
	public class AssetsCache
	{

		public Dictionary<Object, Texture2D> TextureCache = new Dictionary<Object, Texture2D>();
		public static AssetsCache Instance;

		AssetsCache()
		{
#if UNITY_2017_2_OR_NEWER
            EditorApplication.playModeStateChanged += OnPlaymodeStateChanged;
#else
			EditorApplication.playmodeStateChanged += OnPlaymodeStateChanged;
#endif
		}

#if UNITY_2017_2_OR_NEWER
        void OnPlaymodeStateChanged(PlayModeStateChange playModeStateChange)
        {
            TextureCache.Clear();
        }
#else
		void OnPlaymodeStateChanged()
		{
			TextureCache.Clear();
		}
#endif

		public static Texture2D GetAssetPreview(Object asset)
		{
			if (asset is Texture2D)
			{
				ValidateInstance();
				if (Instance.TextureCache.ContainsKey(asset))
				{
					var previewTexture = Instance.TextureCache[asset];
					if (previewTexture)
					{
						if (previewTexture.width > 2) return previewTexture;
					}

					Instance.TextureCache.Remove(asset);
					previewTexture = CreateAssetPreview(asset);
					Instance.TextureCache.Add(asset, previewTexture);
					return previewTexture;
				}

				else
				{
					var previewTexture = CreateAssetPreview(asset);
					Instance.TextureCache.Add(asset, previewTexture);
					return previewTexture;
				}
			}
			else
			{
				Texture2D texture = AssetPreview.GetAssetPreview(asset);
				if (texture) return texture;
				return new Texture2D(80, 80);
			}
		}

		static Texture2D CreateAssetPreview(Object asset)
		{
			var previewTexture = AssetPreview.GetAssetPreview(asset);
			Texture2D convertedTexture = new Texture2D(2, 2, TextureFormat.ARGB32, true, true);
			if (previewTexture)
			{
				convertedTexture.LoadImage(previewTexture.EncodeToPNG());
			}
			return convertedTexture;
		}

		static void ValidateInstance()
		{
			if (Instance == null)
			{
				Instance = new AssetsCache();
			}
		}
	}
}
	

