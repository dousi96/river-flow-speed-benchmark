﻿/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/
using com.earthshaping.Sentieri;
using com.earthshaping.Terrains;
#if ES_TERRAPAINTER
using com.earthshaping.Splatmap;
#endif
using UnityEditor;
using UnityEngine;

public partial class EarthShapingMenuManager
{

#if SENTIERI_BUILDER
	//[MenuItem("Earth Shaping/Sentieri Landscape Manager", false, 1)]
	public static void OpenWindowL(MenuCommand menuCommand)
	{
		var obj = GetSentieriLandscape();
		SentieriWindow.OpenSentieriWindow();
		////var title = new GUIContent("Sentieri",AssetDatabase.LoadAssetAtPath<Texture2D>("Sentieri/Content/Images/icon.jpg"));
		//_sentieriWindow = GetWindow<SentieriWindow>(false, "Sentieri");
		//_sentieriWindow.autoRepaintOnSceneChange = true;
		//_sentieriWindow.minSize = new Vector2(400f, 500f);
		//_sentieriWindow.Show();
		//_sentieriWindow.Focus();
		////sentieriWindow.ShowUtility();
		//if (Selection.activeGameObject && Selection.activeGameObject.GetComponent<SentieriBuilder>())
		//{
		//	_sentieriWindow.Target = Selection.activeGameObject.GetComponent<SentieriBuilder>();
		//}

		// Ensure it gets reparented if this was a context click (otherwise does nothing)
		GameObjectUtility.SetParentAndAlign(obj, menuCommand.context as GameObject);

		// Register the creation in the undo system
		Undo.RegisterCreatedObjectUndo(obj, "Created " + obj.name);

		//Make it active
		Selection.activeObject = obj;
	}


	//[MenuItem("Earth Shaping/Sentieri Builder", false, 2)]
	public static void CreateSentieriObject()
	{
		// Check for a terrain
		var myTerrain = Terrain.activeTerrain;
		if (myTerrain == null)
		{
			EditorUtility.DisplayDialog("Alert",
				"Sentieri needs at least one active terrain in the scene.\nPlease add a terrain and try again", "OK");
			return;
		}



		var num = GameObject.FindObjectsOfType(typeof(SentieriBuilder)).Length ;

		var obj = new GameObject("Sentieri Builder : " + num.ToString("D2"));
		//obj.transform.parent = root.transform;
		obj.transform.position = Vector3.zero;
		obj.AddComponent<SentieriBuilder>();

		Undo.RegisterCreatedObjectUndo(obj, "Created " + obj.name);
		//Make it active
		//Selection.activeObject = obj;
	}


	private static SentieriWindow _sentieriWindow;
	[MenuItem("Tools/EarthShaping/Open Sentieri Manager", false, 0)]
	public static void OpenSentieriWindow()
	{
		//var title = new GUIContent("Sentieri",AssetDatabase.LoadAssetAtPath<Texture2D>("Sentieri/Content/Images/icon.jpg"));
		_sentieriWindow = EditorWindow.GetWindow<SentieriWindow>(false, "Sentieri Manager");
		
		_sentieriWindow.autoRepaintOnSceneChange = true;
		_sentieriWindow.minSize = new Vector2(450f, 900f);
		_sentieriWindow.wantsMouseMove = true;
		//_sentieriWindow.position = new Rect(200, 200, 950, 460);
		_sentieriWindow.Show();
		_sentieriWindow.Focus();
		//sentieriWindow.ShowUtility();
		if (Selection.activeGameObject && Selection.activeGameObject.GetComponent<SentieriBuilder>())
		{
			_sentieriWindow.TargetSentieriBuilder = Selection.activeGameObject.GetComponent<SentieriBuilder>();
		}
		//windowG.wantsMouseMove = true;
		////windowG.minSize = new Vector2(950,460);//460
		////windowG.minSize = new Vector2(800,450);//460
		//windowG.minSize = new Vector2(600, 350);//460
		//										//Rect tempPos = windowG.position;
		//										//Debug.Log(tempPos);
		//										//tempPos.width = 950;
		//										//tempPos.height = 460;
		//windowG.position = new Rect(200, 200, 950, 460);
	
	}

	public static GameObject GetSentieriLandscape()
	{
		var root = GameObject.Find("Sentieri Landscape");
		if (root == null)
		{
			root = new GameObject("Sentieri Landscape");
			root.transform.position = Vector3.zero;
			root.transform.rotation = Quaternion.identity;
			root.AddComponent<ES_LandscapeManager>();
			//var Target = root.AddComponent<SentieriRoutesGenerator>();

			//var scanObj = new GameObject("Scan Grid Area");
			//scanObj.transform.parent = root.transform;
			//var areaProjector = scanObj.AddComponent<ScanGridArea>();

			////TODO add multitile
			//var TerrainBounds = ES_TerrainUtility.GetTerrainBounds(Terrain.activeTerrain);
			//Target.ScannerWidth = (int)TerrainBounds.extents.x * 2;
			//Target.ScannerHeight = (int)TerrainBounds.extents.z * 2;

			//var scanWidth = Target.ScannerWidth;
			//var scanDepth = Target.ScannerHeight;


			////Target.ScanGridCenter.x = myTerrain.transform.position.x * 0.5f;
			////Target.ScanGridCenter.z = myTerrain.transform.position.z * 0.5f;
			//Target.ScanGridCenter = areaProjector.GetCenterPosition();
			//var rectX = Target.ScanGridCenter.x - Target.ScannerWidth * 0.5f;
			//var rectZ = Target.ScanGridCenter.z - Target.ScannerHeight * 0.5f;



			//areaProjector.SetAreaRect(new Rect(rectX, rectZ, scanWidth, scanDepth), Target.ScanGridCenter);

			//Target.AssignProjector(areaProjector);


		}

		return root;
	}



#endif


#if CASCADE
	[MenuItem("Tools/Cascade/Open Cascade Manager", false, 0)]
	public static void LaunchCascadeWindow()
	{
		CascadeWindow.OpenCascadeWindow();
		
	}
#endif
}
