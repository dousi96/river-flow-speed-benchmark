﻿using UnityEditor;
using UnityEngine;

public delegate void ESCreateProfileEventHandler(string name);

public class ESPopupWindowName: PopupWindowContent
{
	public string Name = "";
	public event ESCreateProfileEventHandler OnCreate;
	public string TopLabel = "Insert String Value";
	public override Vector2 GetWindowSize()
	{
		return new Vector2(200, 75);
	}
		
	public override void OnGUI(Rect rect)
	{
		GUILayout.Label(TopLabel, EditorStyles.boldLabel);
		Name = EditorGUILayout.TextField(Name);
			
		if (GUILayout.Button("Save"))
		{
			if (OnCreate != null && Name != "")
			{
				OnCreate(Name);
				editorWindow.Close();
			}
		}
	}
}