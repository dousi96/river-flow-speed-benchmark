﻿
using System.Linq;
using UnityEditor;

namespace com.earthshaping.framework.EditorHelper
{
	public class HelperCompiler
	{
		public static void  AddCompilerDefine(string editorSymbol)
		{
			var symbols =
				PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
			if (!symbols.Contains(editorSymbol))
			{
				symbols += ";" + editorSymbol;
				PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup,
					symbols);
			}

		}
		public static void RemoveCompilerDefine(string editorSymbol)
		{
			var symbols =
				PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
			if (symbols.Contains(editorSymbol))
			{
				var defines	= symbols.Split(';').ToList();
				defines.Remove(editorSymbol);
				symbols = string.Join(";", defines.ToArray());
				 
				PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup,
					symbols);
			}

		}

	}
}