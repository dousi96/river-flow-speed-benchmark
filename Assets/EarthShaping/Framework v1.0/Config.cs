﻿using System;
using com.earthshaping.framework.Enums;
using com.earthshaping.Geometry;
using com.earthshaping.Sentieri;
using UnityEngine;


namespace com.earthshaping.Setting
{
   


	 [Serializable]
	public static partial class Config 
	{

		internal static string HolderVersion = " - 2.0.0 rev.0";

	  
		public static bool ShowHelp = false;
		public static bool ShowHelpInSceneView = true;

		public static PowerOfTwo DefaultCellSize1 = PowerOfTwo._512;
		public static string SentieriVersionInfo = " - Beta 0.5.0 rev.0";

		public const string WindowName = "Sentieri";
		public const string GameObjectName = "_Grapg_";
		public static string SplatmapGPU = "TerraPainter";
		// Folders 

		//public static string DefaultSentieriFolder = "Assets/EarthShaping/Sentieri";
		// Not change FolderChacheName
		public static string FolderTerrainCacheName = "EearthShapingTerrainCache";
		// Settinng
		//	public static string DefaultTerrainCacheFolder = "Assets/EarthShaping/_CACHE_/Terrain";
		public static string DefaultExportFolder = "Assets/EarthShaping/_OUT_Exported";
		public static string DefaultSentieriProfileFolder = "Assets/EarthShaping/Profiles/Profiles_Path";
	
		public static string DefaultTextureFolder = "Assets/EarthShaping/_OUT_PBR_Textures/";
	

		// Default Shaders
		public static string DefaultMeshShader = "Standard";
		public static string DefaultLakeShader = "Lake";
		//public static string RiverShader = "Sentieri/Water_Base_Dev_0_1";
		public static string RiverShader = "Sentieri/Water_River";
		//public static string RiverShader = "Standard";
		public static string MeshTerrainShader = "Sentieri/MeshTerrain_Base";
		public static string TerrainMeshName = "_tempBlendTerrainCollider_";
		public static string HeatmapShader = "Sentieri/TerrainHeatMap";


		// Mesh Creation
#if SENTIERI_BUILDER
		public static MeshType MeshGeneratorType = MeshType.Segment;
#endif
		public static float NewPointDefaultDistance = 10.0f;
		public static readonly float RoadMeshSettingYMaxOffset = 5.0f;
		public static readonly float RoadMeshSettingYMinOffset = -5.0f;
		public static float MeshResolutionMin = 0.5f;
		public static float MeshResolutionMax = 100f;
		public static float MeshYGroudOffsetDefault = 0.02f;

		// Spline
		public static ApproxmiationType SplineType = ApproxmiationType.CatmullRom;
		public static int SegmentPointsCacheSize = 10;

		// Default resolution to draw spline in Scene View. Increasing that you can have perfomance issue in editor mode
		public static int SplineDrawResolution = 10;
		public static Color SplineDrawOddColor = Color.green;
		public static Color SplineDrawEvenColor = Color.red;
		public static Color SplineDrawSelected = Color.blue;

		// Path
		public static float DefaultPathWidth = 6;
		public static float DefaultTextureTiling = 6;

		// Tab Color
		public static Color SelectedTabColor = new Color(99 / 255f, 99 / 255f, 99 / 255f, 1);
		public static Color TabColor = new Color(66 / 255f, 66 / 255f, 66 / 255f, 1);

		public static Color SelectedNodeColor = new Color(55f / 255, 55f / 255, 55f / 255, 1);
		public static Color NodeColor = new Color(33f / 255, 33f / 255, 33f / 255, 1);

		


		public static float HandleSelectedSize = .2f;
		public static float HandleSize = .15f;

		public static Color HandleColor = new Color32(0x92, 0xF1, 0x5A, 0xFF);
		public static Color HandleSelectedColor = Color.green;


		public static bool GizmosDrawTrack;
		public static bool GizmosShowLabel;

		public static float MaxPathWidth = 100f;
		// Distance from Edit Camera to render items
		public static float EditDistanceRender = 1000;
	
		public static Color GizmosLabelColor= Color.red;
		public static bool GizmosSeaLevel = false;
		public static bool GizmosLandscape = false;

		public static Color WaterColor =  new Color(Color.blue.r, Color.blue.g, Color.blue.b, Color.blue.a / 4f);
	
		public static bool ShowNodeGizmos;
		private static object _version;
		private static string _defaultLakeShader;
	
		public const string ProjectorName = "BrushProjectorHolder (temp)";
		public const string ProjectorShaderName = /*"Projector/BrushOverlay";*/"Projector/SentieriPainter";


		public const int MaxVertices16bit = 65534;


	}

}