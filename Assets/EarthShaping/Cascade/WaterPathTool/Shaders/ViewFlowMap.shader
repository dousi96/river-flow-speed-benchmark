﻿Shader "Cascade/ViewFlowMap" {
	Properties {
		
		 [HideInInspector] _MainTex ("", 2D) = "white" {}
         [HideInInspector] _flowSpeed("Stream Speed",Range(0,100)) = 5
         _arrowScale("Arrow Scale",Range(0.01,100)) = 40

	}
	SubShader {
		Tags { "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		
		LOD 200
		
			CGPROGRAM
          /*  #define _DEBUG_VERTEX_COLOR
            #define _DEBUG_UV*/
			#pragma surface surf Standard 
			#pragma target 3.0



			#undef TRANSFORM_TEX
			#define TRANSFORM_TEX(tex,name) float4(tex.xyz ,  tex.w)
			sampler2D _MainTex;
			float _flowSpeed;
			float _arrowScale;


			struct Input {
				float2 uv_MainTex : TEXCOORD0;
				float4 uv3_MainTex1: TEXCOORD2;
				float4 uv4_MainTex1: TEXCOORD3;
				float3 uv_Color : COLOR;
			};

			// Line SDF
			float Line(float2 p, float2 p1, float2 p2)
			{
				float2 center = (p1 + p2) * 0.5;
				float len = length(p2 - p1);
				float2 dir = (p2 - p1) / len;
				float2 rel_p = p - center;
				float dist1 = abs(dot(rel_p, float2(dir.y, -dir.x)));
				float dist2 = abs(dot(rel_p, dir)) - 0.5 * len;
				return max(dist1, dist2);
			}

			float Arrow(float2 p, float2 v, float arrow_scale)
			{
			 
				v *= arrow_scale;
				p -= (floor(p / arrow_scale) + 0.5) * arrow_scale;

				float mv = length(v);
				float mp = length(p);

				if (mv > 0.0)
				{
					float2 dir_v = - v / mv; // Need - to orient with the flow direction
					mv = clamp(mv, 5.0, arrow_scale * 0.5);
					v = dir_v * mv;
					float shaft = Line(p, v, -v);
					float head = min(
						Line(p, v, 0.4 * v + 0.2 * float2(-v.y, v.x)),
						Line(p, v, 0.4 * v + 0.2 * float2(v.y, -v.x)));
				
					return min(shaft, head);
				}
				else
				{
					return mp;
				}
			}


			void surf (Input i, inout SurfaceOutputStandard o) 
			{
				float2 flowUV = i.uv3_MainTex1.zw;
				fixed3 _albedo;
				float3 colorUVSpeed = float3(0,0,0);

				if (flowUV.x == 0 && flowUV.y == 0)
				{
					// Just a placeholder 
					_albedo = tex2D(_MainTex, i.uv_MainTex);
				}
				else
				{
					float2 flowDir = normalize(flowUV);
					float flowSpeed = _Time[1] * 0.25f * _flowSpeed;
					float2 uv = i.uv_MainTex + float2(flowDir.x, flowDir.y) * flowSpeed;
		
					/*	colorUVSpeed = float3(frac(flowDir), 0);
					if (any(saturate(colorUVSpeed) - colorUVSpeed))
						colorUVSpeed.b = 0.5f;*/
				
					_albedo = tex2D(_MainTex, uv);
				}

				float arrow_dist = Arrow(i.uv_MainTex * 200, flowUV.xy * 0.4,_arrowScale);
				float arrowPix = lerp(0, 1,  1-saturate(arrow_dist));
	
			

                float3 colorUV = float3(0, 0, 0.5);	
#if _DEBUG_UV
                colorUV = float3(frac(i.uv_MainTex),0);
				if (any(saturate(i.uv_MainTex) - i.uv_MainTex))
						colorUV.b = 0.5f;
#elif _DEBUG_VERTEX_COLOR		
                colorUV = i.uv_Color;
#else
                _albedo = min(colorUV, _albedo);
				_albedo +=float3(arrowPix, 0, 0);
#endif		

				o.Albedo = _albedo;
				o.Normal = fixed3(0, 0, 1);
				o.Smoothness = 0;
				
                o.Alpha = 1;
				
			}
			ENDCG
		
	}
	FallBack "Diffuse"
}
