Shader "Internal/EarthShaping/CascadeDesign"
{
	Properties
	{
		[HideInInspector]_FlowSpeed("Flow Speed", Float) = 1.75
		_WaterNormal("Water Normal", 2D) = "bump" {}
		_NormalIntensity("Normal Intensity", Range(0,1.5)) = 0.5
		_ShalowColor("Shalow Color", Color) = (1,1,1,0)
		_DeepColor("Deep Color", Color) = (0,0,0,0)
		_WaterDepth("Water Depth", Float) = 1.5
		_WaterFalloff("Water Falloff", Float) = -0.75
		_Distortion("Distortion", Float) = 0.001
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Transparent+0" }
		Cull Back
		GrabPass{ }
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha 
		struct Input
		{
			float2 uv_texcoord;
			float2 uv3_texcoord;
			float4 screenPos;
		};

		float _FlowSpeed;

		uniform half _NormalIntensity;
		uniform sampler2D _WaterNormal;
		uniform float4 _WaterNormal_ST;
	
		uniform half4 _DeepColor;
		uniform half4 _ShalowColor;
		
		uniform sampler2D _CameraDepthTexture;
		
		uniform half _WaterDepth;
		uniform half _WaterFalloff;
		uniform sampler2D _GrabTexture;
		uniform half _Distortion;


#if _DEBUG_FLOWUV

		// Line SDF
		float Line(float2 p, float2 p1, float2 p2)
		{
			float2 center = (p1 + p2) * 0.5;
			float len = length(p2 - p1);
			float2 dir = (p2 - p1) / len;
			float2 rel_p = p - center;
			float dist1 = abs(dot(rel_p, float2(dir.y, -dir.x)));
			float dist2 = abs(dot(rel_p, dir)) - 0.5 * len;
			return max(dist1, dist2);
		}

		float Arrow(float2 p, float2 v)
		{

			v *= _ArrowScale;
			p -= (floor(p / _ArrowScale) + 0.5) * _ArrowScale;

			float mv = length(v);
			float mp = length(p);

			if (mv > 0.0)
			{
				float2 dir_v = -v / mv;

				mv = clamp(mv, 5.0, _ArrowScale * 0.5);
				v = dir_v * mv;
				float shaft = Line(p, v, -v);
				float head = min(Line(p, v, 0.4 * v + 0.2 * float2(-v.y, v.x)), Line(p, v, 0.4 * v + 0.2 * float2(v.y, -v.x)));

				return min(shaft, head);
			}
			else
			{
				return mp;
			}
		}
#endif
#if _DEBUG_FLOWUV
		float _ArrowScale;
#endif
		float3 DoFlowStream(sampler2D surfaceNormal,float2 uv,float strenght)
		{
	 
			float2 normal1_uv = uv + _Time.y * float2(-0.03, 0);
			float2 normal2_uv = uv + _Time.y * float2(-0.04, 0.01);
			float3 normal1 = UnpackScaleNormal(tex2D(surfaceNormal, normal1_uv), strenght);
			float3 normal2 = UnpackScaleNormal(tex2D(surfaceNormal, normal2_uv), strenght);
		 
			return  BlendNormals(normal1, normal2);
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_WaterNormal = i.uv_texcoord * _WaterNormal_ST.xy + _WaterNormal_ST.zw;
			float3 normal = DoFlowStream(_WaterNormal, uv_WaterNormal, _NormalIntensity);
			o.Normal = normal;
	
			float4 screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float eyeDepth = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD(screenPos))));
			float t = saturate( pow( ( abs( ( eyeDepth - screenPos.w ) ) + _WaterDepth ) , -_WaterFalloff ) );
			float4 surfaceColor = lerp( _DeepColor , _ShalowColor , t);
		
#if UNITY_UV_STARTS_AT_TOP
			float uvDirection = -1.0;
	#else
			float uvDirection = 1.0;
#endif
			screenPos.y = uvDirection * (screenPos.y - screenPos.w * 0.5) * _ProjectionParams.x + screenPos.w * 0.5;
			screenPos.xyzw /= screenPos.w;
		
			float4 screenColor = tex2D( _GrabTexture, ( half3( (screenPos).xy ,  0.0 ) + (normal * _Distortion ) ).xy );
			float4 albedo = lerp(surfaceColor, screenColor , t);
			o.Albedo = albedo.rgb;
			o.Occlusion = 0.0;
			o.Alpha = 1;
#if _DEBUG_FLOWUV       
		  /*  o.Alpha = 1;
			o.Normal = half3(0, 0, 1);
			o.Smoothness = 0;

			float2 flow = i.flowDirStr;
			float arrow_dist = Arrow(i.uv_Water * 200, flow * 0.4);
			o.Albedo = float3(0, 0, lerp(0, 1, 1 - saturate(arrow_dist)));*/
#endif
		}

		ENDCG
	}
	Fallback "Diffuse"
}