﻿/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/

using System.Collections.Generic;
using com.earthshaping.Cascade.Terrains;
using com.earthshaping.framework;
using com.earthshaping.framework.Landscape;
using com.earthshaping.framework.Terrain;
using com.earthshaping.Geometry;
using com.earthshaping.Helper.Extension;
using com.earthshaping.ProceduralMesh;
using com.earthshaping.Sentieri.Builder;
using com.earthshaping.Sentieri.ProceduralMesh;
using com.earthshaping.Setting;
using com.earthshaping.Terrains;
using UnityEngine;

namespace com.earthshaping.Cascade
{
	public class CascadeTerrainManager
	{
		public static void StampRiverBedPerCell(CellCacheId[] cellCacheIds,  string objName, bool full, Mesh mesh)
		{

		

			if (full)
			{

				CascadeConformTerrain.TerrainFlattenCascadePerCell( cellCacheIds, mesh, objName);
			}
			else
			{

				
			}
			
		}

		public static TerrainAreaData StampRiverBed(CatmullRom spline, int editorSelectedMarkerIdx, string objName, bool full, Mesh mesh)
		{

			TerrainAreaData result = new TerrainAreaData();

			if (full)
			{

				CascadeConformTerrain.TerrainFlattenCascade( mesh, objName);
			}
			else
			{

				var idxs = spline.GetSegmentMarkerSplineIdx(editorSelectedMarkerIdx);
				var points = new Vector3[5];
				points[0] = spline.MarkerSet[idxs[0]].Position;
				points[1] = spline.MarkerSet[idxs[1]].Position;
				points[2] = spline.MarkerSet[idxs[2]].Position;
				points[3] = spline.MarkerSet[idxs[3]].Position;
				points[4] = spline.MarkerSet[spline.ClampNodeIndex(idxs[3] + 1)].Position;

				float[,] lHeights = esLandscapeManager.GetLandscape.TerrainBackups[0].TerraiHolder.GetHeightMap();
				CascadeConformTerrain.TerrainFlattenCascade(Terrain.activeTerrain, lHeights, points, objName);

				Terrain.activeTerrain.terrainData.SetHeights(0, 0, lHeights);
			}
			return result;
		}


		//public static TerrainAreaData CreateRiverBed_old(RiverPath builder, string objName, bool full, Mesh[] meshes)
		//{
		//	TerrainAreaData result = new TerrainAreaData();
		
		//	if (full)
		//	{
		//		float[,] lHeights = ESLandscapeModel.TerrainBackups[0].TerraiHolder.GetHeightMap();
		//		result = CascadeConformTerrain.TerrainFlattenCascade(ESLandscapeModel.TerrainBackups, meshes, objName, lHeights);

		//	}
		//	else
		//	{
		//		var editorSelectedMarkerIdx = builder.SplineSelectionInfo.IdxSelectedPoint;
		//		var idxs = builder.Spline.GetSegmentMarkerSplineIdx(editorSelectedMarkerIdx);

		//		var points = new Vector3[5];
		//		points[0] = builder.Spline.MarkerSet[idxs[0]].Position;
		//		points[1] = builder.Spline.MarkerSet[idxs[1]].Position;
		//		points[2] = builder.Spline.MarkerSet[idxs[2]].Position;
		//		points[3] = builder.Spline.MarkerSet[idxs[3]].Position;
		//		points[4] = builder.Spline.MarkerSet[builder.Spline.ClampIndex(idxs[3] + 1)].Position;


		//		float[,] lHeights = ESLandscapeModel.TerrainBackups[0].TerraiHolder.GetHeightMap();

		//		CascadeConformTerrain.TerrainFlattenCascade(Terrain.activeTerrain, lHeights, points, objName);

		//		Terrain.activeTerrain.terrainData.SetHeights(0, 0, lHeights);
		//	}
		//	return result;
		//}

	}
}