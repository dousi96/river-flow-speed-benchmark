﻿/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/
using System;

namespace com.earthshaping.Cascade
{
	[Serializable]
	public struct BranchData
	{
		public RiverPath Branch;
		public float Position;
	}
}