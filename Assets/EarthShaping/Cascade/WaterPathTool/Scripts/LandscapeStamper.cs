﻿using System.Collections.Generic;
using System.Linq;
using com.earthshaping.Helper.Extension;
using com.earthshaping.Integration;
using com.earthshaping.ProceduralMesh;
using com.earthshaping.Sentieri;
using com.earthshaping.Sentieri.Geometry;
using com.earthshaping.Setting;
using UnityEngine;

namespace com.earthshaping.Cascade
{
	public class LandscapeStamper
	{
		static readonly CascadePath _cascadePath = new CascadePath();

		public static void StampRiverBed(RiverPath cascadeBuilder,bool CellScanRealTime)
		{

			if (cascadeBuilder.Conformed)
			{


			}
			
			// initialiting
			var lDepthCurves = new List<AnimationCurve>();
			//Force Profile to Active
			cascadeBuilder.Profile.RiverBedSetting.IsActive = true;
			lDepthCurves = new List<AnimationCurve>();
			for (int i = 0; i < cascadeBuilder.MarkerSet.Count; i++)
			{
				lDepthCurves.Add(cascadeBuilder.MarkerSet[i].Shape2DProfile1);
			}

			// Remove MeshEdgeOffset
			var tmpOffset = cascadeBuilder.RiverWidthOffset;
			cascadeBuilder.RiverWidthOffset = 0;
		
			_cascadePath.Init(cascadeBuilder.Spline, cascadeBuilder.Profile, cascadeBuilder.LakeIndices, 
				 cascadeBuilder.SmoothType, cascadeBuilder.DesignModeIsOn, cascadeBuilder.RiverWidthOffset);


			var lPointsPath = _cascadePath.GetSplineOrientedPoints(cascadeBuilder.StampResolution, CascadeManager.MultiThread, CellScanRealTime).ToList();
			cascadeBuilder.RiverWidthOffset = tmpOffset;



			// Find Best mesh setting for 16bit address
			var maxPoints = Mathf.FloorToInt(Config.MaxVertices16bit / lPointsPath.Count);
			var verticesCount = maxPoints * lPointsPath.Count;
			while (verticesCount > Config.MaxVertices16bit)
			{
				maxPoints -= 1;
				verticesCount = maxPoints * lPointsPath.Count;
				if (maxPoints == 0)
				{
					Debug.LogError("River Stamp Sampler Failed.");
					return;
				}
			}

			maxPoints = 30;
			var max = Config.MaxVertices16bit - maxPoints;
			var lThreadSegmentPoints = DataHelper.ArrayUtility.Split(lPointsPath, max / maxPoints - 1);
			// Force Init terrain Manager
			//ESLandscapeController.Init(true);

			CascadePathMesh.Profile = cascadeBuilder.Profile;
			CascadePathMesh.GameObjectHolder = cascadeBuilder.gameObject;
			CascadePathMesh.MeshYGroundOffset = cascadeBuilder.MeshYGroundOffset;
		
			// Executing
			for (int i = 0; i < lThreadSegmentPoints.Count; i++)
			{
				ExecuteSegment(cascadeBuilder, lDepthCurves, maxPoints, lThreadSegmentPoints, i);

			}
			//ltmpSingleMeshEnd.Destroy();
			//ltmpSingleMeshStart.Destroy();
			//var f = ArrayUtility.To2DArray(area.map, heigth, width);

			//Terrain.activeTerrain.terrainData.SetHeights(0, 0, lHeights);
			// Set
			//BlendMap = blends;



#if VEGETATION_STUDIO
			if (cascadeBuilder.VSAutoRefresh)
			{

				var bounds = GeometryUtil.GetBounds(cascadeBuilder.Spline);

				IntegrationScript.VegetationStudio.RefreshTerrainHeightMap(bounds);
			}
#endif
			

			cascadeBuilder.Conformed = true;


		}

		private static void ExecuteSegment(RiverPath riverPath, List<AnimationCurve> lDepthCurves, int maxPoints, List<List<Sentieri.ESOrientedPoint>> lThreadSegmentPoints, int i)
		{
			if (i > 0)
			{
				lThreadSegmentPoints[i].Insert(0, lThreadSegmentPoints[i - 1][lThreadSegmentPoints[i - 1].Count - 1]);
			}
			var riverBedMesh = CascadePathMesh.MeshCascadeGround(lThreadSegmentPoints[i], lDepthCurves, riverPath.cascadeRoot, maxPoints);
			GameObject ltmpSingleMesh = new GameObject(Config.TerrainMeshName);
			ltmpSingleMesh.transform.parent = riverPath.cascadeRoot.transform;
			var col = ltmpSingleMesh.AddComponent<MeshCollider>();
			col.sharedMesh = riverBedMesh;

			//Vector3 forward, center;
			//Mesh capMesh;
			//int lPathPointIdx = 0;
			//int lMarkerId = 0;
			//MeshStartCap(lMarkerId, out forward, out center, out capMesh, lPathPointIdx);
			//GameObject ltmpStartCap = new GameObject(Config.TerrainMeshName);
			//var rot = Quaternion.LookRotation(-forward);
			//ltmpStartCap.transform.rotation = rot;
			//var colStartCap = ltmpStartCap.AddComponent<MeshCollider>();
			//colStartCap.sharedMesh = capMesh;
			//startMarker.GroundObject.GetComponent<MeshFilter>().sharedMesh = capMesh;
			//startMarker.Position = center;
			//startMarker.GroundObject.transform.position = startMarker.Position;
			//GameObject ltmpEndCap = new GameObject(Config.TerrainMeshName);
			//ltmpEndCap.transform.parent = cascadeRoot.transform;
			//var colEndCap = ltmpSingleMesh.AddComponent<MeshCollider>();
			//colEndCap.sharedMesh =1iverBedMesh;

			//if (_cacheCenterPoints.Count == 0) RefreshCachePoints();

			CascadeTerrainManager.StampRiverBedPerCell(riverPath.CellCacheIds, Config.TerrainMeshName, true, riverBedMesh);
			//blends.Add(CascadeTerrainManager.CreateRiverBed(this, lHeights, tmpSingleMeshName, true, riverBedMesh));
			ltmpSingleMesh.Destroy();
		}


	}
}