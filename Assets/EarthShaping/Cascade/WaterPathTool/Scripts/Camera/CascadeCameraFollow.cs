﻿using com.earthshaping.Cascade;
using com.earthshaping.Geometry;
using com.earthshaping.Sentieri;
using com.earthshaping.Terrains;
using UnityEngine;


public class CascadeCameraFollow : MonoBehaviour
{
	

	public int StartNode = 0;
	public int EndNode = -1;
	float _currentTime;
	float _currentDist = 0.0f;
	public float Speed = 0.1f;
	public float CameraHeight = 2.0f;
	public GameObject Path;
	private Vector3 _prev;
	private CatmullRom _spline;
	private void Awake()
	{
		_spline = Path.GetComponent<RiverPath>().Spline;
	
	
	}

	private float _stopDistance;
	void Start()
	{
		if (_spline == null) return;
		_spline.RefreshTotalLength();
	
		_spline.TimeToLength(Mathf.RoundToInt(_spline.TotalLenght) );

		if (EndNode > _spline.MarkerCount - 1) EndNode = _spline.MarkerCount-1;

		_stopDistance = EndNode < 0 ? _spline.TotalLenght : _spline.VertexGetLenght(EndNode);
		_currentDist = _spline.VertexGetLenght(StartNode);
	}

	private void Update()
	{

		if (_spline == null) return;
	
		if (_currentDist > _stopDistance) return;
		_currentTime += Time.deltaTime;
		_currentDist = Mathf.Clamp(_currentDist + Speed, 0, _spline.TotalLenght);
		Vector3 lnext = _spline.GetPositionPerLength(_currentDist);;
		Vector3 position = lnext + Vector3.up*CameraHeight;
		var direction = (lnext - _prev).normalized;
		transform.rotation = Quaternion.LookRotation(direction);
		transform.position = position;
		_prev = lnext;

	}

}
