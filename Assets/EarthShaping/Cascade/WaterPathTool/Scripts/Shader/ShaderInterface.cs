﻿/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/
using System;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace com.earthshaping.Cascade
{
	public class ShaderInterface
	{
		public static Shader CreateShader(string path, string shaderName)
		{
			//Init Material Default
			Assembly editorAssembly = AppDomain.CurrentDomain.GetAssemblies().First(a => a.FullName.StartsWith("Assembly-CSharp-Editor,")); // ',' included to ignore  Assembly-CSharp-Editor-FirstPass
			Type type = editorAssembly.GetTypes().FirstOrDefault(t => t.FullName.Contains("RiverShaderGUI"));
			// Create default new shader
			var inParameters = new object[] { path, shaderName };
			Shader result = (Shader)type.GetMethod("CreateShader", BindingFlags.Public | BindingFlags.Static).Invoke(obj: null, parameters: inParameters);
			return result;
		}
	}
}