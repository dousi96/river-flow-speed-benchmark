/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/

using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using com.earthshaping.framework.EditorHelper;
using com.earthshaping.framework.Textures;


[CustomEditor(typeof(CascadeTextureGenerator))]
public class CascadeTextureGeneratorEditor : Editor 
{
	
	CascadeTextureGenerator  Target { get { return target as CascadeTextureGenerator; }}


	private static readonly GUIContent GCOutputSize = new GUIContent("Packed Texture Size:", "Size of the packed texture");
	private static readonly GUIContent CGSaveTexture = new GUIContent("Save Cascade Texture", "Save the packed texture.");
	private static readonly GUIContent CGNoSaveTexture = new GUIContent("Cascade Packer need all the textures !", "");

	

	[SerializeField]
	private int _selectedTextureSizeIndex = 4;

	private readonly GUIContent[] _outputSize =
	{
		new GUIContent("64"),
		new GUIContent("128"),
		new GUIContent("256"),
		new GUIContent("512"),
		new GUIContent("1024"),
		new GUIContent("2048"),
		new GUIContent("4096"),
		new GUIContent("8192")
	};

	private readonly List<int> _size = new List<int>()
	{
		64,
		128,
		256,
		512,
		1024,
		2048,
		4096,
		8192
	};

	private int previewRes = 256;
	private Rect _rect;

	private bool _isDirty;

	public override void OnInspectorGUI() 
	{

		// Init Packer
		if (Target.Packer.Channels == null)
		{
			InitPacker();

		}
		GUILayout.Box("Textures Packer " + Target.Packer.Version, GUILayout.Height(23), GUILayout.ExpandWidth(true));

		esInspectorHelper.ShowMessage("Select the texture as below. \nFoam -> R Channel / Normal -> GA / Waterfall -> R Channel");

		if (Target.PackedTexturePreview == null && Target.GetTexturesNumber != 0)
		{
			Target.PackedTexturePreview = Target.PackTextures(previewRes);
		}
	
		_selectedTextureSizeIndex = EditorGUILayout.Popup(GCOutputSize, _selectedTextureSizeIndex, _outputSize);

		GUI_DrawPacker();

		if (_isDirty)
		{
		
			var t = Target.PackTextures(_size[_selectedTextureSizeIndex] );
			Target.PackedTexturePreview = t;
			esInspectorHelper.SetObjectDirty(Target);
	
			_isDirty = false;
		}

		_rect = GUILayoutUtility.GetRect(GUIContent.none, GUIStyle.none, GUILayout.Width(256), GUILayout.Height(256));
		_rect.x = 20;
		_rect.y += 20;

		if (Target.PackedTexturePreview != null)
			EditorGUI.DrawPreviewTexture(_rect, Target.PackedTexturePreview);


	}

	private void InitPacker()
	{
		ChannelProfile ch0 = new ChannelProfile
		{
			FriendlyName = "Foam",
		
			Operation = new ChannelMap { RSource_To = Channel.B, GSource_To = Channel.None, BSource_To = Channel.None, ASource_To = Channel.None },
			Index = 0,
			Texture = Target.DefaultTexture(_size[_selectedTextureSizeIndex])
		};
		Target.Packer.AddChannel(ch0);
		ChannelProfile ch1 = new ChannelProfile
        {
			FriendlyName = "Normal",
		
			Operation = new ChannelMap { RSource_To = Channel.None, GSource_To = Channel.R, BSource_To = Channel.None, ASource_To = Channel.G },
			Index = 1,
			NormalRequired = false,
			Texture = Target.DefaultTexture(_size[_selectedTextureSizeIndex])
		};
		Target.Packer.AddChannel(ch1);
		ChannelProfile ch2 = new ChannelProfile
		{
			FriendlyName = "Waterfall",
		
			Operation = new ChannelMap { RSource_To = Channel.A, GSource_To = Channel.None, BSource_To = Channel.None, ASource_To = Channel.None },
			Index = 2,
			Texture = Target.DefaultTexture(_size[_selectedTextureSizeIndex])
		};
		Target.Packer.AddChannel(ch2);
	}

	private void GUI_DrawPacker()
	{
		using (new GUILayout.VerticalScope(GUILayout.Height(120)))
		{
			var validTexturesCount = 0;
			foreach (var ch in Target.Packer.Channels)
			{
				UI_DrawChannel(ch);
				if (ch.Texture)
				{
					validTexturesCount++;
				}
			}
			if (validTexturesCount == Target.Packer.Channels.Count)
			{
				if (GUILayout.Button(CGSaveTexture, GUI.skin.button, GUILayout.Height(40)))
				{
					string textSavePath =
						EditorUtility.SaveFilePanelInProject("Save Cascade Packed Texture", "", "tga", "Save Cascade Packed Texture", "Assets\\Cascade\\ShaderGenerator\\Textures");
					if (textSavePath.Length < 1) return;

					var res = int.Parse(_outputSize[_selectedTextureSizeIndex].text);
					Target.SavePackedTexture(res, textSavePath);

				}
			}
			else
			{
				GUILayout.Button(CGNoSaveTexture, GUI.skin.button, GUILayout.Height(40));
			}
			
		}
	}


	/// <summary>
	/// 
	/// </summary>
	/// <param name="channel"></param>
	private void UI_DrawChannel(ChannelProfile channel)
	{

		using (new GUILayout.VerticalScope(GUILayout.Height(140)))
		{
			GUILayout.Box(channel.FriendlyName,GUILayout.Height(23), GUILayout.ExpandWidth(true));
			using (new GUILayout.HorizontalScope())
			{
				EditorGUI.BeginChangeCheck();
				var tex = (Texture2D)EditorGUILayout.ObjectField(channel.Texture, typeof(Texture2D), false, GUILayout.Width(100), GUILayout.Height(100));
				if (EditorGUI.EndChangeCheck())
				{
					if(tex != null)
					{
						// Check texture is a normal
						if (channel.NormalRequired)
						{
#if UNITY_EDITOR
							if (!EarthShapingEditor.TexturerHelper.CheckNormal(tex))
							{
								EditorUtility.DisplayDialog("Texture error","Texture need to be marked as normal","Ok");
								return;
							}
#endif
						}
						else
						{
#if UNITY_EDITOR
							TextureImporter atPath = (TextureImporter)AssetImporter.GetAtPath(AssetDatabase.GetAssetPath((Object)tex));
							if (!atPath.sRGBTexture && atPath.textureCompression == TextureImporterCompression.Uncompressed)
							// Check uncompressed linear
							//if (!TexturerHelper.CheckUncompressedLinear(tex))
							{
								EditorUtility.DisplayDialog("Texture error", "Texture need to be linear space and not compressed", "Ok");
								return;
							}
#endif
						}
					}
					channel.Texture = tex;

					_isDirty = true;
				}
			}

		}


	}



}



