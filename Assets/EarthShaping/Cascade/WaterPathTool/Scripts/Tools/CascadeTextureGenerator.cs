﻿/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/

using com.earthshaping.framework;
using com.earthshaping.framework.Textures;
using com.earthshaping.Helpers;
using UnityEngine;


[CreateAssetMenu(fileName = "CascadeTexture", menuName = "Cascade/Textures Packer", order = 100)]
public class CascadeTextureGenerator : ScriptableObject
{
	public Texture2D PackedTexturePreview;
	[SerializeField]
	private  TexturesPacker _packer = new TexturesPacker();
	public TexturesPacker Packer
	{
		get { return _packer; }
		set { _packer = value; }
	}

	public int GetTexturesNumber
	{
		get
		{
			int count = 0;

			if (_packer.Channels == null) return count;

			for (var i = 0; i < _packer.ChannelCount; i++)
			{
				if (_packer.GetChannel(i).Texture != null)
				{
					count++;
				}
			}
			return count;

		}
		
	}


	public Texture2D DefaultTexture(int lTextureSize)
	{
		var lBlended = new Color32[lTextureSize * lTextureSize];
		var output = new Texture2D(lTextureSize, lTextureSize, TextureFormat.RGBA32, true, true);
		output.SetPixels32(lBlended);


		return output;
	}

	public Texture2D PackTextures(int lTextureSize)
	{

		var lPackedTexture = new Texture2D(lTextureSize, lTextureSize, TextureFormat.RGBA32, true, true);

		// Scale input textures
		for (var i = 0; i < _packer.ChannelCount; i++)
		{
			var channel = _packer.GetChannel(i);
			if (channel.Texture != null)
			{
				var tmpMap = Instantiate(channel.Texture.GetIgnoringRW());
				esTextureScale.Bilinear(tmpMap, lTextureSize, lTextureSize);
				channel.colors = tmpMap.GetPixels32();
				DestroyImmediate(tmpMap);
			}
		}

		// Blend textures
		var lBlended = new Color32[lTextureSize * lTextureSize];
		for (var i = 0; i < _packer.ChannelCount; i++)
		{
			var channel = _packer.GetChannel(i);
			if (channel.Texture != null)
			{
				lBlended = channel.BlendChannel( lBlended, lTextureSize);
			}
		}
		lPackedTexture.SetPixels32(lBlended);
		lPackedTexture.Apply();
		return lPackedTexture;
	}

	public void SavePackedTexture(int res, string textSavePath)
	{
		if (textSavePath.Length != 0)
		{
			
			string assetName = textSavePath.Substring(textSavePath.LastIndexOf("/"));
			assetName = assetName.Substring(1, assetName.IndexOf(".") - 1);
			textSavePath = textSavePath.Substring(0, textSavePath.LastIndexOf("/"));
			var savePath = string.Format("{0}/{1}.tga", textSavePath, assetName);
		
			PackTextures(res).EncodeToTGA(false).WriteAllBytes(savePath);

#if UNITY_EDITOR
			UnityEditor.AssetDatabase.ImportAsset(savePath, UnityEditor.ImportAssetOptions.ForceUpdate);
			var importer = (UnityEditor.TextureImporter)UnityEditor.AssetImporter.GetAtPath(savePath);
			importer.wrapMode = TextureWrapMode.Repeat;
			importer.textureCompression = UnityEditor.TextureImporterCompression.Uncompressed;
			importer.sRGBTexture = false;
			importer.maxTextureSize = res;
		
			importer.SaveAndReimport();
#endif
		}
	}



}
