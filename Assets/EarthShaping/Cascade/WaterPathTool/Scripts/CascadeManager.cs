﻿/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using com.earthshaping.Cascade.Terrains;
using com.earthshaping.framework;
using com.earthshaping.framework.Enums;
using com.earthshaping.framework.JobSystem;
using com.earthshaping.framework.Landscape;
using com.earthshaping.framework.Terrain;
using com.earthshaping.framework.ThreadManager;
using com.earthshaping.Geometry;
using com.earthshaping.Helper.Extension;
using com.earthshaping.Integration;
using com.earthshaping.ProceduralMesh;
using com.earthshaping.Sentieri;
using com.earthshaping.Sentieri.Geometry;
using com.earthshaping.Setting;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.UI;

namespace com.earthshaping.Cascade
{

	[ExecuteInEditMode]
	public  class CascadeManager :   MonoBehaviour
	{
		#region Variable Declaration
		static CascadeManager _instance;

		public static CascadeManager Instance
		{
			get { return _instance; }
		}

		public static bool Logging = true;
	
		private const string camName = "UnderWaterFXCam";

		private Camera mUnderwaterCam;
		public RawImage debugImage;
	
		public static bool EnableUnderWater;
		public static Camera CameraMain;
		public static List<RiverPath> CascadeBuilders = new List<RiverPath>();

		public JobStatus _TaskStatus = JobStatus.IsIdle;

		public List<RiverPath> RiverPaths = new List<RiverPath>();
		RenderTexture _underWaterFieldTexture;
		RenderTexture _riverMap;


		public static bool ShowCellGrid;
		public static bool ShowCascadeBuilderCells;
		public static bool ShowTerrainBoundary;
		private float time = 0;
		private float refreshTime = 1;

		[SerializeField] private bool _enableConformt;
		public static bool EnableConform
		{
			get {
				return _instance == null ? false : _instance._enableConformt;
			}
			set { if(_instance) _instance._enableConformt = value;
			}
		}
		[SerializeField] private bool _enableStitcht;
		public static bool EnableStitch
		{
			get { return _instance._enableStitcht; }
			set { _instance._enableStitcht = value; }
		}
		[SerializeField] private float _tileEdgeWidth = 0.01f;
		public static float TileEdgeWidth
		{
			get { return _instance._tileEdgeWidth; }
			set {  _instance._tileEdgeWidth = value; }
		}
		[SerializeField] private float _blendPower = 0.01f;
		public static float BlendPower
		{
			get { return _instance._blendPower; }
			set { _instance._blendPower = value; }
		}


		
		

		#region multiThreadSetting
		public static List<esTaskJob> taskQueue;
		
		[SerializeField] private MultiThreadSetting MThreadSetting = new MultiThreadSetting()
		{
			CellScanSize = PowerOfTwo._128,
			iThreadCount = 2
		};
		public static PowerOfTwo CellSize
		{
			get { return  _instance !=null ? _instance.MThreadSetting.CellScanSize : PowerOfTwo._256 ; }
			set { if (_instance != null) _instance.MThreadSetting.CellScanSize = value; }
		}

		public static int ThreadCount
		{
			get { return _instance != null ? _instance.MThreadSetting.iThreadCount : 1; }
			set { if (_instance != null) _instance.MThreadSetting.iThreadCount = value; }
		}
		
		public static bool MultiThread
		{
			get { return _instance !=null && _instance.MThreadSetting.IsMultiThread;}
			set { if(_instance!=null) _instance.MThreadSetting.IsMultiThread = value; }
		}
		public static bool EnableRealTimeScanCell
		{
			get { return _instance != null && _instance.MThreadSetting.EnableRealTimeScanCell; }
			set { if (_instance != null) _instance.MThreadSetting.EnableRealTimeScanCell = value; }
		}



		
		#endregion

		#endregion

		#region Unity Events
		public void Awake()
		{

			//if (Application.isPlaying)
			//{
			//	InitHeightField();
			//	CreateUnderWaterFXCamera();
			//	debugImage.texture = _underWaterFieldTexture;
			//}

		}
		private void Update()
		{
			//if (EnableUnderWater && Application.isPlaying)
			//	UpdateUnderwaterCam();

			if (_TaskStatus == JobStatus.IsIdle) return;

			if (_TaskStatus == JobStatus.IsInitializing)
			{




			}
			else if (_TaskStatus == JobStatus.IsRunning)
			{

				//_TaskStatus = JobStatus.IsExecuted;
				//Debug.Log("To IsExecuted");

			}
			else if (_TaskStatus == JobStatus.IsExecuted)
			{

				Debug.Log("Job was succesfully executed");
				_TaskStatus = JobStatus.IsIdle;
			}

		}

		private void OnEnable()
		{
			_instance = this;
#if UNITY_EDITOR
			if (!Application.isPlaying)
				EditorApplication.update += UpdateEditorCallback;
#endif
		}
		private void OnDisable()
		{
			_instance = null;
#if UNITY_EDITOR
			if (!Application.isPlaying)
				EditorApplication.update -= UpdateEditorCallback;
#endif

		}

		private void OnDrawGizmos()
		{

			//if (Terrain.activeTerrains.Length != ESLandscapeManager.GetTerrainCount)
			//	ESLandscapeManager.Init((int)CellScanSize);

			if (ShowTerrainBoundary)
			{
				var esTerrains = esLandscapeManager.ESTerrains;
				Gizmos.color = Color.cyan;
				var max = esLandscapeManager.HeightRange;

				var height = max.y - max.x;
				var cy = 0.0f;
				for (int i = 0; i < esTerrains.Length; i++)
				{
					var fullBounds1 = esTerrains[i].GetBounds;
					cy += fullBounds1.center.y;
				}
				cy /= esTerrains.Length;

				for (int i = 0; i < esTerrains.Length; i++)
				{
					var fullBounds1 = esTerrains[i].GetBounds;

					var topPoint = fullBounds1.max; // top
					var bottomPoint = fullBounds1.min; ;

					var cubeSize = bottomPoint - topPoint;
					cubeSize.y = height;
					var center = fullBounds1.center;
					center.y = cy;


					Gizmos.DrawWireCube(center, cubeSize);
				}


			}

			if (ShowCellGrid)
			{
				//Vector3 cameraPosition = SceneView.currentDrawingSceneView.camera.transform.position;
				Gizmos.color = Color.white;
				for (int i = 0; i < esLandscapeManager.GetTerrainCount; i++)
				{

					var terrain = esLandscapeManager.ESTerrains[i];
					if (terrain.Cells == null) continue;
					for (int j = 0; j < terrain.Cells.Length; j++)
					{
						//	float distance = Vector3.Distance(cameraPosition, terrain.Cells[j].Center);
						//	if (distance < 30)
						//	{
						//		var cell = terrain.Cells[j];
						//		Gizmos.DrawWireCube(cell.Center, cell.Size);
						//	}
						var cell = terrain.Cells[j];
						Gizmos.DrawWireCube(cell.Center, cell.Size);
					}


				}

			}


			if (ShowCascadeBuilderCells)
			{
				Gizmos.color = Color.blue;
				for (int n = 0; n < CascadeBuilders.Count; n++)
				{
					CascadeBuilders[n].DrawCells();
				}
			}


		}

		private void OnDestroy()
		{
			DestroyImmediate(esLandscapeManager.Instance.gameObject);
		}

		#endregion

		#region Update Event Delegate

		private void UpdateEditorCallback()
		{
			if (Time.realtimeSinceStartup - time > refreshTime)
			{
				//Debug.Log(Time.realtimeSinceStartup - time);
				RiverPaths.Clear();
				for (int i = 0; i < CascadeBuilders.Count; i++)
				{
					RiverPaths.Add(CascadeBuilders[i]);
				}

				time = Time.realtimeSinceStartup;
				if (_instance == null) _instance = this;
			}

			if (_instance._TaskStatus == JobStatus.IsInitializing)
			{
				QueueExecute();
				
			}


		}


		#endregion

		#region TaskQueue Job
		//static bool _createHolder = false;
		static int lockTime = 10;
		List<AnimationCurve> _depthCurves;
		List<GameObject> _buffer = new List<GameObject>();
		List<ESOrientedPoint> _pointsPath;
		CascadePath _cascadePath = new CascadePath();
		List<List<ESOrientedPoint>> _threadSegmentPoints;

		private void QueueExecute()
		{
			_instance._TaskStatus = JobStatus.IsRunning;
			bool lAborted = false;
			int lTotal = taskQueue.Count;
			float lProgress = 0;
			try
			{

				for (int i = 0; i < lTotal; i++)
				{
					lProgress += 1.0f / lTotal;
#if UNITY_EDITOR
					if (EditorUtility.DisplayCancelableProgressBar("Processing Jobs", taskQueue[i].Name + " - Task " + (i + 1) + "/" + lTotal, lProgress))
					{
						lAborted = true;
					}
#endif

					if (!lAborted)
					{
						taskQueue[i].Action.Invoke();
					}
					else
					{
						Debug.Log("Job Processing User aborted!");
					
						var err = 0f / 0f;
						if (err == 0) return; // TODO Fix to VS unused variable
					}

					//Debug.Log("Job " + i + " Executed");

				}
			}
			catch (Exception ex)
			{
				Debug.LogError(ex.Message);
			}
			finally
			{
				// Force Buffer Purge
				foreach (var item in _buffer)
				{
					if(item && item.gameObject)
						item.gameObject.Destroy();
				}
				_buffer.Clear();
				taskQueue.Clear();

				_instance._TaskStatus = JobStatus.IsIdle;
#if UNITY_EDITOR
				EditorUtility.ClearProgressBar();
#endif
			}






		}

	
		public static void QueueRefreshEsLandscapeManager()
		{

			if (taskQueue == null) taskQueue = new List<esTaskJob>();

			var terrainsCount = UnityEngine.Terrain.activeTerrains.Length;

			var scanScene = new esTaskJob
			{
				JobId = 0,
				Name = "Scan Scene",
				Action = () =>
				{
					esLandscapeManager.LoadLandscape();
				}
			};
			taskQueue.Add(scanScene);


			if(EnableConform)
			{
				for (int i = 0; i < terrainsCount; i++)
				{
					int idx = i;
					var task = new esTaskJob
					{
						JobId = i + 1,
						Name = "Load and Init Tile",
						Action = () =>
						{
							esLandscapeManager.LoadTerrainHolder(idx);
							esLandscapeManager.InitTile(idx, (int)CascadeManager.CellSize);
						}
					};
					taskQueue.Add(task);
				}

				var tileIdx = new esTaskJob
				{
					JobId = 0,
					Name = "Check Indices",
					Action = () =>
					{
						esLandscapeManager.AssignTilesIndices();
					}
				};
				taskQueue.Add(tileIdx);

			}

		

			

			_instance._TaskStatus = JobStatus.IsInitializing;


		}
		
		public static bool QueueUndoAll()
		{
			
			if (taskQueue == null) taskQueue = new List<esTaskJob>();

			taskQueue.Clear();
			for (int i = 0; i < esLandscapeManager.ESTerrains.Length; i++)
			{
				int idx = i;

				var task = new esTaskJob
				{
					JobId = idx,
					Name = "Restore Height Map",
					Action = () => esLandscapeManager.RestoreHeightMap(idx)
				};
				taskQueue.Add(task);

			}
		

			_instance._TaskStatus = JobStatus.IsInitializing;
		

			return true;
		}

		public static void QueueStampAll()
		{
			if (taskQueue == null) taskQueue = new List<esTaskJob>();
			for (int i = 0; i < CascadeBuilders.Count; i++)
			{
				int id = i;
				var task = new esTaskJob
				{
					JobId = id,
					Name = "Stamp Builder",
					Action = () =>
					{
						CascadeBuilders[id].Fix4LostProfile();
						LandscapeStamper.StampRiverBed(CascadeBuilders[id], CascadeManager.EnableRealTimeScanCell);
					}
				};

				taskQueue.Add(task);
			}
			if (EnableStitch)
			{
				QueueStitchLandscapeTiles(BlendPower, TileEdgeWidth);
			}
		
			_instance._TaskStatus = JobStatus.IsInitializing;
		}

		//
	
	
	
		public static void QueueStampSelected(string Id)
		{
			if (taskQueue == null) taskQueue = new List<esTaskJob>();
	
			for (int i = 0; i < CascadeBuilders.Count; i++)
			{
				if (CascadeBuilders[i].Id == Id)
				{
					int idx = i;
					int idCounter = 0;
					var cascadeBuilder = CascadeBuilders[idx];
				

					var init = new esTaskJob
					{
						JobId = idCounter++,
						Name = "Init Conform",
						Action = () =>
						{
							_instance._depthCurves = new List<AnimationCurve>();
							for (int k = 0; k < cascadeBuilder.MarkerSet.Count; k++)
							{
								_instance._depthCurves.Add(cascadeBuilder.MarkerSet[k].Shape2DProfile1);
							}
							_instance.InitStampSystem(cascadeBuilder);
							cascadeBuilder.Fix4LostProfile();
							_instance.Wait(lockTime);
						}

					};
					taskQueue.Add(init);


					//// Init Cell			
					//for (int k = 0; k < _instance._threadSegmentPoints.Count; k++)
					//{
					//	for (int w = 0; w < _instance._threadSegmentPoints[k].Count; w++)
					//	{

					//		int k1 = k;
					//		int w1 = w;

					//		var c = new TaskJob
					//		{
					//			Id = idCounter++,
					//			Name = "Scan Landscape",
					//			task = () =>
					//			{
					//				_instance._threadSegmentPoints[k1][w1].SetCellId(false);
					//			}

					//		};

					//		taskQueue.Add(c);

					//	}

					//}

					var loadBuffer = new esTaskJob
					{
						JobId = idCounter++,
						Name = "Load Buffer",
						Action = () =>
						{

							var segments = _instance._threadSegmentPoints;
							CascadePathMesh.Profile = cascadeBuilder.Profile;
							CascadePathMesh.GameObjectHolder = cascadeBuilder.gameObject;
							CascadePathMesh.MeshYGroundOffset = cascadeBuilder.MeshYGroundOffset;
							for (int k = 0; k < segments.Count; k++)
							{
								if (k > 0)
								{
									segments[k].Insert(0, segments[k - 1][segments[k - 1].Count - 1]);
								}
								var riverBedMesh = CascadePathMesh.MeshCascadeGround(segments[k], _instance._depthCurves, cascadeBuilder.cascadeRoot, 30);//30 maxpoint
								GameObject ltmpSingleMesh = new GameObject(Config.TerrainMeshName);
								ltmpSingleMesh.transform.parent = cascadeBuilder.cascadeRoot.transform;
								var col = ltmpSingleMesh.AddComponent<MeshCollider>();
								col.sharedMesh = riverBedMesh;
								_instance._buffer.Add(ltmpSingleMesh);
							}
							_instance.Wait(lockTime);
						}
					};
					taskQueue.Add(loadBuffer);

					var stamp = new esTaskJob
					{
						JobId = idCounter++,
						Name = "Carve Lanscape",
						Action = () =>
						{
							for (int k = 0; k < _instance._buffer.Count; k++)
							{
								CascadeConformTerrain.Stamp(_instance._buffer[k].gameObject.GetComponent<MeshCollider>().sharedMesh, Config.TerrainMeshName);
							}
							_instance.Wait(lockTime);
						}

					};
					taskQueue.Add(stamp);

					var purgeBuffer = new esTaskJob
					{
						JobId = idCounter++,
						Name = "Purge Buffer",
						Action = () =>
						{
							for (int k = 0; k < _instance._buffer.Count; k++)
							{
								_instance._buffer[k].Destroy();
							}
							_instance._buffer.Clear();
							_instance.Wait(lockTime);
						}
					};
					taskQueue.Add(purgeBuffer);

#if VEGETATION_STUDIO
					var vegetationStudio = new esTaskJob
					{
						JobId = i,
						Name = "Vegetation Studio Control",
						Action = () =>
						{
							if (cascadeBuilder.VSAutoRefresh)
							{
								var bounds = GeometryUtil.GetBounds(cascadeBuilder.Spline);
								IntegrationScript.VegetationStudio.RefreshTerrainHeightMap(bounds);
							}
							_instance.Wait(lockTime);
						}
					};
					taskQueue.Add(vegetationStudio);
#endif

					var endTask = new esTaskJob
					{
						JobId = i,
						Name = "End Control",
						Action = () =>
						{
							cascadeBuilder.Conformed = true;
							_instance.Wait(lockTime*10);
						}
					};
					taskQueue.Add(endTask);
				}

			}
			if (EnableStitch)
			{
				QueueStitchLandscapeTiles(BlendPower, TileEdgeWidth);
			}
			

			_instance._TaskStatus = JobStatus.IsInitializing;

		}
		public static void QueueStitchLandscapeTiles(float BlendPower, float TileEdgeWidth)
		{

			var tasks = esStitchLandscape.QueueStitchLandscapeTiles(BlendPower, TileEdgeWidth);
			taskQueue.AddRange(tasks);
			_instance._TaskStatus = JobStatus.IsInitializing;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="cascadeBuilder"></param>
		void InitStampSystem(RiverPath cascadeBuilder)
		{

			//Force Profile to Active
			cascadeBuilder.Profile.RiverBedSetting.IsActive = true;
			// Remove MeshEdgeOffset
			var tmpOffset = cascadeBuilder.RiverWidthOffset;
			cascadeBuilder.RiverWidthOffset = 0;

			_cascadePath.Init(cascadeBuilder.Spline, cascadeBuilder.Profile, cascadeBuilder.LakeIndices,
				 cascadeBuilder.SmoothType, cascadeBuilder.DesignModeIsOn, cascadeBuilder.RiverWidthOffset);
			_pointsPath = _cascadePath.GetSplineOrientedPoints(cascadeBuilder.StampResolution, CascadeManager.MultiThread, false).ToList();
			cascadeBuilder.RiverWidthOffset = tmpOffset;


			// Find Best mesh setting for 16bit address
			var maxPoints = Mathf.FloorToInt(Config.MaxVertices16bit / _pointsPath.Count);
			//var verticesCount = maxPoints * _pointsPath.Count;
			//while (verticesCount > Config.MaxVertices16bit)
			//{
			//	maxPoints -= 1;
			//	verticesCount = maxPoints * lPointsPath.Count;
			//	if (maxPoints == 0)
			//	{
			//		Debug.LogError("River Stamp Sampler Failed.");
			//		return;
			//	}
			//}

			maxPoints = 30;
			var max = Config.MaxVertices16bit - maxPoints;
			_threadSegmentPoints = DataHelper.ArrayUtility.Split(_pointsPath, max / maxPoints - 1);



		}
		#endregion

		private void Wait(int ms)
		{
			Thread.Sleep(ms);
		}

		#region ConformMode



		public void SetConformMode(bool IsActive)
		{
			EnableConform = IsActive;
		}

		# endregion

		#region WIP

		void InitHeightField()
		{

			if (Screen.width > 0 && Screen.height > 0)
			{
				_underWaterFieldTexture = new RenderTexture(Screen.width, Screen.height, 16)
				{
					name = "Underwater Field Mask Texture",
					isPowerOfTwo = false,
					hideFlags = HideFlags.DontSave,
					filterMode = FilterMode.Bilinear,
					wrapMode = TextureWrapMode.Clamp,
					//format = RenderTextureFormat.RGB565
				}; 
		
			}
		}

		void CreateUnderWaterFXCamera()
		{
			var cam = new GameObject(camName);
			cam.transform.SetParent(transform,false);
			mUnderwaterCam = cam.AddComponent<Camera>();
			mUnderwaterCam.cullingMask = 1 << LayerMask.NameToLayer("Water");
			mUnderwaterCam.targetTexture = _underWaterFieldTexture;
			mUnderwaterCam.clearFlags = CameraClearFlags.Color;

			//mUnderwaterCam.gameObject.SetActive(false);

		}

		public void UpdateUnderwaterCam()
		{
			Camera cam = Camera.main;
			if (cam == null) return;
			Vector3 camPos = cam.transform.position;

		

			mUnderwaterCam.clearFlags = CameraClearFlags.SolidColor;
			mUnderwaterCam.backgroundColor = Color.black;

			mUnderwaterCam.transform.position = cam.transform.position;
			mUnderwaterCam.transform.rotation = cam.transform.rotation;

			mUnderwaterCam.Render();
			debugImage.texture = _underWaterFieldTexture;
			Shader.SetGlobalTexture("_underWaterMask", _underWaterFieldTexture);

		}

		#endregion

		void Log(string message)
		{
			if (!Logging) return;
			Debug.Log(message);

		}

		public static void UpdateCells()
		{
			for (int i = 0; i < CascadeBuilders.Count; i++)
			{
				CascadeBuilders[i].RefreshCascadePath();
			}
		}
	




	}


}

