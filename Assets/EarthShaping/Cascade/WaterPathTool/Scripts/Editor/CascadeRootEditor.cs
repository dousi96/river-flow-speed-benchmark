/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/
using UnityEngine;
using UnityEditor;
using System.Collections;
using com.earthshaping.Cascade;
using com.earthshaping.Sentieri;


[CustomEditor(typeof(CascadeRoot))]
public class CascadeRootEditor : EarthShapingBaseEditor 
{
	
	CascadeRoot  Target { get { return target as CascadeRoot; }}
	

	// Initialization
	void OnEnable() 
	{
		if (Selection.activeGameObject == Target.gameObject)
		{
			Selection.activeGameObject = Target.GetComponentInParent<RiverPath>().gameObject;
		}
	}
	void OnDisable()
	{
		
	}
	// Drawing the Custom Inspector
    public override void OnInspectorGUI() 
	{
    		InspectorDescription = "CascadeRoot";
			showLogo = true;
			LogoName = "Sentieri_UI_titlebar_Builder";
			ShowOpenWindowButton = false;
			base.OnInspectorGUI();

	}
	
	public virtual void OnSceneGUI()
	{
		var lHandleDefaultColor = Handles.color;


		Handles.color = lHandleDefaultColor;
	}
}



