﻿/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/

using System;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using com.earthshaping.Cascade;
using com.earthshaping.EditorUI;
using com.earthshaping.framework;
using com.earthshaping.framework.Common;
using com.earthshaping.framework.Enums;
using com.earthshaping.framework.Landscape;
using com.earthshaping.Sentieri;
using GameObject = UnityEngine.GameObject;
using Random = System.Random;


[CustomEditor(typeof(CascadeManager))]
public class CascadeManagerEditor : EarthShapingBaseEditor 
{
	
	//CascadeManager  Target { get { return target as CascadeManager; }}
	

	// Initialization
	void OnEnable() 
	{
		
	}
	void OnDisable()
	{
		
	}
	// Drawing the Custom Inspector
    public override void OnInspectorGUI() 
	{
    		InspectorDescription = "CascadeManager";
			showLogo = true;
			LogoName = "Cascade_Splash_v2";
			ShowOpenWindowButton = false;
			base.OnInspectorGUI();
			if (GUILayout.Button("Open Cascade Manager"))
			{
				CascadeWindow.OpenCascadeWindow();
			}

    }
//	private ESToolbar _toolbar;
//	private JobStatus _applicationStatus = JobStatus.IsIdle;

//	private void InitToolBar()
//	{
//		ESButton btnDesignMode = new ESButton
//		{
//			Name = "btnCancelTask",
//			con = new GUIContent("Cancel Task", "Abort a running task"),
//			GuiStyle = EditorStyles.miniButton,
//			IsActive = true
//		};
//		btnDesignMode.ButtonClick += () =>
//		{
//			//if (TargetRiverPath != null)
//			//{
//			//	TargetRiverPath.DesignModeIsOn = !TargetRiverPath.DesignModeIsOn;
//			//	TargetRiverPath.SetDesignMode();
//			//	TargetRiverPath.RefreshCascadePath();
//			//}
//		};

//		var btns = new List<Dictionary<int, ESButton>>
//		{
	
//			new Dictionary<int, ESButton> { {0, btnDesignMode } },

//		};
//		_toolbar = new ESToolbar { Buttons = btns, Title = "Cascade Task Manager",Id = 1};
		
//}


	

	
}



