﻿using System.Collections;
using System.Collections.Generic;
using com.earthshaping.Cascade;
using com.earthshaping.framework.EditorHelper;
using com.earthshaping.framework.InspectorHelper;
using com.earthshaping.framework.Landscape;
using com.earthshaping.framework.Terrain;
using com.earthshaping.Geometry;
using com.earthshaping.Setting;
using UnityEditor;
using UnityEngine;

public class CascadeUI
{


	static int across;
	static int down;
	static int tWidth;
	static int tHeight;
	public static int toolbarInt = 0;
	public static  string[] toolbarStrings = new string[] { "Map", "Object" };
	public static int UI_LandscapeDataSource(int _selectedSourceTerrain )
	{

		//if(Terrain.activeTerrains.Length != ESLandscapeManager.ESTerrains.Length)
		//	CascadeManager.RefreshEsLandscapeManager();

		//using (var tggle = new ESInspectorHelper.DynamicGroup("Terrains in Landscape", 0, 0))
		//{
		//	if (tggle.ToggleGroup)
		//	{
		//		for (int i = 0; i < terrainManager.Landscape.esTerrains.Length; i++)
		//		{
		//			using (new EditorGUILayout.HorizontalScope())
		//			{
		//				var terrain = terrainManager.Landscape.esTerrains[i].TTerrain;
		//				//int My1Dindex = (row * lengthOfRow) + column; // Indexes
		//				var deleteTerrain = (Terrain)EditorGUILayout.ObjectField("Terrain : " + (i + 1), terrainManager.Landscape.esTerrains[i].TTerrain, typeof(Terrain), true);
		//				if (deleteTerrain == null && !ESLandscapeManager.AutoLoadTerrains)
		//				{
		//					terrainManager.TerrainRemove(terrain);
		//				}
		//				EditorGUILayout.ObjectField("", ESLandscapeModel.TerrainBackups[i].TerraiHolder, typeof(TerrainHolderData), true);
		//			}



		//		}
		//	}
		//}
		IntVector2 landscapeSize = new IntVector2(esLandscapeManager.GetLandscape.LandscapeSize.X, esLandscapeManager.GetLandscape.LandscapeSize.Z);
		EditorGUILayout.Vector2Field("Landscape Size : ", landscapeSize.ToVector2);
		esInspectorHelper.ShowMessage("Select a terrain from below list clickîng on that", Config.ShowHelp);

		tWidth = landscapeSize.X;
		tHeight = landscapeSize.Z;

		EditorGUILayout.LabelField("Landscape Tiles Number : " + esLandscapeManager.ESTerrains.Length);
		var lBackgroundColor = GUI.backgroundColor;
		using (var group = new esInspectorUI.DynamicGroup("Show Terrain Tile Map", 0, 9))
		{
			if (group.ToggleGroup)
			{

				if (esLandscapeManager.ESTerrains == null || esLandscapeManager.ESTerrains.Length < 1)
				{
					using (new EditorGUILayout.HorizontalScope())
					{
						using (new EditorGUILayout.VerticalScope(esInspectorHelper.BoxStyle))
						{
							GUILayout.Label("No Terrain in the active scene");

						}

					}
				}
				toolbarInt = GUILayout.Toolbar(toolbarInt, toolbarStrings);
				if(toolbarInt == 1)
				{
					var counter = 0;
					for (var h = 0; h < tHeight; h++)
					{
						using (new GUILayout.HorizontalScope())
						{
							for (var w = 0; w < tWidth; w++)
							{

								/*		esLandscapeManager.ESTerrains[counter].TTerrain =*/


								var t = EditorGUILayout.ObjectField(esLandscapeManager.ESTerrains[counter++].TTerrain, typeof(Terrain), true) as Terrain;
								// To remove the warning
								if (t == null) continue;


							}

						}

					}
				}
				else
				{
					for (int z = 0; z < landscapeSize.Z; z++)
					{
						using (new GUILayout.HorizontalScope())
						{
							for (int x = 0; x < landscapeSize.X; x++)
							{
								int idx = x + z * (int)landscapeSize.X;//h* tWidth +w
								var labelButton = string.Format("[{0}-{1}]", z, x);

								labelButton = string.Format("[{0}-{1}]",
									tWidth - 1 - esLandscapeManager.ESTerrains[idx].TileIdx.X,
									esLandscapeManager.ESTerrains[idx].TileIdx.Z);
								var content = new GUIContent(labelButton, "Click to Select");

								GUI.backgroundColor = (_selectedSourceTerrain == idx) ? esInspectorHelper.ColorButtonGreen : lBackgroundColor;
								if (GUILayout.Button(content, GUILayout.Height(20f), GUILayout.MaxWidth(120f)))
								{
									_selectedSourceTerrain = idx;

									//if(SelectObject)
									//	Selection.activeObject = ESLandscapeManager.ESTerrains[idx].TTerrain.gameObject;
									//if (SelectAsset)
									//	EditorGUIUtility.PingObject(ESLandscapeManager.ESTerrains[idx].TData);

								}

							}
						}

					}
				}

				
				
					

		

			}
		}
		GUI.backgroundColor = lBackgroundColor;
		//esInspectorHelper.DrawColoredLine(Color.green);

		if (esLandscapeManager.ESTerrains == null || esLandscapeManager.ESTerrains.Length < 1)
		{
			using (new EditorGUILayout.HorizontalScope())
			{
				using (new EditorGUILayout.VerticalScope(esInspectorHelper.BoxStyle))
				{
					GUILayout.Label("No Terrain in the active scene");

				}

			}
		}
		else if (_selectedSourceTerrain == -1 )
		{
			using (new EditorGUILayout.HorizontalScope())
			{
				using (new EditorGUILayout.VerticalScope(esInspectorHelper.BoxStyle))
				{
					GUILayout.Label("No Terrain Selected");

				}

			}
		}
		else if(toolbarInt == 0)
		{
			var lLabelToogleBox = "Terrain Details";
			using (var _showTerrainDetails = new esInspectorUI.Section(lLabelToogleBox, esInspectorUI.Section.Type.HeaderBox, esInspectorUI.Style.GreenDark))
			{
				if (_showTerrainDetails.ToggleSection)
				{
					if (_selectedSourceTerrain>-1)
					{
						var ES = esLandscapeManager.ESTerrains[_selectedSourceTerrain];

						using (new EditorGUILayout.VerticalScope(esInspectorHelper.BoxStyle))
						{
							EditorGUILayout.LabelField("Terrain Size: " + ES.GetWidth + "m by " + ES.GetWidth + "m");
							EditorGUILayout.LabelField("Terrain Height: " + ES.GetHeight + "m");
							EditorGUILayout.LabelField("Terrain Heightmap Resolution: " + ES.TData.heightmapResolution);
							//int numberOfTrees = 0;
							//// Find all landscape terrains
							//landscapeTerrains = landscapeGameObject.GetComponentsInChildren<Terrain>();
							//if (landscapeTerrains != null)
							//{
							//	for (index = 0; index < landscapeTerrains.Length; index++)
							//	{
							//		numberOfTrees += landscapeTerrains[index].terrainData.treeInstanceCount;
							//	}
							//}
							//EditorGUILayout.LabelField("Terrains In Landscape: " + landscapeTerrains.Length.ToString());
							//EditorGUILayout.LabelField("Trees In Landscape: " + numberOfTrees.ToString());
							//EditorGUILayout.LabelField("Meshes In Landscape: " + landscape.numberOfMeshes.ToString());
							//EditorGUILayout.LabelField("Prefabs In Landscape: " + landscape.numberOfMeshPrefabs.ToString());

							if (float.IsInfinity(esLandscapeManager.GetLandscape.HeightRange.x) || float.IsInfinity(esLandscapeManager.GetLandscape.HeightRange.y))
							{
								EditorGUILayout.LabelField("Landscape Height Min: 0m Max: 0m");
							}
							else
							{
								EditorGUILayout.LabelField("Landscape Height Min: " + esLandscapeManager.GetLandscape.HeightRange.x.ToString("0.0") + "m Max: " + esLandscapeManager.GetLandscape.HeightRange.y.ToString("0.0") + "m");
							}
						}
					}
				}
			}
		}

		return _selectedSourceTerrain;
	}

	
	
	
	public static void UI_LandscapeTileDetails(int idx)
	{

		if(idx == -1) return;
		var terrainManager = esLandscapeManager.Instance;

		if (esLandscapeManager.ESTerrains == null || esLandscapeManager.ESTerrains.Length < 1) return;

		using (new EditorGUILayout.HorizontalScope())
		{
			var terrain = terrainManager.Landscape.esTerrains[idx].TTerrain;
			//int My1Dindex = (row * lengthOfRow) + column; // Indexes
			if (!esLandscapeManager.AutoLoadTerrains)
			{
				var deleteTerrain = (Terrain)EditorGUILayout.ObjectField("Terrain : " + terrainManager.Landscape.esTerrains[idx].TileIdx.ToString(), terrainManager.Landscape.esTerrains[idx].TTerrain, typeof(Terrain), true);
				if (deleteTerrain == null)
				{
					terrainManager.TerrainRemove(terrain);
				}


			}
			else
			{
				EditorGUILayout.ObjectField("Terrain : "+ terrainManager.Landscape.esTerrains[idx].TileIdx.ToString(), terrainManager.Landscape.esTerrains[idx].TTerrain, typeof(Terrain), true);

			}
			//using (new EditorGUILayout.HorizontalScope(InspectorHelper.BoxStyle)) 
			//{
			//	EditorGUILayout.LabelField("Terrain Holder Setting");
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.ObjectField("", esLandscapeManager.GetLandscape.TerrainBackups[idx].TerraiHolder, typeof(TerrainHolderData), true);
			//	EditorGUILayout.ObjectField(ESLandscapeModel.TerrainBackups[_selectedSourceTerrain].TerraiHolder, typeof(TerrainHolderData), true, GUILayout.Width(120f));
			if (EditorGUI.EndChangeCheck())
			{
				//Target.terrainBackup = source as TerrainHolderData;

			}
			//}
			//EditorGUILayout.ObjectField("", ESLandscapeModel.TerrainBackups[_selectedSourceTerrain].TerraiHolder, typeof(TerrainHolderData), true);
		}
	}
}
