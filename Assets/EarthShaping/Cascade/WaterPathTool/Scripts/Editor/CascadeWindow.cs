﻿/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/
#define MESH_PAINTER_
using com.earthshaping.EditorUI;

using System;
using System.Collections.Generic;

using System.Text;
using com.earthshaping.Cascade;
using com.earthshaping.framework;
using com.earthshaping.framework.DataStructure;
using com.earthshaping.framework.Landscape;

using UnityEngine;
using UnityEditor;
using com.earthshaping.framework.EditorHelper;
using com.earthshaping.framework.Enums;
using com.earthshaping.framework.InspectorHelper;
using com.earthshaping.Helper.Extension;

using com.earthshaping.Integration;
using com.earthshaping.Setting;

using com.earthshaping.ProceduralMesh;

using com.earthshaping.framework.Audio;
using UnityEditorInternal;

public  class CascadeWindow  : EditorWindow
{

	#region Window Manager

	private static CascadeWindow _cascadeWindow;
	public static void OpenCascadeWindow()
	{
		_cascadeWindow = GetWindow<CascadeWindow>(false, "Cascade");
		_cascadeWindow.autoRepaintOnSceneChange = true;
		_cascadeWindow.minSize = new Vector2(450f, 450f);
		_cascadeWindow.wantsMouseMove = true;
	
		_cascadeWindow.Show();
		_cascadeWindow.Focus();

		
		if (Selection.activeGameObject && Selection.activeGameObject.GetComponent<RiverPath>())
		{
			_cascadeWindow.TargetRiverPath = Selection.activeGameObject.GetComponent<RiverPath>();
		}

	
	}

	

	
	#endregion

	#region Variables Declaration

	public RiverPath TargetRiverPath;
	public bool ClosedSurface;
	// Used for the scroll area
	private Vector2 _scrollPos = Vector3.zero;
	
	[SerializeField]
	private CascadeMainMenu _activeMenuOption;

	private bool _sceneDirty;
	private bool _repaint;

	
	readonly GUILayoutOption _buttonWidth = GUILayout.Width(160);
	// Used to import path
	private GameObject _waypointsRoot;

	// Used to stamp path
	[SerializeField]
	private bool stampPrefab = false;
	[SerializeField]
	private bool exportCollider;
	private bool createFBXOnExport;


	float MaxSurroundValue = 1000.0f;
	


	private enum GroundProfileIdx
	{
		RiverBed1 = 0,
		RiverBed2 = 1,
		RiverBed3
	}
	private GroundProfileIdx _selectedGroundProfileIdx = GroundProfileIdx.RiverBed1;
	private Dictionary<int, AnimationCurve> groundProfiles;
	private void InitGroundProfile()
	{
		groundProfiles = new Dictionary<int, AnimationCurve>
		{
			{0, MeshPrimitives.RiverBed1()},
			{1, MeshPrimitives.RiverBed2()},
			{2, MeshPrimitives.RiverBed3()}
		};
	}


	private int _selectedMarker = -1;
	private string _labelText;

	[SerializeField]
	private List<GameObject> _builders = new List<GameObject>();
	[SerializeField]
	int _selectedBuilderIdx = -1;

	public bool TargetRiverPathIsNull
	{

		get { return TargetRiverPath == null; }

	}

	#endregion

	#region Content Text

	readonly GUIContent GCApplyAll = new GUIContent("Apply to all", "Apply selected setting to all points and set as default value.");
	readonly GUIContent GCImport = new GUIContent("Import", "Import the point from the root and create the surface.");
	readonly GUIContent GCExport = new GUIContent("Export", "Export the point as game objects.");
	readonly GUIContent GCExportMap = new GUIContent("Export Texture Map", "Generate texture map.");

	readonly GUIContent GCEnable = new GUIContent("Enable", "Enable function.");

	readonly GUIContent GCShowHelp = new GUIContent("Show Help", "Enable Inspector Help Hints.");




	#endregion

	#region Menu Tabs
	private readonly string[] _toolBarOptions = { "Global", "Per Node", "Segments", "Landscape", "Lighting", "Imp/Exp",  "Flow Map" , "Debug/Setting", };

	#endregion

	#region EditView Toolbar 

	private ESToolbar _toolbar;
	ESButton btnConformHeader;
	ESButton btnConformSelected;
	ESButton btnConformAll;
	ESButton btnRestore;
	ESButton btnMove;
	ESButton btnScale;
	void InitToolbar()
	{
		ESButton btnDesignMode = new ESButton
		{
			Name = "btnDesignMode",
			con = new GUIContent("Design Mode Off", "Enable/Disable Design Mode"),
			GuiStyle = EditorStyles.miniButton,
			IsActive = true
		};
		btnDesignMode.ButtonClick += () =>
		{
			if (TargetRiverPath != null)
			{
				TargetRiverPath.DesignModeIsOn = !TargetRiverPath.DesignModeIsOn;
				TargetRiverPath.SetDesignMode();
				TargetRiverPath.RefreshCascadePath();
				btnDesignMode.GuiStyle = TargetRiverPath.DesignModeIsOn ?
				esInspectorHelper.GuiStyleColoredGreen : EditorStyles.miniButton;

				var content = TargetRiverPath.DesignModeIsOn ?
						 new GUIContent("Design Mode Off", "Enable/Disable Design Mode") :
						new GUIContent("Design Mode On", "Enable/Disable Design Mode");


				btnDesignMode.con = content;

			}
		};

		btnConformHeader = new ESButton
		{
			Name = "btnConformHeader",
			con = new GUIContent("Conform Terrain", "Conform Terrain"),
			GuiStyle = EditorStyles.helpBox,
			IsActive = true
		};

		btnConformSelected = new ESButton
		{
			Name = "btnConformSelected",
			con = new GUIContent("Selected", "Conform Terrain to Selected Active Cascade Builder"),
			GuiStyle = EditorStyles.miniButtonLeft,
			IsActive = true
		};
		btnConformSelected.ButtonClick += () =>
		{
			if (TargetRiverPath != null)
			{
				_op = SelectedConformType.Selected;
				CascadeManager.QueueStampSelected(TargetRiverPath.Id);

			}
		};
		btnConformAll = new ESButton
		{
			Name = "btnConformAll",
			con = new GUIContent("All", "Conform Terrain to All Active Cascade Builders"),
			GuiStyle = EditorStyles.miniButtonRight,
			IsActive = true
		};
		btnConformAll.ButtonClick += () =>
		{
			//	if (TargetRiverPath != null)
			//	{
			_op = SelectedConformType.Selected;
			//CascadeManager.QueueStampSelected(TargetRiverPath.Id);
			CascadeManager.QueueStampAll();
			//}
		};

		btnRestore = new ESButton
		{
			Name = "btnRestoreSelected",
			con = new GUIContent("Undo", "Undo conform operation for selected"),
			GuiStyle = EditorStyles.miniButton,
			IsActive = true
		};
		btnRestore.ButtonClick += () =>
		{
			if (TargetRiverPath != null)
			{

				_op = SelectedConformType.Selected;
				TargetRiverPath.RestoreRiverBed();


			}
		};
		btnMove = new ESButton
		{
			Name = "btnSetMoveTool",
			con = new GUIContent("M", "Set Move Marker Mode"),
			GuiStyle = EditorStyles.miniButtonLeft,
			IsActive = true
		};
		btnMove.ButtonClick += () =>
		{
			Tools.current = Tool.Move;
		};
		btnScale = new ESButton
		{
			Name = "btnSetScaleTool",
			con = new GUIContent("S", "Set Scale Width Mode"),
			GuiStyle = EditorStyles.miniButtonRight,
			IsActive = true
		};
		btnScale.ButtonClick += () =>
		{
			Tools.current = Tool.Scale;


		};

		ESButton btnRotate = new ESButton
		{
			Name = "btnSetRotateTool",
			con = new GUIContent("R", "Reserved for future use"),
			GuiStyle = EditorStyles.miniButtonMid,
			IsActive = true
		};

		ESButton btnAutoSlope = new ESButton
		{
			Name = "btnAutoSlope",
			con = new GUIContent("AutoSlope", "Auto slope"),
			GuiStyle = EditorStyles.miniButton,
			IsActive = true
		};
		btnAutoSlope.ButtonClick += () =>
		{
			if (TargetRiverPath != null)
			{
				TargetRiverPath.AutoSlope();
			}
		}; ;
		ESButton btnRew = new ESButton
		{
			Name = "btnRew",
			con = new GUIContent("<<", "Go to First"),
			GuiStyle = EditorStyles.miniButton,
			IsActive = true
		};
		btnRew.ButtonClick += () =>
		{
			GoToFirstNode();
		};
		ESButton btnForw = new ESButton
		{
			Name = "btnForw",
			con = new GUIContent(">>", "Go to Last"),
			GuiStyle = EditorStyles.miniButton,
			IsActive = true
		};
		btnForw.ButtonClick += () =>
		{
			GoToLastNode();
		};
		ESButton btnNext = new ESButton
		{
			Name = "btnNext",
			con = new GUIContent(">", "Go to next"),
			GuiStyle = EditorStyles.miniButtonRight,
			IsActive = true
		};
		btnNext.ButtonClick += () =>
		{
			GoToNextNode();
		};
		ESButton btnPrevi = new ESButton
		{
			Name = "btnPrevi",
			con = new GUIContent("<", "Goto Prevoius"),
			GuiStyle = EditorStyles.miniButtonLeft,
			IsActive = true
		};
		btnPrevi.ButtonClick += () =>
		{
			GoToPreviousNode();
		};

		var btns = new List<Dictionary<int, ESButton>>
		{
			new Dictionary<int, ESButton> { {0, btnMove },{1, btnScale },{2, btnRotate } },
			new Dictionary<int, ESButton> { {0, btnRew },{1, btnPrevi },{2, btnNext } ,{3, btnForw } },
			new Dictionary<int, ESButton> { {0, btnAutoSlope } },
			new Dictionary<int, ESButton> { {0, btnDesignMode } },

			new Dictionary<int, ESButton> { {0, btnConformHeader } },
			new Dictionary<int, ESButton> { {0, btnConformSelected }, { 1, btnConformAll } },
			new Dictionary<int, ESButton> { {0, btnRestore } }


		};
		_toolbar = new ESToolbar { Buttons = btns, Title = "Cascade" };

	}



	private void SetToolbarButtonsState()
	{
		btnScale.GuiStyle = Tools.current == Tool.Scale ? esInspectorHelper.GuiStyleColoredGreen : EditorStyles.miniButton;
		btnMove.GuiStyle = Tools.current == Tool.Move ? esInspectorHelper.GuiStyleColoredGreen : EditorStyles.miniButton;
		btnConformHeader.IsActive= btnConformAll.IsActive = btnConformSelected.IsActive = btnRestore.IsActive = ManagerIsPresent && CascadeManager.EnableConform ? true : false;
	}

	#endregion

	#region Cascade UI Creator
	/// <summary>
	/// Cache image buttons
	/// </summary>

	private GUIContent[] _cacheManualImageButtons;
	private void Cache_RefreshImageButton()
	{
		string[] ops = { "River", "Lake" };
		string[] tooltips = { "Create river mixed path", "Create lake surface" };
		_cacheManualImageButtons = new GUIContent[ops.Length];
		for (int i = 0; i < ops.Length; i++)
		{
			var textureLogo = Resources.Load<Texture2D>("Buttons/Cascade_UI_Icon_Manual_" + ops[i]);
			_cacheManualImageButtons[i] = new GUIContent
			{
				image = textureLogo,
				tooltip = tooltips[i]
			};
		}

	}

	void RefreshCascadeList()
	{
		var sentieriBuilders = GameObject.FindObjectsOfType(typeof(RiverPath));
		_builders = new List<GameObject>(sentieriBuilders.Length);
		for (int i = 0; i < sentieriBuilders.Length; i++)
		{
			_builders.Add((sentieriBuilders[i] as RiverPath).gameObject);
		}

		_selectedBuilderIdx = -1;

	}

	#endregion

	#region Unity Editor Events

	private void OnDisable()
	{
		SceneView.onSceneGUIDelegate -= OnSceneGuiDelegate;
	}
	private void OnEnable()
	{
		SceneView.onSceneGUIDelegate += OnSceneGuiDelegate;
	}
	//bool _createHolder = true;
	// Run 10 times per second
	// Delete to every 2 seconds
	float _managerCheckTimeRange = 2.0f;
	double _nextCheck = 0;
	public void OnInspectorUpdate()
	{
		ManagerIsPresent = GameObject.FindObjectOfType<CascadeManager>()!=null;
		// Check that there is a manager
		if (!ManagerIsPresent) return;
		if (EditorApplication.timeSinceStartup > _nextCheck)
		{
			_nextCheck = EditorApplication.timeSinceStartup + _managerCheckTimeRange;
			_DelegateLandscapeManager();

			if (/*CascadeManager.EnableConform &&*/ (esLandscapeManager.ESTerrains == null || Terrain.activeTerrains.Length != esLandscapeManager.ESTerrains.Length) )
			{
				CascadeManager.QueueRefreshEsLandscapeManager();
			}
		
		}

		base.Repaint();
	}

	private void OnSceneGuiDelegate(SceneView sceneView)
	{

		if (_toolbar == null) InitToolbar();
		SetToolbarButtonsState();
		
		_toolbar.Show();

		if (Config.GizmosSeaLevel)
		{
			//ESLandscapeManager.Instance.DrawGizmosSeaLevel();
			DrawGizmosSeaLevel(Config.WaterColor);
		}
		if (_repaint)
		{
			
			Repaint();
			_repaint = false;
		}
	}

	

	public void DrawGizmosSeaLevel(Color waterColor)
	{
		
		UnityEditor.Handles.color = waterColor;
		//Gizmos.DrawCube(_landscapeBound.center, _landscapeBound.size);
		//UnityEditor.Handles.DrawSolidRectangleWithOutline()
		//Terrain dimensions
		UnityEditor.Handles.color = Color.white;
		UnityEditor.Handles.DrawWireCube(esLandscapeManager.Instance.SeaPlaneCenter, esLandscapeManager.Instance.SeaPlaneSize);

	}
	//private List<IUnityJob> UnityJobs;
	//private IUnityJob activeJob;


	void OnGUI()
	{
		_sceneDirty = false;

		_scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);
		esInspectorHelper.Space(15);
		esInspectorHelper.LogoHeader("Cascade_Splash_v2");


		UI_Builders_Creation();
		UI_MultiThread();
		esInspectorHelper.LineSingle();
		UI_CascadeBuildersList();
		esInspectorHelper.LineSingle();


		if (TargetRiverPath == null && Selection.activeGameObject != null)
		{
			TargetRiverPath = Selection.activeGameObject.GetComponent<RiverPath>();
		}
		if (TargetRiverPath == null)
		{
			UI_MainToolbar();
			UI_ShowGuiSelectedOption();
			EditorGUILayout.EndScrollView();
			esInspectorHelper.Space(20);
			UI_WindowManagerFooter();
			return;
		}

		if (TargetRiverPath != null && TargetRiverPath.RepaintCustomInspector)
		{
			Repaint();
			TargetRiverPath.RepaintCustomInspector = false;
		}

		ClosedSurface = TargetRiverPath.ClosedSurface;
		Gui_SubBlock_SurfaceMeshInfo();

		UI_MainToolbar();

		if (_builders.Count < 1)
		{
			esInspectorHelper.ShowMessage("Scene Empty");

		}
		else if (TargetRiverPath == null && _selectedMarker == -1)
		{
			esInspectorHelper.ShowMessage("Select one");

		}
		else
		{
			UI_ShowGuiSelectedOption();
		}

		EditorGUILayout.EndScrollView();

		if (_sceneDirty)
		{
			SetSceneDirty();
			TargetRiverPath.RefreshCascadePath();

			_sceneDirty = false;

		}
		esInspectorHelper.Space(20);

		UI_WindowManagerFooter();

	}


	#endregion

	#region Init Landscape Manager
   
	void _DelegateLandscapeManager()
	{
		if (esLandscapeManager.Instance.LoadOrCreateTerrainHolderData == null)
		{
			esLandscapeManager.Instance.LoadOrCreateTerrainHolderData += LoadOrCreateTerrainHolderData;
			//Debug.Log("LoadOrCreateTerrainHolderData Initialized");
		}
		
	}

	static TerrainHolderData LoadOrCreateTerrainHolderData(int idx)
	{
		
		TerrainHolderData assetTerrainHolderData = default(TerrainHolderData);//= ScriptableObject.CreateInstance<TerrainHolderData>();
		esTerrainManagerEditor.FolderCacheName = Config.FolderTerrainCacheName;
		assetTerrainHolderData = esTerrainManagerEditor.GetTerrainHolderData(esLandscapeManager.ESTerrains[idx].TTerrain, idx);
		return assetTerrainHolderData;
	}
	#endregion

	#region UI Menu

	int tab4row = 4;
	private void UI_MainMenuSelection()
	{
		_activeMenuOption = (CascadeMainMenu)GUILayout.SelectionGrid((int)_activeMenuOption, _toolBarOptions, tab4row, GUILayout.Width(400), GUILayout.Height(45));
	}

	#endregion

	#region UI Core Component

	void UI_HELPMESSAGE(string msg)
	{
		esInspectorHelper.ShowMessage(msg, Config.ShowHelp);

	}
	private void UI_MultiThread()
	{
		var current = GUI.backgroundColor;
		if (CascadeManager.MultiThread)
		{
			GUI.backgroundColor = new Color(0.0f, 0.78f, 0.0f);
		}
		else
		{
			GUI.backgroundColor = new Color(0.78f, 0.468f, 0.390f);
		}

		using (new EditorGUILayout.HorizontalScope(esInspectorHelper.BoxStyle))
		{
			CascadeManager.MultiThread = EditorGUILayout.Toggle("Multi Thread", CascadeManager.MultiThread);
			if (CascadeManager.MultiThread)
			{
				CascadeManager.ThreadCount = EditorGUILayout.IntSlider("Number of Threads", CascadeManager.ThreadCount, 1, SystemInfo.processorCount);
			}
		}
		GUI.backgroundColor = current;
		
	}

	private void UI_MainToolbar()
	{
		// Main Toolbar Menun
		using (new GUILayout.HorizontalScope(EditorStyles.helpBox))
		{
			GUILayout.FlexibleSpace();
			UI_MainMenuSelection();
			GUILayout.FlexibleSpace();
		}
	}



	private void UI_Builders_Creation()
	{

		UI_HELPMESSAGE(CascadeHelp.BuilderCreation);
		if (_cacheManualImageButtons == null || _cacheManualImageButtons.Length == 0)
			Cache_RefreshImageButton();

		
		//using (new GUILayout.VerticalScope(InspectorHelper.BoxStyle))
		//{

		int lSelected = -1;
		using (new GUILayout.HorizontalScope(esInspectorHelper.BoxStyle))
		{
			GUILayout.FlexibleSpace();
			lSelected = esInspectorHelper.DrawSelectionGrid(_cacheManualImageButtons, -1, 74);
			GUILayout.FlexibleSpace();

		}
		if (lSelected == 0)
		{
			RiverPath.CreateCascadeBuilder(false);
			RefreshCascadeList();
		}
		if (lSelected == 1)
		{
			RiverPath.CreateCascadeBuilder(true);
			RefreshCascadeList();
		}
			

		//}

	}

	private void UI_ShowGuiSelectedOption()
	{
		switch (_activeMenuOption)
		{
			case CascadeMainMenu.GlobalSetting:
				if (esInspectorHelper.SectionTitleAndHelp("Global Setting", CascadeHelp.GlobalSettingURL))
					Gui_Inspector_GlobalSetting();
				break;
			case CascadeMainMenu.PerNodeSetting:
				if (esInspectorHelper.SectionTitleAndHelp("Per Node Setting", CascadeHelp.PerNodeSettingURL))
					Gui_Inspector_PerNodeSetting();
				break;
			case CascadeMainMenu.SegmentSetting:
				if (esInspectorHelper.SectionTitleAndHelp("Cascade Segment Setting", CascadeHelp.SegmentSettingURL))
					Gui_Inspector_CascadeSegmentSetting();
				break;
			case CascadeMainMenu.RiverBed:
				if (esInspectorHelper.SectionTitleAndHelp("Landscape Setting", CascadeHelp.LandscapeSettingURL))
				{
					Gui_LandscapeConformSetting();
					if (CascadeManager.EnableConform) GUI_Inspector_Landscape();
				}
				
				break;
			case CascadeMainMenu.Lighting:
				if (esInspectorHelper.SectionTitleAndHelp("Light Manager", CascadeHelp.LightManagerURL))
					Gui_Inspector_LightManager();
				break;
			case CascadeMainMenu.ImportExport:
				if (esInspectorHelper.SectionTitleAndHelp("Cascade Import/Export", CascadeHelp.ImportExportURL))
				{
					Gui_Inspector_ImportExport();
					Gui_Inspector_Stamp();
				}
				
				break;
			case CascadeMainMenu.Debug:
				if (esInspectorHelper.SectionTitleAndHelp("General Settings", CascadeHelp.GeneralSettingURL))
				{
					Gui_Inspector_Settings();
				}
				break;
			case CascadeMainMenu.Painter:
				if (esInspectorHelper.SectionTitleAndHelp("Flow Map ", CascadeHelp.FlowMapURL))
					Gui_Inspector_FlowMap();
				break;
		}
	}

	
	private void UI_CascadeBuildersList()
	{
		// Check if something delete in hierarchy
		var sentieriBuilders = GameObject.FindObjectsOfType(typeof(RiverPath));
		
		if ( sentieriBuilders.Length != this._builders.Count)
		{
			RefreshCascadeList();
		}

		
		using (var toogle = new esInspectorUI.DynamicGroup("Cascade Builders",0,0, esInspectorUI.DynamicGroup.HeaderType.Foldout, 0, 30))
		{
			if (toogle.ToggleGroup)
			{
				if (_builders.Count < 1)
				{
					_selectedBuilderIdx = -1;
					esInspectorHelper.ShowWarning("There are no active Builders in the scene.");
				}
				else
				{

					var buildersDelete = -1;
					var buildersDisable = -1;
					using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
					{
						//List<int> idxToRemove = new List<int>();
						for (int i = 0; i < _builders.Count; i++)
						{

							if (_builders[i] == null)
							{
								buildersDelete = i;
								continue;
							}
							// TODO Move to Inspector Helper
							string txtColourName = "Black";
							if (EditorGUIUtility.isProSkin)
							{
								txtColourName = "#ffffffaa";
							}
							else
							{
								txtColourName = "black";
							}


							using (new GUILayout.HorizontalScope(esInspectorHelper.BoxStyle))
							{
								if (GUILayout.Button("Select", GUILayout.MaxWidth(70f)))
								{
									_selectedBuilderIdx = i;

									Selection.activeObject = _builders[i];
								}
								var _isSelectedBuilder = Selection.activeObject == _builders[i];

								//if (_isSelectedBuilder)
								//	_activeBuilderIdx = i;

								_labelText = "<color=" + txtColourName + ">" + ((_isSelectedBuilder) ? "<b>" : "") + _builders[i].GetComponent<RiverPath>().FriendlyName +
											(_isSelectedBuilder ? "</b>" : "") + "</color>";
								EditorGUILayout.LabelField(_labelText, esInspectorHelper.LabelFieldRichText);
								var lColor = GUI.backgroundColor;

								if (GUILayout.Button(new GUIContent("Disable", "Deactivate the selected builder"), GUILayout.MaxWidth(60f)))
								{
									buildersDisable = i;
								}
								GUI.backgroundColor = new Color(0.78f, 0.468f, 0.390f);
								if (GUILayout.Button(new GUIContent("Delete", "Delete the selected builder"), GUILayout.MaxWidth(50f)))
								{
									buildersDelete = i;
								}
								GUI.backgroundColor = lColor;

							}
						}
					}
					if (buildersDelete != -1)
					{
						var todestroy = _builders[buildersDelete];
						_builders.RemoveAt(buildersDelete);
						todestroy.Destroy();
					}
					if (buildersDisable != -1)
					{
						var todestroy = _builders[buildersDisable];
						_builders.RemoveAt(buildersDisable);
						todestroy.SetActive(false);
					}


				}


			}
		}
		// Sync with Inspector
		for (int i = 0; i < _builders.Count; i++)
		{
			if (Selection.activeObject == _builders[i]) _selectedBuilderIdx = i;
		}

		
		if (_builders == null || _builders.Count < 1) _selectedBuilderIdx = -1;

		try
		{
			TargetRiverPath = _selectedBuilderIdx > -1 ? _builders[_selectedBuilderIdx].GetComponent<RiverPath>() : null;

		}
		catch
		{
			RefreshCascadeList();

		}


	}

	private void UI_WindowManagerFooter()
	{
		using (new GUILayout.HorizontalScope(EditorStyles.helpBox))
		{
			Config.ShowHelp = EditorGUILayout.Toggle(GCShowHelp, Config.ShowHelp);
			GUILayout.Label("Cascade Manager " + Config.CascadeVersion);
		}
	}

#if FXUnderwater
	private void UI_UnderWater()
	{

		using (var toogle = new ESInspectorHelper.DynamicGroup("Cascade Undewater", 0, 0, ESInspectorHelper.DynamicGroup.HeaderType.Foldout, 0, 30))
		{
			if (toogle.ToggleGroup)
			{

				using (new EditorGUILayout.VerticalScope(GUI.skin.box))
				{
					CascadeManager.EnableUnderWater = EditorGUILayout.Toggle("Enable Underwater", CascadeManager.EnableUnderWater);

					if (CascadeManager.EnableUnderWater)
					{
						// Check if there is a main camera
						if (Camera.main != null)
						{
							if (CascadeManager. cameraMain == null)
							{
								CascadeManager.cameraMain = Camera.main;
							}
							CascadeManager.cameraMain = (Camera)EditorGUILayout.ObjectField("Main Camera", CascadeManager.cameraMain, typeof(Camera), true);

							if (!CascadeManager.cameraMain.gameObject.GetComponent<CascadeUnderWaterFx>())
							{
								CascadeManager.cameraMain.gameObject.AddComponent<CascadeUnderWaterFx>();
							}



						}
						else
						{
							InspectorHelper.ShowWarning("Cascade cannot find a Camera tagged as MainCamera\nKindly setup one to use Underwater capabilities.");
						}


					}
					

				}

			


			}
		}
	}
#endif

	#endregion

	#region GUI Inspector Block


	private void Gui_Inspector_GlobalSetting()
	{

			if (TargetRiverPathIsNull) return;

	

			if (ClosedSurface)
			{
				Gui_Inspector_NodeSetting_ClosedSurface();
	#if FXUnderwater
				UI_UnderWater();
	#endif
				return;
			}

			EditorGUILayout.Space();
	
			using (var toogle = new esInspectorUI.DynamicGroup("Surface Setting", 0, 0, esInspectorUI.DynamicGroup.HeaderType.Foldout, 0, 30))
			if (toogle.ToggleGroup)
			{
			

				esInspectorHelper.Space(2);
				esInspectorHelper.ShowWarning("Changes to global parameters will overwrite the local ones");
				esInspectorHelper.Space(2);
		
				using (var section = new esInspectorUI.Section("Cascade Surface", esInspectorUI.Section.Type.OpenClose, esInspectorUI.Style.Purple))
				{
					UI_HELPMESSAGE(CascadeHelp.CascadeSurface);

					if (section.ToggleSection)
					{
						Gui_SubBlock_SurfaceParameters();
						Gui_SubBlock_SurfaceGlobalProfile();
						Gui_SubBlock_SurfaceGlobalShader();
					}
				}
			
				using (var section = new esInspectorUI.Section("Cascade Mesh", esInspectorUI.Section.Type.OpenClose, esInspectorUI.Style.Blue))
				{
					UI_HELPMESSAGE(CascadeHelp.CascadeMesh);
					if (section.ToggleSection) Gui_SubBlock_MeshParameters();
				}
				using (var section = new esInspectorUI.Section("Cascade Material", esInspectorUI.Section.Type.OpenClose, esInspectorUI.Style.GreenDark))
				{
					UI_HELPMESSAGE(CascadeHelp.CascadeMaterial);
					if (section.ToggleSection)
						Gui_SubBlock_Material();

				}

			}

		using (var toogle = new esInspectorUI.DynamicGroup("Edge Setting", 0, 0, esInspectorUI.DynamicGroup.HeaderType.Foldout, 0, 30))
			if (toogle.ToggleGroup)
			{
		
				using (var section = new esInspectorUI.Section("Path Edges", esInspectorUI.Section.Type.HeaderBox, esInspectorUI.Style.Orange))
				{
					UI_HELPMESSAGE(CascadeHelp.PathEdges);
					if (section.ToggleSection)
					{


						using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
						{
							EditorGUILayout.LabelField("<b>Left Side</b>", esInspectorHelper.LabelFieldRichText);
							EditorGUI.BeginChangeCheck();
							var rbS = TargetRiverPath.Profile.EdgeNoiseLeft;
							rbS.Scale = EditorGUILayout.Slider(new GUIContent("Scale"), rbS.Scale, 0, 100);
							rbS.NoiseOffsetX = EditorGUILayout.Slider(new GUIContent("Offset"), rbS.NoiseOffsetX, 0, 100);
							rbS.NoiseAmplitude = EditorGUILayout.Slider(new GUIContent("Amplitude"), rbS.NoiseAmplitude, 0.1f, 15);
							//rbS.CutOff = EditorGUILayout.Slider(new GUIContent("Edges Offset"), rbS.CutOff, 0, 200);
							if (EditorGUI.EndChangeCheck())
							{
								TargetRiverPath.Profile.EdgeNoiseLeft = rbS;

								TargetRiverPath.RefreshAll = true;
								_sceneDirty = true;
							}
						}
						using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
						{
							EditorGUILayout.LabelField("<b>Right Side</b>", esInspectorHelper.LabelFieldRichText);
							EditorGUI.BeginChangeCheck();
							var rbS = TargetRiverPath.Profile.EdgeNoiseRight;
							rbS.Scale = EditorGUILayout.Slider(new GUIContent("Scale"), rbS.Scale, 0, 100);
							rbS.NoiseOffsetX = EditorGUILayout.Slider(new GUIContent("Offset"),
								rbS.NoiseOffsetX, 0, 100);
							rbS.NoiseAmplitude = EditorGUILayout.Slider(new GUIContent("Amplitude"), rbS.NoiseAmplitude, 0.1f, 15);
							//rbS.CutOff = EditorGUILayout.Slider(new GUIContent("Edges Offset"), rbS.CutOff, 0, 200);
							if (EditorGUI.EndChangeCheck())
							{
								TargetRiverPath.Profile.EdgeNoiseRight = rbS;
								TargetRiverPath.RefreshAll = true;
								_sceneDirty = true;
							}

						}


					}
				}

			}

		using (var toogle = new esInspectorUI.DynamicGroup("Ground Setting", 0, 0, esInspectorUI.DynamicGroup.HeaderType.Foldout, 0, 30))
			if (toogle.ToggleGroup)
			{
		

				using (var section = new esInspectorUI.Section("Ground Bed", esInspectorUI.Section.Type.HeaderBox, esInspectorUI.Style.Yellow))
				{
					if (section.ToggleSection)
					{
						Gui_SubBlock_DepthParameters();
						Gui_SubBlock_RiverBedProfile();
					}
				}
		

			}

		using (var toogle = new esInspectorUI.DynamicGroup("Ground Digging", 0, 0, esInspectorUI.DynamicGroup.HeaderType.Foldout, 0, 30))
			if (toogle.ToggleGroup)
			{
				UI_HELPMESSAGE(CascadeHelp.GroundDigging);
				using (var section = new esInspectorUI.Section("Terrain Blending", esInspectorUI.Section.Type.HeaderBox, esInspectorUI.Style.Red))
				{
				
					if (section.ToggleSection)
					{

						var lm = TargetRiverPath.LandscapeModifier;

						var rb = TargetRiverPath.Profile.RiverBedSetting;

						EditorGUI.BeginChangeCheck();

						rb.MeshSubdvision = EditorGUILayout.IntSlider("Virtual Mesh Resolution", TargetRiverPath.Profile.RiverBedSetting.MeshSubdvision, 12, 200);
						EditorGUILayout.Separator();
						rb.TerrainHeightOffset = EditorGUILayout.Slider("Global Bank Offset", TargetRiverPath.Profile.RiverBedSetting.TerrainHeightOffset, 0, 2);

						if (EditorGUI.EndChangeCheck())
						{

							TargetRiverPath.Profile.RiverBedSetting = rb;

							_repaint = true;
							_sceneDirty = true;
							return;
						}
						_updateAllNodes = EditorGUILayout.Toggle("Update All Nodes", _updateAllNodes);
						if (_updateAllNodes)
						{
							esInspectorHelper.Space(2);
							esInspectorHelper.ShowWarning("Changes to below parameters will overwrite the local ones");
							esInspectorHelper.Space(2);
						}
						GUILayout.Space(5);


						TargetRiverPath.KeepSize = EditorGUILayout.Toggle("Look Size", TargetRiverPath.KeepSize);
						bool lKeepSize = TargetRiverPath.KeepSize;
						EditorGUI.BeginChangeCheck();
						lm.LeftPadding = EditorGUILayout.Slider("Default Left Padding", lm.LeftPadding, 0, 1);
						if (lKeepSize)
							lm.RightPadding = lm.LeftPadding;

						lm.RightPadding = EditorGUILayout.Slider("Default Right Padding", lm.RightPadding, 0, 1);

						if (lKeepSize)
							lm.LeftPadding = lm.RightPadding;

						EditorGUILayout.Separator();


						lm.LeftSurronding = EditorGUILayout.Slider("Default Left Surrounding", lm.LeftSurronding, 0, MaxSurroundValue);
						if (lKeepSize)
							lm.RightSurronding = lm.LeftSurronding;
						lm.LeftBlendingCurve = EditorGUILayout.CurveField(new GUIContent("Left Profile"), TargetRiverPath.LandscapeModifier.LeftBlendingCurve);

						EditorGUILayout.Separator();

						lm.RightSurronding = EditorGUILayout.Slider("Default Right Surrounding", lm.RightSurronding, 0, MaxSurroundValue);
						lm.RightBlendingCurve = EditorGUILayout.CurveField(new GUIContent("Right Profile"), TargetRiverPath.LandscapeModifier.RightBlendingCurve);
						if (lKeepSize)
							lm.LeftSurronding = lm.RightSurronding;


						// TODO Init in RiverPath
						if (TargetRiverPath.Profile == null) return;




						if (EditorGUI.EndChangeCheck())
						{
							TargetRiverPath.LandscapeModifier = lm;
							TargetRiverPath.Profile.RiverBedSetting = rb;

							if (_updateAllNodes)
								TargetRiverPath.SetMarkersLandscapeModifierDefault();

							_repaint = true;
							_sceneDirty = true;
						}



					}

				}


			}


#if VEGETATION_STUDIO
		using (var toogle = new esInspectorUI.DynamicGroup("Vegetation Studio Setting", 0, 0, esInspectorUI.DynamicGroup.HeaderType.Foldout, 0, 30))
#else
		using (var toogle = new esInspectorUI.DynamicGroup("Landscape Vegetation", 0, 0,
			esInspectorUI.DynamicGroup.HeaderType.Foldout, 0, 30))
#endif
		{
			if (toogle.ToggleGroup)
			{
				UI_HELPMESSAGE(CascadeHelp.LandscapeVegetation);
				Gui_SubBlock_Vegetation();
			}
		}



		//lLabelToogleBox = "Mesh Settings";
		//_showMeshParameter = InspectorHelper.HeaderDynamicGroup(lLabelToogleBox, _showMeshParameter, HeaderType.Foldout, 0, 30);
		//if (_showMeshParameter)
		//{
		//	_showPathParameter = false;
		//	_showRiverBedTool = false;
		//	_showDesignTool = false;
		//	//_showMeshParameter = false;

		//	Gui_SubBlock_MeshParameters();
		//	Gui_SubBlock_Material();

		//}

		using (var toogle = new esInspectorUI.DynamicGroup("Audio Setting", 0, 0, esInspectorUI.DynamicGroup.HeaderType.Foldout, 0, 30))
		if (toogle.ToggleGroup)
		{
				TargetRiverPath.AudioNodeFrequency = EditorGUILayout.IntSlider("Node Frequency", TargetRiverPath.AudioNodeFrequency,1, TargetRiverPath.MarkerCount);
				
				TargetRiverPath.DefaultAudioSetting = AudioSourceGUI.OnGUI(TargetRiverPath.DefaultAudioSetting);

				TargetRiverPath.EnableAudioManager =  EditorGUILayout.Toggle("Update All Nodes", TargetRiverPath.EnableAudioManager);

				using (new EditorGUILayout.HorizontalScope("box"))
				{
					if (GUILayout.Button("Apply to All"))
					{
						TargetRiverPath.CreateAudioManager();

					}
					if (GUILayout.Button("Delete All"))
					{
						TargetRiverPath.AudioManagerDeleteAll();

					}
				}
					


		}
		using (var toogle = new esInspectorUI.DynamicGroup("Design Utilities", 0, 0, esInspectorUI.DynamicGroup.HeaderType.Foldout, 0, 30))
		if (toogle.ToggleGroup)
		{

			Gui_SubBlock_CascadeConnections();
			Gui_SubBlock_DesignTool();

		}

		esInspectorHelper.FlexibleSpace();
		if (GUILayout.Button("Reverse Path Direction", _buttonWidth))
		{
			
			TargetRiverPath.InvertPath();
		}
		

		}
	
	private void Gui_SubBlock_Vegetation_ClosedSurface()
	{

			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{

				if (TargetRiverPath.MarkerCount > 0)
				{

					EditorGUI.BeginChangeCheck();

					TargetRiverPath.VSMaskMainEnable = EditorGUILayout.Toggle("Enable Vegetation Main Mask", TargetRiverPath.VSMaskMainEnable);

				
					if (TargetRiverPath.VSMaskMainEnable)
					{
						TargetRiverPath.VSMaskMainOffset = EditorGUILayout.FloatField("Main Mask Edge Padding", TargetRiverPath.VSMaskMainOffset);
						TargetRiverPath.VSAutoRefresh = EditorGUILayout.Toggle("Enable Auto Refresh", TargetRiverPath.VSAutoRefresh);
						using (new GUILayout.HorizontalScope())
						{
							if (GUILayout.Button("Refresh Mask"))
							{
								TargetRiverPath.Vegetation_RefreshMask();
							}
							esInspectorHelper.FlexibleSpace();
						}
					}

				
				if (EditorGUI.EndChangeCheck())
					{
							TargetRiverPath.Vegetation_RefreshMask();
					}
				}
				else
				{
					esInspectorHelper.ShowWarning("You need at least 1 point to enable vegetaion mask feature.");
				}
			}
		}

	private int _vegetationTypeIndexL;
	private int _vegetationTypeIndexR;

#if VEGETATION_STUDIO
	bool VS(List<AwesomeTechnologies.VegetationTypeSettings> VegetationTypeList, ref int _vegetationTypeIndex)
	{
		var ch = false;
		if (GUILayout.Button("Add vegetation type"))
		{
			AwesomeTechnologies.VegetationTypeSettings newVegetationTypeSettings =
				new AwesomeTechnologies.VegetationTypeSettings();

			VegetationTypeList.Add(newVegetationTypeSettings);
			_vegetationTypeIndex = VegetationTypeList.Count - 1;

		}
		int VegetationTypeListCount = VegetationTypeList.Count;
		string[] packageNameList = new string[VegetationTypeListCount];
		for (int i = 0; i < VegetationTypeListCount; i++)
		{
			packageNameList[i] = (i + 1).ToString() + ". Item";
		}


		if (VegetationTypeList.Count > 0)
		{
			if (_vegetationTypeIndex > VegetationTypeList.Count - 1)
				_vegetationTypeIndex =VegetationTypeList.Count - 1;
			_vegetationTypeIndex = EditorGUILayout.Popup("Selected item", _vegetationTypeIndex, packageNameList);

			EditorGUI.BeginChangeCheck();
			GUILayout.BeginVertical("box");

			VegetationTypeList[_vegetationTypeIndex].Index = (AwesomeTechnologies.VegetationTypeIndex)EditorGUILayout.EnumPopup("Vegetation type",
			VegetationTypeList[_vegetationTypeIndex].Index);
			VegetationTypeList[_vegetationTypeIndex].Density = EditorGUILayout.Slider("Density", VegetationTypeList[_vegetationTypeIndex].Density, 0f, 1f);
			VegetationTypeList[_vegetationTypeIndex].Size = EditorGUILayout.Slider("Size", VegetationTypeList[_vegetationTypeIndex].Size, 0f, 2f);

			if (GUILayout.Button("Delete selected item"))
			{
			VegetationTypeList.RemoveAt(_vegetationTypeIndex);
			}

			GUILayout.EndVertical();

			if (EditorGUI.EndChangeCheck())
			{
				SetSceneDirty();
				ch = true;
			}
		}
		return ch;
	}
#endif

		private void Gui_SubBlock_Vegetation()
		{
			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{


#if VEGETATION_STUDIO
			if(TargetRiverPath.MarkerCount<2)
				{
					esInspectorHelper.ShowWarning("You need at least 2 points to enable vegetaion mask feature.");
				}
				else
				{
					bool lRefreshMasks = false;
			  
					EditorGUI.BeginChangeCheck();
					var origFontStyle = EditorStyles.label.fontStyle;
					EditorStyles.label.fontStyle = FontStyle.Bold;
					TargetRiverPath.VSAutoRefresh = EditorGUILayout.Toggle("Enable Auto Refresh", TargetRiverPath.VSAutoRefresh, EditorStyles.toggle);
					EditorStyles.label.fontStyle = origFontStyle;
					using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
					{
						using (new GUILayout.HorizontalScope())
						{

							TargetRiverPath.VSMaskMainEnable = EditorGUILayout.Toggle("Enable Vegetation Main Mask", TargetRiverPath.VSMaskMainEnable);

							if (TargetRiverPath.VSMaskMainEnable)
							{
								esInspectorHelper.FlexibleSpace();
								if (GUILayout.Button("Refresh Masks")) lRefreshMasks = true;

							}
						}

						if (TargetRiverPath.VSMaskMainEnable)
						{
							using (new GUILayout.VerticalScope(esInspectorHelper.backGroundB))
							{
								TargetRiverPath.VsMaskModeMain = (IntegrationScript.VegetationStudio.VSMaskMode)EditorGUILayout.EnumPopup("Mask Mode", TargetRiverPath.VsMaskModeMain);
								TargetRiverPath.VSMaskMainOffset = EditorGUILayout.FloatField("Mask Edge Padding", TargetRiverPath.VSMaskMainOffset);
								TargetRiverPath.VS_dp_coefficient = EditorGUILayout.Slider("Mask Tollerance", TargetRiverPath.VS_dp_coefficient, 2, 0);

							}
						}
					}


					using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
					{
						using (new GUILayout.HorizontalScope())
						{
							TargetRiverPath.VSMaskLEnable = EditorGUILayout.Toggle("Enable Vegetation Left Edge Mask", TargetRiverPath.VSMaskLEnable);
							if (TargetRiverPath.VSMaskLEnable)
							{
								esInspectorHelper.FlexibleSpace();
								if (GUILayout.Button("Refresh Masks")) lRefreshMasks = true;
							}
						}

						if (TargetRiverPath.VSMaskLEnable)
						{

							using (new GUILayout.VerticalScope(esInspectorHelper.backGround1))
							{
								
							
								TargetRiverPath.VsMaskModeL = (IntegrationScript.VegetationStudio.VSMaskMode)EditorGUILayout.EnumPopup("Mask Mode", TargetRiverPath.VsMaskModeL);
								TargetRiverPath.VSMaskLOffset = EditorGUILayout.FloatField("Mask Edge Padding", TargetRiverPath.VSMaskLOffset);
								TargetRiverPath.VS_dp_coefficientL = EditorGUILayout.Slider("Mask Tollerance", TargetRiverPath.VS_dp_coefficientL, 2, 0);
								TargetRiverPath.VS_WidthL = EditorGUILayout.Slider("Width", TargetRiverPath.VS_WidthL, 0.1f, 50);



								lRefreshMasks = VS(TargetRiverPath.VegetationTypeListL, ref _vegetationTypeIndexL);
								if (lRefreshMasks)
								{
									esInspectorHelper.SetObjectDirty(TargetRiverPath.Profile, false);
									esInspectorHelper.SetObjectDirty(TargetRiverPath, false);
								}

							}
						}
					}
					using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
					{
						using (new GUILayout.HorizontalScope())
						{
							TargetRiverPath.VSMaskREnable = EditorGUILayout.Toggle("Enable Vegetation Right Edge Mask", TargetRiverPath.VSMaskREnable);
							if (TargetRiverPath.VSMaskREnable)
							{
								esInspectorHelper.FlexibleSpace();
								if (GUILayout.Button("Refresh Masks")) lRefreshMasks = true;
							}
						}

						if (TargetRiverPath.VSMaskREnable)
						{

							using (new GUILayout.VerticalScope(esInspectorHelper.backGround2))
							{
								TargetRiverPath.VsMaskModeR = (IntegrationScript.VegetationStudio.VSMaskMode)EditorGUILayout.EnumPopup("Mask Mode", TargetRiverPath.VsMaskModeR);
								TargetRiverPath.VSMaskROffset = EditorGUILayout.FloatField("Mask Edge Padding", TargetRiverPath.VSMaskROffset);
								TargetRiverPath.VS_dp_coefficientR = EditorGUILayout.Slider("Mask Tollerance", TargetRiverPath.VS_dp_coefficientR, 2, 0);
								TargetRiverPath.VS_WidthR = EditorGUILayout.Slider("Width", TargetRiverPath.VS_WidthR, 0.1f, 50);



								lRefreshMasks = VS(TargetRiverPath.VegetationTypeListR, ref _vegetationTypeIndexR);
								if (lRefreshMasks)
								{
									esInspectorHelper.SetObjectDirty(TargetRiverPath.Profile, false);
									esInspectorHelper.SetObjectDirty(TargetRiverPath, false);
								}

						}
					}
				}

			if (EditorGUI.EndChangeCheck())
					{
						lRefreshMasks = true;
						SetSceneDirty();
					}


					if (lRefreshMasks) TargetRiverPath.Vegetation_RefreshMask();




				}
#endif

			}
		}

		private void Gui_Inspector_NodeSetting_ClosedSurface()
		{
			if (TargetRiverPathIsNull) return;
			//string lLabelToogleBox = "Cascade Mesh Parameters";
			//_showMeshParameter = InspectorHelper.HeaderDynamicGroup(lLabelToogleBox, _showMeshParameter, HeaderType.Foldout, 0, 30);
			//if (_showMeshParameter)
			//{
				Gui_SubBlock_MeshParameters();
			//}
			//lLabelToogleBox = "Cascade Material Parameters";
			//_showMaterial = InspectorHelper.HeaderDynamicGroup(lLabelToogleBox, _showMaterial, HeaderType.Foldout, 0, 30);
			//if (_showMaterial)
			//{
		
		
#if VEGETATION_STUDIO
			using (var toogle = new esInspectorUI.DynamicGroup("Vegetation Studio Setting", 0, 0, esInspectorUI.DynamicGroup.HeaderType.Foldout, 0, 30))
				if (toogle.ToggleGroup)
				{
					Gui_SubBlock_Vegetation_ClosedSurface();
				}
#endif


			//}
		}

		private void Gui_Inspector_PerNodeSetting()
		{
			if (TargetRiverPathIsNull) return;

			using (new GUILayout.VerticalScope("box"))
			{
				Gui_SubBlock_MoveMarkerIndex(true);
			}

			Gui_SubBlock_PerNodeSetting();
		
		}

		private void Gui_Inspector_CascadeSegmentSetting()
		{
			if (TargetRiverPathIsNull) return;
			EditorGUILayout.Space();
	
			if (_cascadeList == null) InstanciateCascadesReorderableLis();

		
			_cascadeList.DoLayoutList();
			if (_cascadeList.index >= 0)
			{
				GUILayout.Space(5f);
				GUILayout.BeginVertical(esInspectorHelper.BoxStyle);
				if (UI_DetailCascade(TargetRiverPath.Cascades[_cascadeList.index]))
				{
					_sceneDirty = true;
					_repaint = true;
				}

				GUILayout.EndVertical();
			}
		}

		private bool _showProbesSetting;
		private void Gui_Inspector_LightManager()
		{
			if (TargetRiverPathIsNull) return;
			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				TargetRiverPath.ReflectionProbeDistance = EditorGUILayout.Slider("Probes Distance", TargetRiverPath.ReflectionProbeDistance, 10f, 400f);
				TargetRiverPath.ReflectionProbeHeight = EditorGUILayout.Slider("Probes Water Surface Height", TargetRiverPath.ReflectionProbeHeight, 0f, 50f);
				using (var toogle = new esInspectorUI.DynamicGroup("Cascade Reflection Probes", 0, 0, esInspectorUI.DynamicGroup.HeaderType.Foldout, 0, 30))
				{
					if (toogle.ToggleGroup)
					{

						if (TargetRiverPath.ReflectionProbes.Count == 0)
						{
							esInspectorHelper.ShowMessage("There aren't Reflection Probes");
						}
						else
						{
							var buildersDelete = -1;
							using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
							{
								//List<int> idxToRemove = new List<int>();
								for (int i = 0; i < TargetRiverPath.ReflectionProbes.Count; i++)
								{

									if (TargetRiverPath.ReflectionProbes[i] == null)
									{
										//idxToRemove.Add(i);
										continue;
									}

									using (new GUILayout.HorizontalScope(esInspectorHelper.BoxStyle))
									{
										EditorGUILayout.LabelField(TargetRiverPath.ReflectionProbes[i].name);

										if (GUILayout.Button("Setting", GUILayout.MaxWidth(70f)))
										{
											EditorUtility.DisplayDialog("Cascade Message",
												"Function is limited to single Cascade Builder.\nIt will be available in next updates", "Ok");
										}
										if (GUILayout.Button("Remove", GUILayout.MaxWidth(70f)))
										{
											buildersDelete = i;
										}
										GUILayout.FlexibleSpace();
									}
								}
							}
							if (buildersDelete != -1)
							{
								var toDestroy = TargetRiverPath.ReflectionProbes[buildersDelete].gameObject;
								TargetRiverPath.ReflectionProbes.RemoveAt(buildersDelete);
								toDestroy.Destroy();
							}
						}

					}
				}
				if (GUILayout.Button("Create Probes"))
				{
					TargetRiverPath.CreateReflectionProbes();
				}
				if (GUILayout.Button("Remove All"))
				{

					foreach (var p in TargetRiverPath.ReflectionProbes)
					{
						var toDestroy = p.gameObject;
						toDestroy.Destroy();
					}
					TargetRiverPath.ReflectionProbes.Clear();
				}

			}

		}

		private void Gui_Inspector_FlowMap()
		{
			if (TargetRiverPathIsNull) return;

			

#if MESH_PAINTER


			if (TargetRiverPath.EnableMeshPainter)
			{
				if (GUILayout.Button("Stop Paint"))
				{
					TargetRiverPath.FlowMapPainter(false);

				}
			}
			else
			{
				if (GUILayout.Button("Start Paint"))
				{
					TargetRiverPath.FlowMapPainter(true);
				}
			}
			using (new GUILayout.VerticalScope(InspectorHelper.BoxStyle))
			{
				if (TargetRiverPath.MeshPainter!=null) TargetRiverPath.MeshPainter.Inspector_Draw_UI();
			}
#else
			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				esInspectorHelper.ShowWarning("Flowmap Setting is will be delived in next updates.");
			}
#endif

		}

		private void Gui_Inspector_Stamp()
		{
			if (TargetRiverPathIsNull) return;
			using (new GUILayout.HorizontalScope(esInspectorHelper.BoxStyle))
			{
				stampPrefab = EditorGUILayout.Toggle("Save Prefab", stampPrefab);
				//copyTexturesIntoExportFolder = EditorGUILayout.Toggle("Clone Textures", copyTexturesIntoExportFolder);
				//exportCollider = EditorGUILayout.Toggle("Export Collider", exportCollider);
				esInspectorHelper.FlexibleSpace();
				if (GUILayout.Button("Stamp Cascade"))
				{
					CreatePrefabCascadePath();
				}
			}
	
		}
		private void Gui_Inspector_Settings()
		{

			//using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			//{
			//	CascadeManager.EnableRealTimeScanCell = EditorGUILayout.Toggle("Enable Realtime Scan", CascadeManager.EnableRealTimeScanCell);
			//	var newCellSize = (PowerOfTwo)EditorGUILayout.EnumPopup("Scan Cell Size", CascadeManager.CellSize);
			//	if (newCellSize != CascadeManager.CellSize)
			//	{
			//		CascadeManager.CellSize = newCellSize;
			//		CascadeManager.QueueRefreshEsLandscapeManager();
			//		SetSceneDirty();

			//	}
			//}
		
			if(ManagerIsPresent && CascadeManager.EnableConform)
			{
				using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
				{
					CascadeManager.ShowTerrainBoundary = EditorGUILayout.Toggle(new GUIContent("Show Landscape Terrain Boundaries", "Draw Gizmos for each terrain tile"), CascadeManager.ShowTerrainBoundary);
					//CascadeManager.ShowCascadeBuilderCells = EditorGUILayout.Toggle("Show Bulder", CascadeManager.ShowCascadeBuilderCells);
					//CascadeManager.ShowCellGrid = EditorGUILayout.Toggle(new GUIContent("Show Cell Grid", "Draw Gizmos for each cell"), CascadeManager.ShowCellGrid);
				}
			}
			else if(TargetRiverPath == null)
			{
				esInspectorHelper.ShowMessage("You should select an active Cascade Builder.", true);
			}
			
			if (TargetRiverPath == null) return;
			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				//EditorGUI.BeginChangeCheck();
				//TargetRiverPath.DesignModeIsOn = EditorGUILayout.Toggle("Enable Design View", TargetRiverPath.DesignModeIsOn);
				//if (EditorGUI.EndChangeCheck())
				//{

				//	TargetRiverPath.SetDesignMode();
				//	_repaint = true;
				//	_sceneDirty = true;
				//}
			TargetRiverPath.StampResolution =
				EditorGUILayout.Slider("Stamp Points Per Unit", TargetRiverPath.StampResolution, 0.5f, 2f);
		}
			
		


	

	}
		private void Gui_Inspector_ImportExport()
			{

				using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
				{

					Gui_SubBlockImportMarker();

					Gui_SubBlockExportMarker();

				}
			}



	#endregion

	#region GUI Landascape Manager 

	private int _selectedTileId = -1;
	private bool ManagerIsPresent = false;


	bool _showStichAdvanced;
	/// <summary>
	/// 
	/// </summary>
	private void GUI_Inspector_Landscape()
	{
		if (!ManagerIsPresent) return;

		var terrainManager = esLandscapeManager.Instance;
		if(terrainManager == null) return;
		if (terrainManager.Landscape.esTerrains == null) return;// CascadeManager.QueueRefreshEsLandscapeManager();

		using (var group = new esInspectorUI.DynamicGroup("Conform Setting", 0, 9))
		{

			if (group.ToggleGroup)
			{


				using (new EditorGUILayout.VerticalScope(esInspectorHelper.BoxStyle))
				{
					EditorGUI.BeginChangeCheck();

					CascadeManager.EnableStitch = EditorGUILayout.Toggle("Enable Automatic Stitch", CascadeManager.EnableStitch);
					
					if (CascadeManager.EnableStitch)
					{
						using (new EditorGUILayout.VerticalScope(esInspectorHelper.BoxStyle))
						{
							_showStichAdvanced = EditorGUILayout.Toggle("Show Advaced Settings", _showStichAdvanced);
							if (_showStichAdvanced)
							{
								CascadeManager.TileEdgeWidth = EditorGUILayout.Slider("Tile Edge (%): " + (CascadeManager.TileEdgeWidth * 100).ToString("f0"), CascadeManager.TileEdgeWidth, 0.01f, 0.5f);
								CascadeManager.BlendPower = EditorGUILayout.Slider("Blend Power :" + (CascadeManager.BlendPower * 100).ToString("f0"), CascadeManager.BlendPower, 0.01f, 0.5f);
								using (new GUILayout.HorizontalScope())
								{
									if (GUILayout.Button("Execute Stitch"))
									{
										CascadeManager.QueueStitchLandscapeTiles(CascadeManager.BlendPower, CascadeManager.TileEdgeWidth);
									}
									GUILayout.FlexibleSpace();
								}
							}
						}
					}


					if(EditorGUI.EndChangeCheck())
					{
						SetSceneDirty();
					}

				}
			}

		}

		using (var group = new esInspectorUI.DynamicGroup("Landscape Manager",0,9))
		{
			if (group.ToggleGroup /*&& TargetRiverPath != null*/)
			{
				//using (new EditorGUILayout.VerticalScope(esInspectorHelper.BoxStyle))
				//{
				//	var autoLoadTerrains = EditorGUILayout.Toggle("Auto Load Terrains", esLandscapeManager.AutoLoadTerrains);
				//	if (autoLoadTerrains != esLandscapeManager.AutoLoadTerrains)
				//	{
				//		esLandscapeManager.AutoLoadTerrains = autoLoadTerrains;
				//		if (autoLoadTerrains)
				//		{
				//			CascadeManager.QueueRefreshEsLandscapeManager();
				//		}

				//	}
				//	if (!autoLoadTerrains)
				//	{
				//		var newTerrain = EditorGUILayout.ObjectField("Add Terrain", null, typeof(Terrain), true) as Terrain;

				//		if (newTerrain != null)
				//		{
				//			terrainManager.TerrainManualAdd(newTerrain, (int)CascadeManager.CellSize);

				//		}

				//	}
				//}

			
				using (new EditorGUILayout.VerticalScope(esInspectorHelper.BoxStyle))
				{
					esInspectorHelper.ShowMessage("Terrain Details provides you with useful information about your landscape.", Config.ShowHelp);
					_selectedTileId = CascadeUI.UI_LandscapeDataSource(_selectedTileId);

					CascadeUI.UI_LandscapeTileDetails(_selectedTileId);


					if (GUILayout.Button("Refresh Terrains from Scene"))
					{
						CascadeManager.QueueRefreshEsLandscapeManager();
					}
				}

			}

		}

		

	}
	


	#endregion

	#region GUI Sub Block

	private void Gui_SubBlock_CascadeConnections()
		{
		
		}

		private void Gui_SubBlock_DesignTool()
		{

			//GUILayout.Label("<b>Mesh Terrain</b>", InspectorHelper.LabelFieldRichText);
			//using (new GUILayout.VerticalScope(InspectorHelper.BoxStyle))
			//{
			//	TargetRiverPath.MeshMode = EditorGUILayout.Toggle("Mesh Mode", TargetRiverPath.MeshMode);
			//	if (TargetRiverPath.MeshMode)
			//	{
			//		TargetRiverPath.Meshlayers = EditorGUILayout.LayerField("Layer ", TargetRiverPath.Meshlayers);
			//	}
	
			//}
			//TargetRiverPath.DrawGizmos = EditorGUILayout.Toggle("Show Gizmos", TargetRiverPath.DrawGizmos);
			//TargetRiverPath.HideSurfaces = EditorGUILayout.Toggle("Hide Water Surface", TargetRiverPath.HideSurfaces);
			//EditorGUILayout.Space();
			EditorGUI.BeginChangeCheck();
			TargetRiverPath.DebugFlowMode = EditorGUILayout.Toggle("View Flow", TargetRiverPath.DebugFlowMode);
			if (EditorGUI.EndChangeCheck())
			{
				TargetRiverPath.SetMaterial(TargetRiverPath.DebugFlowMode
					? new Material(Shader.Find("Cascade/ViewFlowMap"))
					: TargetRiverPath.RiverMaterial);
			}
			EditorGUILayout.Space();
			//using (new GUILayout.VerticalScope(InspectorHelper.BoxStyle))
			//{
			//	TargetRiverPath.DrawSpline = EditorGUILayout.Toggle("Draw Spline", TargetRiverPath.DrawSpline);
			//}

			GUILayout.Label("<b>Steepness Control</b>", esInspectorHelper.LabelFieldRichText);
			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				//TargetRiverPath.EnableDepthConstrain = EditorGUILayout.Toggle("Force Depth On New Point", TargetRiverPath.EnableDepthConstrain);
				using (new GUILayout.HorizontalScope())
				{
					var min = TargetRiverPath.slopeRange.minValue;
					var max = TargetRiverPath.slopeRange.maxValue;
					EditorGUILayout.LabelField("Slope Path Range", GUILayout.MaxWidth(146));
					min = EditorGUILayout.FloatField(min, GUILayout.MaxWidth(30));
					EditorGUILayout.MinMaxSlider(ref min, ref max, 0, 1);
					max = EditorGUILayout.FloatField(max, GUILayout.MaxWidth(30));
					TargetRiverPath.slopeRange = new ESMinMaxRange(min, max);
				}
				using (new GUILayout.HorizontalScope())
				{
					esInspectorHelper.FlexibleSpace(2);
					if (GUILayout.Button("Auto Slope"))
					{
						TargetRiverPath.AutoSlope();
					}
				}
			
			}
		


		}

	
		private void Gui_SubBlock_Material()
		{
			
			using (new EditorGUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				var currentColor = GUI.backgroundColor;
				GUI.backgroundColor = esInspectorHelper.ColorFieldBackgroundAdd;
				CascadeProfile profileToLoad = EditorGUILayout.ObjectField("Load Cascade Material Preset", null, typeof(CascadeProfile), true) as CascadeProfile;
				GUI.backgroundColor = currentColor;
				if (profileToLoad != null)
				{
					TargetRiverPath.RiverMaterial = profileToLoad.DefaultMaterial;
					TargetRiverPath.SetMaterial(TargetRiverPath.RiverMaterial);
			  
				}
		
			}
			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{

				//var cascade = TargetRiverPath.ShaderSetting;
				//if (InspectorHelper.TextureBox("Foam", ref cascade.Texture2D))
				//{

				//	TargetRiverPath.ShaderSetting = cascade;
				//	var material = TargetRiverPath.RiverMaterial;
				//	material.SetTexture("_Water", cascade.Texture2D);

				//}

				EditorGUI.BeginChangeCheck();
				TargetRiverPath.RiverMaterial =
					EditorGUILayout.ObjectField("Cascade Material", TargetRiverPath.RiverMaterial, typeof(Material), false) as Material;

				if (EditorGUI.EndChangeCheck())
				{
					TargetRiverPath.SetMaterial(TargetRiverPath.RiverMaterial);
					_repaint = true;

				}

				using (new GUILayout.HorizontalScope())
				{

					if (GUILayout.Button(new GUIContent("New","Create new shader and apply to selected Cascade"), _buttonWidth, GUILayout.MaxWidth(80f)))
					{
						TargetRiverPath.CreateCascadeShader();
						 EditorGUIUtility.PingObject(TargetRiverPath.RiverMaterial);
					}
				   

					if (GUILayout.Button(new GUIContent("Clone", "Clone the existing shader and apply to selected Cascade"), _buttonWidth, GUILayout.MaxWidth(80f)))
					{
						Material riverMat = TargetRiverPath.RiverMaterial;
					

						string path = AssetDatabase.GetAssetPath(riverMat);
						string newPath = AssetDatabase.GenerateUniqueAssetPath(path);
						AssetDatabase.CopyAsset(path, newPath);
						AssetDatabase.Refresh();
						TargetRiverPath.RiverMaterial = (Material)AssetDatabase.LoadAssetAtPath(newPath, typeof(Material));
						TargetRiverPath.SetMaterial(TargetRiverPath.RiverMaterial);
						_repaint = true;
						EditorGUIUtility.PingObject(TargetRiverPath.RiverMaterial);

					}
					GUILayout.FlexibleSpace();

				}

			}
		
		}

		private void Gui_SubBlock_SurfaceGlobalProfile()
		{
			esInspectorHelper.ShowMessage("You Can Define a 2D Profile for the water surface", Config.ShowHelp);
			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				if (TargetRiverPath.Profile.RiverSetting.Shape2DProfile == null)
					TargetRiverPath.Profile.RiverSetting.Shape2DProfile = MeshPrimitives.ProfileFlat();

				using (new GUILayout.HorizontalScope())
				{
					esInspectorHelper.FlexibleSpace();
					if (GUILayout.Button(GCApplyAll))
					{
						TargetRiverPath.SetMarkersDefaultProfile2DMeshCurves();
						TargetRiverPath.RefreshAll = true;
						_sceneDirty = true;
					}
				}
				var rs = TargetRiverPath.Profile.RiverSetting;
				EditorGUI.BeginChangeCheck();
				rs.TerrainHeightOffset = EditorGUILayout.Slider("Global Surface Offset", rs.TerrainHeightOffset, 0, 4);
				if (EditorGUI.EndChangeCheck())
				{
					TargetRiverPath.Profile.RiverSetting = rs;
					_sceneDirty = true;
				}
			


				//EditorGUI.BeginChangeCheck();
				var label = "Surface Profile";
				EditorGUILayout.CurveField(new GUIContent(label), TargetRiverPath.Profile.RiverSetting.Shape2DProfile, GUILayout.Height(75));
				//if (EditorGUI.EndChangeCheck())
				//{
				//	_sceneDirty = true;
				//}

			
			}

			EditorGUI.BeginChangeCheck();
			float noiseScale;
			float noiseAmplitude;
			EditorGUILayout.LabelField("<b>Surface Bumbiness</b>", esInspectorHelper.LabelFieldRichText);
			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				noiseScale = EditorGUILayout.Slider(new GUIContent("Scale"), TargetRiverPath.Profile.RiverSetting.NoiseSettings.Scale, 0, 40);
				noiseAmplitude = EditorGUILayout.Slider(new GUIContent("Strength"),
					TargetRiverPath.Profile.RiverSetting.NoiseSettings.NoiseAmplitude, 0, 20);
			}
			if (EditorGUI.EndChangeCheck())
			{

				if (noiseScale != TargetRiverPath.Profile.RiverSetting.NoiseSettings.Scale)
				{
					var noise = TargetRiverPath.Profile.RiverSetting.NoiseSettings;
					noise.Scale = noiseScale;
					TargetRiverPath.Profile.RiverSetting.NoiseSettings = noise;
				}
				if (noiseAmplitude != TargetRiverPath.Profile.RiverSetting.NoiseSettings.NoiseAmplitude)
				{
					var noise = TargetRiverPath.Profile.RiverSetting.NoiseSettings;
					noise.NoiseAmplitude = noiseAmplitude;
					TargetRiverPath.Profile.RiverSetting.NoiseSettings = noise;
				}

				_repaint = true;
				_sceneDirty = true;
				TargetRiverPath.RefreshAll = true;
			}
		}

		private void Gui_SubBlock_SurfaceGlobalShader()
		{
			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				var nodeSetting = TargetRiverPath.CascadeNodeSetting;
				EditorGUI.BeginChangeCheck();
				nodeSetting.Foam = EditorGUILayout.Slider("Foam Amplification", nodeSetting.Foam, -1, 1);
				nodeSetting.Swirl = EditorGUILayout.Slider("Swirl Intensity", nodeSetting.Swirl, 0, 1);
				nodeSetting.Turbolence = EditorGUILayout.Slider("Turbolence Intensity", nodeSetting.Turbolence, 0, 1);
				nodeSetting.FlotSam = EditorGUILayout.Slider("Flotsam Intensity", nodeSetting.FlotSam, 0, 1);
				if (EditorGUI.EndChangeCheck())
				{
					TargetRiverPath.CascadeNodeSetting = nodeSetting;
					TargetRiverPath.SetMarkersCascadeNodeSetting(nodeSetting);
					_sceneDirty = true;
				}
			}
		}
		private enum SelectedConformType
		{
			Selected,
			All
		}

		private Vector2 TexscrollPos;
	
		private int _selTexture;

		[SerializeField]
		private SelectedConformType _op = SelectedConformType.Selected;
		public void Gui_LandscapeConformSetting()
		{
			if (!ManagerIsPresent) return;
			using (new EditorGUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				esInspectorHelper.ShowMessage("Conform Terrain Help", Config.ShowHelp);
				EditorGUI.BeginChangeCheck();
				CascadeManager.EnableConform = EditorGUILayout.Toggle("Enable Terrain Conform", CascadeManager.EnableConform);
				if (EditorGUI.EndChangeCheck())
				{
				
					if (CascadeManager.EnableConform)
					{
						CascadeManager.QueueRefreshEsLandscapeManager();
					}
					SetSceneDirty();
					SceneView.RepaintAll();
				}
				if (CascadeManager.EnableConform)
				{
					using (new EditorGUILayout.HorizontalScope())
					{
						_op = (SelectedConformType)EditorGUILayout.EnumPopup("Path to Conform", _op);
						if (GUILayout.Button("Conform Landscape"))
						{
							if (_op == SelectedConformType.Selected)
							{
								CascadeManager.QueueStampSelected(TargetRiverPath.Id);
							}
							else
							{
								//EditorUtility.DisplayDialog("Cascade Message",
								//	"Function is limited to single Cascade Builder.\nIt will be available in next updates", "Ok");
								CascadeManager.QueueStampAll();
							}
						}
					}
					if (GUILayout.Button("Restore Landscape"))
					{
						CascadeManager.QueueUndoAll();
						TargetRiverPath.Conformed = false;
					}
				}
			}
			//if (TargetRiverPath == null)
			//{
			//	using (new EditorGUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			//	{

			//		if (GUILayout.Button("Restore Landscape"))
			//		{
			//			CascadeManager.QueueUndoAll();
			//		}
			//	}
			//	return;
			//}


			//EditorGUI.BeginChangeCheck();
			//TargetRiverPath.RealTimeTerrain = EditorGUILayout.Toggle("Real Time", TargetRiverPath.RealTimeTerrain);
			//if (EditorGUI.EndChangeCheck())
			//{
			//	_repaint = true;
			//	_sceneDirty = true;
			//}


			//const string lLabelActive = "(A)";//"( Active )";

			//var lLabelToogleBox = "Terrain " + (path.LandscapeModifier.ConformTerrain ? lLabelActive : "");
			//_conformTerrainOptions[0] = lLabelToogleBox;
			//lLabelToogleBox = "Grass " + (path.LandscapeModifier.RemoveGrass ? lLabelActive : "");
			//_conformTerrainOptions[1] = lLabelToogleBox;
			//lLabelToogleBox = "Tree " + (path.LandscapeModifier.RemoveTrees ? lLabelActive : "");
			//_conformTerrainOptions[2] = lLabelToogleBox;
			//lLabelToogleBox = "Texture " + (path.LandscapeModifier.ReplaceTexture ? lLabelActive : "");
			//_conformTerrainOptions[3] = lLabelToogleBox;

			//_conformOption = (LandscapeConformOptions)GUILayout.Toolbar((int)_conformOption, _conformTerrainOptions);

			//float lMax = 0;
			//float rMax = 1;


			//switch (_conformOption)
			//{
			//	case LandscapeConformOptions.Terrain:
		}

		private void Gui_SubBlock_Profile2DSinglePoint(int idx)
		{
			esInspectorHelper.ShowMessage("You Can Define a 2D Profile for the water surface specifice for a Node", Config.ShowHelp);
			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{

				EditorGUI.BeginChangeCheck();
				var label = "Node Surface Profile";
				// TODO Remove in rc1. 
				if (TargetRiverPath.Spline.MarkerSet[idx].Shape2DProfile == null)
					TargetRiverPath.Spline.MarkerSet[idx].Shape2DProfile = esAnimationCurveUtility.CreateCopy(TargetRiverPath.Profile.RiverSetting.Shape2DProfile);
				EditorGUILayout.CurveField(new GUIContent(label), TargetRiverPath.Spline.MarkerSet[idx].Shape2DProfile, GUILayout.Height(75));
				if (EditorGUI.EndChangeCheck())
				{
		
					_sceneDirty = true;
				}
			}
		}
	
		private void Gui_SubBlock_PerNodeSetting()
		{
			//Gui_SubBlock_CapSetting();
			var selectedIdx = TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint;
		
			if (selectedIdx != -1)
			{

				using (var toogle = new esInspectorUI.DynamicGroup("Surface Setting", 0, 0, esInspectorUI.DynamicGroup.HeaderType.Foldout, 0, 30))
				{
					if (toogle.ToggleGroup)
					{
			
						using (new EditorGUILayout.HorizontalScope())
						{
							var newPosition = EditorGUILayout.Vector3Field("Selected Path Point : " + selectedIdx, TargetRiverPath.MarkerSet[selectedIdx].Position);
							if (newPosition != TargetRiverPath.MarkerSet[selectedIdx].Position)
							{
								TargetRiverPath.PointMove(selectedIdx, newPosition);
								_sceneDirty = true;
								_repaint = true;
							}
						}

						using (new EditorGUILayout.HorizontalScope())
						{
							var newWidth = EditorGUILayout.FloatField("River Marker Width", TargetRiverPath.PathSelectedWidth(selectedIdx));
							if (newWidth != TargetRiverPath.PathSelectedWidth(selectedIdx))
							{
								TargetRiverPath.SetMarkerPathWidth(selectedIdx, newWidth);
								_repaint = true;
								_sceneDirty = true;
							}
							if (GUILayout.Button(GCApplyAll, _buttonWidth))
							{
								TargetRiverPath.PathDefaultWidth = newWidth;
								TargetRiverPath.SetMarkersPathWidth();
								_repaint = true;
								_sceneDirty = true;
							}
						}
						Gui_SubBlock_Profile2DSinglePoint(selectedIdx);

						using (new EditorGUILayout.VerticalScope())
						{
							var nodeSetting = TargetRiverPath.MarkerSet[selectedIdx].CascadeNodeSetting;

							EditorGUI.BeginChangeCheck();
							nodeSetting.Foam = EditorGUILayout.Slider("Foam Amplification", nodeSetting.Foam, -1, 1);
							nodeSetting.Swirl = EditorGUILayout.Slider("Swirl Intensity", nodeSetting.Swirl, 0, 1);
							nodeSetting.Turbolence = EditorGUILayout.Slider("Turbolence Intensity", nodeSetting.Turbolence, 0, 1);
							nodeSetting.FlotSam = EditorGUILayout.Slider("Flotsam Intensity", nodeSetting.FlotSam, 0, 1);

							if (EditorGUI.EndChangeCheck())
							{

								//if (TargetRiverPath.PointSelection == SegmentSelection.Selected)
								//{
								TargetRiverPath.SetMarkerCascadeNodeSetting(nodeSetting, selectedIdx);
								//}

								//else
								//{
								//	TargetRiverPath.SetMarkersCascadeNodeSetting(nodeSetting);
								//}

								_sceneDirty = true;
							}
						}


					}
				}


				using (var toogle = new esInspectorUI.DynamicGroup("Ground Setting", 0, 0, esInspectorUI.DynamicGroup.HeaderType.Foldout, 0, 30))
				{ 
					if (toogle.ToggleGroup)
					{

					

						using (new EditorGUILayout.HorizontalScope())
						{
							var newDepth = EditorGUILayout.FloatField("Node Depth", TargetRiverPath.PathSelectedDepth(selectedIdx));
							if (newDepth != TargetRiverPath.PathSelectedDepth(selectedIdx))
							{
								TargetRiverPath.SetMarkerRiverDepth(newDepth, selectedIdx);
								_repaint = true;
								_sceneDirty = true;
							}
							if (GUILayout.Button(GCApplyAll, _buttonWidth))
							{

								TargetRiverPath.RiverDefaultDepth = newDepth;
								TargetRiverPath.SetMarkersRiverDepth();
								_repaint = true;
								_sceneDirty = true;
							}
						}
						Gui_SubBlock_RiverBedProfilePerNode(selectedIdx);



					}
				}
				using (var toogle = new esInspectorUI.DynamicGroup("Audio Setting", 0, 0, esInspectorUI.DynamicGroup.HeaderType.Foldout, 0, 30))
				if (toogle.ToggleGroup)
				{
					if(TargetRiverPath.audioManager.SourcesCount>0)
					{
						var audioSetting = AudioSourceGUI.OnGUI(TargetRiverPath.audioManager.GetSourceSetting(selectedIdx));

						if (!audioSetting.IsEqual(TargetRiverPath.audioManager.GetSourceSetting(selectedIdx)))
						{
							TargetRiverPath.audioManager.SetAudioSource(audioSetting, selectedIdx);
						}
					}

					

				}

			}
			else
			{
				esInspectorHelper.ShowWarning("No Path Point Selected");
			}

		

		}

		private int _findPointPos = -1;

		private void Gui_SubBlock_CapSetting()
		{

			EditorGUI.BeginChangeCheck();
			TargetRiverPath.CapSegments = EditorGUILayout.IntSlider("Segment Number", TargetRiverPath.CapSegments, 2, 100);
			TargetRiverPath.CapSubdivision = EditorGUILayout.IntSlider("Sub Divisions", TargetRiverPath.CapSubdivision, 1, 10);

			using (var toggle = new esInspectorUI.Section("Start Cap Setting"))
			{
				if (toggle.ToggleSection)
				{
					TargetRiverPath.EnableStartCap = EditorGUILayout.Toggle("IsActive", TargetRiverPath.EnableStartCap);
					TargetRiverPath.CapStartProfile = EditorGUILayout.CurveField(new GUIContent("Terrain Blending Profile"), TargetRiverPath.CapStartProfile, GUILayout.Height(45));

				}
			}
			using (var toggle = new esInspectorUI.Section("End Cap Setting"))
			{

				if (toggle.ToggleSection)
				{
					TargetRiverPath.EnableEndCap = EditorGUILayout.Toggle("IsActive", TargetRiverPath.EnableEndCap);
					TargetRiverPath.CapEndProfile = EditorGUILayout.CurveField(new GUIContent("Terrain Blending Profile"), TargetRiverPath.CapEndProfile, GUILayout.Height(45));

				}


			}
			if (EditorGUI.EndChangeCheck())
			{
				_repaint = true;
				_sceneDirty = true;
				TargetRiverPath.RefreshAll = true;
			}
	


		}

		[SerializeField] private int _findZoomDistanceInt;
		[SerializeField] private bool _followSplineForward;
		private void Gui_SubBlock_MoveMarkerIndex(bool showPreview)
		{
	
			_findPointPos = -1;

			EditorGUI.BeginChangeCheck();
			TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint = EditorGUILayout.IntSlider(TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint, 0,
				TargetRiverPath.MarkerSet.Count - 1);
			if (EditorGUI.EndChangeCheck())
			{
				_findPointPos = TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint;
				SceneView.RepaintAll();
			}

			_followSplineForward = EditorGUILayout.Toggle("Update Scene View", _followSplineForward);
			using (new EditorGUILayout.HorizontalScope())
			{
				if (GUILayout.Button("<<"))
				{
					GoToLastNode();
				}
				if (GUILayout.Button("<"))
				{
					GoToPreviousNode();
				}
				if (GUILayout.Button(">"))
				{
					GoToNextNode();
				}
				if (GUILayout.Button(">>"))
				{
					GoToLastNode();
				}
				//var gotoPoint = EditorGUILayout.IntField(TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint);
				//if (GUILayout.Button("GOTO"))
				//{
				//	//	if(gotoPoint != TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint)

				//}
			}
			if(_followSplineForward)
			{
				_findZoomDistanceInt = Mathf.RoundToInt(TargetRiverPath.findZoomDistance);
				_findZoomDistanceInt = EditorGUILayout.IntSlider(new GUIContent("View Level Zoom", "The distance to zoom out when (F)inding a path point in the scene view"),
					_findZoomDistanceInt, 2, 1000);
				if (_findZoomDistanceInt != TargetRiverPath.findZoomDistance)
				{
					var marker = TargetRiverPath.GetSelectedMarker;
				var lCameraViewRotation = Quaternion.LookRotation(
								TargetRiverPath.Spline.VertexGetDirection(TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint));
						
					//	Move the scene view camera to the selected point
					SceneViewHelper.PositionCameraSceneView(marker.Position, _findZoomDistanceInt, GetType(), lCameraViewRotation);
					TargetRiverPath.findZoomDistance = _findZoomDistanceInt;
				}
				if (_findPointPos > -1)
				{

					var marker = TargetRiverPath.GetSelectedMarker;
					var forward = TargetRiverPath.Spline.VertexGetDirection(TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint);


					//	Move the scene view camera to the selected point
					SceneViewHelper.PositionCameraSceneView(marker.Position + 30 * Vector3.up, _findZoomDistanceInt, GetType(), Quaternion.LookRotation(forward));

					_findPointPos = -1;
				}
			}
			
		}

		private void Gui_SubBlock_SurfaceMeshInfo()
		{
			StringBuilder sb = new StringBuilder();
			if (TargetRiverPath.ClosedSurface)
			{

				sb.AppendLine("Area : " + TargetRiverPath.AreaSurface);
			}
			else
			{
				sb.AppendLine("Path Lenght : " + TargetRiverPath.PathLenght);
			}
	
			sb.AppendLine("Vertices Count : " + TargetRiverPath.verticesCount);
			sb.AppendLine("Triangles Count : " + TargetRiverPath.trianglesCount);
			esInspectorHelper.ShowMessage(sb.ToString());
			if (TargetRiverPath.verticesCount > 65500) esInspectorHelper.ShowError("Failure : max number of vertices");
		}

		private void Gui_SubBlock_MeshParameters()
		{
			//EditorGUI.BeginChangeCheck();
			//using (new GUILayout.VerticalScope(InspectorHelper.BoxStyle))
			//{
			//	TargetRiverPath.AddCollider = EditorGUILayout.Toggle("Create Collider", TargetRiverPath.AddCollider);
			
			//	TargetRiverPath.Tag = EditorGUILayout.TagField(
			//		"Tag:",
			//		TargetRiverPath.Tag);
			//	TargetRiverPath.Layer = EditorGUILayout.LayerField(
			//		"Layer:",
			//		TargetRiverPath.Layer);


			//}
			//if (EditorGUI.EndChangeCheck())
			//{
			//	_sceneDirty = true;
			//	_repaint = true;
			//	TargetRiverPath.RefreshAll = true;
			//}

			if (ClosedSurface)
			{
				Gui_SubBlock_MeshParameters_CS();
				return;
			}

			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{

				EditorGUI.BeginChangeCheck();
				//TargetRiverPath.HideWireFrameInEditor = EditorGUILayout.Toggle("Hide Mesh Wirerrame", TargetRiverPath.HideWireFrameInEditor);
				TargetRiverPath.HideMesh = EditorGUILayout.Toggle("Hide Mesh", TargetRiverPath.HideMesh);
				if (EditorGUI.EndChangeCheck())
				{

					_sceneDirty = true;
					TargetRiverPath.RefreshAll = true;
				}

				EditorGUI.BeginChangeCheck();
				TargetRiverPath.SmoothType = (SmoothAlgorithm)EditorGUILayout.EnumPopup("Edge Smoother", TargetRiverPath.SmoothType);
				if (EditorGUI.EndChangeCheck())
				{
					_repaint = true;
					_sceneDirty = true;
					TargetRiverPath.RefreshAll = true;
				}
			}

			//var pathIn = TargetRiverPath.MyJunctionIn.PathIn;
			//var pathOut = TargetRiverPath.MyJunctionIn.PathOut;
			EditorGUI.BeginChangeCheck();
			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				//Target.QuadSize = EditorGUILayout.IntSlider("Quad Size", Target.QuadSize, 1, 10);
			//	//Target.MeshResolution = EditorGUILayout.IntSlider("Mesh Resolution", Target.MeshResolution, 1, 10);
				esInspectorHelper.Space(5);
				//TargetRiverPath.MeshResolution = 1/((float)EditorGUILayout.IntSlider("Mesh Resolution", (int)(1 / (float)TargetRiverPath.MeshResolution), 1, 100));
				TargetRiverPath.MeshResolution = EditorGUILayout.Slider("Mesh Resolution", TargetRiverPath.MeshResolution, 0.5f, 50f);

				//	TargetRiverPath.VResolution = EditorGUILayout.Slider("Mesh V Sampler Distance", TargetRiverPath.VResolution, 0.01f, 1);
				//	TargetRiverPath.VResolution = EditorGUILayout.Slider("Mesh V Sampler Distance", TargetRiverPath.VResolution, 0.5f, 20f);//2of
				TargetRiverPath.MeshSubDivision =
					EditorGUILayout.IntSlider("Mesh Sub Division", TargetRiverPath.MeshSubDivision, 1, 15);

			//         if (pathIn.Path == null && pathOut.Path == null)
			//{
			//	TargetRiverPath.MeshSubDivision =
			//		EditorGUILayout.IntSlider("Mesh Sub Division", TargetRiverPath.MeshSubDivision, 1, 15);
			//}
			//else
			//{

			//	if (pathIn.HasPath)
			//	{
			//		TargetRiverPath.MeshSubDivision = (int)Mathf.Round(pathIn.GetWidth
			//			 * pathIn.Path.MeshSubDivision );
			//	}
			//	else if (pathOut.HasPath)
			//		TargetRiverPath.MeshSubDivision = (int)Mathf.Round(pathOut.Path.MeshSubDivision * pathOut.GetWidth);

			//	InspectorHelper.ShowWarning("Value is locked becuase branching");
			//	EditorGUILayout.LabelField("Mesh Sub Division : " + TargetRiverPath.MeshSubDivision);

			//	//	EditorGUILayout.IntSlider("Mesh Sub Division", TargetRiverPath.UDivision, 1, 15);
			//}

			//InspectorHelper.Space(5);
			//	TargetRiverPath.MeshYGroundOffset = EditorGUILayout.Slider("Mesh Y Offset", TargetRiverPath.MeshYGroundOffset, -1f, 1f);
			TargetRiverPath.AddCollider =
				   EditorGUILayout.Toggle("Generate Collider", TargetRiverPath.AddCollider);

			}
			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				TargetRiverPath.UVScale = EditorGUILayout.FloatField("UV Scale", TargetRiverPath.UVScale);
				TargetRiverPath.Distorsion = EditorGUILayout.Slider("Distorsion ( Experimental )", TargetRiverPath.Distorsion, 0.01f, 1f);
				TargetRiverPath.FlipClockWise = EditorGUILayout.Toggle("Flip ClockWise", TargetRiverPath.FlipClockWise);
		}
			if (EditorGUI.EndChangeCheck())
			{
				_sceneDirty = true;
				_repaint = true;
				TargetRiverPath.RefreshAll = true;
			}
		}

		private void Gui_SubBlock_MeshParameters_CS()
		{


			using (var toogle = new esInspectorUI.DynamicGroup("Surface Setting", 0, 0, esInspectorUI.DynamicGroup.HeaderType.Foldout, 0, 30))
			{

				if(toogle.ToggleGroup)
				{
					var lTargetCascadeLake = TargetRiverPath.Lake;
					EditorGUI.BeginChangeCheck();

					using (var section = new esInspectorUI.Section("Cascade Surface Spawner", esInspectorUI.Section.Type.OpenClose, esInspectorUI.Style.Purple))
					{
						if (section.ToggleSection)
						{
							lTargetCascadeLake.FillStep = EditorGUILayout.IntSlider("Scan Steps", lTargetCascadeLake.FillStep, 1, 360);
							lTargetCascadeLake.ScanRadius = EditorGUILayout.Slider("Scan Radius", lTargetCascadeLake.ScanRadius, 1, 1000);
							lTargetCascadeLake.EdgePadding = EditorGUILayout.FloatField("Edge Padding", lTargetCascadeLake.EdgePadding);

						}
			
					}

					using (var section = new esInspectorUI.Section("Cascade Mesh", esInspectorUI.Section.Type.OpenClose, esInspectorUI.Style.Blue))
					{
						if (section.ToggleSection)
						{
							lTargetCascadeLake.Tiling = EditorGUILayout.Slider("UV Scale", lTargetCascadeLake.Tiling, 0, 100);
							lTargetCascadeLake.pointDensity = EditorGUILayout.IntSlider("Vertices Density", lTargetCascadeLake.pointDensity, 100, 2);

						}
					}

					if (EditorGUI.EndChangeCheck() )
					{
						TargetRiverPath.MeshRefreshClosed();
					}
					using (var section = new esInspectorUI.Section("Cascade Material", esInspectorUI.Section.Type.OpenClose, esInspectorUI.Style.GreenDark))
					{
						if (section.ToggleSection) Gui_SubBlock_Material();
					}
				
					using (var section =new esInspectorUI.Section("Edge Gizmos", esInspectorUI.Section.Type.OpenClose, esInspectorUI.Style.Green))
					{
						if (section.ToggleSection)
						{
							TargetRiverPath.GizmosShowSurfaceSpawner =
								EditorGUILayout.Toggle("Show Edge Points", TargetRiverPath.GizmosShowSurfaceSpawner);
							TargetRiverPath.GizmosSurfaceSpawnerSize =
								EditorGUILayout.Slider("Gizmos Size", TargetRiverPath.GizmosSurfaceSpawnerSize, 0.5f, 10);
							TargetRiverPath.GizmosSurfaceSpawnerColor =
								EditorGUILayout.ColorField("Gizmos Color", TargetRiverPath.GizmosSurfaceSpawnerColor);
						}
					}
				
				}
			}

		}

		private void Gui_SubBlock_SurfaceParameters()
		{
			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				using (new GUILayout.HorizontalScope())
				{

					TargetRiverPath.PathDefaultWidth = EditorGUILayout.FloatField("River Default Width", TargetRiverPath.PathDefaultWidth);
					if (GUILayout.Button(GCApplyAll, _buttonWidth))
					{
						TargetRiverPath.SetMarkersPathWidth();
						_repaint = true;
						_sceneDirty = true;
						TargetRiverPath.RefreshAll = true;
					}

				}
				EditorGUI.BeginChangeCheck();
				TargetRiverPath.RiverWidthOffset = EditorGUILayout.Slider("Surface Edges Offset", TargetRiverPath.RiverWidthOffset, -5, 5);
				if (EditorGUI.EndChangeCheck())
				{

					_repaint = true;
					_sceneDirty = true;
					TargetRiverPath.RefreshAll = true;
				}


			}
		}

		private void Gui_SubBlock_DepthParameters()
		{

			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				using (new GUILayout.HorizontalScope())
				{
					TargetRiverPath.RiverDefaultDepth = EditorGUILayout.FloatField("River Default Depth", TargetRiverPath.RiverDefaultDepth);
					if (GUILayout.Button(GCApplyAll, _buttonWidth))
					{
						TargetRiverPath.SetMarkersRiverDepth();
						//TargetRiverPath.RefreshSplinePoint();
						_repaint = true;
						_sceneDirty = true;
						TargetRiverPath.RefreshAll = true;
					}
				}
			}
		}

		private void Gui_SubBlock_RiverBedProfile()
		{
			if (groundProfiles == null) InitGroundProfile();
			esInspectorHelper.ShowMessage("You Can Define a 2D Profile for the River Bed", Config.ShowHelp);
			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				// TODO That should be not needed if profile is initialized properly.
				if (TargetRiverPath.Profile.RiverBedSetting.Shape2DProfile == null)
					TargetRiverPath.Profile.RiverBedSetting.Shape2DProfile = esAnimationCurveUtility.CreateCopy(MeshPrimitives.RiverBed1());

				using (new GUILayout.HorizontalScope())
				{
					esInspectorHelper.FlexibleSpace();
					if (GUILayout.Button(GCApplyAll))
					{
						TargetRiverPath.SetMarkerDefaultRiverBedProfile();
						_sceneDirty = true;
						TargetRiverPath.RefreshAll = true;
					}
				}

				EditorGUI.BeginChangeCheck();
				_selectedGroundProfileIdx = (GroundProfileIdx) EditorGUILayout.EnumPopup("Ground Profile", _selectedGroundProfileIdx);
				if (EditorGUI.EndChangeCheck())
				{
					TargetRiverPath.Profile.RiverBedSetting.Shape2DProfile = esAnimationCurveUtility.CreateCopy(groundProfiles[(int) _selectedGroundProfileIdx]);
					_sceneDirty = true;
					TargetRiverPath.RefreshAll = true;
				}

				EditorGUI.BeginChangeCheck();
				var label = "River Bed Profile";
				EditorGUILayout.CurveField(new GUIContent(label), TargetRiverPath.Profile.RiverBedSetting.Shape2DProfile, GUILayout.Height(75));
				if (EditorGUI.EndChangeCheck())
				{
					TargetRiverPath.RefreshAll = true;
					_sceneDirty = true;
				}
			}
			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				EditorGUILayout.LabelField("<b>Ground</b>", esInspectorHelper.LabelFieldRichText);
				EditorGUI.BeginChangeCheck();
				var rbS = TargetRiverPath.Profile.RiverBedSetting.NoiseSettings;
				rbS.Scale = EditorGUILayout.Slider(new GUIContent("Scale"), rbS.Scale, 0, 1);
				rbS.NoiseAmplitude = EditorGUILayout.Slider(new GUIContent("Amplitude"),
					rbS.NoiseAmplitude, 0, 1);
				rbS.CutOff = EditorGUILayout.Slider(new GUIContent("Cutoff"),
					rbS.CutOff, 0, 1);
				if (EditorGUI.EndChangeCheck())
				{
					TargetRiverPath.Profile.RiverBedSetting.NoiseSettings = rbS;
					_sceneDirty = true;
					TargetRiverPath.RefreshAll = true;
				}

			}



		}

		private void Gui_SubBlock_RiverBedProfilePerNode(int selectedIdx)
		{
	
				if (selectedIdx != -1)
				{
					esInspectorHelper.ShowMessage("You Can Define a 2D Profile for the River Bed", Config.ShowHelp);
					using (new GUILayout.VerticalScope())
					{

						//using (new GUILayout.HorizontalScope())
						//{

						//	var newDepth = EditorGUILayout.FloatField("River Marker Depth", TargetRiverPath.PathSelectedDepth(selectedIdx));
						//	if (newDepth != TargetRiverPath.PathSelectedDepth(selectedIdx))
						//	{
						//		TargetRiverPath.SetMarkerRiverDepth(newDepth, selectedIdx);
						//		_repaint = true;
						//		_sceneDirty = true;
						//	}
						//	if (GUILayout.Button(GCApplyAll, _buttonWidth))
						//	{

						//		TargetRiverPath.RiverDefaultDepth = newDepth;
						//		TargetRiverPath.SetMarkersRiverDepth();
						//		_repaint = true;
						//		_sceneDirty = true;
						//	}
						//}
						string label = "River Bed Profile";
						esInspectorHelper.ShowMessage("You Can Define a 2D Profile for the water ground specific for a Node", Config.ShowHelp);
						using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
						{
							// TODO Remove in RC1
							if (TargetRiverPath.Spline.MarkerSet[selectedIdx].Shape2DProfile1 == null)
								TargetRiverPath.Spline.MarkerSet[selectedIdx].Shape2DProfile1 = esAnimationCurveUtility.CreateCopy(TargetRiverPath.Profile.RiverBedSetting.Shape2DProfile);
							EditorGUI.BeginChangeCheck();
							EditorGUILayout.CurveField(new GUIContent(label), TargetRiverPath.Spline.MarkerSet[selectedIdx].Shape2DProfile1, GUILayout.Height(75));
							if (EditorGUI.EndChangeCheck())
							{
								_repaint = true;
								_sceneDirty = true;
							}
						}

						//lm.LeftPadding = lm.RightPadding;
						using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
						{

							//EditorGUI.BeginChangeCheck();
							//_selectedGroundProfileIdx = (GroundProfileIdx)EditorGUILayout.EnumPopup("Ground Profile", _selectedGroundProfileIdx);
							//if (EditorGUI.EndChangeCheck())
							//{
							//	TargetRiverPath.Profile.RiverBedSetting.Shape2DProfile = ESAnimationCurveUtility.CreateCopy(groundProfiles[(int)_selectedGroundProfileIdx]);
							//	_sceneDirty = true;
							//}
							TargetRiverPath.KeepSize = EditorGUILayout.Toggle("Look Size", TargetRiverPath.KeepSize);
							bool lKeepSize = TargetRiverPath.KeepSize;

							var lm = TargetRiverPath.MarkerSet[selectedIdx].LandscapeModifier;

							EditorGUI.BeginChangeCheck();

							lm.LeftPadding = EditorGUILayout.Slider("Left Edge Padding", lm.LeftPadding, 0, 1);
							if(lKeepSize)
								lm.RightPadding = lm.LeftPadding;

							lm.RightPadding = EditorGUILayout.Slider("Right Edge Padding", lm.RightPadding, 0, 1);
							if (lKeepSize)
								lm.LeftPadding = lm.RightPadding;

							lm.LeftSurronding = EditorGUILayout.Slider("Left Edge Surrounding", lm.LeftSurronding, 0,MaxSurroundValue);
							if (lKeepSize)
								lm.RightSurronding = lm.LeftSurronding;

							lm.LeftBlendingCurve = EditorGUILayout.CurveField(new GUIContent("Left Profile"), TargetRiverPath.LandscapeModifier.LeftBlendingCurve);

							EditorGUILayout.Separator();
							//ShowButton();
							lm.RightSurronding = EditorGUILayout.Slider("Right Edge Surrounding", lm.RightSurronding, 0, MaxSurroundValue);
							lm.RightBlendingCurve = EditorGUILayout.CurveField(new GUIContent("Right Profile"), TargetRiverPath.LandscapeModifier.RightBlendingCurve);
							if (lKeepSize)
								lm.LeftSurronding = lm.RightSurronding;


							//// TODO Init in RiverPath
							//if (TargetRiverPath.Profile == null) return;

							//var rb = TargetRiverPath.Profile.RiverBedSetting;
							//rb.TerrainHeightOffset = EditorGUILayout.Slider("Bank Offset", TargetRiverPath.Profile.RiverBedSetting.TerrainHeightOffset, 0, 2);
							//var rs = TargetRiverPath.Profile.RiverSetting;
							//rs.TerrainHeightOffset = EditorGUILayout.Slider("Surface Offset", rs.TerrainHeightOffset, 0, 2);
							//EditorGUILayout.Space();
							//rb.MeshSubdvision = EditorGUILayout.IntSlider("U SubDivision", TargetRiverPath.Profile.RiverBedSetting.MeshSubdvision, 1, 200);

							if (EditorGUI.EndChangeCheck())
							{
								TargetRiverPath.MarkerSet[selectedIdx].LandscapeModifier = lm;
								//TargetRiverPath.Profile.RiverBedSetting = rb;
								//TargetRiverPath.Profile.RiverSetting = rs;

								_repaint = true;
								_sceneDirty = true;
							}
						}
					}
				}
			
		
		}

		/// <summary>
		/// GUI to Export Spline Control Points or Waypoints ( you can set up the distance )
		/// </summary>
		private void Gui_SubBlockExportMarker()
		{
			if (TargetRiverPathIsNull) return;
			GUILayout.Label("Export");
			using (new GUILayout.VerticalScope(GUI.skin.box))
			{
				using (new GUILayout.HorizontalScope())
				{

					if (GUILayout.Button(GCExport))
					{
						TargetRiverPath.ExportMarkers();
					}
					GUILayout.FlexibleSpace();
				}
			}
			GUILayout.Label("Export Map");
			using (new GUILayout.VerticalScope(GUI.skin.box))
			{
				using (new GUILayout.HorizontalScope())
				{

					if (GUILayout.Button(GCExportMap))
					{

						string textureSavePath = EditorUtility.SaveFilePanelInProject("Save Cascade Map", "", "png", "Save Cascade Map");
						if (textureSavePath.Length < 1) return;

						if (textureSavePath.Length != 0)
						{
							string assetName = textureSavePath.Substring(textureSavePath.LastIndexOf("/"));
							assetName = assetName.Substring(1, assetName.IndexOf(".") - 1);
							textureSavePath = textureSavePath.Substring(0, textureSavePath.LastIndexOf("/"));

							var meshPath = string.Format("{0}/{1}.png", textureSavePath, assetName);
	
							var texture = TargetRiverPath.ExportMarkersMap(meshPath);
							if (null == texture)
							{
								Debug.LogWarning("Cascade Texture Map Export Failed!");
								return;
							}
							AssetDatabase.Refresh();
						
					}

				
					}
					GUILayout.FlexibleSpace();
				}
			}

	}

#pragma warning disable 0414
		private string[] _optionsImporter = { "Disable", "Delete"};
#pragma warning restore 0414
		private int _selectetImporterAction;
		private Material _importMaterial;
		/// <summary>
		/// GUI to Import Spline Control Point for PathId = 0
		/// </summary>
		private void Gui_SubBlockImportMarker()
		{

			GUILayout.Label("Import");
			using (new GUILayout.VerticalScope(GUI.skin.box))
			{
				using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
				{
					GUILayout.Label("<b>Cascade Markers</b>", esInspectorHelper.LabelFieldRichText);
					using (new GUILayout.HorizontalScope())
					{
						_waypointsRoot = EditorGUILayout.ObjectField("Root Marker Points", _waypointsRoot, typeof(GameObject), true, GUILayout.MaxWidth(400f)) as GameObject;
						if (GUILayout.Button(GCImport))
						{
							if (_waypointsRoot == null) return;
							if (TargetRiverPath == null)
							{
								TargetRiverPath = RiverPath.CreateCascadeBuilder(false);

							}
							TargetRiverPath.ImportMarkers(_waypointsRoot);
							//Target.SceneCameraSelectedMarkerIdx = 0;
							//Target.ImportMarkers(Target.WaypointsRoot);
						}
						GUILayout.FlexibleSpace();
					}
				}
				esInspectorHelper.Space(8);
			
				_shpwImportExternalAsset = EditorGUILayout.Toggle("3rd Party Assets Importer", _shpwImportExternalAsset);
				if(_shpwImportExternalAsset)
					Gui_SubBlockImportExternalAssets();

			}
	}
	
  [SerializeField]
		private bool _shpwImportExternalAsset = false;
		private void Gui_SubBlockImportExternalAssets()
		{
			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				GUILayout.Label("<b>R.A.M.</b>", esInspectorHelper.LabelFieldRichText);
				using (new GUILayout.VerticalScope())
				{
					using (new GUILayout.HorizontalScope())
					{
						esInspectorHelper.FlexibleSpace();
#if !ES_RAM_IMPORTER
						if (GUILayout.Button(GCEnable))
						{
							if (RAMIntegration.IsInstalled)
							{
								Debug.Log("R.A.M. is Installed");
								// Add define symbol
								HelperCompiler.AddCompilerDefine("ES_RAM_IMPORTER"); 

							}
							else
							{
								Debug.LogWarning("R.A.M. is Not Installed");
								HelperCompiler.RemoveCompilerDefine("ES_RAM_IMPORTER");
							}
						}
#else
							if (GUILayout.Button("Disable"))
							{
								HelperCompiler.RemoveCompilerDefine("ES_RAM_IMPORTER");
							}
#endif

					}




#if ES_RAM_IMPORTER

						EditorGUILayout.Space();
						_selectetImporterAction = EditorGUILayout.Popup("Action on source : ", _selectetImporterAction, _optionsImporter, EditorStyles.popup);
						_importMaterial = EditorGUILayout.ObjectField("Cascade Materiali", _importMaterial, typeof(Material), true, GUILayout.MaxWidth(400f)) as Material;

						if (GUILayout.Button(GCImport))
						{
							// Check using Reflection
							var activeRAMs = FindObjectsOfType<RamSpline>();
							foreach (var ramSpline in activeRAMs)
							{
								var cascade = RiverPathEditor.CreateCascadeBuilder(false);
								//cascade.EnableDepthConstrain = false;
								cascade.Init();
								for (int i = 0; i < ramSpline.controlPoints.Count; i++)
								{

									Vector3 pointLocalRam = (Vector3) ramSpline.controlPoints[i] + ramSpline.transform.position;

									var point = cascade.transform.TransformPoint(pointLocalRam);

									cascade.NodeAdd(point, i);
									cascade.SetMarkerPathWidth(i, ramSpline.controlPoints[i].w);
							
								}
								cascade.RefreshCascadePath();
								if (_importMaterial)
									cascade.SetMaterial(_importMaterial);
								if (_selectetImporterAction == 0)
								{
									ramSpline.gameObject.SetActive(false);
								}
								else
								{
									ramSpline.gameObject.Destroy();
								}
							}

						}		
#endif
				}
			}


			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				GUILayout.Label("<b>EasyRoad3D (WIP)</b>", esInspectorHelper.LabelFieldRichText);
				using (new GUILayout.VerticalScope())
				{
					using (new GUILayout.HorizontalScope())
					{
						esInspectorHelper.FlexibleSpace();
#if !ES_ER3_IMPORTER
						if (GUILayout.Button("WIP"))
						//if (GUILayout.Button(GCEnable))
						{
							//if (IntegrationScript.isEasyRoads3DInstalled())
							//{
							//	Debug.Log("EasyRoad3D is Installed");
							//	// Add define symbol
							//	HelperCompiler.AddCompilerDefine("ES_ER3_IMPORTER");
							//}
							//else
							//{
							//	Debug.LogWarning("EasyRoad3D is Not Installed");
							//	HelperCompiler.RemoveCompilerDefine("ES_ER3_IMPORTER");
							//}
						}
#else
							if (GUILayout.Button("Disable"))
							{
								HelperCompiler.RemoveCompilerDefine("ES_ER3_IMPORTER");
							}
#endif

					}

#if ES_ER3_IMPORTER

						EditorGUILayout.Space();
						_selectetImporterAction = EditorGUILayout.Popup("Action on source : ", _selectetImporterAction, _optionsImporter, EditorStyles.popup);
						_importMaterial = EditorGUILayout.ObjectField("Cascade Material", _importMaterial, typeof(Material), true, GUILayout.MaxWidth(400f)) as Material;

						if (GUILayout.Button(GCImport))
						{

					

						}		
#endif
				}
			}
		}


#endregion

#region Cascade Ordered List

		private ReorderableList _cascadeList;
		/// <summary>
		/// Create the reorderable list
		/// </summary>
		private void InstanciateCascadesReorderableLis()
		{
			_cascadeList = new ReorderableList(TargetRiverPath.Cascades, typeof(CascadeMarker), false, true, false, false)
			{
				drawHeaderCallback = DrawListHeader,
				drawFooterCallback = DrawListFooter,
				drawElementCallback = DrawListElement,
				onAddCallback = OnListItemAdd,
				onSelectCallback = OnListItemSelect,
				onRemoveCallback = OnListItemRemove,
				footerHeight = 17f
			};

		}

		private void OnListItemRemove(ReorderableList list)
		{
			if (EditorUtility.DisplayDialog("Warning!", "Are you sure you want to delete the item?", "Yes", "No"))
			{
				int idx = list.index;
				TargetRiverPath.Cascades[idx].gameObject.Destroy();
				TargetRiverPath.Cascades.RemoveAt(idx);
				list.index--;
				_sceneDirty = true;
				_repaint = true;
			}

		}

		private void OnListItemSelect(ReorderableList list)
		{


		}

		private void OnListItemAdd(ReorderableList list)
		{

			if (_newCascadeType == CascadeType.Waterfall)
			{

				EditorUtility.DisplayDialog("Cascade Alert Message","Please select Lake.\nWaterfall is reserveed for next version", "Ok");
				return;
			}
			
	
			string lName = "Cascade Marker " + (TargetRiverPath.Cascades.Count + 1);
			GameObject cascade = new GameObject(lName);
			cascade.AddComponent<MeshRenderer>();
			cascade.AddComponent<MeshFilter>();
			var cas = cascade.AddComponent<CascadeMarker>();
			cas.Name = lName;
			cascade.transform.parent = TargetRiverPath.cascadeRoot.transform;
			cascade.transform.position = TargetRiverPath.cascadeRoot.transform.position;
			cas.start = -1;
			cas.end = -1;
			cas.Type = _newCascadeType;


			TargetRiverPath.Cascades.Add(cas);
			_cascadeList.index = TargetRiverPath.Cascades.Count - 1;

		}

		private void DrawListElement(Rect rRect, int rIndex, bool rIsActive, bool rIsFocused)
		{
			if (rIndex < TargetRiverPath.Cascades.Count)
			{
				string lName = TargetRiverPath.Cascades[rIndex] == null ? "null" : TargetRiverPath.Cascades[rIndex].Name;

				rRect.y += 2;
				EditorGUI.LabelField(new Rect(rRect.x, rRect.y, rRect.width, EditorGUIUtility.singleLineHeight), lName);
			}
		}

		[SerializeField]
		private CascadeType _newCascadeType;
		private void DrawListFooter(Rect rect)
		{
			Rect lShapeRect = new Rect(rect.x, rect.y + 1, rect.width - 4 - 28 - 28, 16);
			_newCascadeType = (CascadeType)EditorGUI.EnumPopup(lShapeRect, _newCascadeType);

			Rect lAddRect = new Rect(lShapeRect.x + lShapeRect.width + 4, lShapeRect.y, 28, 15);
			if (GUI.Button(lAddRect, new GUIContent("+", "Add Cascade Item."), EditorStyles.miniButtonLeft))
			{
				OnListItemAdd(_cascadeList);
			}

			Rect lDeleteRect = new Rect(lAddRect.x + lAddRect.width, lAddRect.y, 28, 15);
			if (GUI.Button(lDeleteRect, new GUIContent("-", "Delete Cascade Item."), EditorStyles.miniButtonRight))
			{
				OnListItemRemove(_cascadeList);
			};

		}

		private void DrawListHeader(Rect rect)
		{
			EditorGUI.LabelField(rect, "Cascades Definition");

		}

		private bool _showStart;
		private bool _showEnd;
		[SerializeField] private bool _updateAllNodes;
	

		public bool targetCascadeOnInspectorGUI(CascadeMarker marker)
		{

			using (new GUILayout.VerticalScope(GUI.skin.box))
			{
				var lbGUIbackgroundColor = GUI.backgroundColor;


				EditorGUILayout.LabelField(marker.Type.ToString());
				EditorGUILayout.Space();

				EditorGUI.BeginChangeCheck();

				if (marker.start != -1 && marker.end != -1)
					if (GUILayout.Button(marker.IsActive ? "Disable" : "Enable", GUILayout.Width(75f)))
					{

						marker.IsActive = !marker.IsActive;
						esInspectorHelper.SetObjectDirty(marker);
						return true;
					}

				marker.Name = EditorGUILayout.TextField("Name", marker.Name);


				if (_showStart)
					GUI.backgroundColor = Color.yellow;
				using (new GUILayout.HorizontalScope())
				{
					EditorGUILayout.IntField("Start Point", marker.start);
					if (GUILayout.Button("Set Point"))
					{
						_showStart = true;
						_showEnd = false;
					}
					GUILayout.FlexibleSpace();
				}
				GUI.backgroundColor = lbGUIbackgroundColor;

				if (marker.start != -1)
				{
					if (_showEnd)
						GUI.backgroundColor = Color.yellow;
					using (new GUILayout.HorizontalScope())
					{
						EditorGUILayout.IntField("End Point", marker.end);
						if (GUILayout.Button("Set Point"))
						{
							_showStart = false;
							_showEnd = true;
						}
						GUILayout.FlexibleSpace();
					}
				}
				GUI.backgroundColor = lbGUIbackgroundColor;


				if (EditorGUI.EndChangeCheck())
				{
					esInspectorHelper.SetObjectDirty(marker);
					return true;
				}


			}
			int idx = -1;
			if (_showStart)
			{
				idx = UI_SelectMarker("Select Start Point", marker.start, true, marker.end);
				if (idx != -1)
				{
					marker.start = idx;
					esInspectorHelper.SetObjectDirty(marker);
					return true;
				}
			}
			if (_showEnd)
			{
				idx = UI_SelectMarker("Select End Point", marker.end, false, marker.start);
				if (idx != -1)
				{
					marker.end = idx;
					esInspectorHelper.SetObjectDirty(marker);
					return true;
				}
			}
			return false;
		}
	

		private int UI_SelectMarker(string info, int current, bool isStart, int previous)
		{
			var lGUIbackgroundColor = GUI.backgroundColor;
			esInspectorHelper.SectionTitleAndHelp(info, "");

			using (new GUILayout.VerticalScope(esInspectorHelper.BoxStyle))
			{
				using (new GUILayout.HorizontalScope())
				{
					GUILayout.FlexibleSpace();
					if (GUILayout.Button("Close", GUILayout.MaxWidth(70f)))
					{
						_showStart = false;
						_showEnd = false;
						_selectedMarker = -1;
						return -1;
					}
				}
				int from = 0;
				int to = TargetRiverPath.MarkerSet.Count;
				if (isStart)
				{
					from = 0;
					to = previous == -1 ? TargetRiverPath.MarkerSet.Count : previous;
				}

				else
				{
					from = previous + 1;
				}


				for (int i = from; i < to; i++)
				{
					if (i == current)
						GUI.backgroundColor = Color.green;
					if (i == _selectedMarker)
						GUI.backgroundColor = Color.yellow;
					using (new GUILayout.HorizontalScope(esInspectorHelper.BoxStyle))
					{
						EditorGUILayout.LabelField("Point " + i);
						if (GUILayout.Button("Select", GUILayout.MaxWidth(70f)))
						{
							_selectedMarker = i;
							TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint = i;
						}
						if (_selectedMarker == i)
						{
							if (GUILayout.Button("Confirm", GUILayout.MaxWidth(70f)))
							{
								_showStart = false;
								_showEnd = false;
								_selectedMarker = -1;

								return i;
							}

						}
						GUILayout.FlexibleSpace();
					}
					GUI.backgroundColor = lGUIbackgroundColor;
				}
			}
			return -1;
		}
		private bool UI_DetailCascade(CascadeMarker targetCascade)
		{
			if (targetCascade == null) return false;
			return targetCascadeOnInspectorGUI(targetCascade);

		}
	#endregion

	private void GoToLastNode()
	{
		if (TargetRiverPath == null) return;
		TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint = TargetRiverPath.MarkerSet.Count - 1;
		_findPointPos = TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint;
		SceneView.RepaintAll();
	}

	private void GoToNextNode()
	{
		if (TargetRiverPath == null) return;
		if (TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint < TargetRiverPath.MarkerSet.Count - 1)
			TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint += 1;
		_findPointPos = TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint;
		SceneView.RepaintAll();
	}

	private void GoToPreviousNode()
	{
		if (TargetRiverPath == null) return;
		if (TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint > 0)
			TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint -= 1;
		_findPointPos = TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint;
		SceneView.RepaintAll();
	}

	private void GoToFirstNode()
	{
		if (TargetRiverPath == null) return;
		TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint = 0;
		_findPointPos = TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint;
		SceneView.RepaintAll();
	}
	#region Support Method
	/// <summary>
	/// 
	/// </summary>
	private void SetSceneDirty()
	{
		esInspectorHelper.SetObjectDirty(esLandscapeManager.gameobject, false);
		if (TargetRiverPath)
		{
			esInspectorHelper.SetObjectDirty(TargetRiverPath, false);
			if (TargetRiverPath.Profile)
				esInspectorHelper.SetObjectDirty(TargetRiverPath.Profile, false);
		}
		if(CascadeManager.Instance) esInspectorHelper.SetObjectDirty(CascadeManager.Instance, false);

	}
	void CreatePrefabCascadePath()
	{

#if UNITY_EDITOR
		var lMergedMeshes = TargetRiverPath.cascadeRoot;
		Mesh mesh = lMergedMeshes.GetComponent<MeshFilter>().sharedMesh;
		if (mesh == null)
		{

			return;
		}
		string meshSavePath = EditorUtility.SaveFilePanelInProject("Save Cascade mesh", "", "asset", "Save Cascade mesh");
		if (meshSavePath.Length < 1) return;

		if (meshSavePath.Length != 0)
		{
			string assetName = meshSavePath.Substring(meshSavePath.LastIndexOf("/"));
			assetName = assetName.Substring(1, assetName.IndexOf(".") - 1);
			meshSavePath = meshSavePath.Substring(0, meshSavePath.LastIndexOf("/"));

			var meshPath = string.Format("{0}/{1}.asset", meshSavePath, assetName);

			Mesh tempMesh = (Mesh)UnityEngine.Object.Instantiate(mesh);

			AssetDatabase.CreateAsset(tempMesh, meshPath);
			if (stampPrefab)
			{
				GameObject tempObject = new GameObject(assetName);
				tempObject.transform.position = lMergedMeshes.gameObject.transform.position;
				tempObject.transform.rotation = lMergedMeshes.gameObject.transform.rotation;
				var mf = tempObject.AddComponent<MeshFilter>();
				mf.sharedMesh = tempMesh;
				var mr = tempObject.AddComponent<MeshRenderer>();
				mr.material = TargetRiverPath.RiverMaterial;
				string prefabPath = string.Format("{0}/{1}.prefab", meshSavePath, assetName);
				PrefabUtility.CreatePrefab(prefabPath, tempObject);
				tempObject.Destroy();
			}
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

#endif

		}
#endregion

}