﻿
/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/
using UnityEditor;

namespace com.earthshaping.CompilerDefinition
{
	[InitializeOnLoad]
	public class CompilerDefineRiver : Editor
	{
		private static readonly string _EditorSymbol = "CASCADE";

		static CompilerDefineRiver()
		{
			var symbols =
				PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
			if (!symbols.Contains(_EditorSymbol))
			{
				symbols += ";" + _EditorSymbol;
				PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup,
					symbols);
			}
		}
	}
}
