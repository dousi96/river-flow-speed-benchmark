﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.earthshaping.Cascade
{
	public class CascadeHelp
	{

		public static string GlobalSettingURL = "http://www.earthshaping.com/cas_global-setting/";
		public static string FlowMapURL = "http://www.earthshaping.com/cas_flow-map/";
		public static string GeneralSettingURL = "http://www.earthshaping.com/cas_general-setting/";
		public static string PerNodeSettingURL = "http://www.earthshaping.com/cas_per-node-setting/";
		public static string LightManagerURL = "http://www.earthshaping.com/cas_light-manager/";
		public static string LandscapeSettingURL = "http://www.earthshaping.com/cas_landscape-setting/";
		public static string SegmentSettingURL = "http://www.earthshaping.com/cas_segment-setting/";
		public static string ImportExportURL = "http://www.earthshaping.com/cas_import-export/";

		public static string BuilderCreation = "Select type of landscape path and create";
		public static string PathEdges = "Add organic random variation for left and right edge for all the path";
		public static string CascadeSurface = "Setup paramaters for surface of the water";
		public static string CascadeMesh = "Setup the mesh resolution";
		public static string CascadeMaterial = "Load and Create Cascade Shader and Material";
		public static string LandscapeVegetation = "Integration with Vegetation Studio ( require the asset ) on your landscape";
		public static string GroundDigging = "Setup paramenter for your river bed ground";
		public static string Procedural = "ssss";
	}

}
