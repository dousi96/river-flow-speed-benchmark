﻿using System;

[Serializable]
public enum CascadeMainMenu
{
	GlobalSetting,
	PerNodeSetting,
	SegmentSetting,
	RiverBed,
	Lighting,
	ImportExport,

	Painter,
	Debug
	

}