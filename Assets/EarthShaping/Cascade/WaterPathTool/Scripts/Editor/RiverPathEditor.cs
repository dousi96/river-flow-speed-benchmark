﻿/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/

#define MESH_PAINTER__

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using com.earthshaping.EarthShaping;
using com.earthshaping.Cascade;

using com.earthshaping.framework;
using com.earthshaping.framework.EditorHelper;
using com.earthshaping.framework.Geometry;
using com.earthshaping.Geometry;
using com.earthshaping.Sentieri;
using JBooth.River;
using UnityEditor;
using UnityEngine;
using com.earthshaping.Helper.Extension;
using com.earthshaping.Helpers;
using com.earthshaping.ProceduralMesh;
using com.earthshaping.Sentieri.ProceduralMesh;
using com.earthshaping.Setting;
using UnityEditorInternal;


[CustomEditor(typeof(RiverPath))]
public class RiverPathEditor : EarstShapingMarkerEditor
{


	#region Variable Declaration
	private RiverPath Target { get { return base.target as RiverPath; } }
	private readonly Vector3 _widthLabelOffset = Vector3.up * 4f;

	private const string NodeHelpMessage = "Add Node After Last : L Ctrl + L Mouse Click\n"
											+ "Insert Node Before Selected : L Shif + L Mouse Click\n"
											+ "Delete Selected Node : L Ctrl + Press Backspace\n\nAlign All Nodes Heigth to Selected : Press L Shif-L\n"
											+ "Snap All Nodes to Ground : Press L Shif-G\n"
											+ "Snap Selected Node to Ground : Press Ctrl-G\n"
		;

	private const string NodeHelpMessageLake = "Add Cascade Surface Spawner : L Ctrl + L Mouse Click\n"
												+ "Delete Selected Cascade Surface Spawne :  L Ctrl + Press Backspace\n"
												+ "Move Marker to refresh the surface";



	//readonly GUILayoutOption _buttonWidth = GUILayout.Width(160);
	[SerializeField] private CascadeMainMenu _activeMenuOption;
	// Used to import path
	private GameObject _waypointsRoot;
	[SerializeField] private bool _uiDebug;
#pragma warning disable 0414
	private Terrain _activeTerrain;
#pragma warning restore 0414
	//private ES_TerrainManager _TerrainStamper;

	//private GUIContent[] _guiContent;
	//private Texture2D[] _toolbarTextures;

	#endregion

	#region GUIContent Declaration

	#endregion

	
	
	#region Unity Events
	private void OnEnable()
	{
		Target.SplineSelectionInfo = new SplineSelectionInfo();
		Tools.hidden = true;
	}

	void OnDisable()
	{
		Tools.hidden = false;
	}

	public override void OnInspectorGUI()
	{
#if EARTHSHAPING_DEBUG
		_uiDebug = EditorGUILayout.Toggle("Show Debug UI", _uiDebug);
		if (_uiDebug)
		{
			DrawDefaultInspector();
			return;
		}
#endif
		LogoName = "Cascade_Splash_v2";
		showLogo = true ;
		ShowOpenWindowButton = false;
		base.OnInspectorGUI();
		if (GUILayout.Button("Open Cascade Manager"))
		{
			CascadeWindow.OpenCascadeWindow();
		}

	}

	private void OnSceneGUI()
	{

#if MESH_PAINTER
		if (Target.MeshPainter == null)
			Target.MeshPainter = new ESMeshPainter();

		Target.MeshPainter.MeshGameObject = Target.cascadeRoot;
	
		Target.CanEditSpline = _activeMenuOption == 0;
	
		if (Target.EnableMeshPainter)
		{
			HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
			Target.MeshPainter.SceneGUI_Painter();

			return;
		}
#endif       

		var lHandleDefaultColor = Handles.color;
		// Grab the UI event in sceneview
		Event guiEvent = Event.current;
		if (guiEvent.type == EventType.Repaint)
		{
		
		}

		SceneGUI_Draw_MarkerInfo();
		SceneGUI_Draw_Markers(Handles.SphereHandleCap);

		if (Target.CanEditSpline)
		{
			// Draw Handles for the selected Marker Point
			_sceneDirty = SceneGUI_Draw_Move_SelectedMarker(_sceneDirty);

			//Cursor Draw Path Add node
			SceneGUI_Draw_SplinePointer(guiEvent);

			// Help Message on Screen
			Handles.BeginGUI();
			esInspectorHelper.ShowSceneHelp(Target.ClosedSurface ? NodeHelpMessageLake : NodeHelpMessage, Color.white, esInspectorHelper.menuBackgroundColor, true);
			Handles.EndGUI();

			// Handle Input Commands
			SceneGUI_InputManager(guiEvent, PointAdd, PointInsert, PointDelete, PointAlignGround, PointAlignAllHeightToSelected, PointAlignAllGround);
		}



		if (_repaint)
		{
			HandleUtility.Repaint();
			_repaint = false;
		}
		//disable selection
		if (guiEvent.type == EventType.Layout)
		{
			HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
		}

		if (_sceneDirty)
		{
			esInspectorHelper.SetObjectDirty(Target, false);
			// If point changed, rebuild
			// ??Add a delayed mode 
			Target.RefreshCascadePath();
			_sceneDirty = false;
		}

		Handles.color = lHandleDefaultColor;
	}
	#endregion

	#region OnSceneGUI Managers

	public override bool SceneGUI_Draw_Move_SelectedMarker(bool isChanged)
	{
		if (Target.MarkerSet.Count == 0) return isChanged;

		int idx = Target.SplineSelectionInfo.IdxSelectedPoint;

		if (idx < 0) return isChanged;

		var lselectedMarker = Target.MarkerSet[idx];
		Vector3 lCurrentPosition = lselectedMarker.Position;

		var lSelectedTool = Tools.current;
		//TargetRiverPath.Spline.FixUpdateCurve();
		var time = Target.Spline.VertexGetTime(idx);

		var myPoint = Target.Spline.GetPositionTangent(time);
		var dir2REdge = Vector3.Cross(Vector3.up, myPoint.Tangent).normalized;
		var dirForward = Vector3.Cross(Vector3.up, -dir2REdge).normalized;

		if (dirForward == Vector3.zero) dirForward = Vector3.forward; // Case when we have only one point

		// Width Central Lane Handle
		var handleSize = HandleUtility.GetHandleSize(lCurrentPosition) * Config.HandleSelectedSize * 0.5f;

		if(Target.ClosedSurface)
		{

			Vector3 lNewPosition = Handles.PositionHandle(lCurrentPosition, Quaternion.LookRotation(dirForward));
			if (lNewPosition != lCurrentPosition)
			{
				Target.PointMoveClosed(idx, lNewPosition);
				Target.MeshRefreshClosed();
				isChanged = false;

			}
			return isChanged;
		}


		switch (lSelectedTool)
		{
			
			case Tool.Move:
				Vector3 lNewPosition = Handles.PositionHandle(lCurrentPosition, Quaternion.LookRotation(dirForward));
				if (lNewPosition != lCurrentPosition)
				{
					if (MustCreateNewNode)
					{
						MustCreateNewNode = false;
						PointAddCloned();
						//_selection = AddClonedNode(_selection);
						//_selection.SetDirection(_selection.Direction + newPosition - _selection.Position);
						//_selection.SetPosition(newPosition);
					}
					else
					{

						var guiEvent = Event.current;
						if (guiEvent.alt)
						{
							Target.PointMoveAll(idx, lNewPosition);
							isChanged = true;
							guiEvent.Use();
						}
						else
						{
							//_selection.SetDirection(_selection.Direction + newPosition - _selection.Position);
							//_selection.SetPosition(newPosition);
							Target.PointMove(idx, lNewPosition);
							isChanged = true;

						}


					}


				}
				
				//Handles.color = Handles.xAxisColor;
				//Vector3 lNewPosition = Handles.Slider(lCurrentPosition, Vector3.right, HandleUtility.GetHandleSize(lCurrentPosition) * 1f, Handles.ArrowHandleCap, 0.01f) ;
				//Handles.color = Handles.xAxisColor;
				//lNewPosition = Handles.Slider(lCurrentPosition, Vector3.up, HandleUtility.GetHandleSize(lCurrentPosition) * 1f, Handles.ArrowHandleCap, 0.01f) ;
				//Handles.color = Handles.yAxisColor;
				//lNewPosition = Handles.Slider(lCurrentPosition, Vector3.forward, HandleUtility.GetHandleSize(lCurrentPosition) * 1f, Handles.ArrowHandleCap, 0.01f) ;
				//if (lNewPosition != lCurrentPosition)
				//{
				//	Target.PointMove(idx, lNewPosition);
				//	isChanged = true;
				//}

				break;
			case Tool.Rotate:
				var nodeSetting = Target.MarkerSet[idx].CascadeNodeSetting;
				EditorGUI.BeginChangeCheck();
				nodeSetting.FlotSam = CircleHandle(lCurrentPosition, handleSize, nodeSetting.FlotSam, 0.005f);
				//nodeSetting.FlotSoam2 = CircleHandle(lCurrentPosition + Vector3.up * 0.5f, handleSize, nodeSetting.FlotSoam2, 0.005f);
				//nodeSetting.FlotSoam3 = CircleHandle(lCurrentPosition + Vector3.up * 1.0f, handleSize, nodeSetting.FlotSoam3, 0.005f);
				if (EditorGUI.EndChangeCheck())
				{
					Target.MarkerSet[idx].CascadeNodeSetting = nodeSetting;
					_repaint = true;
					isChanged = true;
				}
				break;
			case Tool.Scale:

				Handles.color = Color.magenta;
				var radius = Target.GetMarkerPathWidth(idx) * 0.5f;
				float radius1 = radius - 0.05f;
				Handles.CircleHandleCap(-1, lCurrentPosition, Quaternion.LookRotation(Vector3.up), radius, EventType.Repaint);
				Handles.CircleHandleCap(-1, lCurrentPosition, Quaternion.LookRotation(Vector3.up), radius1, EventType.Repaint);

				Handles.color = Color.cyan;

				var radiusHandlerCenterL = lCurrentPosition + dir2REdge * radius;
				//var radiusHandlerCenterR = lCurrentPosition - dir2REdge * radius;

				//var newPosL = Handles.FreeMoveHandle(radiusHandlerCenterL, Quaternion.identity, handleSize, Vector3.one,
				//	Handles.SphereHandleCap);
				//var newPosR = Handles.FreeMoveHandle(radiusHandlerCenterR, Quaternion.identity, handleSize, Vector3.one, Handles.SphereHandleCap);


				//var radius = Target.GetMarkerPathWidth(idx) * 0.5f;
				//var radiusHandlerCenterL = lCurrentPosition + Vector3.Cross(lselectedMarker.Tangent, Vector3.up).normalized * radius;

				Handles.color = Color.red;
				float size = HandleUtility.GetHandleSize(lCurrentPosition) * 1f;
				const float snap = 0.1f;
				EditorGUI.BeginChangeCheck();
				float newScale = Handles.ScaleSlider(lselectedMarker.Width, radiusHandlerCenterL, Vector3.forward, Quaternion.identity, size, snap);
				if (EditorGUI.EndChangeCheck())
				{

					var guiEvent = Event.current;
					if (guiEvent.alt)
					{
						Target.SetMarkersPathWidthAll(newScale);
						guiEvent.Use();
					}
					else
					{
						Target.SetMarkerPathWidth(Target.SplineSelectionInfo.IdxSelectedPoint, newScale);
					}

					_repaint = true;
					isChanged = true;
				}
				break;
		}
		

		// TODO Uncomment when junction added
		//bool HasSource = Target.MyJunctionIn.PathIn.Path != null;
		//if (HasSource && idx == 0)
		//{
		//	var sourceSpline = Target;
		//	var vPointL = sourceSpline.PathPointsLeft[0];
		//	var vPointR = sourceSpline.PathPointsRight[0];
		//	var direction = (vPointL - vPointR).normalized;
		//	//	var newvPointMin = Handles.PositionHandle(vPointL, Quaternion.LookRotation(direction));
		//	handleSize = HandleUtility.GetHandleSize(vPointL);
		//	Handles.color = Handles.zAxisColor;
		//	var newvPointMin  = Handles.Slider(vPointL, direction, handleSize, Handles.ArrowHandleCap, 0.01f);


		//	if (newvPointMin != vPointL)
		//	{
		//		var lLdirection = (newvPointMin - vPointL).normalized;
		//		var min =
		//			Target.MyJunctionIn.PathIn.Start * Target.MyJunctionIn.PathIn.Path.MeshSubDivision;
		//		var max =
		//			Target.MyJunctionIn.PathIn.End * Target.MyJunctionIn.PathIn.Path.MeshSubDivision;
		//		bool refresh = false;
		//		if (lLdirection == direction)
		//		{
		//			min -= 1;
		//			refresh = true;
		//		}
		//		else if(lLdirection == -direction)
		//		{
		//			min += 1;
		//			refresh = true;
		//		}

		//		if(refresh)
		//		{
		//			Target.MyJunctionIn.PathIn.Start -= 1;
		//			min = Mathf.Clamp((int)min, 0, Target.MyJunctionIn.PathIn.Path.MeshSubDivision);
		//			max = Mathf.Clamp((int)max, 0, Target.MyJunctionIn.PathIn.Path.MeshSubDivision);

		//			if (min == max)
		//			{
		//				if (min > 0)
		//					min--;
		//				else
		//					max++;
		//			}
		//			Target.MeshSubDivision = (int)(max - min);
		//			Target.MyJunctionIn.PathIn.Start = min / Target.MyJunctionIn.PathIn.Path.MeshSubDivision;
		//			Target.MyJunctionIn.PathIn.End = max / Target.MyJunctionIn.PathIn.Path.MeshSubDivision;

		//			Target.RefreshSplinePoint();
		//		}
		//	}
		//	Handles.color = Handles.zAxisColor;
		//	var newvPointMax = Handles.Slider(vPointR, -direction,handleSize, Handles.ArrowHandleCap, 0.01f);
		//	if (newvPointMax != vPointR)
		//	{
		//		var lRdirection = (newvPointMax - vPointR).normalized;
		//		var min =
		//			Target.MyJunctionIn.PathIn.Start * Target.MyJunctionIn.PathIn.Path.MeshSubDivision;
		//		var max =
		//			Target.MyJunctionIn.PathIn.End * Target.MyJunctionIn.PathIn.Path.MeshSubDivision;
		//		bool refresh = false;
		//		if (lRdirection == direction)
		//		{


		//			max -= 1;
		//			refresh = true;
		//		}
		//		else if (lRdirection == -direction)
		//		{
		//			max += 1;
		//			refresh = true;
		//		}

		//		if (refresh)
		//		{
		//			Target.MyJunctionIn.PathIn.Start -= 1;
		//			min = Mathf.Clamp((int)min, 0, Target.MyJunctionIn.PathIn.Path.MeshSubDivision);
		//			max = Mathf.Clamp((int)max, 0, Target.MyJunctionIn.PathIn.Path.MeshSubDivision);

		//			if (min == max)
		//			{
		//				if (min > 0)
		//					min--;
		//				else
		//					max++;
		//			}
		//			Target.MeshSubDivision = (int)(max - min);
		//			Target.MyJunctionIn.PathIn.Start = min / Target.MyJunctionIn.PathIn.Path.MeshSubDivision;
		//			Target.MyJunctionIn.PathIn.End = max / Target.MyJunctionIn.PathIn.Path.MeshSubDivision;

		//			Target.RefreshSplinePoint();
		//		}
		//	}

		//}




		//// Width Central Lane Handle
		//var handleSize = HandleUtility.GetHandleSize(lCurrentPosition) * Config.HandleSelectedSize * 0.5f;
		//Handles.color = Color.magenta;
		//var radius = Target.GetPathWidt() * 0.5f;
		//float radius1 = radius - 0.01f;
		//Handles.CircleHandleCap(-1, lCurrentPosition, Quaternion.LookRotation(Vector3.up), radius, EventType.repaint);
		//Handles.CircleHandleCap(-1, lCurrentPosition, Quaternion.LookRotation(Vector3.up), radius1, EventType.repaint);

		//Handles.color = Color.cyan;
		//var radiusHandlerCenterL = lCurrentPosition + Vector3.Cross(lselectedMarker.Tangent, Vector3.up).normalized * radius;
		//var radiusHandlerCenterR = lCurrentPosition - Vector3.Cross(lselectedMarker.Tangent, Vector3.up).normalized * radius;

		//var newPosL = Handles.FreeMoveHandle(radiusHandlerCenterL, Quaternion.identity, handleSize, Vector3.one,
		//	Handles.SphereHandleCap);
		//var newPosR = Handles.FreeMoveHandle(radiusHandlerCenterR, Quaternion.identity, handleSize, Vector3.one, Handles.SphereHandleCap);


		//var newRadiusL = Mathf.Abs(Vector3.Distance(newPosL, lCurrentPosition));
		//var newRadiusR = Mathf.Abs(Vector3.Distance(newPosR, lCurrentPosition));
		//if (newRadiusL > 0 && Mathf.Abs(newRadiusL - radius) > 0.01f)
		//{

		//	Target.SetPathWidt(newRadiusL * 2);


		//	isChanged = true;
		//	_repaint = true;
		//}
		//else if (newRadiusR > 0 && Mathf.Abs(newRadiusR - radius) > 0.01f)
		//{

		//	Target.SetPathWidt(newRadiusR * 2);

		//	isChanged = true;
		//	_repaint = true;
		//}



		return isChanged;
	}


	private string info;
	public override void SceneGUI_Draw_MarkerInfo(int index =-1)
	{
		//if (Target.ClosedSurface)
		//{
		//	//if (Target.CenterSurfaceAdded)
		//	//{
		//	//var mesg = string.Format("Lake Spawner");
		//	//Handles.Label(Target.CenterSurface + _widthLabelOffset, mesg, InspectorHelper.HandleLabelStyle);

		//	//}


		//	//var mesg = string.Format("Lake Spawner");
		//	//Handles.Label(Target.CenterSurface + _widthLabelOffset, mesg, InspectorHelper.HandleLabelStyle);


		//	return;
		//}


		int idx = index == -1 ? Target.SplineSelectionInfo.IdxSelectedPoint : index;

		if (idx == -1) return;
		var lSelectedMarker = Target.MarkerSet[idx];
		var lbackgroundColor = GUI.backgroundColor;


		var dis = Target.Spline.VertexGetLenght(idx);
		// Draw information
		GUI.backgroundColor = new Color(1f, 1f, 1f, 0.3f);

		if (Target.ClosedSurface)
		{
			info = string.Format("Flat Surface Spawner  #{0}", idx);

		}
		else
		{
				var drp = idx == 0 ? 0 : Target.Spline.VertexGetPosition(idx - 1).y - Target.Spline.VertexGetPosition(idx).y;
		
				info = string.Format("Marker #{0}\n\n Width : {1:0.##}m - Distance : {2:0.##}m\n\nDrop : {3:0.##}m", idx,
				lSelectedMarker.Width,dis , drp);
		}

		
	
		Handles.Label(lSelectedMarker.Position + _widthLabelOffset, info, esInspectorHelper.HandleLabelStyle);

		GUI.backgroundColor = lbackgroundColor;
	}

	public override void SceneGUI_Draw_Markers(Handles.CapFunction capFunction)
	{
		var markers = Target.MarkerSet;
		if (Target.ClosedSurface /*&& Target.CenterSurfaceAdded*/)
		{
			//var centerPosition = Target.CenterSurface;
			//var newPos =Handles.PositionHandle(centerPosition,Quaternion.identity);
			//if(newPos!= centerPosition)
			//{
			//	Target.CenterSurface = newPos;
			//	Target.MeshClosedRefresh();
			//	return;
			//}
			for (int i = 0; i < markers.Count; i++)
			{
				var lPosition = markers[i].Position;
				var lHandleSize = Config.HandleSize * HandleUtility.GetHandleSize(lPosition);
			
				Handles.color = GetMarkerColor(markers[i].StreamType);
				
				if (i == Target.SplineSelectionInfo.IdxSelectedPoint)
				{
					Handles.Button(lPosition, Quaternion.identity, lHandleSize, lHandleSize * 1.2f, Handles.CubeHandleCap);

					Target.RepaintCustomInspector = true;
				}
				else if (Handles.Button(lPosition, Quaternion.identity, lHandleSize, lHandleSize, capFunction))
				{
					Target.SplineSelectionInfo.IdxSelectedPoint = i;
				}

			}

			return;
		}


		for (int i = 0; i < markers.Count; i++)
		{
			var lPosition = markers[i].Position;
			var lHandleSize = Config.HandleSize * HandleUtility.GetHandleSize(lPosition);
			var isCriticalPoint = !Target.CheckSlope(i);


			Handles.color = i == 0 ? Color.cyan :
						isCriticalPoint ? Color.red :
						GetMarkerColor(markers[i].StreamType);
	
			lHandleSize = isCriticalPoint ? lHandleSize * 1.5f : lHandleSize;
			if (i == Target.SplineSelectionInfo.IdxSelectedPoint)
			{
				Handles.Button(lPosition, Quaternion.identity, lHandleSize, lHandleSize * 1.2f, Handles.CubeHandleCap);
			
				Target.RepaintCustomInspector = true;
			}
			else if (Handles.Button(lPosition, Quaternion.identity, lHandleSize, lHandleSize, capFunction))
			{
				Target.SplineSelectionInfo.IdxSelectedPoint = i;
			}

		

			// Middle points
			//if (false && i < markers.Count - 1)
			//{

			//	var dis = Target.Spline.VertexGetLenght(i) +
			//		(Target.Spline.VertexGetLenght(i + 1) - Target.Spline.VertexGetLenght(i)) * 0.5f;
			//	var t = Target.Spline.LengthToTime(dis);
			//	var lMiddlepointPosition = Target.Spline.EvalPosition(t);

			//	// Draw help message
			//	Ray mouseRay = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
			//	float middlePointVirtualPlaneHeight = lMiddlepointPosition.y;
			//	float dstToDrawPlane = (middlePointVirtualPlaneHeight - mouseRay.origin.y) / mouseRay.direction.y;
			//	Vector3 mousePosition = mouseRay.GetPoint(dstToDrawPlane);
			//	if (Vector3.Distance(mousePosition, lMiddlepointPosition) < lHandleSize)
			//	{

			//		//var lbackgroundColor = GUI.backgroundColor;
			//		//GUI.backgroundColor = InspectorHelper.menuBackgroundColor;
			//		var info = "Click Left Mouse Buttom to insert a new point " + i + " n";
			//		Handles.Label(lMiddlepointPosition + _widthLabelOffset, info, InspectorHelper.HandleLabelStyle);
			//		//GUI.backgroundColor = lbackgroundColor;
			//	}



			//	Handles.color =Color.gray;
			//	if (Handles.Button(lMiddlepointPosition, Quaternion.identity, lHandleSize, lHandleSize, Handles.CubeHandleCap))
			//	{


			//	}


			//}



		}
	}

	public Color GetMarkerColor(WaterStreamType type)
	{
		var color = Config.HandleColor;

		switch (type)
		{

			case WaterStreamType.River:
				color = new Color(0.0f, 1.0f, 0.0f);
				break;
			case WaterStreamType.WaterfallStart:
				color = new Color(0.5f, 0.23f, 0.58f);
				break;
			case WaterStreamType.WaterfallEnd:
				color = new Color(1f, 0.05f, 0.88f, 0.99f);
				break;
			case WaterStreamType.Lake:
				color = new Color(0, 0, 1, 1); ;
				break;

		}


		return color;
	}

	private float CircleHandle(Vector3 lCurrentPosition, float handleSize, float currentValue, float snap = 0.001f,float viewRadius = 4f)
	{
		var viewAngle = 360 * currentValue;
		Vector3 viewAngleA = new Vector3(Mathf.Sin(-viewAngle * 0.5f * Mathf.Deg2Rad), 0, Mathf.Cos(-viewAngle * 0.5f * Mathf.Deg2Rad));
		Vector3 viewAngleB = new Vector3(Mathf.Sin(viewAngle * 0.5f * Mathf.Deg2Rad), 0, Mathf.Cos(viewAngle * 0.5f * Mathf.Deg2Rad));

		Handles.color = new Color(1, 1, 1, 0.5f);
		

		Handles.DrawSolidArc(lCurrentPosition, Vector3.up, viewAngleA, viewAngle, viewRadius);
		Handles.color = Color.red;
		Handles.DrawWireArc(lCurrentPosition, Vector3.up, Vector3.right, 360, viewRadius);
		Handles.DrawLine(lCurrentPosition, lCurrentPosition + viewAngleA * viewRadius);
		Handles.DrawLine(lCurrentPosition, lCurrentPosition + viewAngleB * viewRadius);

		Handles.Label(lCurrentPosition, currentValue.ToString("f2"));

		Handles.FreeRotateHandle(Quaternion.identity, lCurrentPosition, handleSize);

		Handles.color = Color.green;
		var currentCirclePosition = lCurrentPosition + viewAngleB * viewRadius;
		var newPosCircle = Handles.FreeMoveHandle(currentCirclePosition, Quaternion.identity, handleSize, Vector3.one,
			Handles.SphereHandleCap);
		if (newPosCircle != currentCirclePosition)
		{
			currentValue = Mathf.Clamp01(currentValue + snap);
		}

		Handles.color = Color.magenta;
		var currentCirclePositionDec = lCurrentPosition + viewAngleA * viewRadius;
		var newPosCircleDec = Handles.FreeMoveHandle(currentCirclePositionDec, Quaternion.identity, handleSize, Vector3.one,
			Handles.SphereHandleCap);
		if (newPosCircleDec != currentCirclePositionDec)
		{
			currentValue = Mathf.Clamp01(currentValue - snap);
		}

		return currentValue;
	}


	#endregion

	#region Vertices Operations


	void PointAddCloned()
	{
		int idx = Target.SplineSelectionInfo.IdxSelectedPoint;
		var lselectedMarker = Target.MarkerSet[idx];
		Target.NodeAdd(lselectedMarker.Position, idx);
		Target.SplineSelectionInfo.IdxSelectedPoint = idx;
		//if (idx == TargetRiverPath.MarkerSet.Count - 1)
		//{
		//	TargetRiverPath.PointAdd(lselectedMarker.Position, idx);
		//}
		//else
		//{
		//	//TargetRiverPath.PointInsert(index + 1, res);
		//}


		//PathPoint res = new PathPoint();
		//int newPointIndex = TargetRiverPath.MarkerSet.Count;
		//Undo.RecordObject(TargetRiverPath, "Add River Point");
		//TargetRiverPath.PointAdd(position, newPointIndex);
		//TargetRiverPath.SplineSelectionInfo.IdxSelectedPoint = newPointIndex;


		//return res;
	}

	//PathPoint AddClonedNode(PathPoint node)
	//{

	//	int index = Target.MarkerSet.IndexOf(node);
	//	PathPoint res = new PathPoint();
	//	res.SetPosition(node.Position);
	//	res.SetDirection(node.Direction);

	//	if (index == Target.MarkerSet.Count - 1)
	//	{
	//		//TargetRiverPath.PointAdd(res);
	//	}
	//	else
	//	{
	//		//TargetRiverPath.PointInsert(index + 1, res);
	//	}
	//	return res;
	//}

	private bool PointAdd()
	{
		Vector3 position;
		Vector2 positionUV;
		if (RaycastMousePosition(out position, out positionUV))
		{
			int newPointIndex = Target.MarkerSet.Count;
			Undo.RecordObject(Target, "Add Cascade Marker");
			Target.NodeAdd(position, newPointIndex);
			Target.SplineSelectionInfo.IdxSelectedPoint = newPointIndex;
			return true;
		}

		return false;
	}

	private bool PointInsert()
	{
		if (Target.ClosedSurface || Target.SplineSelectionInfo.IdxSelectedPoint < 1) return false;
		//Undo.RecordObject(Target, "Insert River Point");
		Target.SplineSelectionInfo.IdxSelectedPoint = Target.PointAddBeforeSelected( Target.SplineSelectionInfo.IdxSelectedPoint); ;
		return true;
	}

	private bool PointDelete()
	{
		if (Target.SplineSelectionInfo.IdxSelectedPoint < 0) return false;

		bool alert = false;
		if (Target.Cascades.Count > 0)
		{

			for (int i = 0; i < Target.Cascades.Count; i++)
			{
				if(alert) continue;
				if (Target.Cascades[i].IsActive)
				{
					alert = true;
				}
			}
		}
		if (alert)
		{
			EditorUtility.DisplayDialog("Cascade Message", "You need to disable lake segment before delete a node", "OK");
			return false;
		}

		Target.SplineSelectionInfo.IdxSelectedPoint = Target.PointDelete(Target.SplineSelectionInfo.IdxSelectedPoint);

		return true;
	}

	private bool PointAlignGround()
	{
		if (Target.PointAlignGround(Target.SplineSelectionInfo.IdxSelectedPoint))
		{
			_sceneDirty = true;
			_repaint = true;
		}
		return true;
	}

	private bool PointAlignAllHeightToSelected()
	{
		if (Target.PointAlignAllHeightToSelected(Target.SplineSelectionInfo.IdxSelectedPoint))
		{
			_sceneDirty = true;
			_repaint = true;
		}
		return true;
	}

	private bool PointAlignAllGround()
	{
		if (Target.PointAlignAllGround())
		{
			_sceneDirty = true;
			_repaint = true;
		}
		return true;
	}

	#endregion

}