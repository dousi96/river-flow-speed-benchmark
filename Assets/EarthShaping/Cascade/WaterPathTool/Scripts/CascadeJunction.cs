﻿/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/
using System;

namespace com.earthshaping.Cascade
{
	public class CascadeJunction
	{
		public JunctionData PathIn = new JunctionData()
		{
			Start = 0.0f,
			End = 1.0f
		};
		public JunctionData PathOut = new JunctionData()
		{
			Start = 0.0f,
			End = 1.0f
		};

	}
	[Serializable]
	public class JunctionData
	{
		public RiverPath Path;
		public float Start;
		public float End;
		public float GetWidth { get { return End - Start; } }
		public bool HasPath { get { return Path != null; } }
	}

}