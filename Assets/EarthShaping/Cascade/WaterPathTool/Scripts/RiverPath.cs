﻿/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/
#define SENTIERI_BUILDER__
#define MESH_PAINTER__
using System;
using System.Collections.Generic;
using System.Linq;
using com.earthshaping.Sentieri.Geometry;
using com.earthshaping.framework;
using com.earthshaping.framework.DataStructure;

using com.earthshaping.framework.Geometry;
using com.earthshaping.framework.Landscape;
using com.earthshaping.framework.Terrain;
using com.earthshaping.framework.Textures;
using com.earthshaping.framework.Utility;
using com.earthshaping.Geometry;
using com.earthshaping.Helper.Extension;
using com.earthshaping.ProceduralMesh;
using com.earthshaping.Integration;
using com.earthshaping.Sentieri;
using com.earthshaping.Sentieri.ProceduralMesh;
using com.earthshaping.Setting;
using JBooth.River;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Rendering;
using com.earthshaping.framework.Audio;
using com.earthshaping.Terrains;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace com.earthshaping.Cascade
{
	[ExecuteInEditMode]
	public class RiverPath : MonoBehaviour
	{
		#region Variables Declaration

		public string Id = "NA";
#if SENTIERI_BUILDER
		//public SentieriBuilder sentieriBuilder;
#endif
		
		public bool MeshMode;
		public bool DesignModeIsOn = false;
		public bool HideWireFrameInEditor;
		public LayerMask Meshlayers;
		public SplineSelectionInfo SplineSelectionInfo = new SplineSelectionInfo();

		public GameObject cascadeRoot;
		public GameObject probesRoot;
		public GameObject audioRoot;

		public string FriendlyName
		{
			get { return gameObject.name; }
			set { gameObject.name = value; }
		}

		private readonly CascadePath _cascadePath = new CascadePath();
		public SmoothAlgorithm SmoothType = SmoothAlgorithm.Lerp;

		public Material RiverMaterial;
		private Material _riverBedDesignMaterial;
		private Material _surfaceDesignMaterial;

		public bool Conformed = false;
		public bool RefreshAll;


		[SerializeField] private List<TerrainAreaData> _blendMap;

		public List<TerrainAreaData> BlendMap
		{
			get { return _blendMap; }
			set { _blendMap = value; }
		}

		public float MeshResolution = 2f; //0.25f;
		public int MeshSubDivision = 3;
		public float UVScale = 25;
		public float Distorsion = 0.5f;
		public bool FlipClockWise;

		[SerializeField] private CascadeShaderSetting _shaderSetting;

		public CascadeShaderSetting ShaderSetting
		{
			get { return _shaderSetting; }
			set { _shaderSetting = value; }
		}

		public float PathDefaultWidth = 8f;
		public float RiverDefaultDepth = 1.5f;
		public float RiverWidthOffset = 0.0f;
		public float MeshYGroundOffset;

		public bool AddCollider;
		public string Tag;

		//[NonSerialized]
		public List<Vector3> tempPathPoints = new List<Vector3>();

		public bool GizmosShowSurfaceSpawner;
		public float GizmosSurfaceSpawnerSize;
		public Color GizmosSurfaceSpawnerColor = Color.red;
		public int trianglesCount;
		public int verticesCount;
		public float PathLenght;
		public float AreaSurface;
		public bool EnableMeshPainter;
		public float StampResolution = 1f;

		public bool HideMesh;
		public bool KeepSize;

		[SerializeField] private bool _canEditSpline = true;

		public bool CanEditSpline
		{
			get { return /*!_closedSurface &&*/ _canEditSpline; }
			set { _canEditSpline = value; }
		}

		//Closed surface
		[SerializeField] private SurfaceMeshGenerator _lake = new SurfaceMeshGenerator();

		public SurfaceMeshGenerator Lake
		{
			get { return _lake; }
			set { _lake = value; }
		}
		[SerializeField] private SegmentSelection _pointSelection = SegmentSelection.All;
		public SegmentSelection PointSelection
		{
			get { return _pointSelection; }
			set { _pointSelection = value; }
		}
		[SerializeField] private CatmullRom _spline;
		public CatmullRom Spline
		{
			get { return _spline; }
		}

		[SerializeField] private CascadeProfile _cascadeProfile;
		public CascadeProfile Profile
		{
			get { return _cascadeProfile; }
			set { _cascadeProfile = value; }
		}

		[SerializeField] private CellCacheId[] cellCacheIds;
		public CellCacheId[] CellCacheIds
		{
			get { return cellCacheIds;}
			set {cellCacheIds = value;}
		}

		public bool RepaintCustomInspector;

		[SerializeField] private esLandscapeModifierData _landscapeModifier = new esLandscapeModifierData
		{
			ConformTerrain = true,
			LeftPadding = 0.01f,
			RightPadding = 0.01f,
			LeftBlendingCurve = AnimationCurve.EaseInOut(0, 0, 1, 1),
			RightBlendingCurve = AnimationCurve.EaseInOut(0, 0, 1, 1),
			RightSurronding = 8,
			LeftSurronding = 8,
			GroundOffset = 0.5f

		};

		public esLandscapeModifierData LandscapeModifier
		{
			get { return _landscapeModifier; }
			set { _landscapeModifier = value; }
		}

#if ESMESH_PAINTER
		public ESMeshPainter MeshPainter;
#endif
		public bool DebugFlowMode;

		[SerializeField] private CascadeNodeSetting _cascadeNodeSetting = new CascadeNodeSetting()
		{
			Turbolence = 1,
			Swirl = 1,
			Foam = 0,
			FlotSam = 1
		};

		public CascadeNodeSetting CascadeNodeSetting
		{
			get { return _cascadeNodeSetting; }
			set { _cascadeNodeSetting = value; }
		}

		/// <summary>
		/// List of Spline Markers ( read-only )
		/// </summary>
		public List<PathPoint> MarkerSet
		{
			get { return _spline != null ? _spline.MarkerSet : (_spline = new CatmullRom()).MarkerSet; }

		}

		/// <summary>
		/// Return number of markers
		/// </summary>
		public int MarkerCount
		{
			get { return _spline != null ? _spline.MarkerSet.Count : 0; }

		}

		public float findZoomDistance = 50;

		public PathPoint GetSelectedMarker
		{
			get { return _spline != null ? _spline.MarkerSet[SplineSelectionInfo.IdxSelectedPoint] : null; }
		}

		[SerializeField] private ESMinMaxRange _slopeRange = new ESMinMaxRange(0.05f, 0.15f);

		public ESMinMaxRange slopeRange
		{
			get { return _slopeRange; }
			set { _slopeRange = value; }
		}


		public bool EnableAudioManager;

		// LightProbe
		public float ReflectionProbeDistance = 50f;
		public float ReflectionProbeHeight = 3.5f;
		[SerializeField] private List<ReflectionProbe> _reflectionProbes = new List<ReflectionProbe>();

		public List<ReflectionProbe> ReflectionProbes
		{
			get { return _reflectionProbes; }
			set { _reflectionProbes = value; }
		}

		[SerializeField] private readonly CascadeJunction _myJunctionIn = new CascadeJunction();
		public CascadeJunction MyJunctionIn
		{
			get { return _myJunctionIn; }
		}

		[SerializeField] private bool _closedSurface;

		public bool ClosedSurface
		{
			get { return _closedSurface; }
			set { _closedSurface = value; }
		}

		private MeshFilter _meshFilter;
		private MeshRenderer _meshRenderer;
		private MeshCollider _meshCollider;

		private readonly List<IntVector2> _waterfallIndices = new List<IntVector2>();

		private readonly List<IntVector2> _lakeIndices = new List<IntVector2>();
		public List<IntVector2> LakeIndices
		{
			get { return _lakeIndices; }
		}
		List<ESOrientedPoint> _cachePoints = new List<ESOrientedPoint>();
		public List<CascadeMarker> Cascades = new List<CascadeMarker>();

// Cap Variables
		public bool EnableStartCap;
		public AnimationCurve CapStartProfile = AnimationCurve.EaseInOut(0, 0, 0, 1);
		public bool EnableEndCap;
		public AnimationCurve CapEndProfile = AnimationCurve.EaseInOut(0, 0, 0, 1);
		public int CapSegments = 4;
		public int CapSubdivision = 1;

		// Audio Manager
		public int AudioNodeFrequency = 1;
		public esAudioManager audioManager = new esAudioManager();
		[SerializeField] private esAudioSourceSettings _defaultAudioSetting = new esAudioSourceSettings()
		{
			volume = 0.2f,
			priority = 193,
			playOnAwake = true,
			loop = true,
			minDistance = 1,
			maxDistance = 100,
			spatialBlend = 0.95f,
			dopplerLevel = 0,
			rolloffMode =  AudioRolloffMode.Custom
		};
		public esAudioSourceSettings DefaultAudioSetting
		{
			get { return _defaultAudioSetting; }
			set { _defaultAudioSetting = value; }
		}

		#endregion

		#region Unity Default Events


		private void OnDisable()
		{
			//#if UNITY_EDITOR
			//EditorApplication.update -= UpdateEditorCallback;
			//#endif
			CascadeManager.CascadeBuilders.Remove(this);
		}

		private void OnEnable()
		{
			if (Id == "NA")
				Id = framework.Util.Utility.GetUniqueID();
			//EditorApplication.update += UpdateEditorCallback;
			CascadeManager.CascadeBuilders.Add(this);

		}

		//		private float time = 0;
		//		private void UpdateEditorCallback()
		//		{
		//			//if (Time.realtimeSinceStartup - time > 0.15)
		//			//{
		//				//Debug.Log(Time.realtimeSinceStartup - time);
		//				if (update)
		//				{
		//					GenerateMesh();
		//					update = false;
		//				}

		//			//	time = Time.realtimeSinceStartup;

		//			//}

		//		}

		private void Awake()
		{

#if SENTIERI_BUILDER
			//if (sentieriBuilder == null)
			//{
			//	sentieriBuilder = GetComponent<SentieriBuilder>();
			//	sentieriBuilder.Init();
			//}
#endif
			Init();

		}



		#endregion

		#region Builder Manager
		//public esRouting SentieriPathGenerator;
		public static RiverPath CreateCascadeBuilder(bool closedSurface)
		{

			// Check for an active terrain
			var myTerrain = Terrain.activeTerrain;
			if (myTerrain == null)
			{
#if UNITY_EDITOR
				EditorUtility.DisplayDialog("Alert",
					"Cascade needs at least one active terrain in the scene.\nPlease add a terrain and try again", "OK");
#endif
				return null;
			}
			var lRootObject = GameObject.Find("Cascade Landscape");
			if (lRootObject == null)
			{
				lRootObject = new GameObject("Cascade Landscape");
				lRootObject.transform.position = Vector3.zero;
				lRootObject.transform.rotation = Quaternion.identity;
				lRootObject.AddComponent<CascadeManager>();

				//var lScanObj = new GameObject("Scan Grid Area");
				//lScanObj.transform.parent = lRootObject.transform;
				//lScanObj.AddComponent<esScanGridArea>();

			}

			var count = lRootObject.transform.childCount;

			var pathType = !closedSurface ? "River" : "Lake";
			var objectBuilderName = pathType + " Cascade Builder : " + count.ToString("D2");
	
			var builderObj = new GameObject(objectBuilderName);
			builderObj.transform.parent = lRootObject.transform;
			builderObj.transform.position = Vector3.zero;

			var builder = builderObj.AddComponent<RiverPath>();
			//builder.cascadeRoot = cascadeRoot;
			builder.ClosedSurface = closedSurface;

			//{
			//	var router = builder.gameObject.AddComponent<esRouting>();
			//	router.Init();
			//	builder.SentieriPathGenerator = router;
			//	var lAreaProjector = FindObjectOfType<esScanGridArea>();
			//	if (lAreaProjector == null)
			//	{
			//		Debug.LogError("Sentieri Scanner is not in the scene ");
			//		return builder;
			//	}
				
			//	////TODO add multitile
			//	var lTerrainBounds = ESTerrainUtility.GetLandscapeBounds(esLandscapeManager.ESTerrains);
			//	router.scanGridArea.ScannerWidth = (int)lTerrainBounds.extents.x * 2;
			//	router.scanGridArea.ScannerHeight = (int)lTerrainBounds.extents.z * 2;

			//	var scanWidth = router.scanGridArea.ScannerWidth;
			//	var scanDepth = router.scanGridArea.ScannerHeight;

			//	//Target.ScanGridCenter.x = myTerrain.transform.position.x * 0.5f;
			//	//Target.ScanGridCenter.z = myTerrain.transform.position.z * 0.5f;
			//	router.scanGridArea.ScanGridCenter = lAreaProjector.GetCenterPosition();
			//	var rectX = router.scanGridArea.ScanGridCenter.x - router.scanGridArea.ScannerWidth * 0.5f;
			//	var rectZ = router.scanGridArea.ScanGridCenter.z - router.scanGridArea.ScannerHeight * 0.5f;
			//	lAreaProjector.SetAreaRect(new Rect(rectX, rectZ, scanWidth, scanDepth), router.scanGridArea.ScanGridCenter);
			//	router.SetScanGridArea(lAreaProjector);

			//}

#if UNITY_EDITOR
			Selection.activeGameObject = builderObj;
#endif
			return builder;
		}


		#endregion

		#region Init Components

		public void Init()
		{
			if (cascadeRoot == null)
			{
				cascadeRoot = new GameObject("Cascade Surface");
				cascadeRoot.transform.position = Vector3.zero;
				cascadeRoot.transform.parent = transform;

				_meshFilter = cascadeRoot.AddComponent<MeshFilter>();
				_meshCollider = cascadeRoot.AddComponent<MeshCollider>();

				MeshRenderer meshRenderer = cascadeRoot.AddComponent<MeshRenderer>();
				meshRenderer.receiveShadows = false;
				meshRenderer.shadowCastingMode = ShadowCastingMode.Off;
#if UNITY_EDITOR
				if (meshRenderer.sharedMaterial == null)
					meshRenderer.sharedMaterial = AssetDatabase.GetBuiltinExtraResource<Material>("Default-Diffuse.mat");
				cascadeRoot.AddComponent<RiverRenderer>();
#endif
			}
			else
			{
				_meshFilter = cascadeRoot.GetComponent<MeshFilter>();
				_meshCollider = cascadeRoot.GetComponent<MeshCollider>();
				_meshRenderer = cascadeRoot.GetComponent<MeshRenderer>();
			}

			CheckReflectionProbesRoot();
			CheckAudioManagerRoot();

			CheckAssignedMaterial();

			// Init the Spline 
			if (_spline == null)
				_spline = new CatmullRom();
			else
				_spline.MarkerInitialitation();

			if (SplineSelectionInfo == null)
				SplineSelectionInfo = new SplineSelectionInfo();

			if (CellCacheIds == null)
				CellCacheIds = new CellCacheId[0];

			InitCascadeProfile();

		}

		private TerrainHolderData LoadOrCreateTerrainHolderData(int arg1)
		{
			return null;
		}

		private void InitCascadeProfile()
		{

			if (_cascadeProfile != null) return;

			_cascadeProfile = ScriptableObject.CreateInstance<CascadeProfile>();

			var mat = new ElementMaterial
			{
				UVScale = this.UVScale

			};

			var river = new RiverItem
			{
				GlobalWitdh = PathDefaultWidth,
				ElementMaterial = mat,
				IsActive = true,
				TerrainHeightOffset = 0.7f
			};
			_cascadeProfile.RiverSetting = river;

			var bed = new RiverBedItem
			{
				Shape2DProfile = MeshPrimitives.RiverBed1(),
				MeshSubdvision = 12,
				ElementMaterial = new ElementMaterial
				{
					UVScale = 1
				}
			};
			_cascadeProfile.RiverBedSetting = bed;

		}

		private void CheckAssignedMaterial()
		{
#if UNITY_EDITOR

			if (RiverMaterial == null)
			{
				//DefaultMaterial
				RiverMaterial =
					Resources.Load<Material>(
						"CascadeDesign"); //AssetDatabase.GetBuiltinExtraResource<Material>("Default-Diffuse.mat");
				var waterRenderer = cascadeRoot.GetComponent<MeshRenderer>();
				waterRenderer.sharedMaterial = RiverMaterial;
			}

			if (_riverBedDesignMaterial == null)
				_riverBedDesignMaterial = Resources.Load<Material>("WireframeBed");

			if (_surfaceDesignMaterial == null)
				_surfaceDesignMaterial = Resources.Load<Material>("WireframeSurface");

			//#else
			//			riverBedDesignMaterial = AssetDatabase.GetBuiltinExtraResource<Material>("Default-Diffuse.mat");
			//			surfaceDesignMaterial = AssetDatabase.GetBuiltinExtraResource<Material>("Default-Diffuse.mat");


#endif
		}

		public void CreateCascadeShader()
		{

#if UNITY_EDITOR

			string path =
				EditorUtility.SaveFilePanelInProject("Save Cascade Material", "cascade", "shader", "Save Cascade Material");

			if (path.Length != 0)
			{

				string shaderName = com.earthshaping.framework.Extension.ESAssetTool.GetAssetNameAndDirectory(ref path);
				// Create material
				Material lMaterial = new Material(ShaderInterface.CreateShader(path, shaderName));
				// Save Material

				string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + shaderName + ".mat");

				AssetDatabase.CreateAsset(lMaterial, assetPathAndName);
				AssetDatabase.Refresh();
				AssetDatabase.SaveAssets();
				RiverMaterial = AssetDatabase.LoadAssetAtPath<Material>(assetPathAndName);
				var waterRenderer = cascadeRoot.GetComponent<MeshRenderer>();
				waterRenderer.sharedMaterial = RiverMaterial;
			}



#endif
		}




		#endregion

		#region Support Node Creation

		private PathPoint startMarker;
		private PathPoint endMarker;

		public void SetDesignMode()
		{

			if (ClosedSurface)
			{
				DesignModeIsOn = false;
				return;
			}

			if (MarkerCount < 1) return;
			startMarker = new PathPoint {Position = _spline.GetFirstMarker.Position};
			endMarker = new PathPoint {Position = _spline.GetLatestMarker.Position};

			if (startMarker == null || endMarker == null)
			{
				return;
			}

			CascadePath.Init(cascadeRoot, _surfaceDesignMaterial, _riverBedDesignMaterial);
			CascadePath.SetDesignMode(DesignModeIsOn,_spline.MarkerSet, startMarker, endMarker);

			RefreshAll = DesignModeIsOn;
		}

		#endregion

		#region Marker and Path Properties Get/Set

		public float PathSelectedDepth(int selected)
		{
			return _spline.MarkerSet[selected].Depth;
		}

		public float PathSelectedWidth(int selected)
		{
			return _spline.MarkerSet[selected].Width;
		}

		public void SetMaterial(Material material)
		{
			cascadeRoot.GetComponent<MeshRenderer>().material = material;
		}

		public float GetMarkerPathWidth(int selectedMarker)
		{
			return _spline.MarkerSet[selectedMarker].Width;
		}

		public void InvertPath()
		{
			_spline.MarkerSet.Reverse();

			//List<PathPoint> tPath = new List<PathPoint>(_spline.MarkerSet.Count);
			//for (int i = 0; i < _spline.MarkerSet.Count; i++)
			//{
			//	tPath.Add(_spline.MarkerSet[_spline.MarkerSet.Count - i-1]);
			//}

			RefreshCascadePath();
		}

		/// <summary>
		/// Set the witdh for selected marker
		/// </summary>
		/// <param name="selectedIndex"></param>
		/// <param name="width"></param>
		public void SetMarkerPathWidth(int selectedIndex, float width)
		{
			_spline.MarkerSet[selectedIndex].Width = width;
		}

		public void SetMarkersPathWidthAll(float width)
		{
			for (int i = 0; i < _spline.MarkerSet.Count; i++)
			{
				SetMarkerPathWidth(i, width);
			}

			PathDefaultWidth = width;
		}

		public void SetMarkerRiverHeight(float y)
		{
			_spline.MarkerSet[SplineSelectionInfo.IdxSelectedPoint].Position += Vector3.up * y;
			_spline.VertexSetPosition(SplineSelectionInfo.IdxSelectedPoint,
				_spline.MarkerSet[SplineSelectionInfo.IdxSelectedPoint].Position);
		}

		/// <summary>
		/// Apply Default path with to all marker
		/// </summary>
		public void SetMarkersPathWidth()
		{
			for (int i = 0; i < _spline.MarkerSet.Count; i++)
			{
				SetMarkerPathWidth(i, PathDefaultWidth);
			}
		}

		public void SetMarkerRiverDepth(float depth, int selectedIndex)
		{
			_spline.MarkerSet[selectedIndex].Depth = depth;
			//PointDisplaceDepth(selectedIndex);

		}

		public void SetMarkersRiverDepth()
		{
			for (int i = 0; i < _spline.MarkerSet.Count; i++)
			{
				SetMarkerRiverDepth(RiverDefaultDepth, i);
			}

		}

		public void SetMarkersCascadeNodeSetting(CascadeNodeSetting nodeSetting)
		{
			_cascadeProfile.GlobalNodeSetting = nodeSetting;
			for (int i = 0; i < _spline.MarkerSet.Count; i++)
			{
				SetMarkerCascadeNodeSetting(nodeSetting, i);
			}

		}

		public void SetMarkerCascadeNodeSetting(CascadeNodeSetting nodeSetting, int selectedIndex)
		{
			_spline.MarkerSet[selectedIndex].CascadeNodeSetting = nodeSetting;
		}

		public void SetMarkersLandscapeModifierDefault()
		{
			for (int i = 0; i < MarkerSet.Count; i++)
			{

				_spline.MarkerSet[i].LandscapeModifier = new esLandscapeModifierData
				{
					ConformTerrain = LandscapeModifier.ConformTerrain,
					LeftPadding = LandscapeModifier.LeftPadding,
					RightPadding = LandscapeModifier.RightPadding,
					LeftSurronding = LandscapeModifier.LeftSurronding,
					RightSurronding = LandscapeModifier.RightSurronding,
					LeftBlendingCurve = esAnimationCurveUtility.CreateCopy(LandscapeModifier.LeftBlendingCurve),
					RightBlendingCurve = esAnimationCurveUtility.CreateCopy(LandscapeModifier.RightBlendingCurve),
				};




			}
		}

		public void SetMarkersDefaultProfile2DMeshCurves()
		{
			for (int i = 0; i < MarkerSet.Count; i++)
			{
				_spline.MarkerSet[i].Shape2DProfile =
					esAnimationCurveUtility.CreateCopy(_cascadeProfile.RiverSetting.Shape2DProfile);

			}

		}

		public void SetMarkerDefaultRiverBedProfile()
		{
			for (int i = 0; i < MarkerSet.Count; i++)
			{
				_spline.MarkerSet[i].Shape2DProfile1 =
					esAnimationCurveUtility.CreateCopy(_cascadeProfile.RiverBedSetting.Shape2DProfile);

			}

		}


		private void RefreshCachePoints()
		{

			
			_cascadePath.Init(_spline, _cascadeProfile, _lakeIndices, SmoothType, DesignModeIsOn, RiverWidthOffset);
			var lSplineSampleRes = MeshResolution; //1.0f + (1.0f - VResolution)*MaxVRes;
			_cachePoints = _cascadePath.GetSplineOrientedPoints(
				lSplineSampleRes, CascadeManager.MultiThread, CascadeManager.EnableRealTimeScanCell).ToList();

			RefreshCellsCacheId();

		}

		void RefreshCellsCacheId()
		{
			// Refresh Terrain Cells
			var cacheId = new List<CellCacheId>();
			for (int i = 0; i < _cachePoints.Count; i++)
			{
				var cell = _cachePoints[i].CellCacheId;
				if (cell.IsValid && cacheId.Contains(cell) == false)
					cacheId.Add(cell);
			}

			CellCacheIds = cacheId.ToArray();
		}

		#endregion

		#region Marker Point Edit

		public void NodeAdd(Vector3 position, int newPointIndex)
		{
			if (_spline == null)
				_spline = new CatmullRom();

			if (ClosedSurface)
			{
				position.y = MarkerSet.Count == 0 ? position.y : MarkerSet[0].Position.y;
			}

			PathPoint marker = new PathPoint
			{
				Position =
					position, //new Vector3(position.x, MarkerSet.Count > 0 ? MarkerSet[newPointIndex - 1].Position.y : position.y,position.z),
				Width = MarkerSet.Count > 0 ? MarkerSet[newPointIndex - 1].Width : PathDefaultWidth,
				StreamType = WaterStreamType.River,
				Depth = MarkerSet.Count > 0 ? MarkerSet[newPointIndex - 1].Depth : RiverDefaultDepth,

				Shape2DProfile1 = MarkerSet.Count > 0
					? esAnimationCurveUtility.CreateCopy(MarkerSet[newPointIndex - 1].Shape2DProfile1)
					: esAnimationCurveUtility.CreateCopy(_cascadeProfile.RiverBedSetting.Shape2DProfile),

				Shape2DProfile = MarkerSet.Count > 0
					? esAnimationCurveUtility.CreateCopy(MarkerSet[newPointIndex - 1].Shape2DProfile)
					: esAnimationCurveUtility.CreateCopy(_cascadeProfile.RiverSetting.Shape2DProfile),

				CascadeNodeSetting = _cascadeProfile.GlobalNodeSetting,

				Rotation = Quaternion.identity // Already default . I can remove. Check it out

			};


			_spline.MarkerSet.Insert(newPointIndex, marker);
			_spline.MarkerSet[newPointIndex].LandscapeModifier = new esLandscapeModifierData()
			{
				ConformTerrain = true,
				LeftPadding =
					newPointIndex > 0 ? MarkerSet[newPointIndex - 1].LandscapeModifier.LeftPadding : LandscapeModifier.LeftPadding,
				RightPadding = newPointIndex > 0
					? MarkerSet[newPointIndex - 1].LandscapeModifier.RightPadding
					: LandscapeModifier.RightPadding,
				LeftSurronding = newPointIndex > 0
					? MarkerSet[newPointIndex - 1].LandscapeModifier.LeftSurronding
					: LandscapeModifier.LeftSurronding,
				RightSurronding = newPointIndex > 0
					? MarkerSet[newPointIndex - 1].LandscapeModifier.RightSurronding
					: LandscapeModifier.RightSurronding,
				LeftBlendingCurve = newPointIndex > 0
					? esAnimationCurveUtility.CreateCopy(MarkerSet[newPointIndex - 1].LandscapeModifier.LeftBlendingCurve)
					: LandscapeModifier.LeftBlendingCurve,
				RightBlendingCurve = newPointIndex > 0
					? esAnimationCurveUtility.CreateCopy(MarkerSet[newPointIndex - 1].LandscapeModifier.RightBlendingCurve)
					: LandscapeModifier.RightBlendingCurve
			};


			//_spline.MarkerSet[newPointIndex].Position = PositionTopDownHill(newPointIndex, position);
			_spline.VertexAddLast(_spline.MarkerSet[newPointIndex].Position);
			PointHeigthSet(newPointIndex, _spline.MarkerSet[newPointIndex].Position.y);

			if (DesignModeIsOn)
			{
				CascadePath.Init(cascadeRoot, _surfaceDesignMaterial, _riverBedDesignMaterial);
				CascadePath.AddNode(_spline.MarkerSet[newPointIndex], newPointIndex);
			}

		}


		/// <summary>
		/// Insert a new marker for Cascade Open Surface
		/// </summary>
		/// <param name="selectedIndex"></param>
		/// <returns></returns>
		public int PointAddBeforeSelected(int selectedIndex)
		{
			var prevPoint = _spline.MarkerSet[selectedIndex - 1].Position;
			var selectedPoint = _spline.MarkerSet[selectedIndex].Position;

			PathPoint newMarker = new PathPoint
			{
				Position = Vector3.LerpUnclamped(selectedPoint, prevPoint, 0.5f),
				Width = PathDefaultWidth,
				StreamType = WaterStreamType.River,
				Depth = MarkerSet.Count > 0 ? MarkerSet[selectedIndex - 1].Depth : RiverDefaultDepth,
				CascadeNodeSetting = _cascadeProfile.GlobalNodeSetting,
				//Shape2DProfile = ESAnimationCurveUtility.CreateCopy(Profile.RiverSetting.Shape2DProfile),
				Shape2DProfile1 = MarkerSet.Count > 0
					? esAnimationCurveUtility.CreateCopy(MarkerSet[selectedIndex - 1].Shape2DProfile1)
					: esAnimationCurveUtility.CreateCopy(_cascadeProfile.RiverBedSetting.Shape2DProfile),

				Shape2DProfile = MarkerSet.Count > 0
					? esAnimationCurveUtility.CreateCopy(MarkerSet[selectedIndex - 1].Shape2DProfile)
					: esAnimationCurveUtility.CreateCopy(_cascadeProfile.RiverSetting.Shape2DProfile),


				Rotation = Quaternion.identity,
				LandscapeModifier = new esLandscapeModifierData()
				{
					ConformTerrain = true,
					LeftPadding =
						selectedIndex > 0 ? MarkerSet[selectedIndex - 1].LandscapeModifier.LeftPadding : LandscapeModifier.LeftPadding,
					RightPadding = selectedIndex > 0
						? MarkerSet[selectedIndex - 1].LandscapeModifier.RightPadding
						: LandscapeModifier.RightPadding,
					LeftSurronding = selectedIndex > 0
						? MarkerSet[selectedIndex - 1].LandscapeModifier.LeftSurronding
						: LandscapeModifier.LeftSurronding,
					RightSurronding = selectedIndex > 0
						? MarkerSet[selectedIndex - 1].LandscapeModifier.RightSurronding
						: LandscapeModifier.RightSurronding,
					LeftBlendingCurve = selectedIndex > 0
						? esAnimationCurveUtility.CreateCopy(MarkerSet[selectedIndex - 1].LandscapeModifier.LeftBlendingCurve)
						: LandscapeModifier.LeftBlendingCurve,
					RightBlendingCurve = selectedIndex > 0
						? esAnimationCurveUtility.CreateCopy(MarkerSet[selectedIndex - 1].LandscapeModifier.RightBlendingCurve)
						: LandscapeModifier.RightBlendingCurve
				}

			};

			_spline.MarkerSet.Insert(selectedIndex, newMarker);
			_spline.VertexInsertBefore(selectedIndex, newMarker.Position);
			PointHeigthSet(selectedIndex, _spline.MarkerSet[selectedIndex].Position.y);

			if (DesignModeIsOn)
			{
				CascadePath.Init(cascadeRoot, _surfaceDesignMaterial, _riverBedDesignMaterial);
				CascadePath.AddNode(_spline.MarkerSet[selectedIndex], selectedIndex);
			}

			return selectedIndex;

		}

		

		public void PointMoveClosed(int selectedIndex, Vector3 newPosition)
		{
			PointMove(selectedIndex, newPosition);
		}

		public void PointMoveAll(int selectedIndex, Vector3 newPosition)
		{
			var deltaY = newPosition.y - _spline.MarkerSet[selectedIndex].Position.y;
			for (int i = 0; i < MarkerSet.Count; i++)
			{
				_spline.MarkerSet[i].Position += Vector3.up * deltaY;
				_spline.VertexSetPosition(i, _spline.MarkerSet[i].Position);

				if (DesignModeIsOn)
				{
					//CascadePath.Init(cascadeRoot, _surfaceDesignMaterial, _riverBedDesignMaterial);
					if (i == 0)
					{
						startMarker.Position += Vector3.up * deltaY;
						startMarker.GroundObject.transform.position += Vector3.up * deltaY;

					}
					else if (i == MarkerCount - 1)
					{
						endMarker.Position += Vector3.up * deltaY;
						endMarker.GroundObject.transform.position += Vector3.up * deltaY;

					}

					//CascadePath.AddNode(_spline.MarkerSet[newPointIndex], newPointIndex);
				}

			}


#if SENTIERI_BUILDER
			//sentieriBuilder.EditorSelectedMarkerIdx = selectedIndex;
			//sentieriBuilder.VertexSetPositionForSelected(_spline.MarkerSet[selectedIndex].Position);
#endif
		}


		public void PointMove(int selectedIndex, Vector3 newPosition)
		{
			
			//if (selectedIndex > 0)
			//{
			//	var yPrev = _spline.MarkerSet[selectedIndex - 1].Position.y;
			//	if (newPosition.y > yPrev) newPosition.y = yPrev;
			//}
			//_spline.MarkerSet[selectedIndex].Position = newPosition;
	
			_spline.MarkerSet[selectedIndex].Position = newPosition;

			
			PointHeigthSet(selectedIndex, _spline.MarkerSet[selectedIndex].Position.y);
			//_spline.VertexSetPosition(selectedIndex, newPosition);

			// Keep all at same heigth
			if (ClosedSurface)
			{
				PointAlignAllHeightToSelected(selectedIndex);
			}

			if (DesignModeIsOn)
			{
				//CascadePath.Init(cascadeRoot, _surfaceDesignMaterial, _riverBedDesignMaterial);
				if (selectedIndex == 0)
				{
					startMarker.Position = newPosition;
					startMarker.GroundObject.transform.position = newPosition;

				}
				else if( selectedIndex == MarkerCount-1)
				{
					endMarker.Position = newPosition;
					endMarker.GroundObject.transform.position = newPosition;

				}
				//CascadePath.AddNode(_spline.MarkerSet[newPointIndex], newPointIndex);
			}

			RefreshAll = false;

#if SENTIERI_BUILDER
			//sentieriBuilder.EditorSelectedMarkerIdx = selectedIndex;
			//sentieriBuilder.VertexSetPositionForSelected(_spline.MarkerSet[selectedIndex].Position);
#endif
		}

		public int PointDelete(int selectedIndex)
		{

		
			var selectedPoint = _spline.MarkerSet[selectedIndex];



			if (DesignModeIsOn)
			{
				if (MarkerSet.Count>1)
				{
					_spline.MarkerSet[selectedIndex - 1].GroundObject.GetComponent<MeshFilter>().sharedMesh = null;
					_spline.MarkerSet[selectedIndex - 1].SegmentObject.GetComponent<MeshFilter>().sharedMesh = null;
				}
				selectedPoint.GroundObject.Destroy();
				selectedPoint.SegmentObject.Destroy();
			}
			

			_spline.MarkerSet.Remove(selectedPoint);

			selectedPoint.gameObject.Destroy();

			_spline.VertexRemove(selectedIndex);
			RefreshAll = false;


			return selectedIndex == 0 ? -1 : selectedIndex - 1;
		}

		public void PointRotate(int selectedIndex, Vector3 newPosition)
		{
			
		}

		public bool PointAlignAllHeightToSelected(int idxSelectedPoint)
		{
			if (idxSelectedPoint < 0) return false;
			var y = _spline.MarkerSet[idxSelectedPoint].Position.y;
			for (int i = 0; i < MarkerSet.Count; i++)
			{
				var position = _spline.MarkerSet[i].Position;
				position.y = y;
				_spline.MarkerSet[i].Position = position;
				_spline.VertexSetPosition(i, position);
			}
			RefreshAll = true;
			return true;
		}

		public bool PointAlignGround(int idxSelectedPoint)
		{
			
			var position = TerrainUtility.SnapPointToTerrain(_spline.MarkerSet[idxSelectedPoint].Position);
			_spline.MarkerSet[idxSelectedPoint].Position = position;
			_spline.VertexSetPosition(idxSelectedPoint, position);
			RefreshAll = false;
			return true;
		}

		public bool PointAlignAllGround()
		{
			for (int i = 0; i < _spline.MarkerCount; i++)
			{
				PointAlignGround(i);
			}
			RefreshAll = true;
			return true;
		}

	
		//TODO Bad code .. to change making more elegant. No issue from perfomance point of view
		void PointHeigthSet(int selectedIndex, float height)
		{
			// index = 5  -- 0 1 -2 -3 -4 -5 -6 -7 8 9 10 --count 11

			if (_spline.MarkerSet[selectedIndex].StreamType != WaterStreamType.Lake)
			{
				var p = _spline.MarkerSet[selectedIndex].Position;
				p.y = height;
				_spline.MarkerSet[selectedIndex].Position = p;
				_spline.VertexSetPosition(selectedIndex,p);
			

				return;
			}
			int endIndex = selectedIndex;
			int startIndex = selectedIndex;
			bool stop = false;
			for (int i = selectedIndex+1; i < _spline.MarkerSet.Count; i= i+1)
			{
				if(stop)continue;
				if (_spline.MarkerSet[i].StreamType == WaterStreamType.Lake)
					endIndex = i;
				else
				{
					stop = true;
				}
			}
			stop = false;
			for (int i = selectedIndex-1; i >= 0; i=i-1)
			{
				if (stop) continue;
				if (_spline.MarkerSet[i].StreamType == WaterStreamType.Lake)
				{
					startIndex = i;
				}
				else
				{
					stop = true;
				}

			}

			for (int i = startIndex; i <= endIndex; i++)
			{
				var p =  _spline.MarkerSet[i].Position;
				p.y = height;
				_spline.MarkerSet[i].Position = p;
				_spline.VertexSetPosition(i, p);
			}

			
		}

		#endregion

		#region Marker Position Control Path

		Vector3 PositionDownTopHill(int i,  Vector3 position)
		{
			if (_spline.MarkerSet.Count == 0) return position; // First add point
			
			//var nextIdx = i+1;
			//var prevIdx = i-1;
			if (i == 0)
			{
				var yMax = _spline.MarkerSet[i + 1].Position.y;
				if (position.y > yMax) position.y = yMax;

			}
			else if(i == _spline.MarkerSet.Count)
			{
				var yMin = _spline.MarkerSet[i - 1].Position.y;
				if (position.y < yMin) position.y = yMin;
			}
			else
			{
				var yMax = _spline.MarkerSet[i + 1].Position.y;
				var yMin = _spline.MarkerSet[i - 1].Position.y;
				if (position.y > yMax) position.y = yMax;
				if(position.y< yMin) position.y = yMin;
			}
			//var prevY = _spline.MarkerSet[i - 1].Position.y;
			//var nextY = _spline.MarkerSet[i + 1].Position.y;
			//if (position.y > prevY)
			//	position.y = prevY;
			return position;
		}

		

		public bool CheckSlope(int index)
		{
			var slopeIsOk = true;
			if (index == 0) return true;
			if (!_spline.MarkerSet[index].IsLake && _spline.MarkerSet[index].Position.y >= _spline.MarkerSet[index - 1].Position.y)
			{
				slopeIsOk = false;
			}

			return slopeIsOk;
		}

		public void AutoSlope()
		{
			bool refresh = false;
			for (int i = 1; i < _spline.MarkerSet.Count; i++)
			{
				// Check y but not for lake ( TODO check only for the first point )
				if (!_spline.MarkerSet[i].IsLake && _spline.MarkerSet[i].Position.y >= _spline.MarkerSet[i - 1].Position.y)
				{
					var offSet = slopeRange.GetRandomValue();
					var newPosition = new Vector3(_spline.MarkerSet[i].Position.x, _spline.MarkerSet[i - 1].Position.y - offSet,
						_spline.MarkerSet[i].Position.z);
					PointMove(i, newPosition);
					refresh = true;
				}

			}

			if (refresh)
			{
				RefreshAll = true;
				RefreshCascadePath();
			}
		}


		#endregion


		#region Audio Manager
		private void CheckAudioManagerRoot()
		{
			if (audioRoot == null)
			{
				audioRoot = new GameObject("Audio Manager");
				audioRoot.transform.position = Vector3.zero;
				audioRoot.transform.parent = transform;
			}


		}

	
		public void CreateAudioManager()
		{
			CheckAudioManagerRoot();
			AudioManagerDeleteAll();
			for (int i = 0; i < MarkerCount; i+=AudioNodeFrequency)
			{
				var audio = new GameObject("Audio Source ( " + i + " )");
				audio.transform.position = MarkerSet[i].Position;
				audio.transform.rotation = Quaternion.identity;
				audio.transform.parent = audioRoot.transform;
				var audioSource = audio.AddComponent<AudioSource>();
			
				var audios = new esAudioSource()
				{
					source = audioSource,
					Setting = _defaultAudioSetting
				};
				audioManager.AudioSourceAdd(audios);
				audioManager.SetAudioSource(_defaultAudioSetting, (i/AudioNodeFrequency));

			}
		}

		public void AudioManagerDeleteAll()
		{
			audioManager.AudioSourceClear();

			foreach (var o in audioRoot.GetComponentsInChildren<Transform>())
			{
				if (o.gameObject != audioRoot)
					o.gameObject.Destroy();
			}
		}

		#endregion

		#region Lighting


		private void CheckReflectionProbesRoot()
		{
			if (probesRoot == null)
			{
				probesRoot = new GameObject("Probes Root");
				probesRoot.transform.position = Vector3.zero;
				probesRoot.transform.parent = transform;
			}


		}

		public void CreateReflectionProbes()
		{

			CheckReflectionProbesRoot();

			foreach (var o in probesRoot.GetComponentsInChildren<ReflectionProbe>())
			{
				o.gameObject.Destroy();
			}
	
			_reflectionProbes.Clear();
			float lastDis = 0;

			for (float dis = 0; dis < PathLenght; dis+= ReflectionProbeDistance)
			{

				var time = _spline.LengthToTime(dis);
				var pos =_spline.GetPosition(time);

				var probe = new GameObject("Reflection Probe ( " + dis/ ReflectionProbeDistance + " )");
				probe.transform.position = pos +  ReflectionProbeHeight * Vector3.up;
				probe.transform.rotation = Quaternion.identity;
				probe.transform.parent = probesRoot.transform;

				var rp = probe.AddComponent<ReflectionProbe>();
				rp.mode = ReflectionProbeMode.Realtime;
			

				_reflectionProbes.Add(rp);
				lastDis = dis;
			}
			// Add at last one
			if (lastDis < PathLenght)
			{
				var time = _spline.LengthToTime(PathLenght);
				var pos = _spline.GetPosition(time);
				var probe = new GameObject("Reflection Probe ( " + _reflectionProbes.Count + " )");
				probe.transform.position = pos + ReflectionProbeHeight * Vector3.up;
				probe.transform.rotation = Quaternion.identity;

				var rp = probe.AddComponent<ReflectionProbe>();
				rp.mode = ReflectionProbeMode.Realtime;
				probe.transform.parent = probesRoot.transform;

				_reflectionProbes.Add(rp);
			}

		}
		
		#endregion

		#region Import / Export

		public void ExportMarkers()
		{
			GameObject lRoot = new GameObject("River Path Markers");
			lRoot.transform.position = Vector3.zero;
			lRoot.transform.rotation = Quaternion.identity;
			
			for (int i = 0; i < MarkerSet.Count; i++)
			{
				GameObject markerPoint = new GameObject("Maker Point " + i);
				markerPoint.transform.position = MarkerSet[i].Position;
				markerPoint.transform.parent = lRoot.transform;
			}
		}
		public void ImportMarkers(Vector3[] waypoints)
		{
			if (waypoints == null || waypoints.Length < 2) return;

			_spline = new CatmullRom();

			for (int i = 0; i < waypoints.Length; i++)
			{
				NodeAdd(waypoints[i], i);
			}
			RefreshCascadePath();

		}
		public void ImportMarkers(GameObject root)
		{

			if (root == null) return;

			var waypoints = root.GetChildrenNoRoot<Transform>().ToVector3();
			if (waypoints == null || waypoints.Length < 2) return;

			_spline = new CatmullRom();

			for (int i = 0; i < waypoints.Length; i++)
			{
				NodeAdd(waypoints[i], i);
			}
			RefreshCascadePath();
		}

		#endregion

		#region Mesh Generation

		void MeshCheckComponents()
		{

			if (_meshFilter == null)
				_meshFilter = cascadeRoot.GetComponent<MeshFilter>();
			if (_meshCollider == null)
				_meshCollider = cascadeRoot.GetComponent<MeshCollider>();
			if (_meshRenderer == null)
				_meshRenderer = cascadeRoot.GetComponent<MeshRenderer>();
		}

		public void RefreshCascadePath(bool external = false)
		{
			// TODO Stramline better
			if(ClosedSurface) return;

			// TODO  Temporary to avoid issue when migrate project
			if (_cascadeProfile == null) InitCascadeProfile();
			// TODO Temporary to avoid issue when migrate project
			{
				MeshCheckComponents();

			}

			//MeshResolution = PathDefaultWidth / MeshSubdivion;
			if (MarkerSet.Count < 2)
			{
				_meshFilter.sharedMesh = null;
				_meshCollider.sharedMesh = null;
				return;
			}

			// TODO Cache and refresh only when required
			// Event Drive when you change cascades defintions ??!!!
			{
				// Reset StreamType
				for (int i = 0; i < MarkerSet.Count; i++)
				{
					_spline.MarkerSet[i].StreamType = WaterStreamType.River;
				}
				_waterfallIndices.Clear();
				_lakeIndices.Clear();
				// Refresh Cascade Segments
				for (int i = 0; i < Cascades.Count; i++)
				{
					// Safe clause.
					if (Cascades[i] == null)
					{
						Cascades.Remove(Cascades[i]);
						continue;
					}
					
					// If Cascade Segment is enabled
					if (Cascades[i].IsActive)
					{
						if (Cascades[i].Type == CascadeType.Lake)
						{
							_lakeIndices.Add(new IntVector2(Cascades[i].start, Cascades[i].end));

							for (int j = Cascades[i].start; j <= Cascades[i].end; j++)
							{
								_spline.MarkerSet[j].StreamType = WaterStreamType.Lake;

							}

						}
						else if (Cascades[i].Type == CascadeType.Waterfall)
						{
							_waterfallIndices.Add(new IntVector2(Cascades[i].start, Cascades[i].end));
							var startWaterfall = Cascades[i].start;
							var endWaterfall = Cascades[i].end;

							_spline.MarkerSet[startWaterfall].StreamType = WaterStreamType.WaterfallStart;
							_spline.MarkerSet[endWaterfall].StreamType = WaterStreamType.WaterfallStart;

						}
					}
					else
					{
						Cascades[i].gameObject.GetComponent<MeshFilter>().sharedMesh = null;
					}
				}

			}

		
			MeshRiverGenerate(!HideMesh);

			MeshBedGenerate(DesignModeIsOn);

			RefreshAll = false;
			Conformed = false;
			// New Generator
			//NewGenerator(isRootCall);

		}

		void MeshRiverGenerate(bool generateMesh)
		{

			RefreshCachePoints();
			if (!generateMesh) 
			{
				_meshFilter.sharedMesh = null;
				_meshCollider.sharedMesh = null;
				return;
			}

			List<AnimationCurve> lAnimationCurves = new List<AnimationCurve>();
			for (int i = 0; i < MarkerSet.Count; i++)
			{
				lAnimationCurves.Add(MarkerSet[i].Shape2DProfile);
			}
		

			_cascadeProfile.RiverSetting.MeshResolution = MeshResolution; 
			_cascadeProfile.RiverSetting.MeshSubdvision = MeshSubDivision;

			_cascadeProfile.RiverSetting.GlobalWitdh = PathDefaultWidth;
			_cascadeProfile.RiverSetting.ElementMaterial.UVScale = UVScale;

			//_profile.RiverSetting.MeshResolution = MeshResolution;
			//_profile.RiverSetting.MeshSubdvision = Mathf.CeilToInt(PathDefaultWidth / step);

			_cascadeProfile.RiverSetting.FlipTextureClockWise = FlipClockWise;

			//meshCollider.sharedMesh = meshFilter.sharedMesh;

			CascadePathMesh.Profile = _cascadeProfile;
			CascadePathMesh.GameObjectHolder = this.gameObject;
			CascadePathMesh.MeshYGroundOffset = MeshYGroundOffset;


			CascadePathMesh.HideWireFrameInEditor = HideWireFrameInEditor;

			if (DesignModeIsOn)
			{
				_meshFilter.sharedMesh = new Mesh();
			
				int start = 0;
				int last = 4;
				if (RefreshAll)
				{
					start = 0;
					last = MarkerSet.Count;
				}

				var markerIdxs = RefreshAll ? new[] { -1 } : Spline.GetSegmentMarkerSplineIdx(SplineSelectionInfo.IdxSelectedPoint);

				for (int j = start; j < last; j++)
				{

					var idx = RefreshAll ? j : markerIdxs[j];
					var lsegmentPathOrientedPoints = _cascadePath.GetSegmentPoints(idx, _cachePoints);

					if (lsegmentPathOrientedPoints.Count < 2) continue;

					var riverMeshSegment = CascadePathMesh.MeshCascadeSurface(lsegmentPathOrientedPoints, false, Distorsion, lAnimationCurves);

					//var riverBedMesh1 = CascadePathMesh.MeshSegmentRiverBedCascade(lsegmentPathOrientedPoints, depthCurves, cascadeRoot);

					// TODO . reorganize per component and put into extrude
					var cascadeDesignRoot = cascadeRoot;
					var newVertices = new Vector3[riverMeshSegment.vertices.Length];
					for (int i = 0; i < riverMeshSegment.vertices.Length; i++)
					{

						//	var worldPosition = lcurMarker.SegmentHolderObject.transform.TransformPoint(llaneMesh.vertices[i]);
						var worldPosition = _spline.MarkerSet[idx].GroundObject.gameObject.transform.InverseTransformPoint(riverMeshSegment.vertices[i]);

						//	newVertices[i] = lcurMarker.SegmentHolderObject.transform.InverseTransformPoint(newPosition);
						newVertices[i] = cascadeDesignRoot.gameObject.transform.TransformPoint(worldPosition);
					}
					riverMeshSegment.vertices = newVertices;
					riverMeshSegment.RecalculateBounds();
					riverMeshSegment.RecalculateNormals();
					riverMeshSegment.RecalculateTangents();

					_spline.MarkerSet[idx].SegmentObject.GetComponent<MeshFilter>().sharedMesh = riverMeshSegment;

				}

				return;
			}


			var riverMesh = CascadePathMesh.MeshCascadeSurface(_cachePoints, false, Distorsion, lAnimationCurves);

			PathLenght = _spline.TotalLenght;
			trianglesCount = riverMesh.triangles.Length / 3;
			verticesCount = riverMesh.vertices.Length;

#if UNITY_2017_3_OR_NEWER
			if(verticesCount > 4000000)
			{
				throw new System.Exception("Over 4000000 vertices detected, exiting extrusion. Try switching splination axis and make sure your imported FBX file has proper import scale. Make sure the mesh isn't too small and make sure the distance isn't too large.");
			
			}
#else
			if (verticesCount > 65500)
			{
				throw new System.Exception("Over 65000 vertices detected, exiting extrusion. Try switching splination axis and make sure your imported FBX file has proper import scale. Make sure the mesh isn't too small and make sure the distance isn't too large.");
			
			}
#endif

			

			// TODO move to generator 
			var noiseScale = _cascadeProfile.RiverSetting.NoiseSettings.Scale;
			if (noiseScale > 0)
			{
				var newVertices = new Vector3[riverMesh.vertices.Length];

				for (int i = 0; i < riverMesh.vertices.Length; i++)
				{
					var worldPosition = cascadeRoot.transform.TransformPoint(riverMesh.vertices[i]);
					var noise = Mathf.PerlinNoise(worldPosition.x / noiseScale, worldPosition.z / noiseScale);
					var newPosition = worldPosition + _cascadeProfile.RiverSetting.NoiseSettings.NoiseAmplitude * noise * Vector3.up;
					newVertices[i] = cascadeRoot.transform.InverseTransformPoint(newPosition);
				}
				riverMesh.vertices = newVertices;
			}


			// Assign Mesh
			_meshFilter.sharedMesh = riverMesh;
			// Assign Collider if active
			_meshCollider.sharedMesh = AddCollider ? _meshFilter.sharedMesh : null;
			// Vegetation Mask Refresh is active
			if (VSAutoRefresh && VSMaskAnyIsEnabled)
			{
				Vegetation_RefreshMask();
			}

			//EditorUtility.SetSelectedRenderState(GameObjectHolder.GetComponent<Renderer>(),
			//	HideWireFrameInEditor ? EditorSelectedRenderState.Hidden : EditorSelectedRenderState.Highlight);
			//stopwatch.Stop();
			//UnityEngine.Debug.Log("Mesh Time Completions: " + stopwatch.ElapsedMilliseconds + "ms");
		}


		private List<Vector3> _points = new List<Vector3>();
		public void MeshRefreshClosed()
		{


			// TODO Temporary to avoid issue when migrate project
			{
				if (_meshFilter == null)
					_meshFilter = cascadeRoot.GetComponent<MeshFilter>();
				if (_meshCollider == null)
					_meshCollider = cascadeRoot.GetComponent<MeshCollider>();
				if (_meshRenderer == null)
					_meshRenderer = cascadeRoot.GetComponent<MeshRenderer>();
			}

			// No Shawod
			_meshRenderer.shadowCastingMode = ShadowCastingMode.Off;
			_meshRenderer.receiveShadows = true;

			_points = CascadePath.GetContour(_lake, MarkerSet);

			if (_points.Count < 1)
			{
				_meshFilter.sharedMesh = null;
				trianglesCount = 0;
				verticesCount = 0;
				_meshCollider.sharedMesh = null;
				return;
			}

			cascadeRoot.transform.position = MarkerSet[0].Position;
			SurfaceMeshGenerator generator = new SurfaceMeshGenerator
			{
				EdgePadding = _lake.EdgePadding,
				pointDensity = _lake.pointDensity,
				Vertices = _points,
				meshHolder = cascadeRoot
			};

			FlowMapBaker baker = new FlowMapBaker(generator.GetSurface());
			_meshFilter.sharedMesh = baker.Refresh(cascadeRoot, true);

			trianglesCount = _meshFilter.sharedMesh.triangles.Length / 3;
			verticesCount = _meshFilter.sharedMesh.vertices.Length;

			_meshCollider.sharedMesh = AddCollider ? _meshFilter.sharedMesh : null;
			if (VSAutoRefresh && VSMaskAnyIsEnabled)
			{
				Vegetation_RefreshMask();
			}

		}

		void MeshBedGenerate(bool IsActive)
		{
			if (!IsActive) return;

			// Force to true. TODO check init
			_cascadeProfile.RiverBedSetting.IsActive = true;
			var depthCurves = new List<AnimationCurve>();
			//var cumLenght = 0.0f;
			//var noiseScale = _cascadeProfile.RiverBedSetting.NoiseSettings.Scale;
			for (int v = 0; v < MarkerSet.Count; v++)
			{
			//	if (v > 0)
			//	{
			//		cumLenght += Vector3.Distance(MarkerSet[v].Position, MarkerSet[v - 1].Position);
			//	}
				var surfaceProfile = esAnimationCurveUtility.CreateCopy(MarkerSet[v].Shape2DProfile1);

   //             for (float t = 0; t <= 1; t += 0.1f)
			//	{
			//		var noise = Mathf.PerlinNoise(t / noiseScale, cumLenght / noiseScale);
			//		if (noise < _cascadeProfile.RiverBedSetting.NoiseSettings.CutOff)
			//		{
			//			var y = noise * _cascadeProfile.RiverBedSetting.NoiseSettings.NoiseAmplitude;
			//			var dy = surfaceProfile.Evaluate(t);
			//			surfaceProfile.AddKey(t, Mathf.Clamp(y + dy, 0, MarkerSet[v].Depth * 0.8f));
			//		}
			//	}
				depthCurves.Add(surfaceProfile);
			}

			int start = 0;
			int last = 4;
			if (RefreshAll)
			{
				start = 0;
				last = MarkerSet.Count;
			}
		
			var markerIdxs = RefreshAll ? new[] { -1 } : Spline.GetSegmentMarkerSplineIdx(SplineSelectionInfo.IdxSelectedPoint);
			EnableStartCap = false;
			EnableEndCap = false;

			for (int j = start; j < last; j++)
			{

				var lMarkerId = RefreshAll ? j : markerIdxs[j];
				if ( lMarkerId == 0 && EnableStartCap)
				{
					//Vector3 forward, center;
					//Mesh capMesh;
					//int lPathPointIdx = 0;
					//MeshStartCap(lMarkerId, out forward, out center, out capMesh, lPathPointIdx);

					//startMarker.GroundObject.GetComponent<MeshFilter>().sharedMesh = capMesh;
					//startMarker.Position = center;
					//startMarker.GroundObject.transform.position = startMarker.Position;

					//var rot = Quaternion.LookRotation(-forward);

					//startMarker.GroundObject.transform.rotation = rot;

					//startMarker.GroundObject.GetComponent<MeshRenderer>().sharedMaterial = _riverBedDesignMaterial;
				}
				else if (lMarkerId == MarkerCount - 1 && EnableEndCap)
				{
					//Vector3 forward, center;
					//Mesh capMesh;
					//int lPathPointIdx = _splineCenterPoints.Count - 1;
					//MeshEndCap(lMarkerId, out forward, out center, out capMesh, lPathPointIdx);

					//// Note that left and right are flipped
					//endMarker.GroundObject.GetComponent<MeshFilter>().sharedMesh = capMesh;

					//endMarker.Position = center;
					//endMarker.GroundObject.transform.position = endMarker.Position;

					//var rot = Quaternion.LookRotation(forward);

					//endMarker.GroundObject.transform.rotation = rot;

					//endMarker.GroundObject.GetComponent<MeshRenderer>().sharedMaterial = _riverBedDesignMaterial;


				}

				var lsegmentPathOrientedPoints = _cascadePath.GetSegmentPoints(lMarkerId, _cachePoints);
				if (lsegmentPathOrientedPoints.Count < 2) continue;

				var riverBedMesh = CascadePathMesh.MeshCascadeGround(lsegmentPathOrientedPoints, depthCurves, cascadeRoot);

				var newVertices = new Vector3[riverBedMesh.vertices.Length];
				var cascadeDesignRoot = cascadeRoot;
				for (int i = 0; i < riverBedMesh.vertices.Length; i++)
				{
					var worldPosition = _spline.MarkerSet[lMarkerId].GroundObject.gameObject.transform.InverseTransformPoint(riverBedMesh.vertices[i]);
					newVertices[i] = cascadeDesignRoot.gameObject.transform.TransformPoint(worldPosition);
				}
				riverBedMesh.vertices = newVertices;
				riverBedMesh.RecalculateBounds();
				riverBedMesh.RecalculateNormals();
				riverBedMesh.RecalculateTangents();
				_spline.MarkerSet[lMarkerId].GroundObject.GetComponent<MeshFilter>().sharedMesh = riverBedMesh;

			
			}

	
		}


		public Texture2D ExportMarkersMap(string path)
		{
			// Create texture
		
			// Loop
			//float[,] lHeights = ESLandscapeModel.TerrainBackups[idx].TerraiHolder.GetHeightMap();
			var rivermesh = cascadeRoot.GetComponent<MeshFilter>().sharedMesh;
			GameObject ltmpSingleMesh = new GameObject(Config.TerrainMeshName);
			ltmpSingleMesh.transform.parent = cascadeRoot.transform;
			var col = ltmpSingleMesh.AddComponent<MeshCollider>();
			col.sharedMesh = rivermesh;

			// Get Terrain
			// Force Init terrain Manager
			//ESLandscapeController.Init(true);
			int idx = 0;
			float[,] Heights = esLandscapeManager.GetLandscape.TerrainBackups[idx].TerraiHolder.GetHeightMap();
			float[,] lHeights = new float[Heights.GetLength(0), Heights.GetLength(1)];

			CascadeTerrainManager.StampRiverBed(this._spline, this.SplineSelectionInfo.IdxSelectedPoint, Config.TerrainMeshName, true, rivermesh);

			ltmpSingleMesh.Destroy();

			Texture2D map = esTextureHelper.CascadeMap(lHeights);

			map.SaveAsHeightMapPng(path,true);
			return map;
		}

	

		public void RestoreRiverBed()
		{


			if (CascadeManager.QueueUndoAll())
			{
#if VEGETATION_STUDIO
				if (VSAutoRefresh)
				{
					var bounds = GeometryUtil.GetBounds(_spline);
					IntegrationScript.VegetationStudio.RefreshTerrainHeightMap(bounds);
				}
#endif
			}



		}


		#endregion

		#region Profile

		/// <summary>
		/// TODO Temporary Fix
		/// </summary>
		public void Fix4LostProfile()
		{
			// TODO  Temporary to avoid issue when migrate project
			if (_cascadeProfile == null) InitCascadeProfile();
		}

		#endregion

		#region VSIntegation
		public GameObject VegMaskRoot;
		public GameObject VegMaskEdgeLRoot;
		public GameObject VegMaskEdgeRRoot;
		public bool replaceTexture;

		public bool VSMaskMainEnable = false;
		public bool VSMaskLEnable = false;
		public bool VSMaskREnable = false;

		public float VS_dp_coefficient = 1.65f;
		public float VS_dp_coefficientL = 1.65f;
		public float VS_dp_coefficientR = 1.65f;
		public float VS_WidthL = 10f;
		public float VS_WidthR = 10f;


		public bool VSAutoRefresh = false;
		
		public bool VSMaskAnyIsEnabled { get { return VSMaskMainEnable || VSMaskLEnable || VSMaskREnable; } }
	

		public struct VSMaskSetting
		{
			public IntegrationScript.VegetationStudio.VSMaskMode MaskMode;
			public float MaskOffset;

			public VSMaskSetting(IntegrationScript.VegetationStudio.VSMaskMode mode = IntegrationScript.VegetationStudio.VSMaskMode.Multi, float offset = 0)
			{
				MaskMode = mode;
				MaskOffset = offset;
			}
		}

		public IntegrationScript.VegetationStudio.VSMaskMode VsMaskModeMain =
			IntegrationScript.VegetationStudio.VSMaskMode.Multi;
		public float VSMaskMainOffset = 0f;

		public IntegrationScript.VegetationStudio.VSMaskMode VsMaskModeL =
			IntegrationScript.VegetationStudio.VSMaskMode.Multi;
		public float VSMaskLOffset = 0f;

		public IntegrationScript.VegetationStudio.VSMaskMode VsMaskModeR =
			IntegrationScript.VegetationStudio.VSMaskMode.Multi;
		public float VSMaskROffset = 0f;


		private float _maskLastUpdateTime ;
	
#if VEGETATION_STUDIO
		public List<AwesomeTechnologies.VegetationTypeSettings> VegetationTypeListL = new List<AwesomeTechnologies.VegetationTypeSettings>();
		public List<AwesomeTechnologies.VegetationTypeSettings> VegetationTypeListR = new List<AwesomeTechnologies.VegetationTypeSettings>();
		

#endif

		public void Vegetation_RefreshMask()
		{
			if (_maskLastUpdateTime + 0.1f > Time.realtimeSinceStartup) return;
			_maskLastUpdateTime = Time.realtimeSinceStartup;

#if VEGETATION_STUDIO
			

			if (VSMaskAnyIsEnabled)
			{
				// Check Cache
				if ((_cachePoints == null || _cachePoints.Count == 0) && MarkerSet.Count > 0)
				{
					RefreshCachePoints();
				}
			}
			

			if (VSMaskLEnable)
			{
				VSEdgeMaskL(ref VegMaskEdgeLRoot);

			}
			else
			{
				if (VegMaskEdgeLRoot != null)
				{
					var masks = VegMaskEdgeLRoot.GetComponents<AwesomeTechnologies.VegetationMaskLine>();
					masks.Destroy();
					VegMaskEdgeLRoot.Destroy();
				}

			}


			if (VSMaskREnable)
			{
				VSEdgeMaskR(ref VegMaskEdgeRRoot);

			}
			else
			{

				if (VegMaskEdgeRRoot != null)
				{
					var masks = VegMaskEdgeRRoot.GetComponents<AwesomeTechnologies.VegetationMaskLine>();
					masks.Destroy();
					VegMaskEdgeRRoot.Destroy();
				}
			}

			if (VSMaskMainEnable)
			{
			
				VSMainMask(ref VegMaskRoot);
			}
			else
			{

				if (VegMaskRoot != null )
				{
					var masks = VegMaskRoot.GetComponents<AwesomeTechnologies.VegetationMaskArea>();
					masks.Destroy();
					VegMaskRoot.Destroy();
				}
					
			}


#endif


		}

		

#if VEGETATION_STUDIO
		void VSMainMask(ref GameObject maskRoot)
		{
			if (maskRoot == null)
			{
				maskRoot = new GameObject("_vegetationMask_");
				//mask = VegMaskRoot.AddComponent<AwesomeTechnologies.VegetationMaskArea>();
				maskRoot.transform.parent = this.gameObject.transform;
			}
			maskRoot.transform.parent = this.gameObject.transform;
			// Lake :
			if (ClosedSurface)
			{
				var maskArea = maskRoot.GetOrAddComponent<AwesomeTechnologies.VegetationMaskArea>();
				IntegrationScript.VegetationStudio.UpdateVegationStudioMask(maskArea,
					FrameworkUtility.OffsetPolygon(_points, VSMaskMainOffset - _lake.EdgePadding, false).ToArray(), true);
			}
			else
			{
				// River :


				var masks = maskRoot.GetComponents<AwesomeTechnologies.VegetationMaskArea>().ToList();

				if (VsMaskModeMain == IntegrationScript.VegetationStudio.VSMaskMode.Single)
				{
					if (masks.Count == 0) masks.Add(maskRoot.AddComponent<AwesomeTechnologies.VegetationMaskArea>());
					if (masks.Count > 1)
					{
						for (int i = 1; i < masks.Count; i++)
						{
							masks[i].Destroy();
						}
					}

					Vector3[] vertices = FrameworkUtility.GetEdges(_cachePoints, VS_dp_coefficient);
					var points = FrameworkUtility.OffsetPolygon(vertices.ToList(), VSMaskMainOffset, false).ToArray();
					IntegrationScript.VegetationStudio.UpdateVegationStudioMask(masks[0], points, true);
					return;
				}


				if (masks.Count != MarkerSet.Count)
				{
					// TODO add or remove
					for (int i = masks.Count; i < MarkerSet.Count - 1; i++)
					{
						var mask = maskRoot.AddComponent<AwesomeTechnologies.VegetationMaskArea>();
						mask.MaskName = "Mask " + i;
						masks.Add(mask);
					}
				}

				// Temp Patch to Fix segment generation. Permament fix planned with 1.0.3
				var status = RefreshAll;
				RefreshAll = true;
				int start = 0;
				int last = 4;
				if (RefreshAll)
				{
					start = 0;
					last = MarkerSet.Count;
				}
				var markerIdxs = RefreshAll ? new[] { -1 } : Spline.GetSegmentMarkerSplineIdx(SplineSelectionInfo.IdxSelectedPoint);
				for (int j = start; j < last; j++)
				{
					var markerId = RefreshAll ? j : markerIdxs[j];
					var lsegmentPathOrientedPoints = _cascadePath.GetSegmentPoints(markerId, _cachePoints);
					if (lsegmentPathOrientedPoints.Count < 2) continue;

					Vector3[] vertices = FrameworkUtility.GetEdges(lsegmentPathOrientedPoints, VS_dp_coefficient);
					var points = FrameworkUtility.OffsetPolygon(vertices.ToList(), VSMaskMainOffset, false).ToArray();
					IntegrationScript.VegetationStudio.UpdateVegationStudioMask(masks[markerId], points, true);
				}

				RefreshAll = status;
			}
		}

		private void VSEdgeMaskR(ref GameObject maskRoot)
		{
			if (maskRoot == null)
			{
				maskRoot = new GameObject("_vegetationMaskEdgeR_");
				//mask = VegMaskRoot.AddComponent<AwesomeTechnologies.VegetationMaskArea>();
				maskRoot.transform.parent = this.gameObject.transform;
			}
			maskRoot.transform.parent = this.gameObject.transform;



			// River :
			var masks = maskRoot.GetComponents<AwesomeTechnologies.VegetationMaskLine>().ToList();

			if (VsMaskModeR == IntegrationScript.VegetationStudio.VSMaskMode.Single)
			{
				if (masks.Count == 0) masks.Add(maskRoot.AddComponent<AwesomeTechnologies.VegetationMaskLine>());
				if (masks.Count > 1)
				{
					for (int i = 1; i < masks.Count; i++)
					{
						masks[i].Destroy();
					}
				}

				Vector3[] vertices = FrameworkUtility.GetEdgeLR(_cachePoints, VS_dp_coefficientR, false, -VSMaskROffset);
				//var points = FrameworkUtility.OffsetPolygon(vertices.ToList(), -VSMaskROffset,true).ToArray();
				IntegrationScript.VegetationStudio.UpdateVegationStudioLineMask(masks[0], vertices, "Edge Right", VS_WidthR, VegetationTypeListR);

			}
			else
			{
				if (masks.Count != MarkerSet.Count)
				{

					// TODO add or remove
					for (int i = masks.Count; i < MarkerSet.Count - 1; i++)
					{
						var mask = maskRoot.AddComponent<AwesomeTechnologies.VegetationMaskLine>();
						mask.MaskName = "Edge Left" + i;
						masks.Add(mask);
					}

				}

				// Temp Patch to Fix segment generation. Permament fix planned with 1.0.3
				var status = RefreshAll;
				RefreshAll = true;
				int start = 0;
				int last = 4;
				if (RefreshAll)
				{
					start = 0;
					last = MarkerSet.Count;
				}
				var markerIdxs = RefreshAll ? new[] { -1 } : Spline.GetSegmentMarkerSplineIdx(SplineSelectionInfo.IdxSelectedPoint);
				for (int j = start; j < last; j++)
				{

					var markerId = RefreshAll ? j : markerIdxs[j];
					var lsegmentPathOrientedPoints = _cascadePath.GetSegmentPoints(markerId, _cachePoints);
					if (lsegmentPathOrientedPoints.Count < 2) continue;

					Vector3[] vertices = FrameworkUtility.GetEdgeLR(lsegmentPathOrientedPoints, VS_dp_coefficientR, false, -VSMaskROffset);

					//Vector3[] vertices = FrameworkUtility.GetEdges(lsegmentPathOrientedPoints, VS_dp_coefficientR);
					IntegrationScript.VegetationStudio.UpdateVegationStudioLineMask(masks[markerId], vertices, "Edge Right" + j, VS_WidthR, VegetationTypeListR);

				}

				RefreshAll = status;
			}
		}

		private void VSEdgeMaskL(ref GameObject maskRoot)
		{
			if (maskRoot == null)
			{
				maskRoot = new GameObject("_vegetationMaskEdgeL_");
				//mask = VegMaskRoot.AddComponent<AwesomeTechnologies.VegetationMaskArea>();
				maskRoot.transform.parent = this.gameObject.transform;
			}
			maskRoot.transform.parent = this.gameObject.transform;

			// River :
			var masks = maskRoot.GetComponents<AwesomeTechnologies.VegetationMaskLine>().ToList();

			if (VsMaskModeL == IntegrationScript.VegetationStudio.VSMaskMode.Single)
			{
				if (masks.Count == 0) masks.Add(maskRoot.AddComponent<AwesomeTechnologies.VegetationMaskLine>());
				if (masks.Count > 1)
				{
					for (int i = 1; i < masks.Count; i++)
					{
						masks[i].Destroy();
					}
				}

				Vector3[] vertices = FrameworkUtility.GetEdgeLR(_cachePoints, VS_dp_coefficientL, true, VSMaskLOffset);
				//var points = FrameworkUtility.OffsetPolygon(vertices.ToList(), VSMaskLOffset,true).ToArray();
				IntegrationScript.VegetationStudio.UpdateVegationStudioLineMask(masks[0], vertices, "Mask Edge Left", VS_WidthL, VegetationTypeListL);

			}
			else
			{

				if (masks.Count != MarkerSet.Count)
				{

					// TODO add or remove
					for (int i = masks.Count; i < MarkerSet.Count - 1; i++)
					{
						var mask = maskRoot.AddComponent<AwesomeTechnologies.VegetationMaskLine>();
						mask.MaskName = "Edge Left" + i;
						masks.Add(mask);
					}

				}

				// Temp Patch to Fix segment generation. 
				// Permament fix planned with 1.0.3
				var status = RefreshAll;
				RefreshAll = true;
				int start = 0;
				int last = 4;
				if (RefreshAll)
				{
					start = 0;
					last = MarkerSet.Count;
				}
				var markerIdxs = RefreshAll ? new[] { -1 } : Spline.GetSegmentMarkerSplineIdx(SplineSelectionInfo.IdxSelectedPoint);
				for (int j = start; j < last; j++)
				{

					var markerId = RefreshAll ? j : markerIdxs[j];
					var lsegmentPathOrientedPoints = _cascadePath.GetSegmentPoints(markerId, _cachePoints);
					if (lsegmentPathOrientedPoints.Count < 2) continue;
					Vector3[] vertices = FrameworkUtility.GetEdgeLR(lsegmentPathOrientedPoints, VS_dp_coefficientL, true, VSMaskLOffset);

					//	Vector3[] vertices = FrameworkUtility.GetEdges(lsegmentPathOrientedPoints, VS_dp_coefficientL);
					IntegrationScript.VegetationStudio.UpdateVegationStudioLineMask(masks[markerId], vertices, "Edge Left" + j, VS_WidthL, VegetationTypeListL);

				}

				RefreshAll = status;

			}
		}

#endif

		#endregion

		#region Gizmos

		public void DrawCells()
		{
			
			if (_cachePoints.Count == 0) RefreshCachePoints();
			if (CellCacheIds == null) CellCacheIds = new CellCacheId[0];
			for (int i = 0; i < CellCacheIds.Length; i++)
			{
				var cellCacheId = CellCacheIds[i];
				if (!cellCacheId.IsValid) continue;
				var terrain = esLandscapeManager.ESTerrains[cellCacheId.TerrainId];
				if (terrain.Cells == null) continue;
				var cell = terrain.Cells[cellCacheId.CellId];
				Gizmos.DrawWireCube(cell.Center, cell.Size);
			}


		}

#if UNITY_EDITOR
		public void FlowMapPainter(bool Active)
		{
			EnableMeshPainter = Active;

			GetMeshComponents(true, true, true);
			if (Active)
			{
				CheckAssignedMaterial();
				_meshRenderer.sharedMaterial = new Material(Shader.Find("Cascade/ViewFlowMap"));

				return;
			}
			_meshRenderer.sharedMaterial = RiverMaterial;

		}
		private void GetMeshComponents(bool mFilter, bool mRenderer, bool mCollider)
		{
			if (mFilter) _meshFilter = cascadeRoot.GetComponent<MeshFilter>();
			if (mCollider) _meshCollider = cascadeRoot.GetComponent<MeshCollider>();
			if (mRenderer) _meshRenderer = cascadeRoot.GetComponent<MeshRenderer>();
		}


	

		private void OnDrawGizmos()
		{

	

			if(_meshFilter == null || _meshFilter.sharedMesh == null) return;
			//Gizmos.color = Color.red;
			//// Bounds area
			//var fullBounds = _meshFilter.sharedMesh.bounds;
			//var boundPoint1F = fullBounds.min;
			//var boundPoint2F = fullBounds.max; // top
			//Gizmos.DrawWireCube(_meshFilter.sharedMesh.bounds.center, (boundPoint2F - boundPoint1F));

			if (HideMesh)
				for (int i = 1; i < _cachePoints.Count; i++)
				{
					Gizmos.DrawLine(_cachePoints[i].Position, _cachePoints[i - 1].Position);
				}

		}

		private void OnDrawGizmosSelected()
		{

			if (tempPathPoints.Count>0)
			{
				
				for (int i = 0; i < tempPathPoints.Count - 1; i++)
				{
				
					Gizmos.DrawSphere(tempPathPoints[i], 1);
					Gizmos.DrawLine(tempPathPoints[i], tempPathPoints[i + 1]);
				}
				Gizmos.DrawSphere(tempPathPoints[tempPathPoints.Count-1], 2);
			}
			


			if (GizmosShowSurfaceSpawner && _points.Count > 0)
			{
				
					Gizmos.color = GizmosSurfaceSpawnerColor;
					foreach (var lakeVertex in _points)
					{

						Gizmos.DrawSphere(lakeVertex, GizmosSurfaceSpawnerSize);

					}
			}
			
		}

		
#endif

		#endregion


	}
}