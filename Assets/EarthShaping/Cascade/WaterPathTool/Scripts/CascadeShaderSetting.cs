﻿using UnityEngine;

namespace com.earthshaping.Cascade
{

	[SerializeField]
	public struct CascadeShaderSetting
	{

		public Texture2D Texture2D;
	}
}