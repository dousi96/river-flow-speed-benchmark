﻿/* Copyright 2017-2018 (C) Antonio Ripa - All Rights Reserved
* This file is part of EarthShaping Project 
* Unauthorized copying of this file, via any medium is strictly prohibited by law
* This file is proprietary and confidential
* Written by Antonio Ripa ( antonio.ripa@earthshaping.com )
*/

using System;
using com.earthshaping.framework.Enums;
#if UNITY_EDITOR
#endif
using UnityEngine;

namespace com.earthshaping.framework.ThreadManager
{
	
	[Serializable]
	public struct MultiThreadSetting
	{
		[SerializeField]
		private PowerOfTwo cellScanSize;
		public PowerOfTwo CellScanSize
		{
			get { return cellScanSize; }
			set { cellScanSize = value; }
		}

        [SerializeField]
        private float cellScanRoundSize;
        public float CellScanRoundSize
        {
            get { return cellScanRoundSize; }
            set { cellScanRoundSize = value; }
        }

        [SerializeField]
		private int _iThreadCount;
		public int iThreadCount
		{
			get { return _iThreadCount; }
			set { _iThreadCount = value; }
		}


		[SerializeField]
		private bool _IsMultiThread;
		public bool IsMultiThread
		{
			get { return _IsMultiThread; }
			set { _IsMultiThread = value; }
		}


        [SerializeField]
        private bool _enableRealTimeScanCell;
        public bool EnableRealTimeScanCell
        {
            get { return _enableRealTimeScanCell; }
            set { _enableRealTimeScanCell = value; }
        }

    }

}

