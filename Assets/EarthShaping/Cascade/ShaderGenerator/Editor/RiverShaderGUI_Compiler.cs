﻿//////////////////////////////////////////////////////
// River
// Copyright (c) Jason Booth, slipster216@gmail.com
//////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Text;
using System.Collections.Generic;
using System.Linq;

public partial class RiverShaderGUI : ShaderGUI 
{
	static TextAsset body_main;
	static TextAsset body_sparkles;
	static TextAsset props_body;
	static TextAsset props_refraction;
	static TextAsset props_sparkles;
	static TextAsset props_caustics;
	static TextAsset props_slosh;
	static TextAsset props_rain;
	static TextAsset props_turbulence;
	static TextAsset props_waterfalls;
	static TextAsset props_snow;
	static TextAsset shared;

	static TextAsset props_flotsam1;
	static TextAsset props_flotsam2;
	static TextAsset props_flotsam3;
	static TextAsset props_lake;



	[MenuItem ("Assets/Create/Shader/River Shader")]
	static void NewShader2()
	{
		NewShader();
	}

	[MenuItem ("Assets/Create/River/River Shader")]
	public static Shader NewShader()
	{
		string path = "Assets";
		foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets))
		{
			path = AssetDatabase.GetAssetPath(obj);
			if (System.IO.File.Exists(path))
			{
				path = System.IO.Path.GetDirectoryName(path);
			}
			break;
		}
		path = path.Replace("\\", "/");
		path = AssetDatabase.GenerateUniqueAssetPath(path + "/River.shader");
		string name = path.Substring(path.LastIndexOf("/"));
		name = name.Substring(0, name.IndexOf("."));
		RiverCompiler compiler = new RiverCompiler();
		compiler.Init();
		string ret = compiler.Compile(new string[0], name);
		System.IO.File.WriteAllText(path, ret);
		AssetDatabase.Refresh();
		return AssetDatabase.LoadAssetAtPath<Shader>(path);
	}

	// Added 04.01.2018
	// Used by Cascade Window - do not modify
	public static Shader CreateShader(string path,string shaderName)
	{
		var assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + shaderName + ".shader");
		RiverCompiler compiler = new RiverCompiler();
		compiler.Init();
		string ret = compiler.Compile(new string[0], shaderName);
		System.IO.File.WriteAllText(assetPathAndName, ret);
		AssetDatabase.Refresh();
		return AssetDatabase.LoadAssetAtPath<Shader>(assetPathAndName);
	}


	public class RiverCompiler
	{
		public enum DefineFeature
		{
			_REFRACTION = 0,
			_SPARKLES,
			_CAUSTICS,
			_SLOSH,
			_FLOWWATER,
			_FLOWSPARKLES,
			_MULTICAMSUPPORT,
			_SINUSOIDAL, 
			_RAINDROPS,
			_WATERFALLS,
			_LAKES,
			_LAKERIPPLES,
			_LAKERIPPLES2,
			_LAKERIPPLES3,
			_DEBUG_UV,
			_DEBUG_FLOWUV,
			_DEBUG_DEPTHUV,
			_DEBUG_FOAMUV,
			_DEBUG_TERRAIN,
			_DEBUG_DEPTHCALM,
			_DEBUG_DEPTHOPACITY,
			_DEBUG_FRESNELOPACITY,
			_DEBUG_FOAM,
			_DEBUG_FOAMSTR,
			_DEBUG_FOAMDIST,
			_DEBUG_FADE,
			_DEBUG_COLOR,
			_DEBUG_SHORELINE,
			_FLOTSAM1,
			_FLOTSAM2,
			_FLOTSAM3,
			_FLOTSAM1_SPRITE,
			_FLOTSAM2_SPRITE,
			_FLOTSAM3_SPRITE,
			_SNOW,
			_NORIVER,
			_WAVES,
			_LAKEWAVES,
			_LAKEWAVES2,
			_LAKEWAVES3,
			_LAKEWAVES4,
			_ADVANCEDSTREAM 

		}

		public enum MotionType
		{
			UV, 
			FlowMapped,
		}
		 
		public enum LakeRippleOctaves
		{
			None = 0,
			One,
			Two,
			Three
		}

		public enum WaveLayers
		{
			None = 0,
			One,
			Two,
			Three, 
			Four
		}


		public WaterModes waterModes = WaterModes.RiverOnly;
		public bool refraction = true;
		public bool sparkles = true;
		public bool caustics = false;
		public bool slosh = true;
		public bool multiCam = false;
		public bool turbulence = true;
		public bool waves = false;
		public bool rainDrops = false;
		public bool waterFalls = false;
		public bool snow = false;

		public bool normal1 = false; // Temporary for test

		public LakeRippleOctaves lakeRipples = LakeRippleOctaves.None;
		public WaveLayers lakeWaves = WaveLayers.None;

		public MotionType waterMotion = MotionType.UV;
		public MotionType sparkleMotion = MotionType.FlowMapped;

		public enum FlotsamMode
		{
			None,
			Image,
			SpriteSheet
		}

		public enum WaterModes
		{
			RiverOnly,
			RiverAndLake,
			LakeOnly
		}

		public FlotsamMode flotsam1 = FlotsamMode.None;
		public FlotsamMode flotsam2 = FlotsamMode.None;
		public FlotsamMode flotsam3 = FlotsamMode.None;



		public enum DebugMode
		{
			None = 0,
			UV,
			FlowUV,
			DepthUV,
			FoamUV,
			ShorelineMask,
			Terrain,
			DepthCalm,
			DepthOpacity,
			FresnelOpacity,
			Foam,
			FoamStr,
			FoamDist,
			Fade,
			Color,
		};

		public DebugMode debugMode = DebugMode.None;

		public static string GetFeatureName(DefineFeature feature)
		{
			return System.Enum.GetName(typeof(DefineFeature), feature);
		}

	  public static bool HasFeature(string[] keywords, DefineFeature feature)
	  {
		 string f = GetFeatureName(feature);
		 for (int i = 0; i < keywords.Length; ++i)
		 {
			if (keywords[i] == f)
			   return true;
		 }
		 return false;
	  }

	  public void Unpack(string[] features)
	{
		normal1 = HasFeature(features, DefineFeature._ADVANCEDSTREAM); // temporary for test

		 refraction = HasFeature(features, DefineFeature._REFRACTION);
		 sparkles = HasFeature(features, DefineFeature._SPARKLES);
		 caustics = HasFeature(features, DefineFeature._CAUSTICS);
		 slosh = HasFeature(features, DefineFeature._SLOSH);
		 multiCam = HasFeature(features, DefineFeature._MULTICAMSUPPORT);
		 turbulence = (HasFeature(features, DefineFeature._SINUSOIDAL));
		 waterFalls = (HasFeature(features, DefineFeature._WATERFALLS));
		 rainDrops = HasFeature(features, DefineFeature._RAINDROPS);

		 waterMotion = (HasFeature(features, DefineFeature._FLOWWATER)) ? MotionType.FlowMapped : MotionType.UV;
		 sparkleMotion = (HasFeature(features, DefineFeature._FLOWSPARKLES)) ? MotionType.FlowMapped : MotionType.UV;
		 waterModes = WaterModes.RiverOnly;
		 if (HasFeature(features, DefineFeature._NORIVER))
			waterModes = WaterModes.LakeOnly;
		 else if (HasFeature(features, DefineFeature._LAKES))
			waterModes = WaterModes.RiverAndLake;

		 waves = HasFeature(features, DefineFeature._WAVES);
		 lakeRipples = LakeRippleOctaves.None;
		 if (HasFeature(features, DefineFeature._LAKERIPPLES))
		 {
			lakeRipples = LakeRippleOctaves.One;
		 }
		 if (HasFeature(features, DefineFeature._LAKERIPPLES2))
		 {
			lakeRipples = LakeRippleOctaves.Two;
		 }
		 if (HasFeature(features, DefineFeature._LAKERIPPLES3))
		 {
			lakeRipples = LakeRippleOctaves.Three;
		 }

		lakeWaves = WaveLayers.None;
		if (HasFeature(features, DefineFeature._LAKEWAVES	))
		{
			lakeWaves = WaveLayers.One;
		}
		if (HasFeature(features, DefineFeature._LAKEWAVES2))
		{
			lakeWaves = WaveLayers.Two;
		}
		if (HasFeature(features, DefineFeature._LAKEWAVES3))
		{
			lakeWaves = WaveLayers.Three;
		}
		if (HasFeature(features, DefineFeature._LAKEWAVES4))
		{
			lakeWaves = WaveLayers.Four;
		}



		flotsam1 = FlotsamMode.None;
		 flotsam2 = FlotsamMode.None;
		 flotsam3 = FlotsamMode.None;
		
		 flotsam1 = HasFeature(features, DefineFeature._FLOTSAM1) ? HasFeature(features, DefineFeature._FLOTSAM1_SPRITE) ? FlotsamMode.SpriteSheet : FlotsamMode.Image : FlotsamMode.None;
		 flotsam2 = HasFeature(features, DefineFeature._FLOTSAM2) ? HasFeature(features, DefineFeature._FLOTSAM2_SPRITE) ? FlotsamMode.SpriteSheet : FlotsamMode.Image : FlotsamMode.None;
		 flotsam3 = HasFeature(features, DefineFeature._FLOTSAM3) ? HasFeature(features, DefineFeature._FLOTSAM3_SPRITE) ? FlotsamMode.SpriteSheet : FlotsamMode.Image : FlotsamMode.None;

		 //snow = HasFeature(features, DefineFeature._SNOW);

		 debugMode = DebugMode.None;
		 if (HasFeature(features, DefineFeature._DEBUG_UV))
		 {
			debugMode = DebugMode.UV;
		 }
		 else if (HasFeature(features, DefineFeature._DEBUG_TERRAIN))
		 {
			debugMode = DebugMode.Terrain;
		 }
		 else if (HasFeature(features, DefineFeature._DEBUG_FRESNELOPACITY))
		 {
			debugMode = DebugMode.FresnelOpacity;
		 }
		 else if (HasFeature(features, DefineFeature._DEBUG_FOAMUV))
		 {
			debugMode = DebugMode.FoamUV;
		 }
		 else if (HasFeature(features, DefineFeature._DEBUG_FOAMDIST))
		 {
			debugMode = DebugMode.FoamDist;
		 }
		 else if (HasFeature(features, DefineFeature._DEBUG_FOAM))
		 {
			debugMode = DebugMode.Foam;
		 }
		 else if (HasFeature(features, DefineFeature._DEBUG_FOAMSTR))
		 {
			debugMode = DebugMode.FoamStr;
		 }
		 else if (HasFeature(features, DefineFeature._DEBUG_FLOWUV))
		 {
			debugMode = DebugMode.FlowUV;
		 }
		 else if (HasFeature(features, DefineFeature._DEBUG_DEPTHUV))
		 {
			debugMode = DebugMode.DepthUV;
		 }
		 else if (HasFeature(features, DefineFeature._DEBUG_DEPTHOPACITY))
		 {
			debugMode = DebugMode.DepthOpacity;
		 }
		 else if (HasFeature(features, DefineFeature._DEBUG_DEPTHCALM))
		 {
			debugMode = DebugMode.DepthCalm;
		 }
		 else if (HasFeature(features, DefineFeature._DEBUG_FADE))
		 {
			debugMode = DebugMode.Fade;
		 }
		 else if (HasFeature(features, DefineFeature._DEBUG_COLOR))
		 {
			debugMode = DebugMode.Color;
		 }
		 else if (HasFeature(features, DefineFeature._DEBUG_SHORELINE))
		 {
			debugMode = DebugMode.ShorelineMask;
		 }
			
	  }

	  public void Pack(Material targetMat)
	  {
		 List<string> keywords = new List<string>();

			if(normal1) 
				keywords.Add(GetFeatureName(DefineFeature._ADVANCEDSTREAM));


		if (refraction)
		 {
			keywords.Add(GetFeatureName(DefineFeature._REFRACTION));
		 }
		 if (sparkles)
		 {
			keywords.Add(GetFeatureName(DefineFeature._SPARKLES));
		 }
		 if (caustics)
		 {
			keywords.Add(GetFeatureName(DefineFeature._CAUSTICS));
		 }
		 if (slosh)
		 {
			keywords.Add(GetFeatureName(DefineFeature._SLOSH));
		 }
		 if (multiCam)
		 {
			keywords.Add(GetFeatureName(DefineFeature._MULTICAMSUPPORT));
		 }
		 if (turbulence)
		 {
			keywords.Add(GetFeatureName(DefineFeature._SINUSOIDAL));
		 }
		 if(waves)
		 {
		  keywords.Add(GetFeatureName(DefineFeature._WAVES));
		 }

		 if (rainDrops)
		 {
			keywords.Add(GetFeatureName(DefineFeature._RAINDROPS));
		 }
		 if (waterFalls)
		 {
			keywords.Add(GetFeatureName(DefineFeature._WATERFALLS));
		 }
		 if (waterModes == WaterModes.LakeOnly || waterModes == WaterModes.RiverAndLake)
		 {
			keywords.Add(GetFeatureName(DefineFeature._LAKES));
		 }
		 if (waterModes == WaterModes.LakeOnly)
		 {
			keywords.Add(GetFeatureName(DefineFeature._NORIVER));
		 }

		 if (waterModes != WaterModes.RiverOnly && lakeRipples != LakeRippleOctaves.None)
		 {
			if (lakeRipples == LakeRippleOctaves.One)
			{
			   keywords.Add(GetFeatureName(DefineFeature._LAKERIPPLES));
			}
			else if (lakeRipples == LakeRippleOctaves.Two)
			{
			   keywords.Add(GetFeatureName(DefineFeature._LAKERIPPLES2));
			}
			else if (lakeRipples == LakeRippleOctaves.Three)
			{
			   keywords.Add(GetFeatureName(DefineFeature._LAKERIPPLES3));
			}

		}

		if (waterModes != WaterModes.RiverOnly && lakeWaves != WaveLayers.None)
		{	
			if (lakeWaves == WaveLayers.One)
			{
				keywords.Add(GetFeatureName(DefineFeature._LAKEWAVES));
			}
			else if (lakeWaves == WaveLayers.Two)
			{
				keywords.Add(GetFeatureName(DefineFeature._LAKEWAVES2));
			}
			else if (lakeWaves == WaveLayers.Three)
			{
				keywords.Add(GetFeatureName(DefineFeature._LAKEWAVES3));
			}
			else if (lakeWaves == WaveLayers.Four)
			{
				keywords.Add(GetFeatureName(DefineFeature._LAKEWAVES4));
			}
		}
		if (waterMotion == MotionType.FlowMapped)
		{
			keywords.Add(GetFeatureName(DefineFeature._FLOWWATER));
		}
		if (sparkles && sparkleMotion == MotionType.FlowMapped)
		{
			keywords.Add(GetFeatureName(DefineFeature._FLOWSPARKLES));
		}

		if (flotsam1 != FlotsamMode.None)
		{
			keywords.Add(GetFeatureName(DefineFeature._FLOTSAM1));
			if (flotsam1 == FlotsamMode.SpriteSheet)
			{
				keywords.Add(GetFeatureName(DefineFeature._FLOTSAM1_SPRITE));
			}
		}
		if (flotsam2 != FlotsamMode.None)
		{
		keywords.Add(GetFeatureName(DefineFeature._FLOTSAM2));
		if (flotsam2 == FlotsamMode.SpriteSheet)
		{
			keywords.Add(GetFeatureName(DefineFeature._FLOTSAM2_SPRITE));
		}
		}
		if (flotsam3 != FlotsamMode.None)
		{
		keywords.Add(GetFeatureName(DefineFeature._FLOTSAM3));
		if (flotsam3 == FlotsamMode.SpriteSheet)
		{
			keywords.Add(GetFeatureName(DefineFeature._FLOTSAM3_SPRITE));
		}
		}

		if (snow)
		{
		keywords.Add(GetFeatureName(DefineFeature._SNOW));   
		}



		switch (debugMode)
		{
		case DebugMode.DepthCalm:
			keywords.Add(GetFeatureName(DefineFeature._DEBUG_DEPTHCALM));
			break;
		case DebugMode.DepthOpacity:
			keywords.Add(GetFeatureName(DefineFeature._DEBUG_DEPTHOPACITY));
			break;
		case DebugMode.DepthUV:
			keywords.Add(GetFeatureName(DefineFeature._DEBUG_DEPTHUV));
			break;
		case DebugMode.FlowUV:
			keywords.Add(GetFeatureName(DefineFeature._DEBUG_FLOWUV));
			break;
		case DebugMode.Foam:
			keywords.Add(GetFeatureName(DefineFeature._DEBUG_FOAM));
			break;
		case DebugMode.FoamDist:
			keywords.Add(GetFeatureName(DefineFeature._DEBUG_FOAMDIST));
			break;
		case DebugMode.FoamStr:
			keywords.Add(GetFeatureName(DefineFeature._DEBUG_FOAMSTR));
			break;
		case DebugMode.FoamUV:
			keywords.Add(GetFeatureName(DefineFeature._DEBUG_FOAMUV));
			break;
		case DebugMode.FresnelOpacity:
			keywords.Add(GetFeatureName(DefineFeature._DEBUG_FRESNELOPACITY));
			break;
		case DebugMode.Terrain:
			keywords.Add(GetFeatureName(DefineFeature._DEBUG_TERRAIN));
			break;
		case DebugMode.UV:
			keywords.Add(GetFeatureName(DefineFeature._DEBUG_UV));
			break;
		case DebugMode.Fade:
			keywords.Add(GetFeatureName(DefineFeature._DEBUG_FADE));
			break;
		case DebugMode.Color:
			keywords.Add(GetFeatureName(DefineFeature._DEBUG_COLOR));
			break;
		case DebugMode.ShorelineMask:
			keywords.Add(GetFeatureName(DefineFeature._DEBUG_SHORELINE));
			break;

		}

		targetMat.shaderKeywords = keywords.ToArray();
	}



	  public void Init()
	  {
		 if (body_main == null)
		 {
			string[] paths = AssetDatabase.FindAssets("rivershader_ t:TextAsset");
			for (int i = 0; i < paths.Length; ++i)
			{
			   paths[i] = AssetDatabase.GUIDToAssetPath(paths[i]);
			}

			for (int i = 0; i < paths.Length; ++i)
			{
			   var p = paths[i];

			   if (p.EndsWith("rivershader_shared.txt"))
			   {
				  shared = AssetDatabase.LoadAssetAtPath<TextAsset>(p);
			   }
			   if (p.EndsWith("rivershader_body_main.txt"))
			   {
				  body_main = AssetDatabase.LoadAssetAtPath<TextAsset>(p);
			   }
			   if (p.EndsWith("rivershader_body_sparkles.txt"))
			   {
				  body_sparkles = AssetDatabase.LoadAssetAtPath<TextAsset>(p);
			   }
			   if (p.EndsWith("rivershader_properties_body.txt"))
			   {
				  props_body = AssetDatabase.LoadAssetAtPath<TextAsset>(p);
			   }
			   if (p.EndsWith("rivershader_properties_refraction.txt"))
			   {
				  props_refraction = AssetDatabase.LoadAssetAtPath<TextAsset>(p);
			   }
			   if (p.EndsWith("rivershader_properties_sparkles.txt"))
			   {
				  props_sparkles = AssetDatabase.LoadAssetAtPath<TextAsset>(p);
			   }
			   if (p.EndsWith("rivershader_properties_caustics.txt"))
			   {
				  props_caustics = AssetDatabase.LoadAssetAtPath<TextAsset>(p);
			   }
			   if (p.EndsWith("rivershader_properties_slosh.txt"))
			   {
				  props_slosh = AssetDatabase.LoadAssetAtPath<TextAsset>(p);
			   }
			   if (p.EndsWith("rivershader_properties_raindrops.txt"))
			   {
				  props_rain = AssetDatabase.LoadAssetAtPath<TextAsset>(p);
			   }
			   if (p.EndsWith("rivershader_properties_turbulence.txt"))
			   {
				  props_turbulence = AssetDatabase.LoadAssetAtPath<TextAsset>(p);
			   }
			   if (p.EndsWith("rivershader_properties_waterfalls.txt"))
			   {
				  props_waterfalls = AssetDatabase.LoadAssetAtPath<TextAsset>(p);
			   }
			   if (p.EndsWith("rivershader_properties_flotsam1.txt"))
			   {
				  props_flotsam1 = AssetDatabase.LoadAssetAtPath<TextAsset>(p);
			   }
			   if (p.EndsWith("rivershader_properties_flotsam2.txt"))
			   {
				  props_flotsam2 = AssetDatabase.LoadAssetAtPath<TextAsset>(p);
			   }
			   if (p.EndsWith("rivershader_properties_flotsam3.txt"))
			   {
				  props_flotsam3 = AssetDatabase.LoadAssetAtPath<TextAsset>(p);
			   }
				if (p.EndsWith("rivershader_properties_lake.txt"))
				{
					props_lake = AssetDatabase.LoadAssetAtPath<TextAsset>(p);
				}
			   if (p.EndsWith("rivershader_properties_snow.txt"))
			   {
				  props_snow = AssetDatabase.LoadAssetAtPath<TextAsset>(p);
			   }

				}

		 }
	  }

		
		void WriteIncluden( StringBuilder sb)
		{
			string[] paths = AssetDatabase.FindAssets("rivershader_include t:TextAsset");
			var include = AssetDatabase.LoadAssetAtPath<TextAsset>(AssetDatabase.GUIDToAssetPath(paths[0]));

			sb.AppendLine("   	CGINCLUDE");


			sb.AppendLine(include.text);
			sb.AppendLine("   	ENDCG");
		}

	  void WriteHeader(string[] features, StringBuilder sb)
	  {
		 sb.AppendLine();
		 sb.AppendLine();

		 sb.AppendLine("   SubShader {");
		 if (refraction && !multiCam)
		 {
			sb.AppendLine("      GrabPass { \"_RiverGrab\" }");
		 }

		 if (!refraction)
		 {
			sb.AppendLine("      Tags {\"Queue\" = \"Transparent\" \"RenderType\"=\"Transparent\" }");
		 }
		 else
		 {
			sb.AppendLine("      Tags {\"Queue\" = \"Geometry+110\"}");
		 }

		 sb.AppendLine("      CGPROGRAM");
		 sb.AppendLine();
	  }



	  void WriteFeatures(string[] features, StringBuilder sb)
	  { 
		 sb.AppendLine();
		 for (int i = 0; i < features.Length; ++i)
		 {
			sb.AppendLine("      #define " + features[i] + " 1");
		 }
		 sb.AppendLine();
	  }

	  void WriteFooter(string[] features, StringBuilder b)
	  {
		 b.AppendLine("   }");
		 b.AppendLine("   CustomEditor \"RiverShaderGUI\"");
		 b.AppendLine("}");
	  }

	  void WriteProperties(string[] features, StringBuilder sb)
	  {
		 sb.AppendLine("   Properties {");
		 sb.AppendLine(props_body.text);
		 if (refraction)
		 {
			sb.AppendLine(props_refraction.text);
		 }
		 if (sparkles)
		 {
			sb.AppendLine(props_sparkles.text);
		 }
		 if (caustics)
		 {
			sb.AppendLine(props_caustics.text);
		 }
		 if (slosh)
		 {
			sb.AppendLine(props_slosh.text);
		 }
		 if (rainDrops)
		 {
			sb.AppendLine(props_rain.text);
		 }
		 if (turbulence)
		 {
			sb.AppendLine(props_turbulence.text);
		 }
		 if (waterFalls)
		 {
			sb.AppendLine(props_waterfalls.text);
		 }
		 if (flotsam1 != RiverCompiler.FlotsamMode.None)
		 {
			sb.AppendLine(props_flotsam1.text);
		 }
		 if (flotsam2 != RiverCompiler.FlotsamMode.None)
		 {
			sb.AppendLine(props_flotsam2.text);
		 }
		 if (flotsam3 != RiverCompiler.FlotsamMode.None)
		 {
			sb.AppendLine(props_flotsam3.text);
		 }

		 if (waterModes != WaterModes.RiverOnly)
		{
			sb.AppendLine(props_lake.text);
		}

		 if (snow)
		 {
			sb.AppendLine(props_snow.text);   
		 }

			sb.AppendLine("   }");
	  }
		 


	  static StringBuilder sBuilder = new StringBuilder(25600);
	  public string Compile(string[] features, string name)
	  {
		 Init();
		 Unpack(features);
		 sBuilder.Length = 0;
		 var sb = sBuilder;
		 sb.AppendLine("//////////////////////////////////////////////////////");
		 sb.AppendLine("// Cascade Shader");
		 sb.AppendLine("// Copyright (c) Jason Booth");
		 sb.AppendLine("//");
		 sb.AppendLine("// Auto-generated shader code, don't hand edit!");
		 sb.AppendLine("//   Compiled with Rivers and Lakes " + RiverVersion);
		 sb.AppendLine("//   Unity : " + Application.unityVersion);
		 sb.AppendLine("//   Platform : " + Application.platform);
		 sb.AppendLine("//////////////////////////////////////////////////////");
		 sb.AppendLine();

		 sb.Append("Shader \"");
		 sb.Append(name);

		 sb.AppendLine("\" {");


		 // props
		 WriteProperties(features, sb);

		WriteIncluden(sb); // Temporary incude noise library for ripple test

		WriteHeader(features, sb);

	

		sb.AppendLine();

		 WriteFeatures(features, sb);
		 sb.AppendLine(shared.text);
		 sb.AppendLine(body_main.text);
		 if (sparkles)
		 {
			sb.AppendLine("         CGPROGRAM");
			WriteFeatures(features, sb);
			sb.AppendLine(shared.text);
			sb.AppendLine(body_sparkles.text);
		 }
			
		 WriteFooter(features, sb);

		 string output = sb.ToString();
		 // fix newline mixing warnings..
		 output = System.Text.RegularExpressions.Regex.Replace(output, "\r\n?|\n", System.Environment.NewLine);
		 return output;
	  }

	  public void Compile(Material m, string shaderName = null)
	  {
		 var path = AssetDatabase.GetAssetPath(m.shader);
		 string nm = m.shader.name;
		 if (!string.IsNullOrEmpty(shaderName))
		 {
			nm = shaderName;
		 }

		 string riverShader = Compile(m.shaderKeywords, nm);

		 System.IO.File.WriteAllText(path, riverShader);

		 EditorUtility.SetDirty(m);
		 AssetDatabase.Refresh();
	  }
   }
}
