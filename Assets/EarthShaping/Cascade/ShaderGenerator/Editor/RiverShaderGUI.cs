﻿//////////////////////////////////////////////////////
// River
// Copyright (c) Jason Booth, slipster216@gmail.com
//////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;


public partial class RiverShaderGUI : ShaderGUI
{
   public static readonly string RiverVersion = "1.0";

   RiverCompiler compiler = new RiverCompiler();

   GUIContent CShaderName = new GUIContent("Name", "Menu path with name for the shader");
   GUIContent CShaderRefraction = new GUIContent("Refraction", "Distort the terrain under the water");
   GUIContent CShaderSparkles = new GUIContent("Sparkles", "Generate sparkle pass, which adds sparkling highlights");
   GUIContent CShaderCaustics = new GUIContent("Caustics", "Render Caustics on the river bottom");
   GUIContent CShaderTurbulence = new GUIContent("Turbulence", "Move vertices to create turbulent water?");
   GUIContent CShaderSlosh = new GUIContent("Slosh Water", "Causes water to slosh around, side to side");

   GUIContent CShaderSupportMulticam = new GUIContent("MultiCam Support",
										"Support for multiple cameras with Refraction at runtime, for things like VR stereo rendering- Unfortunately disables refraction in the scene view");

   GUIContent CShaderRainDrops = new GUIContent("Rain Drops", "Support for rain drops on the water surface");

   GUIContent CTexture = new GUIContent("Normal/Foam Texture", "Special texture with normal in RG and foam mask in B");
   GUIContent CNormalStrength = new GUIContent("Normal Strength", "Strength of Normal Map on surface");
   GUIContent CSurfaceTint = new GUIContent("Surface Tint", "Tint color for the water near the surface");
   GUIContent CDepthTint = new GUIContent("Depth Tint", "Tint for color at max depth");
   GUIContent CTintDepth = new GUIContent("Tint Depth", "Depth for depth tint");

   GUIContent CFlowSpeed = new GUIContent("Flow Speed", "Speed of water motion");
   GUIContent CFlowCycle = new GUIContent("Foam Speed", "Rate of foam speed");
   GUIContent CFoamStrength = new GUIContent("Foam Strength", "Strength of Foam on water flats and falls");
   GUIContent CFresnelOpacity = new GUIContent("Fresnel Opacity", "How clear the water is on glancing angles");
   GUIContent CDepthCalm = new GUIContent("Depth Calming", "How deep the water is before the surface becomes calm");
   GUIContent CReflectivity = new GUIContent("Reflectivity", "How reflective the water surface is");
   GUIContent CDepthEdge = new GUIContent("Depth Edge", "How sharp the transition on the edge of the water is");
   GUIContent CFoamEdge = new GUIContent("Foam Edge Width", "How wide the Foam is from the shoreline");
   GUIContent CFoamEdgeStr = new GUIContent("Foam Edge Strength", "How strong is the edge foam?");
   GUIContent CFoamScale = new GUIContent("Foam Scale", "Texture Scale for foam");
   GUIContent CFoamColor = new GUIContent("Foam Color", "Color of center foam");
   GUIContent CFoamEdgeColor = new GUIContent("Foam Edge Color", "Color of foam around edges");

   GUIContent CRefraction = new GUIContent("Refraction", "How much to distort the surface under the water");

   GUIContent CSparkleStrength = new GUIContent("Sparkle Strength", "How strong the sparkles should be on flats and falls");

   GUIContent CTurbulenceRate = new GUIContent("Turbulence Rate", "Speed at which mesh will undulate");
   GUIContent CTurbulenceAmplitude = new GUIContent("Turbulence Amplitude", "Amount of undulation to apply");
   GUIContent CTurbulenceFrequency = new GUIContent("Turbulence Frequency", "Size of ripples");
   GUIContent CSparkleScale = new GUIContent("Sparkle Scale", "Size of sparkles on water");
   GUIContent CSparkleEdge = new GUIContent("Sparkle Edge", "Fades Sparkles around shore edges");

   GUIContent CSparkleSpeedBoost = new GUIContent("Sparkle Speed Boost", "Rate at which sparkles move faster than the flow");

   GUIContent CCausticTexture = new GUIContent("Caustic Texture", "Texture for Caustic effect");
   GUIContent CCausticScale = new GUIContent("Caustic Scale", "Scale of Caustic texture");
   GUIContent CCausticStrength = new GUIContent("Caustic Strength", "Strength of distortion");
   GUIContent CCausticFrequency = new GUIContent("Caustic Frequency", "Speed of distortion");
   GUIContent CCausticAmplitude = new GUIContent("Caustic Amplitude", "How much the effect should show up");
   GUIContent CCausticDepth = new GUIContent("Caustic Depth", "How deep before the effect wears off");

   GUIContent CSloshRate = new GUIContent("Slosh Rate", "How fast the slosh effect has");
   GUIContent CSloshScale = new GUIContent("Slosh Scale", "Scale of slosh effect");
   GUIContent CSloshStrength = new GUIContent("Slosh Strength", "Strength of slosh effect");
   GUIContent CRainIntensity = new GUIContent("Rain Intensity", "How strong the rain is");
   GUIContent CRainUVScale = new GUIContent("Rain UV Scale", "Size of rain drops");

   GUIContent CWaterfalls = new GUIContent("Waterfalls", "Creates smooth waterfalls on hard drops");
   GUIContent CWaterfallSpeed = new GUIContent("Waterfall Speed", "Speed of waterfalls");
   GUIContent CWaterfallUVScale = new GUIContent("Waterfall UV Scale", "UV Scale for waterfall texture");
   GUIContent CWaterfallDensity = new GUIContent("Waterfall Density", "Density of waterfall texture");
   GUIContent CWaterfallThreshold = new GUIContent("Waterfall Threshold", "Angle at which waterfalls start");
   GUIContent CWaterfallOpacity = new GUIContent("Waterfall Opacity", "Opacity of water falls");

   GUIContent CFlotsam1 = new GUIContent("Flotsam 1", "Flotsam Mode, can be none for no flotsam layer, Image for a scrolling texture, or Sprite which uses a texture baesd sprite sheet to splat flotsom randomly in the water");

   GUIContent CFlotsam2 = new GUIContent("Flotsam 2", "Flotsam Mode, can be none for no flotsam layer, Image for a scrolling texture, or Sprite which uses a texture baesd sprite sheet to splat flotsom randomly in the water");

   GUIContent CFlotsam3 = new GUIContent("Flotsam 3", "Flotsam Mode, can be none for no flotsam layer, Image for a scrolling texture, or Sprite which uses a texture baesd sprite sheet to splat flotsom randomly in the water");

   GUIContent CWaterTypes = new GUIContent("Water Types", "Can have lakes and rivers, or just one or the other in the shader");
   GUIContent CLakeRipple = new GUIContent("Lake Surface Ripple", "When enabled, multiple normal samples are combined in multiple directions to create a higher quality ripple effect. Up to three octaves available.");
   GUIContent CLakeWaves = new GUIContent("Lake Waves", "When enabled, multiple normal samples are combined in multiple directions to create a higher quality ripple effect. Up to three octaves available.");



	GUIContent CFlotsamTex = new GUIContent("Flotsam Texture", "Texture for Flotsam Layer");
   GUIContent CFlotsamSpeed = new GUIContent("Flotsam Speed", "Speed verses main speed");
   GUIContent CFlotsamDepth = new GUIContent("Flotsam Depth", "Depth of flotsam");
   GUIContent CFlotsamScale = new GUIContent("Flotsam Scale", "UV Scale of flotsam");
   GUIContent CFlotsamFrequency = new GUIContent("Flotsam Frequency", "How much flotsam should appear");

   GUIContent CFlotsamSheetSize = new GUIContent("Flotsam Sheet Size",
								   "Dimentions of sprite sheet. If using a 4 by 4 sheet, would be 0.25");

   GUIContent CFlotsamMaxIndex = new GUIContent("Flotsam Max Index",
								  "Maximum index to use, so if you only have 12 of the 16 textures on a 4x4 sheet, you can prevent it from using the last few");

   GUIContent CWaterMotion = new GUIContent("Water Motion", "Flow along UVs, or use Flow Mapping");
   GUIContent CSparkleMotion = new GUIContent("Sparkle Motion", "Flow along UVs, or use Flow Mapping");

   //GUIContent CSnow = new UnityEngine.GUIContent("Snow and Freezing", "Allow lake to freeze and get covered with snow");
   GUIContent CFlowDebugArrowScale = new GUIContent("Flow Arrow Scale", "Set the flow arrow scale");


   static GUIContent CSnowDiffTex = new GUIContent("Snow Diffuse", "Diffuse with height map in alpha for snow");
   static GUIContent CSnowNormTex = new GUIContent("Snow NormSAO", "Normal, smoothness, and ao for snow");
   static GUIContent CIceDiff = new GUIContent("Ice Diffuse", "Diffuse texture for ice");
   static GUIContent CIceNormTex = new GUIContent("Ice NormSAO", "Normal, smoothness, and ao for ice");
   static GUIContent CSnowNoise = new GUIContent("Noise", "Noise texture for snow and ice");

   static GUIContent CCrystals = new GUIContent("Crystals", "Blend between soft and icy snow");
   static GUIContent CSnowVsIce = new GUIContent("Snow vs Ice", "Controls how much snow or ice is present");
   static GUIContent CMelt = new GUIContent("Melt", "Creates an area of wetness around the snow edge");
   static GUIContent CUpVector = new GUIContent("Snow Up Vector", "Direction snow came from");
   static GUIContent CHeightRange = new GUIContent("Height Range", "Start and end height for snow coverage");

	static GUIContent CAdvancedStream = new GUIContent("Enriched Stream Mode", "Enable Enriched Stream Mode");

	
   string[] tabs = { "River", "Lake" };
   int curTab = 0;
   GUIContent CTab = new GUIContent("Water Mode", "Are the below settings for rivers or lakes?");

   bool needsCompile = false;


   public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] props)
   {
	  EditorGUI.BeginChangeCheck(); // sync materials
	  Material targetMat = materialEditor.target as Material;


	  compiler.Init();
	  compiler.Unpack(targetMat.shaderKeywords);
	  string shaderName = targetMat.shader.name;


	  EditorGUI.BeginChangeCheck(); // needs compile

	  if (RiverUtilities.DrawRollup("Shader Generator"))
	  {
		 shaderName = EditorGUILayout.DelayedTextField(CShaderName, shaderName);

		 compiler.waterModes = (RiverCompiler.WaterModes)EditorGUILayout.EnumPopup(CWaterTypes, compiler.waterModes);
			   
		compiler.normal1 = EditorGUILayout.Toggle(CAdvancedStream, compiler.normal1);

			EditorGUILayout.BeginHorizontal();
		 compiler.refraction = EditorGUILayout.Toggle(CShaderRefraction, compiler.refraction);
		 if (compiler.refraction)
		 {
			compiler.multiCam = EditorGUILayout.Toggle(CShaderSupportMulticam, compiler.multiCam);
		 }
		 EditorGUILayout.EndHorizontal();
		 compiler.waterMotion = (RiverCompiler.MotionType)EditorGUILayout.EnumPopup(CWaterMotion, compiler.waterMotion);
		 compiler.turbulence = EditorGUILayout.Toggle(CShaderTurbulence, compiler.turbulence);
		 // Waves only if lake mode
		 compiler.waves = compiler.waterModes == RiverCompiler.WaterModes.LakeOnly ? compiler.turbulence : false;

		 compiler.slosh = EditorGUILayout.Toggle(CShaderSlosh, compiler.slosh);
		 compiler.sparkles = EditorGUILayout.Toggle(CShaderSparkles, compiler.sparkles);
		 if (compiler.sparkles)
		 {
			compiler.sparkleMotion =
					(RiverCompiler.MotionType)EditorGUILayout.EnumPopup(CSparkleMotion, compiler.sparkleMotion);
		 }
		 compiler.caustics = EditorGUILayout.Toggle(CShaderCaustics, compiler.caustics);
		 compiler.rainDrops = EditorGUILayout.Toggle(CShaderRainDrops, compiler.rainDrops);
		 compiler.waterFalls = EditorGUILayout.Toggle(CWaterfalls, compiler.waterFalls);
		 //compiler.snow = EditorGUILayout.Toggle(CSnow, compiler.snow);

		 compiler.flotsam1 = (RiverCompiler.FlotsamMode)EditorGUILayout.EnumPopup(CFlotsam1, compiler.flotsam1);
		 compiler.flotsam2 = (RiverCompiler.FlotsamMode)EditorGUILayout.EnumPopup(CFlotsam2, compiler.flotsam2);
		 compiler.flotsam3 = (RiverCompiler.FlotsamMode)EditorGUILayout.EnumPopup(CFlotsam3, compiler.flotsam3);


		if (compiler.waterModes != RiverCompiler.WaterModes.RiverOnly)
		{
			using (new EditorGUILayout.VerticalScope("box"))
			{
				compiler.lakeRipples = (RiverCompiler.LakeRippleOctaves)EditorGUILayout.EnumPopup(CLakeRipple, compiler.lakeRipples);
				// added antoripa 22.05.2018
				compiler.lakeWaves = compiler.waterModes == RiverCompiler.WaterModes.LakeOnly && compiler.turbulence ? (RiverCompiler.WaveLayers)EditorGUILayout.EnumPopup(CLakeWaves, compiler.lakeWaves) : compiler.lakeWaves;
			}

		}

		compiler.debugMode = (RiverCompiler.DebugMode)EditorGUILayout.EnumPopup("Debug Mode", compiler.debugMode);
	  }
	  needsCompile = EditorGUI.EndChangeCheck();


	  // note, ideally we wouldn't draw the GUI for the rest of stuff if we need to compile.
	  // But we can't really do that without causing IMGUI to split warnings about
	  // mismatched GUILayout blocks
	  if (!needsCompile)
	  {
		 // added antoripa 30.12.2017
		 if (compiler.debugMode == RiverCompiler.DebugMode.FlowUV)
		 {
			materialEditor.ShaderProperty(FindProperty("_ArrowScale", props), CFlowDebugArrowScale);
		 }
		#region Material Properties

		if (RiverUtilities.DrawRollup("Material Properties"))
		 {
			using (new GUILayout.VerticalScope(GUI.skin.box))
			{
			   if (compiler.waterModes == RiverCompiler.WaterModes.RiverAndLake)
			   {
				  EditorGUILayout.BeginHorizontal();
				  EditorGUILayout.PrefixLabel(CTab);
				  curTab = GUILayout.Toolbar(curTab, tabs);
				  EditorGUILayout.EndHorizontal();
			   }

			   if (curTab == 0 && compiler.waterModes != RiverCompiler.WaterModes.LakeOnly)
			   {
				  var normalMap = FindProperty("_Water", props);
				  materialEditor.TexturePropertySingleLine(CTexture, normalMap);
				  RiverUtilities.EnforceDefaultTexture(normalMap, "rivershader_default_normalfoam");
				  materialEditor.ShaderProperty(FindProperty("_UVScale", props), "UV Scale");
				  materialEditor.ShaderProperty(FindProperty("_NormalStrength", props), CNormalStrength);

				  materialEditor.ShaderProperty(FindProperty("_FlowSpeed", props), CFlowSpeed);


				  materialEditor.ShaderProperty(FindProperty("_FresnelOpacity", props), CFresnelOpacity);
				  materialEditor.ShaderProperty(FindProperty("_DepthCalming", props), CDepthCalm);
				  materialEditor.ShaderProperty(FindProperty("_Reflectivity", props), CReflectivity);
				  if (compiler.refraction && targetMat.HasProperty("_Refraction"))
				  {
					 materialEditor.ShaderProperty(FindProperty("_Refraction", props), CRefraction);
				  }
				  materialEditor.ShaderProperty(FindProperty("_SurfaceTint", props), CSurfaceTint);
				  materialEditor.ShaderProperty(FindProperty("_DepthTint", props), CDepthTint);
				  materialEditor.ShaderProperty(FindProperty("_DepthDistance", props), CTintDepth);
				  materialEditor.ShaderProperty(FindProperty("_EdgeFade", props), CDepthEdge);
				  materialEditor.ShaderProperty(FindProperty("_FlowCycle", props), CFlowCycle);
				  materialEditor.ShaderProperty(FindProperty("_FoamScale", props), CFoamScale);
				  materialEditor.ShaderProperty(FindProperty("_FoamStrength", props), CFoamStrength);
				  materialEditor.ShaderProperty(FindProperty("_ShorelineFade", props), CFoamEdge);
				  materialEditor.ShaderProperty(FindProperty("_FoamEdgeStrength", props), CFoamEdgeStr);
				  materialEditor.ShaderProperty(FindProperty("_FoamColor", props), CFoamColor);
				  materialEditor.ShaderProperty(FindProperty("_FoamEdgeColor", props), CFoamEdgeColor);

				  if (compiler.sparkles && targetMat.HasProperty("_SparkleStrength"))
				  {
					 materialEditor.ShaderProperty(FindProperty("_SparkleStrength", props), CSparkleStrength);
					 materialEditor.ShaderProperty(FindProperty("_SparkleScale", props), CSparkleScale);
					 materialEditor.ShaderProperty(FindProperty("_SparkleFade", props), CSparkleEdge);
					 materialEditor.ShaderProperty(FindProperty("_SparkleSpeedBoost", props), CSparkleSpeedBoost);
				  }

				  if (compiler.turbulence && targetMat.HasProperty("_TurbulenceAmplitude"))
				  {
					 materialEditor.ShaderProperty(FindProperty("_TurbulenceAmplitude", props), CTurbulenceAmplitude);
				  }

				  if (compiler.slosh && targetMat.HasProperty("_SloshRate"))
				  {
					 materialEditor.ShaderProperty(FindProperty("_SloshStrength", props), CSloshStrength);
					 materialEditor.ShaderProperty(FindProperty("_SloshRate", props), CSloshRate);
					 materialEditor.ShaderProperty(FindProperty("_SloshScale", props), CSloshScale);
				  }
				  if (compiler.flotsam1 != RiverCompiler.FlotsamMode.None && targetMat.HasProperty("_FlotsamStrength1"))
				  {
					 var p = FindProperty("_FlotsamStrength1", props);
					 float str = p.vectorValue.x;
					 EditorGUI.BeginChangeCheck();
					 str = EditorGUILayout.Slider("Flotsam1 Strength", str, 0, 1.0f);
					 if (EditorGUI.EndChangeCheck())
					 {
						p.vectorValue = new Vector2(str, p.vectorValue.y);
					 }
				  }
				  if (compiler.flotsam2 != RiverCompiler.FlotsamMode.None && targetMat.HasProperty("_FlotsamStrength2"))
				  {
					 var p = FindProperty("_FlotsamStrength2", props);
					 float str = p.vectorValue.x;
					 EditorGUI.BeginChangeCheck();
					 str = EditorGUILayout.Slider("Flotsam2 Strength", str, 0, 1.0f);
					 if (EditorGUI.EndChangeCheck())
					 {
						p.vectorValue = new Vector2(str, p.vectorValue.y);
					 }
				  }
				  if (compiler.flotsam3 != RiverCompiler.FlotsamMode.None && targetMat.HasProperty("_FlotsamStrength3"))
				  {
					 var p = FindProperty("_FlotsamStrength3", props);
					 float str = p.vectorValue.x;
					 EditorGUI.BeginChangeCheck();
					 str = EditorGUILayout.Slider("Flotsam3 Strength", str, 0, 1.0f);
					 if (EditorGUI.EndChangeCheck())
					 {
						p.vectorValue = new Vector2(str, p.vectorValue.y);
					 }
				  }
			   }

			  
			   if (	curTab == 1  && compiler.waterModes != RiverCompiler.WaterModes.RiverOnly || 
					curTab == 0 && compiler.waterModes == RiverCompiler.WaterModes.LakeOnly	)
			   {
				  var normalMap = FindProperty("_Lake", props);
				  materialEditor.TexturePropertySingleLine(CTexture, normalMap);
				  RiverUtilities.EnforceDefaultTexture(normalMap, "rivershader_default_normalfoam");
				  materialEditor.ShaderProperty(FindProperty("_LakeUVScale", props), "UV Scale");
				  materialEditor.ShaderProperty(FindProperty("_LakeNormalStrength", props), CNormalStrength);

				  materialEditor.ShaderProperty(FindProperty("_LakeFlowSpeed", props), CFlowSpeed);

				  materialEditor.ShaderProperty(FindProperty("_LakeFresnelOpacity", props), CFresnelOpacity);
				  materialEditor.ShaderProperty(FindProperty("_LakeDepthCalming", props), CDepthCalm);
				  materialEditor.ShaderProperty(FindProperty("_LakeReflectivity", props), CReflectivity);
				  if (compiler.refraction && targetMat.HasProperty("_LakeRefraction"))
				  {
					 materialEditor.ShaderProperty(FindProperty("_LakeRefraction", props), CRefraction);
				  }
				  materialEditor.ShaderProperty(FindProperty("_LakeSurfaceTint", props), CSurfaceTint);
				  materialEditor.ShaderProperty(FindProperty("_LakeDepthTint", props), CDepthTint);
				  materialEditor.ShaderProperty(FindProperty("_LakeDepthDistance", props), CTintDepth);
				  materialEditor.ShaderProperty(FindProperty("_LakeEdgeFade", props), CDepthEdge);
				  materialEditor.ShaderProperty(FindProperty("_LakeFlowCycle", props), CFlowCycle);
				  materialEditor.ShaderProperty(FindProperty("_LakeFoamScale", props), CFoamScale);
				  materialEditor.ShaderProperty(FindProperty("_LakeFoamStrength", props), CFoamStrength);
				  materialEditor.ShaderProperty(FindProperty("_LakeShorelineFade", props), CFoamEdge);
				  materialEditor.ShaderProperty(FindProperty("_LakeFoamEdgeStrength", props), CFoamEdgeStr);
				  materialEditor.ShaderProperty(FindProperty("_LakeFoamColor", props), CFoamColor);
				  materialEditor.ShaderProperty(FindProperty("_LakeFoamEdgeColor", props), CFoamEdgeColor);

				  if (compiler.sparkles && targetMat.HasProperty("_LakeSparkleStrength"))
				  {
					 materialEditor.ShaderProperty(FindProperty("_LakeSparkleStrength", props), CSparkleStrength);
					 materialEditor.ShaderProperty(FindProperty("_LakeSparkleScale", props), CSparkleScale);
					 materialEditor.ShaderProperty(FindProperty("_LakeSparkleFade", props), CSparkleEdge);
					 materialEditor.ShaderProperty(FindProperty("_LakeSparkleSpeedBoost", props), CSparkleSpeedBoost);
				  }

				  if (compiler.turbulence && compiler.waterModes != RiverCompiler.WaterModes.LakeOnly && targetMat.HasProperty("_LakeTurbulenceAmplitude"))
				  {
						materialEditor.ShaderProperty(FindProperty("_LakeTurbulenceAmplitude", props), CTurbulenceAmplitude);
				  }

				  if (compiler.slosh && targetMat.HasProperty("_LakeSloshRate"))
				  {
					 materialEditor.ShaderProperty(FindProperty("_LakeSloshStrength", props), CSloshStrength);
					 materialEditor.ShaderProperty(FindProperty("_LakeSloshRate", props), CSloshRate);
					 materialEditor.ShaderProperty(FindProperty("_LakeSloshScale", props), CSloshScale);
				  }

				  if (compiler.flotsam1 != RiverCompiler.FlotsamMode.None && targetMat.HasProperty("_FlotsamStrength1"))
				  {
					 var p = FindProperty("_FlotsamStrength1", props);
					 float str = p.vectorValue.y;
					 EditorGUI.BeginChangeCheck();
					 str = EditorGUILayout.Slider("Flotsam1 Strength", str, 0, 1.0f);
					 if (EditorGUI.EndChangeCheck())
					 {
						p.vectorValue = new Vector2(p.vectorValue.x, str);
					 }
				  }
				  if (compiler.flotsam2 != RiverCompiler.FlotsamMode.None && targetMat.HasProperty("_FlotsamStrength2"))
				  {
					 var p = FindProperty("_FlotsamStrength2", props);
					 float str = p.vectorValue.y;
					 EditorGUI.BeginChangeCheck();
					 str = EditorGUILayout.Slider("Flotsam2 Strength", str, 0, 1.0f);
					 if (EditorGUI.EndChangeCheck())
					 {
						p.vectorValue = new Vector2(p.vectorValue.x, str);
					 }
				  }
				  if (compiler.flotsam3 != RiverCompiler.FlotsamMode.None && targetMat.HasProperty("_FlotsamStrength3"))
				  {
					 var p = FindProperty("_FlotsamStrength3", props);
					 float str = p.vectorValue.y;
					 EditorGUI.BeginChangeCheck();
					 str = EditorGUILayout.Slider("Flotsam3 Strength", str, 0, 1.0f);
					 if (EditorGUI.EndChangeCheck())
					 {
						p.vectorValue = new Vector2(p.vectorValue.x, str);
					 }
				  }

				  if(compiler.turbulence)
					LakeRippleWavesSetting(materialEditor, props);


				}
			}
			GUILayout.Space(3);
			if (compiler.turbulence && (compiler.waterModes == RiverCompiler.WaterModes.RiverAndLake || compiler.waterModes == RiverCompiler.WaterModes.RiverOnly) )
			{
			   using (new GUILayout.VerticalScope(GUI.skin.box))
			   {
				  materialEditor.ShaderProperty(FindProperty("_TurbulenceRate", props), CTurbulenceRate);
				  materialEditor.ShaderProperty(FindProperty("_TurbulenceFrequency", props), CTurbulenceFrequency);
			   }
			}


			if (compiler.waterFalls && targetMat.HasProperty("_WaterfallSpeed"))
			{
			   using (new GUILayout.VerticalScope(GUI.skin.box))
			   {
				  materialEditor.ShaderProperty(FindProperty("_WaterfallUVScale", props), CWaterfallUVScale);
				  materialEditor.ShaderProperty(FindProperty("_WaterfallSpeed", props), CWaterfallSpeed);
				  materialEditor.ShaderProperty(FindProperty("_WaterfallThickness", props), CWaterfallDensity);
				  materialEditor.ShaderProperty(FindProperty("_WaterfallThreshold", props), CWaterfallThreshold);
				  materialEditor.ShaderProperty(FindProperty("_WaterfallOpacity", props), CWaterfallOpacity);
			   }
			}

			if (compiler.flotsam1 != RiverCompiler.FlotsamMode.None || compiler.flotsam2 != RiverCompiler.FlotsamMode.None ||
			compiler.flotsam3 != RiverCompiler.FlotsamMode.None)
			{
			   if (compiler.flotsam1 != RiverCompiler.FlotsamMode.None && targetMat.HasProperty("_FlotsamTex1"))
			   {
				  using (new GUILayout.VerticalScope(GUI.skin.box))
				  {
					 var flotsam = FindProperty("_FlotsamTex1", props);
					 materialEditor.TexturePropertySingleLine(CFlotsamTex, flotsam);
					 RiverUtilities.EnforceDefaultTexture(flotsam, "rivershader_default_flotsam1");

					 materialEditor.ShaderProperty(FindProperty("_FlotsamSpeed1", props), CFlotsamSpeed);
					 materialEditor.ShaderProperty(FindProperty("_FlotsamDepth1", props), CFlotsamDepth);
					 materialEditor.ShaderProperty(FindProperty("_FlotsamScale1", props), CFlotsamScale);
					 if (compiler.flotsam1 == RiverCompiler.FlotsamMode.SpriteSheet && targetMat.HasProperty("_FlotsamFrequency1"))
					 {
						if (flotsam.textureValue == null || flotsam.textureValue.name == "rivershader_default_flotsam1")
						{
						   flotsam.textureValue = null;
						   RiverUtilities.EnforceDefaultTexture(flotsam, "rivershader_default_flotsam1sheet");
						}
						materialEditor.ShaderProperty(FindProperty("_FlotsamFrequency1", props), CFlotsamFrequency);
						materialEditor.ShaderProperty(FindProperty("_FlotsamSheetSize1", props), CFlotsamSheetSize);
						materialEditor.ShaderProperty(FindProperty("_FlotsamMaxIndex1", props), CFlotsamMaxIndex);
					 }
				  }
			   }
			   if (compiler.flotsam2 != RiverCompiler.FlotsamMode.None && targetMat.HasProperty("_FlotsamTex2"))
			   {
				  using (new GUILayout.VerticalScope(GUI.skin.box))
				  {
					 var flotsam = FindProperty("_FlotsamTex2", props);
					 materialEditor.TexturePropertySingleLine(CFlotsamTex, flotsam);
					 RiverUtilities.EnforceDefaultTexture(flotsam, "rivershader_default_flotsam1");
					 materialEditor.ShaderProperty(FindProperty("_FlotsamSpeed2", props), CFlotsamSpeed);
					 materialEditor.ShaderProperty(FindProperty("_FlotsamDepth2", props), CFlotsamDepth);
					 materialEditor.ShaderProperty(FindProperty("_FlotsamScale2", props), CFlotsamScale);
					 if (compiler.flotsam2 == RiverCompiler.FlotsamMode.SpriteSheet && targetMat.HasProperty("_FlotsamFrequency2"))
					 {
						if (flotsam.textureValue == null || flotsam.textureValue.name == "rivershader_default_flotsam1")
						{
						   flotsam.textureValue = null;
						   RiverUtilities.EnforceDefaultTexture(flotsam, "rivershader_default_flotsam1sheet");
						}
						materialEditor.ShaderProperty(FindProperty("_FlotsamFrequency2", props), CFlotsamFrequency);
						materialEditor.ShaderProperty(FindProperty("_FlotsamSheetSize2", props), CFlotsamSheetSize);
						materialEditor.ShaderProperty(FindProperty("_FlotsamMaxIndex2", props), CFlotsamMaxIndex);
					 }
				  }
			   }
			   if (compiler.flotsam3 != RiverCompiler.FlotsamMode.None && targetMat.HasProperty("_FlotsamTex3"))
			   {
				  using (new GUILayout.VerticalScope(GUI.skin.box))
				  {
					 var flotsam = FindProperty("_FlotsamTex3", props);
					 materialEditor.TexturePropertySingleLine(CFlotsamTex, flotsam);
					 RiverUtilities.EnforceDefaultTexture(flotsam, "rivershader_default_flotsam1");
					 materialEditor.ShaderProperty(FindProperty("_FlotsamSpeed3", props), CFlotsamSpeed);
					 materialEditor.ShaderProperty(FindProperty("_FlotsamDepth3", props), CFlotsamDepth);
					 materialEditor.ShaderProperty(FindProperty("_FlotsamScale3", props), CFlotsamScale);
					 if (compiler.flotsam3 == RiverCompiler.FlotsamMode.SpriteSheet && targetMat.HasProperty("_FlotsamFrequency3"))
					 {
						if (flotsam.textureValue == null || flotsam.textureValue.name == "rivershader_default_flotsam1")
						{
						   flotsam.textureValue = null;
						   RiverUtilities.EnforceDefaultTexture(flotsam, "rivershader_default_flotsam1sheet");
						}
						materialEditor.ShaderProperty(FindProperty("_FlotsamFrequency3", props), CFlotsamFrequency);
						materialEditor.ShaderProperty(FindProperty("_FlotsamSheetSize3", props), CFlotsamSheetSize);
						materialEditor.ShaderProperty(FindProperty("_FlotsamMaxIndex3", props), CFlotsamMaxIndex);
					 }
				  }
			   }
			}


			if (compiler.caustics && targetMat.HasProperty("_CausticTexture"))
			{
			   using (new GUILayout.VerticalScope(GUI.skin.box))
			   {
				  var causticMap = FindProperty("_CausticTexture", props);
				  materialEditor.TexturePropertySingleLine(CCausticTexture, causticMap);
				  RiverUtilities.EnforceDefaultTexture(causticMap, "rivershader_default_caustics");
				  materialEditor.ShaderProperty(FindProperty("_CausticDepth", props), CCausticDepth);
				  materialEditor.ShaderProperty(FindProperty("_CausticScale", props), CCausticScale);
				  materialEditor.ShaderProperty(FindProperty("_CausticStrength", props), CCausticStrength);
				  materialEditor.ShaderProperty(FindProperty("_CausticFrequency", props), CCausticFrequency);
				  materialEditor.ShaderProperty(FindProperty("_CausticAmplitude", props), CCausticAmplitude);
			   }
			}

			if (compiler.rainDrops && targetMat.HasProperty("_RainDropTexture"))
			{
			   using (new GUILayout.VerticalScope(GUI.skin.box))
			   {
				  var rainMap = FindProperty("_RainDropTexture", props);
				  RiverUtilities.EnforceDefaultTexture(rainMap, "rivershader_default_raindrops");
				  materialEditor.ShaderProperty(FindProperty("_RainIntensity", props), CRainIntensity);
				  Vector2 scales = FindProperty("_RainUVScales", props).vectorValue;
				  EditorGUI.BeginChangeCheck();
				  scales = EditorGUILayout.Vector2Field(CRainUVScale, scales);
				  if (EditorGUI.EndChangeCheck())
				  {
					 FindProperty("_RainUVScales", props).vectorValue = scales;
					 EditorUtility.SetDirty(targetMat);
				  }
			   }
			}

			if (compiler.snow && targetMat.HasProperty("_SnowParams"))
			{
			   using (new GUILayout.VerticalScope(GUI.skin.box))
			   {
				  var snowDiff = FindProperty("_SnowDiff", props);
				  var snowNorm = FindProperty("_SnowNormal", props);
				  var iceDiff = FindProperty("_IceDiff", props);
				  var iceNorm = FindProperty("_IceNormal", props);
				  var noise = FindProperty("_SnowNoise", props);
				  materialEditor.TexturePropertySingleLine(CSnowDiffTex, snowDiff);
				  materialEditor.TexturePropertySingleLine(CSnowNormTex, snowNorm);

				  materialEditor.TexturePropertySingleLine(CIceDiff, iceDiff);
				  materialEditor.TexturePropertySingleLine(CIceNormTex, iceNorm);
				  materialEditor.TexturePropertySingleLine(CSnowNoise, noise);

				  RiverUtilities.EnforceDefaultTexture(snowDiff, "cascade_def_snow_diff");
				  RiverUtilities.EnforceDefaultTexture(snowNorm, "cascade_def_snow_normsao");
				  RiverUtilities.EnforceDefaultTexture(snowDiff, "cascade_def_ice_diff");
				  RiverUtilities.EnforceDefaultTexture(snowNorm, "cascade_def_ice_normsao");
				  RiverUtilities.EnforceDefaultTexture(snowDiff, "cascade_def_snow_noise");


				  Vector4 snowUV = FindProperty("_SnowUVScales", props).vectorValue;
				  EditorGUI.BeginChangeCheck();
				  EditorGUILayout.BeginHorizontal();
				  EditorGUILayout.PrefixLabel("UV Scale");
				  snowUV.x = EditorGUILayout.FloatField(snowUV.x);
				  snowUV.y = EditorGUILayout.FloatField(snowUV.y);
				  EditorGUILayout.EndHorizontal();
				  if (EditorGUI.EndChangeCheck())
				  {
					 FindProperty("_SnowUVScales", props).vectorValue = snowUV;
					 EditorUtility.SetDirty(targetMat);
				  }


				  // influence, erosion, crystal, melt
				  Vector4 p1 = FindProperty("_SnowParams", props).vectorValue;
				  Vector4 hr = FindProperty("_SnowHeightAngleRange", props).vectorValue;

				  EditorGUILayout.BeginHorizontal();

				  materialEditor.ShaderProperty(FindProperty("_SnowAmount", props), "Amount");
				  EditorGUILayout.EndHorizontal();

				  EditorGUI.BeginChangeCheck();
				  p1.y = EditorGUILayout.Slider(CSnowVsIce, p1.y, -1, 1);
				  p1.z = EditorGUILayout.FloatField(CCrystals, p1.z);
				  p1.w = EditorGUILayout.Slider(CMelt, p1.w, 0, 1.0f);

				  EditorGUILayout.PrefixLabel(CHeightRange);
				  hr.x = EditorGUILayout.FloatField(hr.x);
				  hr.y = EditorGUILayout.FloatField(hr.y);

				  if (EditorGUI.EndChangeCheck())
				  { 
					 FindProperty("_SnowParams", props).vectorValue = p1;
					 FindProperty("_SnowHeightAngleRange", props).vectorValue = hr;
				  }

				  Vector4 up = targetMat.GetVector("_SnowUpVector");
				  EditorGUI.BeginChangeCheck();
				  Vector3 newUp = EditorGUILayout.Vector3Field(CUpVector, new Vector3(up.x, up.y, up.z));
				  if (EditorGUI.EndChangeCheck())
				  {
					 newUp.Normalize();
					 targetMat.SetVector("_SnowUpVector", new Vector4(newUp.x, newUp.y, newUp.z, 0));
					 EditorUtility.SetDirty(targetMat);
				  }
			   }
			}
		 }
		
		#endregion

		}

		if (needsCompile)
	  {
		 needsCompile = false;
		 targetMat.shaderKeywords = null;
		 compiler.Pack(targetMat);
		 // horrible workaround to GUI warning issues
		 compileMat = targetMat;
		 compileName = shaderName;
		 targetCompiler = compiler;
		 EditorApplication.delayCall += TriggerCompile;
	  }
   }

	[SerializeField] private bool _showAdvancedwaves;
	[SerializeField] private bool _showWaveNoiseGenerator;
	void UI_WavesSetting(int i, MaterialEditor materialEditor, MaterialProperty[] props)
	{

		i++;    // Properties in the shader are counted in base one. so add 1 to the index
		using (new EditorGUILayout.VerticalScope("box"))
		{
			materialEditor.ShaderProperty(FindProperty("_Scale" + i, props), "Waves Scale");
			materialEditor.ShaderProperty(FindProperty("_Speed" + i, props), "Waves Speed");
			materialEditor.ShaderProperty(FindProperty("_Height" + i, props), "Waves Height");
		}
		

	}

	void LakeRippleWavesSetting(MaterialEditor materialEditor, MaterialProperty[] props)
	{
		if (compiler.lakeWaves != RiverCompiler.WaveLayers.None)
		{
			using (new EditorGUILayout.VerticalScope("box"))
			{
				materialEditor.ShaderProperty(FindProperty("_Displacement", props), "Waves Displacement");
				_showAdvancedwaves = EditorGUILayout.Toggle("Waves Advanced Setting", _showAdvancedwaves);

				if (_showAdvancedwaves)
				{
					for (int i = 0; i < (int)compiler.lakeWaves; i++)
					{
						UI_WavesSetting(i, materialEditor, props);
					}

					_showWaveNoiseGenerator = EditorGUILayout.Toggle("Generator Advanced Setting", _showWaveNoiseGenerator);
					if (_showWaveNoiseGenerator)
					{
						using (new EditorGUILayout.VerticalScope("box"))
						{
							materialEditor.ShaderProperty(FindProperty("_Octaves", props), "Octaves");
							materialEditor.ShaderProperty(FindProperty("_Frequency" , props), "Frequency");
							materialEditor.ShaderProperty(FindProperty("_Amplitude" , props), "Amplitude");
							materialEditor.ShaderProperty(FindProperty("_Lacunarity", props), "Lacunarity");
							materialEditor.ShaderProperty(FindProperty("_Persistence", props), "Persistence");
							materialEditor.ShaderProperty(FindProperty("_Offset", props), "Offset");
						}

					}

				}
				

			}
		}

		if (compiler.lakeRipples != RiverCompiler.LakeRippleOctaves.None)
		{
			using (new EditorGUILayout.VerticalScope("box"))
			{
				materialEditor.ShaderProperty(FindProperty("_LakeRippleBlend", props), "Ripple Strength");
				materialEditor.ShaderProperty(FindProperty("_RippleScale", props), "Ripple Scale");
				materialEditor.ShaderProperty(FindProperty("_SmallRipple", props), "Small Ripple Intensity");
				materialEditor.ShaderProperty(FindProperty("_DirectionSmallRipple", props), "Small Ripple Direction");
				if (compiler.lakeRipples != RiverCompiler.LakeRippleOctaves.One)
				{
					materialEditor.ShaderProperty(FindProperty("_MediumRipple", props), "Medium Ripple Intensity");
					materialEditor.ShaderProperty(FindProperty("_DirectionMediumRipple", props), "Medium Ripple Direction");
				}
				if (compiler.lakeRipples == RiverCompiler.LakeRippleOctaves.Three)
				{
					materialEditor.ShaderProperty(FindProperty("_LargeRipple", props), "Large Ripple Intensity");
					materialEditor.ShaderProperty(FindProperty("_DirectionLargeRipple", props), "Large Ripple Direction");
				}
			}
		}
	}



   static Material compileMat;
   static string compileName;
   static RiverCompiler targetCompiler;

   protected void TriggerCompile()
   {
	  targetCompiler.Compile(compileMat, compileName);
   }
}