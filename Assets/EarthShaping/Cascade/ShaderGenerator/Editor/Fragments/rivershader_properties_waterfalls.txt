
      _WaterfallSpeed ("Waterfall Speed", Float) = 0.4
      _WaterfallUVScale("Waterfall Scale", Float) = 0.12
      _WaterfallThickness("Waterfall Thickness", Range(0.25, 4)) = 1
      _WaterfallThreshold("Waterfall Threshold", Range(0.01, 0.99)) = 0.05
      _WaterfallOpacity("Waterfall Opacity", Range(0, 1)) = 0.75