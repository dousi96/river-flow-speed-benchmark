﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class RiverUtilities 
{

   static Dictionary<string, Texture2D> autoTextures = null;

   public static void EnforceDefaultTexture(MaterialProperty texProp, string autoTextureName)
   {
      if (texProp.textureValue == null)
      {
         Texture2D def = GetAutoTexture(autoTextureName);
         if (def != null)
         {
            texProp.textureValue = def;
         }
      }
   }

   public static Texture2D GetAutoTexture(string name)
   {
      if (autoTextures == null)
      {
         autoTextures = new Dictionary<string, Texture2D>();
         var guids = AssetDatabase.FindAssets("rivershader_default_ t:texture2D");
         for (int i = 0; i < guids.Length; ++i)
         {
            Texture2D tex = AssetDatabase.LoadAssetAtPath<Texture2D>(AssetDatabase.GUIDToAssetPath(guids[i]));
            autoTextures.Add(tex.name, tex);
         }
      }
      Texture2D ret;
      if (autoTextures.TryGetValue(name, out ret))
      {
         return ret;
      }
      return null;
   }

   static Dictionary<string, bool> rolloutStates = new Dictionary<string, bool>();
   static GUIStyle rolloutStyle;
   public static bool DrawRollup(string text, bool defaultState = true, bool inset = false)
   {
      if (rolloutStyle == null)
      {
         rolloutStyle = GUI.skin.box;
         rolloutStyle.normal.textColor = EditorGUIUtility.isProSkin ? Color.white : Color.black;
      }
      var oldColor = GUI.contentColor;
      GUI.contentColor = EditorGUIUtility.isProSkin ? Color.white : Color.black;
      if (inset == true)
      {
         EditorGUILayout.BeginHorizontal();
         EditorGUILayout.GetControlRect(GUILayout.Width(40));
      }

      if (!rolloutStates.ContainsKey(text))
      {
         rolloutStates[text] = defaultState;
      }
      if (GUILayout.Button(text, rolloutStyle, new GUILayoutOption[]{GUILayout.ExpandWidth(true), GUILayout.Height(20)}))
      {
         rolloutStates[text] = !rolloutStates[text];
      }
      if (inset == true)
      {
         EditorGUILayout.GetControlRect(GUILayout.Width(40));
         EditorGUILayout.EndHorizontal();
      }
      GUI.contentColor = oldColor;
      return rolloutStates[text];
   }
}
