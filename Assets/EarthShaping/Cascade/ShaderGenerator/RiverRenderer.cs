﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JBooth.River
{
   [ExecuteInEditMode]
   public class RiverRenderer : MonoBehaviour 
   {
      [HideInInspector]
      public bool multiCamSupport = false;

      static Dictionary<Camera, CameraTrackingRefraction> m_Trackers =
         new Dictionary<Camera, CameraTrackingRefraction>();

      #if UNITY_EDITOR
      MeshRenderer mr;
      void Update()
      {
         // see if we need to setup the multicam option
         if (mr == null)
         {
            mr = GetComponent<MeshRenderer>();
         }
         if (mr != null)
         {
            multiCamSupport = (mr.sharedMaterial != null && mr.sharedMaterial.IsKeywordEnabled("_REFRACTION") && mr.sharedMaterial.IsKeywordEnabled("_MULTICAMSUPPORT"));
         }
      }
      #endif

      void OnWillRenderObject () 
      {
         var cam = Camera.current;
         if (!cam)
            return;

         cam.depthTextureMode = (DepthTextureMode)((int)cam.depthTextureMode | 1 << (int)DepthTextureMode.Depth);

         if (multiCamSupport)
         {
            CameraTrackingRefraction tracker;
            if (!m_Trackers.TryGetValue(cam, out tracker))
            {
               tracker = cam.GetComponent<CameraTrackingRefraction>();
               if (!tracker)
               {
                  tracker = cam.gameObject.AddComponent<CameraTrackingRefraction>();
               }
               m_Trackers[cam] = tracker;
            }

            tracker.SetIsNeededThisFrame();
         }
         if (Camera.current != null)
         {
            Shader.SetGlobalMatrix("_gCamToWorldMtx", Camera.current.cameraToWorldMatrix);
         }
   	}
   }
}
