﻿using UnityEngine;
using UnityEngine.Rendering;

namespace JBooth.River
{
   // This script is added to cameras automatically at runtime by the RiverRenderer.
   // using this instread of grabpass allows it to work correctly with multiple cameras
   [ExecuteInEditMode]
   public class CameraTrackingRefraction : MonoBehaviour 
   {
      [System.NonSerialized]
      public int lastRenderedFrame = -1;

      Camera cam;
      CommandBuffer buffer;

      void Awake () 
      {
         cam = Camera.current;
         //cam.depthTextureMode = DepthTextureMode.Depth;
      }

      public void SetIsNeededThisFrame () {
         lastRenderedFrame = Time.frameCount;
         if (buffer == null)
         {
            CreateCommandBuffer();
         }
         enabled = true;
      }

      void Update () 
      {
         if (lastRenderedFrame < Time.frameCount - 1) 
         {
            DestroyCommandBuffer ();
            enabled = false;
         }
      }

      void DestroyCommandBuffer () 
      {
         if (cam != null && buffer != null)
         {
            cam.RemoveCommandBuffer(CameraEvent.AfterSkybox, buffer);
            buffer = null;
         }
      }

      void CreateCommandBuffer ()
      {
         buffer = new CommandBuffer();
         buffer.name = "River Grab";

         int screenCopyID = Shader.PropertyToID("_RiverGrab");
         buffer.GetTemporaryRT (screenCopyID, -1, -1, 0, FilterMode.Bilinear);
         buffer.Blit (BuiltinRenderTextureType.CurrentActive, screenCopyID);

         buffer.SetGlobalTexture("_RefractionGrabTexture", screenCopyID);

         cam.AddCommandBuffer (CameraEvent.AfterSkybox, buffer);
      }
   }
}