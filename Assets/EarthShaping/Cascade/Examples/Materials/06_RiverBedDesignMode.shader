//////////////////////////////////////////////////////
// Cascade Shader
// Copyright (c) Jason Booth
//
// Auto-generated shader code, don't hand edit!
//   Compiled with Rivers and Lakes 1.0
//   Unity : 5.6.3p2
//   Platform : WindowsEditor
//////////////////////////////////////////////////////

Shader "Cascade/06_RiverBedDesignMode" {
   Properties {

      _Water("Water/Foam", 2D) = "white" {}
      _UVScale("UV Scale", Float) = 1

      _NormalStrength("Normal Stength", Range(0, 2)) = 0.5
      _DepthCalming("Depth Calming", Float) = 12
      _ShorelineFade("Shoreline", Range(1, 0.001)) = 0.1
      _EdgeFade("Edge Fade", Range(0.4, 1.0)) = 0.6
      _FlowSpeed("Flow Speed", Float) = 1.0
      _FlowCycle("Flow Cycle", Float) = 0.005
      _FoamStrength("Foam Stregth", Range(0, 1)) = 0.5
      _FoamEdgeStrength("Foam Edge Strength", Range(0, 32)) = 2
      _FoamScale("Foam Scale", Float) = 1
      _FoamColor("Foam Color", Color) = (1,1,1,1)
      _FoamEdgeColor("Foam Edge Color", Color) = (1,1,1,1)
      _FresnelOpacity("Fresnel Opacity", Range(0.2, 8)) = 4
      _Reflectivity("Reflectivity", Range(-1, 1)) = 0
      _SurfaceTint("Surface Tint", Color) = (0.5, 0.5, 0.5, 1)
      _DepthTint("Depth Tint", Color) = (0.3, 0.3, 0.5, 1)
      _DepthDistance("Depth Distance", Float) = 12


      _Lake("Water/Foam", 2D) = "white" {}
      _LakeUVScale("UV Scale", Float) = 1
      _LakeNormalStrength("Normal Stength", Range(0, 2)) = 0.5
      _LakeDepthCalming("Depth Calming", Float) = 12
      _LakeShorelineFade("Shoreline", Range(1, 0.001)) = 0.1
      _LakeEdgeFade("Edge Fade", Range(0.4, 1.0)) = 0.6
      _LakeFlowSpeed("Flow Speed", Float) = 1.0
      _LakeFlowCycle("Flow Cycle", Float) = 0.005
      _LakeFoamStrength("Foam Stregth", Range(0, 1)) = 0.5
      _LakeFoamEdgeStrength("Foam Edge Strength", Range(0, 32)) = 2
      _LakeFoamScale("Foam Scale", Float) = 1
      _LakeFoamColor("Foam Color", Color) = (1,1,1,1)
      _LakeFoamEdgeColor("Foam Edge Color", Color) = (1,1,1,1)
      _LakeFresnelOpacity("Fresnel Opacity", Range(0.2, 8)) = 4
      _LakeReflectivity("Reflectivity", Range(-1, 1)) = 0
      _LakeSurfaceTint("Surface Tint", Color) = (0.5, 0.5, 0.5, 1)
      _LakeDepthTint("Depth Tint", Color) = (0.3, 0.3, 0.5, 1)
      _LakeDepthDistance("Depth Distance", Float) = 12
	   
	  _ArrowScale("Flow Arrow Scale",Range(0.1,100)) = 20


      _Refraction("Refraction", Range(0, 1.0)) = 0.25
      _LakeRefraction("Refraction", Range(0, 1.0)) = 0.25


      _SloshRate("Slosh Rate", Float) = 30
      _SloshStrength("Slosh Strength", Range(0, 0.2)) = 0.05
      _SloshScale("Slosh Scale", Float) = 10
      _LakeSloshRate("Slosh Rate", Float) = 30
      _LakeSloshStrength("Slosh Strength", Range(0, 0.2)) = 0.05
      _LakeSloshScale("Slosh Scale", Float) = 10


      _WaterfallSpeed ("Waterfall Speed", Float) = 0.4
      _WaterfallUVScale("Waterfall Scale", Float) = 0.12
      _WaterfallThickness("Waterfall Thickness", Range(0.25, 4)) = 1
      _WaterfallThreshold("Waterfall Threshold", Range(0.01, 0.99)) = 0.05
      _WaterfallOpacity("Waterfall Opacity", Range(0, 1)) = 0.75
   }


   SubShader {
      GrabPass { "_RiverGrab" }
      Tags {"Queue" = "Geometry+110"}
      CGPROGRAM



      #define _REFRACTION 1
      #define _SLOSH 1
      #define _WATERFALLS 1


      #pragma surface surf Standard vertex:vert alpha:fade fullforwardshadows 

      #pragma target 3.5

      struct Input 
      {
         float2 uv_Water;
         float3 viewDir;
         half4 modifiers;
         float4 screenPos;
         float eyeDepth;
         float3 worldNormal;
         float4 flowDirStr;
   		#if _LAKES
   		float3 worldPos;
   		#endif
         #if _DEBUG_COLOR
         fixed3 color : COLOR;
         #endif
         INTERNAL_DATA
      };

      struct Data
      {
         half3 worldVertexNormal;
         half2 screenUV;
         float viewDepth;
         half fade;
         half depthCalm;
         float fresnelOpacity;
         float depthOpacity;
         float4 depthPos;
      };
	  
	   half _RippleScale;
	   half _SmallRipple;
	   half _MediumRipple;
	   half _LargeRipple;
	   half  _DirectionSmallRipple;
	   half  _DirectionMediumRipple;
	   half  _DirectionLargeRipple;
      half _LakeRippleBlend;
   

      sampler2D _RiverGrab;
      sampler2D_float _CameraDepthTexture;
      float4 _CameraDepthTexture_TexelSize;


      sampler2D _Water;
      half _ShorelineFade;
      half _EdgeFade;
      half3 _SurfaceTint;
      half3 _DepthTint;
      half _UVSpeed;
      float _UVScale;
      half _FlowSpeed;
      half _FlowCycle;
      half _FoamStrength;
      half _SparkleStrength;
      half _SparkleScale;
      half _SparkleSpeedBoost;
      half _SparkleFade;
      half _Refraction;
      half _FresnelOpacity;
      half _Reflectivity;
      half _NormalStrength;
      half _DepthCalming;
      half _DepthDistance;
      half _FoamEdgeStrength;
      half _SloshRate;
      half _SloshStrength;
      half _SloshScale;
      half _FoamScale;
      half4 _FoamColor;
      half4 _FoamEdgeColor;


      sampler2D _Lake;
      half _LakeUVSpeed;
      float _LakeUVScale;
      half _LakeFlowSpeed;
      half _LakeFlowCycle;
      half _LakeNormalStrength;
      half _LakeDepthCalming;
      half _LakeShorelineFade;
      half _LakeEdgeFade;
      half _LakeFoamStrength;
      half _LakeFoamEdgeStrength;
      half _LakeSparkleStrength;
      half _LakeSparkleScale;
      half _LakeSparkleSpeedBoost;
      half _LakeSparkleFade;
      half _LakeFresnelOpacity;
      half _LakeReflectivity;
      half3 _LakeSurfaceTint;
      half3 _LakeDepthTint;
      half _LakeDepthDistance;
      half _LakeTurbulenceAmplitude;
      half _LakeRefraction;
      half _LakeSloshRate;
      half _LakeSloshStrength;
      half _LakeSloshScale;
      half _LakeFoamScale;
      half4 _LakeFoamColor;
      half4 _LakeFoamEdgeColor;

      half _TurbulenceAmplitude;
      half _TurbulenceRate;
      half _TurbulenceFrequency;

      sampler2D _CausticTexture;
      float _CausticScale;
      half _CausticStrength;
      half _CausticFrequency;
      half _CausticAmplitude;
      half _CausticDepth;

     

      float _WaterfallSpeed;
      float _WaterfallUVScale;
      float _WaterfallThickness;
      float _WaterfallThreshold;
      float _WaterfallOpacity;

      #if _RAINDROPS
         sampler2D _RainDropTexture;
         half _RainIntensity;
         float2 _RainUVScales;
      #endif

      #if _FLOTSAM1
         sampler2D _FlotsamTex1;
         half _FlotsamSpeed1;
         half _FlotsamDepth1;
         half _FlotsamScale1;
         half2 _FlotsamStrength1;
         #if _FLOTSAM1_SPRITE
            half _FlotsamFrequency1;
            half2 _FlotsamSheetSize1;
            int _FlotsamMaxIndex1;
         #endif
      #endif

      #if _FLOTSAM2
         sampler2D _FlotsamTex2;
         half _FlotsamSpeed2;
         half _FlotsamDepth2;
         half _FlotsamScale2;
         half2 _FlotsamStrength2;
         #if _FLOTSAM2_SPRITE
            half _FlotsamFrequency2;
            half2 _FlotsamSheetSize2;
            int _FlotsamMaxIndex2;
         #endif
      #endif

      #if _FLOTSAM3
         sampler2D _FlotsamTex3;
         half _FlotsamSpeed3;
         half _FlotsamDepth3;
         half _FlotsamScale3;
         half2 _FlotsamStrength3;
         #if _FLOTSAM3_SPRITE
            half _FlotsamFrequency3;
            half2 _FlotsamSheetSize3;
            int _FlotsamMaxIndex3;
         #endif
      #endif

      #if _SNOW
         UNITY_DECLARE_TEX2D(_SnowDiff);
         UNITY_DECLARE_TEX2D_NOSAMPLER(_SnowNormal);
         UNITY_DECLARE_TEX2D(_IceDiff); 
         UNITY_DECLARE_TEX2D_NOSAMPLER(_IceNormal);
         UNITY_DECLARE_TEX2D(_SnowNoise);
         half4 _SnowParams; // influence, erosion, crystal, melt
         half _SnowAmount;
         half2 _SnowUVScales;
         half3 _SnowUpVector;
         float4 _SnowHeightAngleRange;
      #endif
	  
	  #if _DEBUG_FLOWUV
		float _ArrowScale;
	  #endif

      struct BlendParams
      {
         half normalStrength;
         half depthCalming;
         half shorelineFade;
         half edgeFade;
         half foamStrength;
         half foamEdgeStrength;
         half sparkleStrength;
         half sparkleScale;
         half sparkleSpeedBoost;
         half sparkleFade;
         half fresnelOpacity;
         half reflectivity;
         half refraction;
         half3 surfaceTint;
         half3 depthTint;
         half depthDistance;
         half sloshStrength;
         half UVSpeed;
         float UVScale;
         half sloshRate;
         half sloshScale;
         half flowSpeed;
         half flowCycle;
         half foamScale;
         half4 foamColor;
         half4 foamEdgeColor;

         #if _FLOTSAM1 || _FLOTSAM2 || _FLOTSAM3
         half3 flotsamStrength;
         #endif

      };

      BlendParams GetRiverBlendParams()
      {
         BlendParams p = (BlendParams)0;
         p.normalStrength = _NormalStrength;
         p.depthCalming = _DepthCalming;
         p.shorelineFade = _ShorelineFade;
         p.edgeFade = _EdgeFade;
         p.foamStrength = _FoamStrength;
         p.foamEdgeStrength = _FoamEdgeStrength;
         p.sparkleStrength = _SparkleStrength;
         p.sparkleScale = _SparkleScale;
         p.sparkleSpeedBoost = _SparkleSpeedBoost;
         p.sparkleFade = _SparkleFade;
         p.fresnelOpacity = _FresnelOpacity;
         p.reflectivity = _Reflectivity;
         p.surfaceTint = _SurfaceTint;
         p.depthTint = _DepthTint;
         p.depthDistance = _DepthDistance;
         p.sloshStrength = _SloshStrength;
         p.sloshRate = _SloshRate;
         p.sloshScale = _SloshScale;
         p.refraction = _Refraction;
         p.UVScale = _UVScale;
         p.UVSpeed = _UVSpeed;
         p.flowSpeed = _FlowSpeed;
         p.flowCycle = _FlowCycle;
         p.foamScale = _FoamScale;
         p.foamColor = _FoamColor;
         p.foamEdgeColor = _FoamEdgeColor;
         return p;
      }

      void InitFlotsam(inout BlendParams p, half b)
      {
         #if _FLOTSAM1 || _FLOTSAM2 || _FLOTSAM3
         p.flotsamStrength = _FlotsamStrength1.xxx;
           #if _FLOTSAM2
           p.flotsamStrength.y = _FlotsamStrength2.x;
           #endif
           #if _FLOTSAM3
           p.flotsamStrength.z = _FlotsamStrength3.x;
           #endif
         #endif

         #if _LAKES
            #if _FLOTSAM1
            p.flotsamStrength.x = lerp(_FlotsamStrength1.x, _FlotsamStrength1.y, b);
            #endif
            #if _FLOTSAM2
            p.flotsamStrength.y = lerp(_FlotsamStrength2.x, _FlotsamStrength2.y, b);
            #endif
            #if _FLOTSAM3
            p.flotsamStrength.z = lerp(_FlotsamStrength3.x, _FlotsamStrength3.y, b);
            #endif
         #endif
      }


      BlendParams GetLakeBlendParams()
      {
         BlendParams p = (BlendParams)0;
         p.normalStrength = _LakeNormalStrength;
         p.depthCalming = _LakeDepthCalming;
         p.shorelineFade = _LakeShorelineFade;
         p.edgeFade = _LakeEdgeFade;
         p.foamStrength = _LakeFoamStrength;
         p.foamEdgeStrength = _LakeFoamEdgeStrength;
         p.sparkleStrength = _LakeSparkleStrength;
         p.sparkleScale = _LakeSparkleScale;
         p.sparkleSpeedBoost = _LakeSparkleSpeedBoost;
         p.sparkleFade = _LakeSparkleFade;
         p.fresnelOpacity = _LakeFresnelOpacity;
         p.reflectivity = _LakeReflectivity;
         p.surfaceTint = _LakeSurfaceTint;
         p.depthTint = _LakeDepthTint;
         p.depthDistance = _LakeDepthDistance;
         p.sloshStrength = _LakeSloshStrength;
         p.sloshRate = _LakeSloshRate;
         p.sloshScale = _LakeSloshScale;
         p.refraction = _LakeRefraction;
         p.UVScale = _LakeUVScale;
         p.UVSpeed = _LakeUVSpeed;
         p.flowSpeed = _LakeFlowSpeed;
         p.flowCycle = _LakeFlowCycle;
         p.foamScale = _LakeFoamScale;
         p.foamColor = _LakeFoamColor;
         p.foamEdgeColor = _LakeFoamEdgeColor;
         return p;
      }



      float2 Slosh(Data d, float2 uv, half rate, half strength, half scale)
      {
         float t = _Time.x * rate;
         float2 slosh = float2( strength * cos(t/2.0) * sin(t + uv.y*scale), strength * sin(t/2.0) * cos(t + uv.x*scale) );
         return uv + slosh * d.depthCalm;
      }



      float4x4 _gCamToWorldMtx;
      float GetSceneDepth(Input i, float2 screenUV, out float4 depthPos)
      {
         float rawZ = SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.screenPos));
         float sceneZ = LinearEyeDepth(rawZ);
         float viewDepth = (sceneZ - i.eyeDepth);
         float2 p11_22 = float2(unity_CameraProjection._11, unity_CameraProjection._22);
         float3 vpos = float3( (screenUV * 2 - 1) / p11_22, -1) * sceneZ;
         depthPos = mul(_gCamToWorldMtx, float4(vpos, 1));
         depthPos += float4(_WorldSpaceCameraPos, 0) / _ProjectionParams.z;
         return viewDepth;
      }

      //half2 BlendNormal2(half2 base, half2 blend) {  return normalize(float3(base.xy + blend.xy, 1)).xy; }


      half2 BlendNormal2(half2 base, half2 blend)
      {
         half3x3 nBasis = float3x3(
             half3(1 , base.y, -base.x), // +90 degree rotation around y axis
             half3(base.x, 1, -base.y), // -90 degree rotation around x axis
             half3(base.x, base.y, 1));

         half2 r = normalize(blend.x*nBasis[0] + blend.y*nBasis[1] + nBasis[2]);
         return r*0.5 + 0.5;
      }



      // called once
      Data Prep(inout Input i, BlendParams p, inout SurfaceOutputStandard o)
      {
         Data d;
         UNITY_INITIALIZE_OUTPUT(Data, d);
         i.viewDir = normalize(i.viewDir);
         o.Normal = half3(0,0,1); // always assign first
         d.worldVertexNormal = WorldNormalVector(i, half3(0,0,1));
         d.screenUV = i.screenPos.xy / i.screenPos.w;

         #if UNITY_SINGLE_PASS_STEREO
            // If Single-Pass Stereo mode is active, transform the
            // coordinates to get the correct output UV for the current eye.
            float4 scaleOffset = unity_StereoScaleOffset[unity_StereoEyeIndex];
            d.screenUV = (d.screenUV - scaleOffset.zw) / scaleOffset.xy;
         #endif

         d.viewDepth = GetSceneDepth(i, d.screenUV, d.depthPos);
         return d;
      }

      void InitDataForLayer(Input i, BlendParams p, inout Data d)
      {
         d.fade = saturate(p.edgeFade * d.viewDepth);
         d.depthCalm = lerp(1.0, 0.05, saturate(d.viewDepth*(1.0/p.depthCalming)));
         d.fresnelOpacity = 1 - pow( abs( i.viewDir.z ), p.fresnelOpacity );
         d.depthOpacity = 1 - saturate( d.viewDepth / p.depthDistance );
      }

      float DoWave(float3 pos, float2 uv, float offset, float foam, float blend)
      {
         float tr = 1.0 - saturate(foam);
         tr *= tr;
         tr = 1.0 - tr; 

         half amp = _TurbulenceAmplitude;
         #if _LAKES
         amp = lerp(amp, _LakeTurbulenceAmplitude, blend);
         #endif 

         float yoff = (1 + sin(-_Time.x * _TurbulenceRate + (pos.x + offset) * _TurbulenceFrequency)) * amp;
         float xoff = (1 * cos(_Time.x * _TurbulenceRate + (pos.z + offset) * _TurbulenceFrequency)) * amp;
         return (xoff + yoff) * tr;
      }

      half4 FlowSample(sampler2D tex, float2 flowDir, float speed, float2 uv)
      {
		  flowDir = normalize(flowDir);		
          flowDir *= speed;

          float phase0 = frac(_Time.y * 0.3 * -0.5f + 0.5f);
          float phase1 = frac(_Time.y * 0.3 * -0.5f);

          half4 tex0 = tex2D(tex, uv + float2(0.5, 0.5) + flowDir * phase0);
          half4 tex1 = tex2D(tex, uv + flowDir * phase1);

          float flowLerp = abs((0.5 - phase0) / 0.5);
          return lerp(tex0, tex1, flowLerp);
      }


      #if _DEBUG_FLOWUV

         // Line SDF
         float Line(float2 p, float2 p1, float2 p2) 
         {
            float2 center = (p1 + p2) * 0.5;
            float len = length(p2 - p1);
            float2 dir = (p2 - p1) / len;
            float2 rel_p = p - center;
            float dist1 = abs(dot(rel_p, float2(dir.y, -dir.x)));
            float dist2 = abs(dot(rel_p, dir)) - 0.5 * len;
            return max(dist1, dist2);
         }

         float Arrow(float2 p, float2 v) 
         {
             
            v *= _ArrowScale;
            p -= (floor(p / _ArrowScale) + 0.5) * _ArrowScale;
               
            float mv = length(v);
            float mp = length(p);
            
            if (mv > 0.0) 
            {
               float2 dir_v = - v / mv;
               
               mv = clamp(mv, 5.0, _ArrowScale * 0.5);
               v = dir_v * mv;
               float shaft = Line(p, v, -v);
               float head = min(Line(p, v, 0.4 * v + 0.2 * float2(-v.y, v.x)), Line(p, v, 0.4 * v + 0.2 * float2(v.y, -v.x)));

               return min(shaft, head);
            } 
            else 
            {
               return mp;
            }
         }
      #endif

      

      void vert (inout appdata_full v, out Input o)
      {
         #if _SINUSOIDAL
         //float3 v0 = v.vertex.xyz + float3(0.1, 0, 0);
         //float3 v1 = v.vertex.xyz + float3(0,0,0.1);
         v.vertex.xyz += v.normal * DoWave(v.vertex.xyz, v.texcoord.xy, 0.3, v.texcoord.w, v.color.g) * 0.75 * v.texcoord3.z;
         //v0 += v.normal * DoWave(v0, v.texcoord.xy, 0.3, v.texcoord.w, v.color.g) * 0.75;
         //v1 += v.normal * DoWave(v1, v.texcoord.xy, 0.3, v.texcoord.w, v.color.g) * 0.75;

         //float3 t = normalize(v0-v.vertex.xyz);
         //float3 b = normalize(cross(v.normal, t));
         //t = normalize(cross(b, v.normal));
         //v.normal = cross(t,b);
         //v.tangent.xyz = t;


         #endif
         UNITY_INITIALIZE_OUTPUT(Input, o);
         COMPUTE_EYEDEPTH(o.eyeDepth);
         o.modifiers = v.texcoord3;
         o.modifiers.y *= 2; // scale here
         o.modifiers.w += 1;
         o.modifiers.z = v.color.g; // save an interpolator by packing this, as it's not used in the fragment stage
         o.flowDirStr.xy = v.texcoord2.zw;
         o.flowDirStr.z = v.color.r;
         o.flowDirStr.w = v.color.b; // depth
      }

      half DoCaustics(Input i, Data d)
      {
         float2 uv = d.depthPos.xz * _CausticScale;
         float2 uv1 = uv + float2(sin(_Time.y * _CausticFrequency), cos(_Time.y * _CausticFrequency)) * _CausticAmplitude;
         float2 uv2 = uv * 0.9 + float2(sin(_Time.y * _CausticFrequency * 0.8), cos(_Time.y * _CausticFrequency * 0.8)) * _CausticAmplitude;
         fixed4 c0 = tex2D (_CausticTexture, uv1);
         fixed4 c1 = tex2D (_CausticTexture, uv2);

         c0 = tex2D(_CausticTexture, uv + c0.g * 0.1);
         c1 = tex2D(_CausticTexture, uv + c1.g * 0.113);
         return ((c0.x + c1.b) * 0.5) * _CausticStrength;
      }


      #if _RAINDROPS
      half2 ComputeRipple(float2 uv, half time, half weight)
      {
         half4 ripple = tex2D(_RainDropTexture, uv);
         ripple.yz = ripple.yz * 2 - 1;

         half dropFrac = frac(ripple.w + time);
         half timeFrac = dropFrac - 1.0 + ripple.x;
         half dropFactor = saturate(0.2f + weight * 0.8 - dropFrac);
         half finalFactor = dropFactor * ripple.x * 
                              sin( clamp(timeFrac * 9.0f, 0.0f, 3.0f) * 3.14159265359);

         return half2(ripple.yz * finalFactor * 0.35f);
      }
      #endif

      half2 DoRain(half2 waterNorm, float2 uv)
      {
      #if _RAINDROPS
         half dropStrength = _RainIntensity;
         const float4 timeMul = float4(1.0f, 0.85f, 0.93f, 1.13f); 
         half4 timeAdd = float4(0.0f, 0.2f, 0.45f, 0.7f);
         half4 times = _Time.yyyy;
         times = frac((times * float4(1, 0.85, 0.93, 1.13) + float4(0, 0.2, 0.45, 0.7)) * 1.6);

         float2 ruv1 = uv * _RainUVScales.xy;
         float2 ruv2 = ruv1;

         half4 weights = _RainIntensity.xxxx - float4(0, 0.25, 0.5, 0.75);
         half2 ripple1 = ComputeRipple(ruv1 + float2( 0.25f,0.0f), times.x, weights.x);
         half2 ripple2 = ComputeRipple(ruv2 + float2(-0.55f,0.3f), times.y, weights.y);
         half2 ripple3 = ComputeRipple(ruv1 + float2(0.6f, 0.85f), times.z, weights.z);
         half2 ripple4 = ComputeRipple(ruv2 + float2(0.5f,-0.75f), times.w, weights.w);
         weights = saturate(weights * 4);

         half2 rippleNormal = half2( weights.x * ripple1.xy +
                     weights.y * ripple2.xy + 
                     weights.z * ripple3.xy + 
                     weights.w * ripple4.xy);

         return rippleNormal;                       
      #else
         return half2(0,0);
      #endif
      }



      float DoWaterfall(float2 uv)
      {
         float t = _Time.y * -_WaterfallSpeed;
         uv *= _WaterfallUVScale;
         fixed4 c0 = tex2D(_Water, uv + float2(t, sin(t * 3 + uv.y ) * 0.03));
         fixed4 c1 = tex2D(_Water, uv + float2(t * 1.3, t * 0.02));
         fixed4 c2 = tex2D(_Water, uv + float2(t * 1.1337, t * -0.01827));

         c0.a *= _WaterfallOpacity;
         c1.a *= _WaterfallOpacity;
         float r = pow(c0.a + c1.a + c2.a, _WaterfallThickness);
         return r;
      }

      half4 DoFlotsamLayer(float2 uv, sampler2D tex, half speed, half depth, half scale, float3 viewDir)
      {
         uv = uv * scale;
         uv.x += speed * _Time.y;
         uv += ParallaxOffset(depth, depth, viewDir);
         half4 f = tex2D(tex, uv);
         f.rgb *= 1 - depth;
         return f;
      }

      float FlotsamHash(float2 p)
      {
          p  = 50.0 * frac( p*0.3783099 + float2(0.713,0.103));
          return frac( p.x * p.y * (p.x + p.y) );
      }

      half4 DoFlotsamLayer(float2 uv, sampler2D tex, half speed, half depth, half scale, float3 viewDir, half freq, half2 sheetSize, int maxIndex)
      {
         uv *= scale;
         uv.x += speed * _Time.y;
         uv += ParallaxOffset(depth, depth, viewDir);

         // break uv space into quadrants, can likely refactor a lot of this into one noise function
         float2 quad = floor(uv);
         half alpha = (FlotsamHash(quad) < freq) ? 0 : 1;


         // figure out which cell we're in
         float n = FlotsamHash(quad * 9.0373);
         int idx = floor(n * maxIndex);
         float offset = idx * sheetSize.x;
         float cy = floor(offset);
         float cx = offset - cy;
         cy *= sheetSize.y;

         // figure out our UVs
         uv = frac(uv) * sheetSize.xy + float2(cx, cy);
         half4 f = tex2D(tex, uv);
         f.rgb *= (1 - depth);
         f.a *= alpha;
         return f;
      }

      half3 DoTerrain(Input i, Data d, BlendParams p, half4 wtr, float2 uv)
      {
         // distortion
         #if _REFRACTION
            // refraction can cause the edge pixels on the screen to smear, so we ramp the refraction down around the screen edges
            float2 boxWidth = 0.075;
            float boxContrast = 4.0;
            float2 box = (((d.screenUV - boxWidth)*boxContrast) * (d.screenUV-(1.0-boxWidth)) * boxContrast);
            float str = 1.0 - saturate(max(box.x, box.y));
            half3 terrain = tex2D(_RiverGrab, d.screenUV + wtr.xy * p.refraction * d.fade * p.normalStrength * str).rgb;
         #else
            half3 terrain = half3(0,0,0);
         #endif
         // tint

         half3 tinted = lerp(terrain * p.surfaceTint, p.depthTint, saturate(d.viewDepth / p.depthDistance));

         #if _CAUSTICS
            float caustics = DoCaustics(i, d) * (1 - saturate(d.viewDepth / _CausticDepth));
            tinted.rgb += caustics;
         #endif
         // opacity
         terrain = lerp(terrain, tinted, 1 - d.depthOpacity);
         terrain *= (1.0 - d.fresnelOpacity);

         #if _FLOTSAM1
            #if !_FLOTSAM1_SPRITE
               half4 flotsam1 = DoFlotsamLayer(uv, _FlotsamTex1, _FlotsamSpeed1, _FlotsamDepth1, _FlotsamScale1, i.viewDir);
            #else
               half4 flotsam1 = DoFlotsamLayer(uv, _FlotsamTex1, _FlotsamSpeed1, _FlotsamDepth1, _FlotsamScale1, i.viewDir, _FlotsamFrequency1, _FlotsamSheetSize1, _FlotsamMaxIndex1);
            #endif
         terrain.rgb = lerp(terrain.rgb, flotsam1.rgb, flotsam1.a * p.flotsamStrength.x * i.modifiers.x);
         #endif
         #if _FLOTSAM2
            #if !_FLOTSAM2_SPRITE
               half4 flotsam2 = DoFlotsamLayer(uv, _FlotsamTex2, _FlotsamSpeed2, _FlotsamDepth2, _FlotsamScale2, i.viewDir);
            #else
               half4 flotsam2 = DoFlotsamLayer(uv, _FlotsamTex2, _FlotsamSpeed2, _FlotsamDepth2, _FlotsamScale2, i.viewDir, _FlotsamFrequency2, _FlotsamSheetSize2, _FlotsamMaxIndex2);
            #endif
         terrain.rgb = lerp(terrain.rgb, flotsam2.rgb, flotsam2.a * p.flotsamStrength.y * i.modifiers.x);
         #endif
         #if _FLOTSAM3
            #if !_FLOTSAM3_SPRITE
               half4 flotsam3 = DoFlotsamLayer(uv, _FlotsamTex3, _FlotsamSpeed3, _FlotsamDepth3, _FlotsamScale3, i.viewDir);
            #else
               half4 flotsam3 = DoFlotsamLayer(uv, _FlotsamTex3, _FlotsamSpeed3, _FlotsamDepth3, _FlotsamScale3, i.viewDir, _FlotsamFrequency3, _FlotsamSheetSize3, _FlotsamMaxIndex3);
            #endif
         terrain.rgb = lerp(terrain.rgb, flotsam3.rgb, flotsam3.a * p.flotsamStrength.z * i.modifiers.x);
         #endif
         return terrain;
      }

      half DoWater(Input i, inout SurfaceOutputStandard o, BlendParams p, Data d, sampler2D tex)
      {
         float2 uv = i.uv_Water * -p.UVScale;
         #if _SLOSH
         uv = Slosh(d, uv, p.sloshRate, p.sloshStrength * i.modifiers.y, p.sloshScale);
         #endif


         // sample water
         #if _FLOWWATER
            half4 wtr = FlowSample(tex, i.flowDirStr.xy, p.flowSpeed, uv);
         #else
            float2 motionUV = uv + float2(_Time.x * p.flowSpeed, 0);
            half4 wtr = tex2D(tex, motionUV);
         #endif
         wtr.xy = wtr.xy * 2 - 1;

         // reslosh UVs
         #if _SLOSH && !_FLOWWATER
         uv = Slosh(d, motionUV, _SloshRate * 1.1, p.sloshStrength * 1.1 * i.modifiers.y, p.sloshScale * 1.763);
         #endif

         // second water texture, blended with the first
         #if _FLOWWATER
            half4 wtr2 = FlowSample(tex, i.flowDirStr.xy, p.flowCycle, uv * p.foamScale);
         #else
            half4 wtr2 = tex2D(tex, uv * p.foamScale + float2(1, 0) * _Time.x * p.flowCycle);
         #endif
         wtr2.xy = wtr2.xy * 2 - 1;
         wtr.xy = BlendNormal2(wtr.xy, wtr2.xy);
         wtr.z = wtr.z * wtr2.z;

         // calm normals in deep waters
         wtr.xy *= d.depthCalm;

         #if _RAINDROPS
         half2 rippleNormal = DoRain(wtr.xy, motionUV);
         wtr.xy = lerp(wtr.xy, BlendNormal2(rippleNormal, wtr.xy), _RainIntensity); 
         #endif

         // foam
         half foam = saturate(wtr.b * (i.modifiers.w + d.depthCalm));
         half foamStr = 1.0 - saturate(dot(d.worldVertexNormal, float3(0,1,0)));


         half mainFoam = i.modifiers.w * p.foamStrength.x;
         half shorelineMask = 1-saturate(d.viewDepth * p.shorelineFade);
         half foamDist = max(mainFoam, shorelineMask); // foam on edges of water
         foam *= foamDist;
         foam += foam * foamDist;
         foam *= i.modifiers.w * foamDist * p.foamEdgeStrength; // remask
         foam = saturate(foam);

         half3 foamColor = foam * lerp(p.foamColor.rgb, p.foamEdgeColor.rgb, shorelineMask);

         half3 terrain = DoTerrain(i, d, p, wtr, uv);

         o.Normal.xy = wtr.xy * p.normalStrength;
         o.Smoothness = p.reflectivity + foam + d.fresnelOpacity;
         o.Smoothness = saturate(o.Smoothness);
        
         o.Albedo = terrain + foamColor;

         //half foamAng = (1 - (foamStr*foamStr*foamStr) + 0.3) * 0.7;
         o.Alpha *= saturate(d.viewDepth / p.depthDistance);
         o.Alpha = d.fade;// + (foam * foamAng * 0.1);
         o.Alpha = saturate(o.Alpha);
         o.Albedo = saturate(o.Albedo);

         #if !_REFRACTION
            o.Albedo *= d.fade;
         #else
            o.Albedo = lerp(terrain, o.Albedo, d.fade);
            o.Alpha = 1;
         #endif



         #if _DEBUG_FOAMSTR
            o.Alpha = 1;
            o.Normal = half3(0,0,1);
            o.Smoothness = 0;
            o.Albedo = i.modifiers.www - 1;
         #elif _DEBUG_FOAM
            o.Alpha = 1;
            o.Normal = half3(0,0,1);
            o.Smoothness = 0;
            o.Albedo = foamColor;
         #elif _DEBUG_FOAMDIST
            o.Alpha = 1;
            o.Normal = half3(0,0,1);
            o.Smoothness = 0;
            o.Albedo = foamDist;
         #elif _DEBUG_SHORELINE
            o.Alpha = 1;
            o.Normal = half3(0,0,1);
            o.Smoothness = 0;
            o.Albedo = shorelineMask;
         #endif



         return foamStr;

      }
	  
	  // Added antoripa 31.12.2017
	   #if _LAKES
      half2 DoLakeRipple(float amplitude, float2 worldPosXZ, half waterSpeed,half direction)
      {
         float s = sin ( direction);
         float c = cos ( direction);

         float2x2 mtx = float2x2( c, -s, s, c);
         worldPosXZ = mul(worldPosXZ, mtx);

         float2 uv1 = worldPosXZ + waterSpeed * float2(1, 0);
         float2 uv2 = worldPosXZ + waterSpeed * float2(-1, 0) + float2(0.317, 0.671);
         half2 smallWaveNormal1 = tex2D(_Lake, uv1 * amplitude ).xy;
         half2 smallWaveNormal2 = tex2D(_Lake, uv2 * amplitude ).xy;

         return BlendNormal2(smallWaveNormal1 * 2 - 1, smallWaveNormal2 * 2 - 1);
      }
	   #endif


      #if _SNOW

      float DoSnow(inout SurfaceOutputStandard o, float2 uv, float3 worldNormal, float3 worldNormalVertex, float3 worldPos, Data d, Input i)
      {
         // compute microsplat snow for snow coverage
         uv *= _SnowUVScales.xy;
         half4 snowAlb = UNITY_SAMPLE_TEX2D(_SnowDiff, uv);
         half4 snowNsao = UNITY_SAMPLE_TEX2D_SAMPLER(_SnowNormal, _SnowDiff, uv).garb;

         #if _USEGLOBALSNOWLEVEL 
         float snowLevel = _Global_SnowLevel;
         #else
         float snowLevel = _SnowAmount;
         #endif

         #if _USEGLOBALSNOWHEIGHT
         float snowMin = _Global_SnowMinMaxHeight.x;
         float snowMax = _Global_SnowMinMaxHeight.y;
         #else
         float snowMin = _SnowHeightAngleRange.x;
         float snowMax = _SnowHeightAngleRange.y;
         #endif

         half frozen = _SnowParams.x;
         half snowAge = _SnowParams.z;
         half snowVsIce = _SnowParams.y;
         half melt = _SnowParams.w;


         half snowDot = max(snowLevel/2, dot(worldNormal, _SnowUpVector));
         half snowDotVertex = max(snowLevel/2, dot(worldNormalVertex, _SnowUpVector));

         float snowFade = saturate((worldPos.y - snowMin) / max(snowMax, 0.001));
         snowFade = saturate(snowLevel * snowFade);

         half snowMask = saturate(snowFade);
         snowMask = snowMask * snowMask * snowMask;
         half snowAmount = snowMask * saturate(snowDot);  // up
         snowAmount = saturate(snowAmount * 8);


         // real routine:
         // - Now should be wet near edges, this is where melt happens first. Base on age.
         // - Snow should turn to ice in center and based on noise
         // - Add waterflows to ice based on depth and noise texture
         // - should likely have a separate control which freezes, independent of snow masking
         // 


         // parallax map the ice and sample
         uv += ParallaxOffset(snowAlb.a, 0.1, i.viewDir);
         half4 iceAlb = UNITY_SAMPLE_TEX2D(_IceDiff, uv);
         half4 iceNsao = UNITY_SAMPLE_TEX2D_SAMPLER(_IceNormal, _IceDiff, uv).garb;
         half4 noise = UNITY_SAMPLE_TEX2D(_SnowNoise, uv * 0.03); // separate scale..

         // adjust noise scale by snow vs. ice factor

         snowAmount *= i.flowDirStr.w;
         snowAmount = saturate(snowAmount);

         noise.r = saturate(noise.r + snowVsIce);
         snowAmount -= noise.g * melt;
         snowAmount = saturate(snowAmount);
         // use depth to mod snow..


         // crystals
         float crystals = saturate(0.65 - snowNsao.b);
         snowNsao.b = lerp(snowNsao.b, crystals * snowAge, snowAmount);
         snowNsao.b = lerp(snowNsao.b, snowNsao.b * snowAge, snowAmount);

         snowAlb = lerp(snowAlb, iceAlb, noise.r);
         snowNsao = lerp(snowNsao, iceNsao, noise.r);






         half3 snowNormal = float3(snowNsao.xy * 2 - 1, 1);

         o.Albedo = lerp(o.Albedo, snowAlb.rgb, snowAmount);
         o.Normal = lerp(o.Normal, snowNormal, snowAmount);
         o.Smoothness = snowNsao.b;
         o.Occlusion = lerp(o.Occlusion, snowNsao.w, snowAmount);
         o.Metallic = lerp(o.Metallic, 0.01, snowAmount);

         return snowAmount;
      }

      #endif

	   void surf (Input i, inout SurfaceOutputStandard o) 
	   {

         BlendParams p = GetRiverBlendParams();
         Data d = Prep(i, p, o);
         InitFlotsam(p, i.modifiers.z);
         InitDataForLayer(i, p, d);
         #if _NORIVER
            half foamStr = 0;
         #else
            half foamStr = DoWater(i, o, p, d, _Water);
         #endif
         #if _LAKES
            SurfaceOutputStandard lake = (SurfaceOutputStandard)0;
            p = GetLakeBlendParams();
            InitDataForLayer(i, p, d);
            half lakeFoamStr = DoWater(i, lake, p, d, _Lake);
            foamStr = lerp(foamStr, lakeFoamStr, i.modifiers.z);
            o.Albedo = lerp(o.Albedo, lake.Albedo, i.modifiers.z);
            o.Smoothness = lerp(o.Smoothness, lake.Smoothness, i.modifiers.z);

            o.Alpha = lerp(o.Alpha, lake.Alpha, i.modifiers.z);

            #if _LAKERIPPLES || _LAKERIPPLES2 || _LAKERIPPLES3
      			// Added antoripa 31.12.2017 - lake ripple
      			half waterSpeed = _Time.y * p.flowSpeed;
      			float2 worldPosXZ = (i.worldPos).xz * _RippleScale ;
               half divisor = 1;
      			half2 smallRippleNormal = DoLakeRipple(1, worldPosXZ, waterSpeed,_DirectionSmallRipple);
      			half2 smallRipples = lerp(0, smallRippleNormal, _SmallRipple);
               #if _LAKERIPPLES2 || _LAKERIPPLES3
      			   half2 mediumRippleNormal = DoLakeRipple(0.5, worldPosXZ*0.5, waterSpeed,_DirectionMediumRipple);
      			   half2 mediumRipples = mediumRippleNormal * _MediumRipple;
                  smallRipples += mediumRipples;
                  divisor = 2;
               #endif
               #if _LAKERIPPLES3
      			   half2 largeRippleNormal = DoLakeRipple(0.1, worldPosXZ, waterSpeed,_DirectionLargeRipple);
      			   half2 largeRipples = largeRippleNormal * _LargeRipple;
                  smallRipples += largeRipples;
                  divisor = 3;
               #endif

      			lake.Normal.xy = lerp(lake.Normal.xy, p.normalStrength * smallRipples / divisor, _LakeRippleBlend);
            #endif
            o.Normal.xy = lerp(o.Normal.xy, lake.Normal.xy, i.modifiers.z);
         #endif

         // reconstruct Z once
         o.Normal.z = sqrt(1 - saturate(dot(o.Normal.xy, o.Normal.xy)));

         #if _WATERFALLS
            half waterfallStrength = saturate((foamStr - _WaterfallThreshold) * 1.0 / (1.0 - _WaterfallThreshold));
            float waterfall = DoWaterfall(i.uv_Water);
            waterfallStrength = saturate(waterfallStrength);
            o.Albedo = lerp(o.Albedo, waterfall.xxx, waterfallStrength);
            o.Alpha = lerp(o.Alpha, waterfall * d.fade, waterfallStrength);
            o.Smoothness = lerp(o.Smoothness, 0, waterfallStrength);
            o.Normal = lerp(o.Normal, half3(0,0,1), waterfallStrength);
         #endif


         #if _SNOW
         DoSnow(o, i.uv_Water, float3(0,0,1), float3(0,0,1), i.worldPos, d, i);
         #endif

         #if _DEBUG_UV
            o.Alpha = 1;
            o.Normal = half3(0,0,1);
            o.Smoothness = 0;
            o.Albedo = half3(i.uv_Water, 0);
         #elif _DEBUG_FLOWUV
            o.Alpha = 1;
            o.Normal = half3(0,0,1);
            o.Smoothness = 0;

            float2 flow = i.flowDirStr;
            float arrow_dist = Arrow(i.uv_Water*200, flow * 0.4);
            o.Albedo = float3(0, 0, lerp(0, 1, 1 - saturate(arrow_dist)));
         #elif _DEBUG_FOAMUV
            o.Alpha = 1;
            o.Normal = half3(0,0,1);
            o.Smoothness = 0;
            o.Albedo = i.modifiers.www;
         #elif _DEBUG_DEPTHCALM
            o.Alpha = 1;
            o.Normal = half3(0,0,1);
            o.Smoothness = 0;
            o.Albedo = d.depthCalm;
         #elif _DEBUG_DEPTHOPACITY
            o.Alpha = 1;
            o.Normal = half3(0,0,1);
            o.Smoothness = 0;
            o.Albedo = d.depthOpacity;
         #elif _DEBUG_FRESNELOPACITY
            o.Alpha = 1;
            o.Normal = half3(0,0,1);
            o.Smoothness = 0;
            o.Albedo = d.fresnelOpacity;
         #elif _DEBUG_FADE
            o.Alpha = 1;
            o.Normal = half3(0,0,1);
            o.Smoothness = 0;
            o.Albedo = d.fade;
         #elif _DEBUG_COLOR
            o.Alpha = 1;
            o.Normal = half3(0,0,1);
            o.Smoothness = 0;
            o.Albedo = i.color;;
         #endif
		}
		ENDCG


  
   }
   CustomEditor "RiverShaderGUI"
}
